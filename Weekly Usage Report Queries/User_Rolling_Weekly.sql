DECLARE @EndDate datetime = @@@DATEEND--'2018-07-23'--
;

WITH
AllUsers AS
	(
		select
			'Last 30 days' as DatePeriod
			,count(*) as TotalUsers
		from dbo.DIMCareCoordinator DIMCC
		LEFT JOIN nowpow.Contact C
		on DIMCC.ContactId = C.Id
		where (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
			AND C.CreatedDate < @EndDate
			AND @@@ORG --AND DIMCC.EnterpriseOrgId = 1102926--
	)
	,
ULoginCount AS
	(
		select
				Uact.UserId
				,cast(sum(CASE WHEN Uact.Activity = 0 THEN 1 ELSE 0 END) as int) AS SuccessfulLogin
		from [log].[ExternalUserActivity] as Uact
		where CAST(Uact.[CreatedDate] as date) > (@EndDate - 31) AND CAST(Uact.[CreatedDate] as date) < @EndDate
				and UserId is not null
		group by 
			Uact.UserId
	)
	,
UGroupAssign AS
	(
		select
			ULoginCount.UserId
			,'Last 30 days' as DatePeriod
			,(case when (ULoginCount.SuccessfulLogin >= 0.5) THEN 1 ELSE 0 END) as 'ENGAGED'
			,(case when (ULoginCount.SuccessfulLogin >= 11.5) THEN 1 ELSE 0 END) as 'ACTIVE'
			,(case when (ULoginCount.SuccessfulLogin < 11.5 AND ULoginCount.SuccessfulLogin >= 3.5) THEN 1 ELSE 0 END) as 'MODERATE'
			,(case when (ULoginCount.SuccessfulLogin < 3.5 AND ULoginCount.SuccessfulLogin >= 0.5) THEN 1 ELSE 0 END) as 'INFREQUENT'
			,(case when (ULoginCount.SuccessfulLogin < 0.5 OR ULoginCount.SuccessfulLogin is NULL) THEN 1 ELSE 0 END) as 'INACTIVE'
		from ULoginCount
	)
SELECT
	DatePeriod
	,Active
	,Moderate
	,Infrequent
	,Engaged
	,TotalUsers
	,cast(TotalUsers - Engaged as int) as Inactive
FROM
(
SELECT
	AllUsers.DatePeriod
	,Raw2.Active
	,Raw2.Moderate
	,Raw2.Infrequent
	,Raw2.Engaged
	,AllUsers.TotalUsers
FROM 
(
SELECT 
	Raw.DatePeriod
	,Raw.Active
	,Raw.Moderate
	,Raw.Infrequent
	,Raw.Engaged
	,Raw.TotalUsers
FROM (
	SELECT
		UGroupAssign.DatePeriod
		,sum(UGroupAssign.ACTIVE) as Active
		,sum(UGroupAssign.MODERATE) as Moderate
		,sum(UGroupAssign.INFREQUENT) as Infrequent
		,sum(UGroupAssign.ENGAGED) as Engaged
		,0 as TotalUsers

	FROM UGroupAssign
		JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.UserId = UGroupAssign.UserId
	

	WHERE (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		--AND DIMCC.EnterpriseOrgId = 1102926
		AND @@@ORG --DIMCC.OrgId = 1096869--

	GROUP BY UGroupAssign.DatePeriod

	UNION ALL
	/* Add Blanks to ensure that you can get Total Users and Inactive Users for Orgs with NO engaged Users */
	SELECT
		'Last 30 Days' as DatePeriod
		,0 as Active
		,0 as Moderate
		,0 as Infrequent
		,0 as Engaged
		,0 as TotalUsers
		
	) as Raw
) as Raw2
JOIN AllUsers
ON Raw2.DatePeriod = AllUsers.DatePeriod
) as Raw3