
/*
*Daily Report Query
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*v1.3: Days to Service and Days to Close added
*v1.4: Updated cteRA to include table joins for filtering out ExcludeFromReport and TestPatient associated records
*v1.5: Updated logic to account for common-DB activity conducted by floating users. Logic is structured to match CareCoordinatorId to an activity record using
	1) match on the Contact/UserId AND Site/OrganizationId provided in the activity record, ELSE
	2) match on the Contact/UserId, using the lowest matching CareCoordinatorId (this allows us to avoid double count of activities)
		NOTE: this logic results in the pulling of DEMO DB data for any user who is NOT exclude from reports and was provisioned at a Demo DB prior to go-live.
	Also updated "SiteId" field in the various "RAW" table subqueries to ensure that all SiteIds are tagged as "SiteId" and not "CCSiteId"
*/
DECLARE @EndDate as datetime2 = @@@DATEEND ----'2018-07-19'-- Set as today's date (Monday that report is due)
DECLARE @StartDate as datetime2 = @@@DATEBEG --'2018-07-02'--
;

WITH 
	cteRA AS (
		/* Generate table with Tracked Referral Appointments Attended */
		SELECT 
			RA.[ReferralId] as ReferralId
			, RA.[AppointmentStatusId]
			, RA.[AppointmentDate]
			, rank() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]
		FROM [nowpow].[ReferralAppointment] AS RA
		LEFT JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.ContactId = RA.CreatedByContactId
		LEFT JOIN nowpow.Referral as Ref
		ON Ref.Id = RA.ReferralId
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Ref.MakerPatientId = DimPatient.PatientId
		WHERE RA.AppointmentStatusId = 2 AND RA.IsDeleted <> 1
			/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
			AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
			/*Exclude any activity related to the test patient*/
			AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	)
	,
	cte AS 
	(
	/*Select the earliest appointment using the rank*/
		SELECT 
			cteRA.[ReferralId] as ReferralId
			, cteRA.[AppointmentStatusId]
			, cteRA.AppointmentDate AS AppointmentDate
		FROM cteRA
		WHERE [r] = 1
	)
	,
	cteContClose AS 
	(
		SELECT Ref.Id, Ref.ContactedDate
		FROM nowpow.Referral Ref
		WHERE Ref.ContactResultTypeId = 8
	)
	,
	DayBlanks AS
	(
		SELECT distinct
		 
		 0 as Blank
			, NULL as UserId
			, NULL as UserName
			, NULL as SiteId
			, @@@PLACE
			--, NULL as OrgId
			--, NULL as OrgName
			--, cast(DIMCC.EnterpriseOrgId as varchar) as EnterpriseOrgId
			--, cast(DIMCC.EnterpriseOrgName as varchar) as EnterpriseOrgName
			--, NULL as NetworkOrgId
			--, NULL as NetworkOrgName
			, NULL as CareCoordinatorID
			, cast(dateadd(day,0,DATEADD(day, -7, @EndDate)) as date) as DummyDate
			, 0 as eRxCreated
			, 0 as ReferralsOnSharedErx
			, 0 AS ReferralsOnCreatedErx
			, 0 AS TrackedReferrals
			, 0 AS eRxShared     
			, 0 AS ServiceNudged  
			, 0 AS Logins
			, 0 AS Searches
			, 0 AS Favorites
			, 0 AS Screenings
			, 0 AS eRxLookups
			, 0 AS ServicePrints
			, 0 AS DaysToService
			, 0 AS DaysToClose
			, NULL as PatientId
			, 0 AS TotalSharedReferrals
		from [dbo].DIMCareCoordinator AS DIMCC
		where @@@BLANKORG--DIMCC.EnterpriseOrgId = 1086591 --
      
	UNION ALL
	
	SELECT
			0 as Blank
			, NULL as UserId
			, NULL as UserName
			, NULL as SiteId
			, DayBlanks.OrgId
			, DayBlanks.OrgName
			, DayBlanks.EnterpriseOrgId
			, DayBlanks.EnterpriseOrgName
			, DayBlanks.NetworkOrgId
			, DayBlanks.NetworkOrgName
			, NULL as CareCoordinatorID
			,cast(dateadd(day,1,DummyDate) as date) as DummyDate
			, 0 as eRxCreated
			, 0 as ReferralsOnSharedErx
			, 0 AS ReferralsOnCreatedErx
			, 0 AS TrackedReferrals
			, 0 AS eRxShared     
			, 0 AS ServiceNudged  
			, 0 AS Logins
			, 0 AS Searches
			, 0 AS Favorites
			, 0 AS Screenings
			, 0 AS eRxLookups
			, 0 AS ServicePrints
			, 0 AS DaysToService
			, 0 AS DaysToClose
			, NULL as PatientId
			, 0 AS TotalSharedReferrals

		FROM DayBlanks
		WHERE DummyDate <  DATEADD(day, -1, @EndDate)
	)

select  DayBlanks.Blank
			, cast(DayBlanks.UserId as varchar) as UserId
			, cast(DayBlanks.UserName as varchar) as UserName
			, cast(DayBlanks.SiteId as varchar) as SiteId
			, cast(DayBlanks.OrgId as varchar) as OrgId
			, cast(DayBlanks.OrgName as varchar) as OrgName
			, cast(DayBlanks.EnterpriseOrgId as varchar) as EnterpriseOrgId
			, cast(DayBlanks.EnterpriseOrgName as varchar) as EnterpriseOrgName
			, cast(DayBlanks.NetworkOrgId as varchar) as NetworkOrgId
			, cast(DayBlanks.NetworkOrgName as varchar) as NetworkOrgName
			, DayBlanks.CareCoordinatorID
			, DayBlanks.DummyDate as 'Date'
			, 0 as eRxCreated
			, 0 as ReferralsOnSharedErx
			, 0 AS ReferralsOnCreatedErx
			, 0 AS TrackedReferrals
			, 0 AS eRxShared     
			, 0 AS ServiceNudged  
			, 0 AS Logins
			, 0 AS Searches
			, 0 AS Favorites
			, 0 AS Screenings
			, 0 AS eRxLookups
			, 0 AS ServicePrints
			, 0 AS DaysToService
			, 0 AS DaysToClose
			, NULL as PatientId
			, 0 AS TotalSharedReferrals
			from DayBlanks

UNION ALL

SELECT 
 CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
ELSE 'N/A'
END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS OrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR(200))
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR(200))
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR(200))
ELSE 'N/A'
END AS NetworkOrgName
--, Raw.*
, Raw.CareCoordinatorID 
, Raw.[Date]
, Raw.eRxCreated
, Raw.ReferralsOnSharedErx
, Raw.ReferralsOnCreatedErx
, Raw.TrackedReferrals
, Raw.eRxShared     
, Raw.ServiceNudged  
, Raw.Logins
, Raw.Searches
, Raw.Favorites
, Raw.Screenings
, Raw.eRxLookups
, Raw.ServicePrints
, Raw.DaysToService
, Raw.DaysToClose
, Raw.PatientId
, CASE
WHEN ISNULL(Raw.ServiceNudged,0) + ISNULL(Raw.TrackedReferrals,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) = 0 THEN NULL
ELSE ISNULL(Raw.ServiceNudged,0) + ISNULL(Raw.TrackedReferrals,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) 
END AS TotalSharedReferrals

FROM
(
(
/**eRx Created**/              
SELECT 
	Pre.CareCoordinatorID 
	, Pre.SiteId       
	, CAST(Pre.CreatedDate as date) AS 'Date'                
	, 1 AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, Pre.PatientId

FROM log.Prescription AS Pre
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
SELECT 
	PSI.CareCoordinatorID 
	, PSI.SiteId       
	, CAST(PSI.InteractionDate as date) AS 'Date'                
	, NULL AS eRxCreated
	, COUNT(*) AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
JOIN log.Prescription AS Pre
ON PSI.PrescriptionId = Pre.Id
LEFT JOIN log.PrescriptionService AS PreServ
ON Pre.Id = PreServ.PrescriptionId
WHERE [PSI].[InteractionTypeId] in ('3','4','11') 
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
GROUP BY PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date)         
, PSI.PatientId
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
	SELECT Pre.CareCoordinatorID 
	, Pre.SiteId       
	, CAST(Pre.CreatedDate as date) AS 'Date'                
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, COUNT(*) AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, Pre.PatientId

FROM log.Prescription AS Pre
	LEFT JOIN log.PrescriptionService AS PreServ
	ON Pre.Id = PreServ.PrescriptionId
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
GROUP BY Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date)         
, Pre.PatientId
)
UNION ALL
(
/**Tracked Referrals**/              
SELECT 
	COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
	, Ref.MakerSiteId AS SiteId
	, CAST(Ref.CreatedDate as date) AS 'Date'                
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, 1 AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 
)
UNION ALL
(
/**eRxShared**/            
SELECT 
	PSI.CareCoordinatorID AS CareCoordinatorId           
	,  PSI.SiteID AS SiteId                     
	, CAST(PSI.InteractionDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, 1 AS eRxShared    
	, NULL AS ServiceNudged   
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
WHERE [PSI].[InteractionTypeId] in ('3','4','11')    
)
UNION ALL
(
/**Service Nudged**/            
SELECT 
	PSI.CareCoordinatorID AS CareCoordinatorId           
	,  PSI.SiteID AS SiteId                     
	, CAST(PSI.InteractionDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared   
	, 1 AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*5&6 for Service Nudged*/        
WHERE [PSI].[InteractionTypeId] in ('5','6')    
)
UNION ALL
(
/* Login Activities */
SELECT
	COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
	, NULL AS SiteId
	, CAST(Uact.CreatedDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared    
	, NULL AS ServiceNudged   
	, 1 AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, NULL AS PatientId

FROM [log].[ExternalUserActivity] Uact
/* Append to Uact those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
left join (
	SELECT
		Uact.*
		, CC.Id AS CareCoordinatorId
		, S.Id AS SiteId
	FROM [log].[ExternalUserActivity] Uact
	JOIN nowpow.[site] AS S
	ON Uact.OrganizationId = S.OrganizationId
	JOIN nowpow.[user] U
	ON Uact.UserId = U.Id
	JOIN nowpow.Contact Contact
	ON Contact.Id = U.contactId
	JOIN nowpow.CareCoordinator CC
	ON CC.ContactId = Contact.Id and S.Id = CC.SiteId
	WHERE UAct.Activity = 0
) AS fullmatch
ON Uact.Id = fullmatch.Id
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
JOIN nowpow.[User] U
ON U.Id = Uact.UserId
JOIN nowpow.Contact Contact
ON Contact.Id = U.ContactId
/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.CareCoordinator CC1
ON CC1.ContactId = Contact.Id and CC1.Id = (SELECT MIN(Id) FROM nowpow.carecoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)
WHERE Uact.Activity = 0
)
UNION ALL
(
/**Searches**/            
SELECT lsc.CareCoordinatorId AS CareCoordinatorId           
	,  lsc.SiteId AS SiteId
	, CAST(lsc.CreatedDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared  
	, NULL AS ServiceNudged     
	, NULL AS Logins
	, 1 AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, lsc.PatientId AS PatientId

/*SearchType 1 = Service Search*/
FROM log.SearchCriteria lsc
WHERE lsc.SearchType = 1  
AND lsc.CareCoordinatorId IS NOT NULL
)
UNION ALL
(
/**Favorites**/            
SELECT CC.Id AS CareCoordinatorId           
	, NULL AS SiteId
	, CAST(SSF.DateAdded as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, 1 AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, NULL AS PatientId

/* SubscriberServiceFavorites is a "common-DB" table that stores only the UserId. If a user makes a favorite, this favorite will show up every time a floating user logs into the platform. 
   There is no easy way to discern at which Organization the service was initially favorited. */
FROM nowpow.Contact contact
JOIN nowpow.CareCoordinator CC
/* Add join to only the earliest CC record to prevent duplicate matching of CareCoordinatorId to ContactId, resulting in activity overcount */
on Contact.Id = CC.ContactId and CC.Id = (SELECT MIN (CC1.Id) FROM nowpow.CareCoordinator AS CC1 WHERE CC1.ContactId = Contact.Id)    
JOIN nowpow.[User] U             
ON contact.Id = U.Contactid           
JOIN nowpow.SubscriberServiceFavorite AS SSF            
ON U.Id = SSF.SubscriberUserid    
)
UNION ALL
(
/**Screenings**/            
SELECT SR.CareCoordinatorId AS CareCoordinatorId           
	, SR.SiteId AS SiteId
	, CAST(SR.CompletedDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, 1 AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, SR.PatientId AS PatientId

FROM nowpow.ScreeningResponse AS SR
WHERE SR.CompletedDate IS NOT NULL
)
UNION ALL
(
/**eRxLookups and ServicePrints**/            
SELECT 
	COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
	, UAct.SiteId AS SiteId
	, CAST(UAct.ActionDate as date) AS 'Date'                  
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL   AS Screenings
	, CASE WHEN UAct.Action like '%view%' THEN 1 END AS eRxLookups
	, CASE WHEN UAct.Action like '%print%' THEN 1 END AS ServicePrints
	, NULL AS DaysToService
	, NULL AS DaysToClose
	, NULL AS PatientId

FROM [log].UserActivity AS UAct 
/* Append to Uact those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
LEFT JOIN (
	SELECT
		Uact.*
		, CC.Id AS CareCoordinatorId
	FROM [log].[UserActivity] AS Uact
	JOIN nowpow.[user] U
	ON Uact.UserId = U.Id
	JOIN nowpow.Contact Contact
	ON Contact.Id = U.contactId
	JOIN nowpow.CareCoordinator CC
	ON CC.ContactId = Contact.Id and Uact.SiteId = CC.SiteId
	WHERE UAct.Action like '%view%'
	OR UAct.Action like '%print%'
) AS fullmatch
ON Uact.Id = fullmatch.Id
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
JOIN nowpow.[User] U
ON U.Id = Uact.UserId
JOIN nowpow.Contact Contact
ON Contact.Id = U.ContactId
/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id and CC1.Id = (select MIN(Id) from nowpow.carecoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)

WHERE UAct.Action like '%view%'
OR UAct.Action like '%print%'
)
UNION ALL
(
/**Avg Days to Service Receipt and Avg Days to Referral Close**/  
SELECT  
	Raw.CareCoordinatorId
	, Raw.SiteId
	, Raw.Date             
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, CASE
		WHEN Raw.DaysToService < 0 THEN NULL
		ELSE Raw.DaysToService
	END AS DaysToService
	, CASE
		WHEN Raw.DaysToClose < 0 THEN NULL
		ELSE Raw.DaysToClose
	END AS DaysToClose
	, Raw.PatientId
FROM
	(
	SELECT 
		COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
		, Ref.MakerSiteId AS SiteId
		, CAST(Ref.CreatedDate as date) AS 'Date'                
		/*Calculate the days to the contacted and closed successful date*/
		, DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) as DaysToClose
		/*Calculate the days to service as specified in the introduction*/
		, CASE
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL THEN NULL
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) <= DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) < DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
		END AS DaysToService
		, Ref.MakerPatientId AS PatientId
	FROM nowpow.Referral AS Ref
	/* Append to Ref those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
	LEFT JOIN (
		SELECT
			Ref.*
			, CC.Id AS CareCoordinatorId
			, CC.SiteId
		FROM nowpow.Referral AS Ref
		JOIN nowpow.Contact AS Contact
		ON Contact.Id = Ref.MakerContactId
		JOIN nowpow.CareCoordinator AS CC
		ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
	) AS fullmatch
	/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
	ON Ref.Id = fullmatch.Id
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
		has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
	   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
	   to tie to the record.) */
	JOIN nowpow.CareCoordinator AS CC1
	ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)
	/*Table Joins for filtering referrals begin*/
				LEFT JOIN cte
				ON Ref.Id = cte.ReferralId
				LEFT JOIN cteContClose
				ON Ref.Id = cteContClose.Id
	) AS Raw
	WHERE (Raw.DaysToService IS NOT NULL OR Raw.DaysToClose IS NOT NULL)
)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
LEFT JOIN DIMPatient AS DimPatient
ON Raw.PatientId = DimPatient.PatientId

WHERE [raw].[Date] >= @StartDate
AND [raw].[Date] < @EndDate 
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
/*Exclude any activity related to the test patient*/
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
AND @@@ACTORG



