IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsErxService')				
DROP TABLE fact.StandardMetricsErxService;	

CREATE TABLE fact.StandardMetricsErxService (
	DateKey BIGINT,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	eRxReferralsCreated BIGINT, 
	eRxReferralsShared BIGINT
	);

INSERT INTO fact.StandardMetricsErxService

SELECT ES.DateKey
	, ES.OrganizationId
	, ES.CareOrganizationDatabaseId
	, SUM(ES.eRxReferralsCreated) AS eRxReferralsCreated
	, SUM(ES.eRxReferralsShared) AS eRxReferralsShared
	FROM (
		/* Get eRx Referrals Created */
		SELECT	CAST(CONVERT(VARCHAR(8), ES.CreatedDate,112) AS INT) AS DateKey
			, ES.OrganizationId
			, ES.CareOrganizationDatabaseId
			, 1 AS eRxReferralsCreated
			, 0 eRxReferralsShared
		FROM fact.ErxService AS ES
			LEFT JOIN [dim].[User] AS U
			ON ES.UserId = U.UserId
			LEFT JOIN [dim].[Patient] AS P
			ON ES.PatientId = P.PatientId 
			AND ES.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
		WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
			AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
			AND (ES.ErxSourceTypeId <> 1 OR ES.ErxSourceTypeId IS NULL)
			---Filtering that were on reports
			AND ES.ErxId IS NOT NULL 
			AND ES.UserId IS NOT NULL
		UNION ALL
		
		/* Get eRxs Referrals Shared */
		SELECT CAST(CONVERT(VARCHAR(8), PSI.InteractionDate ,112) AS INT) AS DateKey
			, PSI.OrganizationId
			, PSI.CareOrganizationDatabaseId
			, 0 AS eRxReferralsCreated
			, SUM(ES.ErxServiceQuantity) AS eRxReferralsShared

		FROM fact.PatientSiteInteraction AS PSI
			LEFT JOIN fact.ErxService AS ES
			/* ErxId is only unique to the CareOrganizationDatabaseId, must join on two conditions */
			ON ES.ErxId = PSI.ErxId 
			AND  ES.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
			INNER JOIN [dim].[User] AS U
			ON PSI.UserId = U.UserId
			LEFT JOIN [dim].[Patient] AS P
			ON PSI.PatientId = P.PatientId 
			AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
		WHERE PSI.InteractionTypeId in (3, 4, 11) 
			AND (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
			AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
			AND (ES.ErxSourceTypeId <> 1 OR ES.ErxSourceTypeId IS NULL)
			---Filtering that were on reports
			AND ES.ErxId IS NOT NULL 
			AND PSI.UserId IS NOT NULL
		GROUP BY CAST(CONVERT(VARCHAR(8), PSI.InteractionDate ,112) AS INT)
			, PSI.OrganizationId
			, PSI.CareOrganizationDatabaseId
	) ES
	GROUP BY ES.DateKey
	, ES.OrganizationId
	, ES.CareOrganizationDatabaseId