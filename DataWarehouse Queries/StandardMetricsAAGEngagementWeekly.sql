IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsAAGEngagementWeekly')				
DROP TABLE fact.StandardMetricsAAGEngagementWeekly;	

CREATE TABLE fact.StandardMetricsAAGEngagementWeekly (
	 WeekLastDay Date
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, InteractionPatientCount BIGINT
	, PatientsReceivingReferralsQty BIGINT
	, ReferralsReceivedByPatientsQty BIGINT
	);

INSERT INTO fact.StandardMetricsAAGEngagementWeekly
--Change the year and week number to week last day
SELECT PatCnt.WeekLastDay
	, PatCnt.OrganizationId
	, PatCnt.CareOrganizationDatabaseId
	, SUM(PatCnt.InteractionPatientCount) AS InteractionPatientCount
	, SUM(PatCnt.PatientsReceivingReferralsQty) AS PatientsReceivingReferralsQty
	, SUM(PatCnt.ReferralsReceivedByPatientsQty) AS ReferralsReceivedByPatientsQty
FROM 
	(
	SELECT D.WeekLastDay
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, COUNT(DISTINCT PSI.PatientId) AS InteractionPatientCount
		, 0 AS PatientsReceivingReferralsQty
		, 0 AS ReferralsReceivedByPatientsQty
	
	FROM fact.PatientSiteInteraction AS PSI
		LEFT JOIN [dim].[User] AS U
		ON PSI.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId 
		AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
		INNER JOIN [dim].[Date] AS D
		ON CONVERT(Date, PSI.InteractionDate) = D.DateValue

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
		AND PSI.InteractionTypeId IN (1,2,5,6,7,8,9,10)

	GROUP BY D.WeekLastDay
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId

	UNION ALL

	SELECT 
		Base.WeekLastDay
		, Base.OrganizationId
		, Base.CareOrganizationDatabaseId
		, 0 AS InteractionPatientCount
		, COUNT(DISTINCT Base.PatientId) as PatientsReceivingReferralsQty
		, SUM(Base.Referrals) as ReferralsReceivedByPatientsQty
	FROM (
		/* Get PSI records and Referral Quantity for any Shared eRx received by a Patient, where PatientId IS NOT NULL */
		SELECT Eref.WeekLastDay
			  , Eref.UserId
			  , Eref.OrganizationId
			  , Eref.[Name]
			  , Eref.CareOrganizationDatabaseId
			  , Eref.PatientId
			  , Eref.Referrals
			  , Eref.ErxId
		FROM (
			SELECT 
				D.WeekLastDay
				, PSI.UserId
			    , PSI.OrganizationId
				, Org.[Name]
				, PSI.CareOrganizationDatabaseId
				, PSI.[PatientId]
				, ServQty.Referrals
				, CONCAT(PSI.ErxId,';',PSI.CareOrganizationDatabaseId) as ErxId
					/* ErXIds are not unique ACROSS DBs. Must add the CareOrganizationDatabaseId to the ErxId to make this "ErxId value" truly unique.
					   The same eRx can be shared with given patient multiple times. This row ranking allows us to select only the most recent event when a given eRx was shared. */
				, ROW_NUMBER() OVER (PARTITION BY PSI.PatientId, concat(PSI.ErxId,';',PSI.CareOrganizationDatabaseId) ORDER BY InteractionDate) AS [r] 

			FROM fact.[PatientSiteInteraction] AS PSI
			/* Get count of Referrals for each unique eRx Shared */
				LEFT JOIN (
					SELECT PS.ErxId
						,PS.CareOrganizationDatabaseId
						,SUM(ErxServiceQuantity) as Referrals
					FROM [fact].ErxService as PS
					GROUP BY CareOrganizationDatabaseId, ErxId
					) AS ServQty
				ON PSI.ErxId = ServQty.ErxId and PSI.CareOrganizationDatabaseId = ServQty.CareOrganizationDatabaseId
				JOIN dim.Organization as Org
				ON Org.OrganizationId = PSI.OrganizationId
				INNER JOIN [dim].[Date] AS D
				ON CONVERT(Date, PSI.InteractionDate) = D.DateValue

				WHERE InteractionTypeId IN (3, 4, 11, 22)
				AND PatientId IS NOT NULL --Get only the most recent PSI interaction for which a given eRx was shared with a patient
			) AS Eref
		WHERE Eref.[r] = 1
		--ORDER BY ErxId, PatientId, CareOrganizationDatabaseId
		UNION ALL

		SELECT	D.WeekLastDay
				, PSI.UserId
				, PSI.OrganizationId
				, Org.[Name]
				, PSI.CareOrganizationDatabaseId
				, PSI.[PatientId]
				, 1 as Referrals
				, NULL as ErxId
		FROM fact.PatientSiteInteraction as PSI
			JOIN dim.Organization as Org
			ON Org.OrganizationId = PSI.OrganizationId
			INNER JOIN [dim].[Date] AS D
			ON CONVERT(Date, PSI.InteractionDate) = D.DateValue
		WHERE PatientId IS NOT NULL 
		AND InteractionTypeId in (5, 6, 15)
	) AS Base

	LEFT JOIN [dim].[User] AS U
	ON Base.UserId = U.UserId
	LEFT JOIN [dim].[Patient] AS P
	ON Base.PatientId = P.PatientId 
	AND Base.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
	GROUP BY Base.WeekLastDay
		, Base.OrganizationId
		, Base.CareOrganizationDatabaseId
	) AS PatCnt

GROUP BY PatCnt.WeekLastDay
	, PatCnt.OrganizationId
	, PatCnt.CareOrganizationDatabaseId