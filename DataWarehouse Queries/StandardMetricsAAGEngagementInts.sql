IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsAAGEngagementInts')
DROP TABLE fact.StandardMetricsAAGEngagementInts;	

CREATE TABLE fact.StandardMetricsAAGEngagementInts (
	DateKey BIGINT NULL
	, CareOrganizationDatabaseId BIGINT NULL
	, PatientId BIGINT NULL
	, PatientsReceivingReferralsQty BIGINT NULL
	, ReferralsReceivedByPatientsQty BIGINT NULL
	)
WITH
(
	CLUSTERED INDEX(
		[CareOrganizationDatabaseId],
		[PatientId] 
	),
	DISTRIBUTION = ROUND_ROBIN
);
INSERT INTO fact.StandardMetricsAAGEngagementInts
SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		, PSI.CareOrganizationDatabaseId
		, PSI.PatientId
		, 0 AS PatientsReceivingReferralsQty
		, 0 AS ReferralsReceivedByPatientsQty
	
	FROM fact.PatientSiteInteraction AS PSI
		LEFT JOIN dim.[User] AS U
		ON PSI.UserId = U.UserId

		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId 
		AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
		/* Add join to dim.PatientInteractionType table on interactiontypeid and careorganizationdatabaseid so that the IsVisible field can be used to filter out values */
		
		LEFT JOIN dim.PatientInteractionType AS PIT
		ON PIT.PatientInteractionTypeId = PSI.InteractionTypeId 
		AND PSI.CareOrganizationDatabaseId = PIT.CareOrganizationDatabaseId

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
	AND PIT.IsVisible = 1
	AND PSI.PatientId is not null