/*
Main Query for the Tracked Referral Metrics for the At-A-Glance Reports
The logic of this query is based on the Executive Summary report's Tracked Referral section's "Attended" and "No Show" categorization
This query tracks the following metrics:
	# Tracked Service Referrals Closed Successfully
	# Tracked Service Referrals Closed Unsuccessfully
Add Completed flag to the table
*/
IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsTrackedReferral')				
DROP TABLE fact.StandardMetricsTrackedReferral;	

CREATE TABLE fact.StandardMetricsTrackedReferral (
	DateKey BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, TrackedServiceReferralsClosedSuccessfully BIGINT
	, TrackedServiceReferralsClosedUnsuccessfully BIGINT);

INSERT INTO fact.StandardMetricsTrackedReferral 
SELECT CAST(CONVERT(VARCHAR(8), TR.CreatedDate,112) AS INT) AS DateKey
	, TR.MakerOrganizationId AS OrganizationId
	, TR.CareOrganizationDatabaseId
	,SUM
	(
		CASE 
		WHEN ((TRA.TrackedReferralAppointmentStatusId = '2') OR (TR.ArchivedFlag = '1' AND TR.CompletedFlag = '1' AND TR.ContactResultTypeId = '9')) THEN 1
		ELSE 0
		END
	) AS TrackedServiceReferralsClosedSuccessfully
	,SUM
	(
		CASE 
		WHEN ((TRA.TrackedReferralAppointmentStatusId  = '3') OR (TR.ArchivedFlag = '1' AND TR.CompletedFlag = '0' AND TR.ContactResultTypeId = '9')) THEN 1
		ELSE 0
		END
	) AS TrackedServiceReferralsClosedUnsuccessfully
	FROM fact.TrackedReferral TR
		LEFT JOIN fact.TrackedReferralAppointment TRA
		ON TR.TrackedReferralId = TRA.TrackedReferralId
	GROUP BY CAST(CONVERT(VARCHAR(8), TR.CreatedDate,112) AS INT)
	, TR.MakerOrganizationId
	, TR.CareOrganizationDatabaseId