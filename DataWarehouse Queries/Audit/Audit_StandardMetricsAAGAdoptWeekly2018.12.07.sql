/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [OrganizationId]
      ,[WeekLastDay]
      ,[CareOrganizationDatabaseId]
      ,[TotalUsers]
      ,[EngagedUsers]
      ,[ActiveUsers]
      ,[ModerateUsers]
      ,[InfrequentUsers]
      ,[InactiveUsers]
  FROM [fact].[StandardMetricsAAGAdoptWeekly]
  WHERE TotalUsers != EngagedUsers + InactiveUsers
  AND OrganizationId = 1106387
  ORDER BY [WeekLastDay] DESC
  ;

SELECT P.*
FROM fact.ProcessRolling30DayLoginByWeek P
INNER JOIN dim.[User] U
ON P.UserId = U.UserId
WHERE  U.OrganizationId = 1106387
ORDER BY [WeekLastDay] DESC;