IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsErx')				
DROP TABLE fact.StandardMetricsErx;	

CREATE TABLE fact.StandardMetricsErx (
	DateKey BIGINT,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	eRxCreated BIGINT);

INSERT INTO fact.StandardMetricsErx
SELECT 	CAST(CONVERT(VARCHAR(8), E.CreatedDate,112) AS INT) AS DateKey
		, E.OrganizationId
		, E.CareOrganizationDatabaseId
		, COUNT(*) AS eRxCreated
FROM Fact.Erx E
		LEFT JOIN [dim].[User] AS U
		ON E.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON E.PatientId = P.PatientId AND E.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
GROUP BY CAST(CONVERT(VARCHAR(8), E.CreatedDate,112) AS INT)
	, E.OrganizationId
	, E.CareOrganizationDatabaseId
