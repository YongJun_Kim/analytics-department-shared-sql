/*
Main Query for the refeerals, main for the Match detail report.

	Service Nudged(PSI 5,6): added
	TrackedReferrals(fact.Referral): added
	Referrals on Shared Erx(fact.ErxService) (PSI 3,4,11): 
	Service Prints(UACT %Print%):

	Current execution time : 1 min 36 seconds
	Possible Index Optimization plan: 
		fact.PatientSiteInteraction
			InteractionTypeId
		fact.ErxService
			CareCoordinatorDatabaseId
			eRxId
		dim.eRxLocation
			CareCoordinatorDatabaseId
			eRxId
		
*/

IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMEtricsAAGMatchDetail')				
DROP TABLE fact.StandardMEtricsAAGMatchDetail;	

CREATE TABLE fact.StandardMEtricsAAGMatchDetail (
	DateKey BIGINT
	, ServiceId BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, UserId BIGINT
	, PatientId BIGINT
	, PatientLatitude DECIMAL(8,4)
	, PatientLongitude DECIMAL(8,4)
	, ServiceLatitude DECIMAL(8,4)
	, ServiceLongitude DECIMAL(8,4)
	, Distance DECIMAL(28,10)
	, ReferralName VARCHAR(10)
	, TotalReferrals BIGINT);

INSERT INTO fact.StandardMEtricsAAGMatchDetail

SELECT AR.DateKey
	, AR.ServiceId
	, AR.OrganizationId
	, AR.CareOrganizationDatabaseId
	, AR.UserId
	, AR.PatientId
	, AR.Latitude AS PatientLatitude
	, AR.Longitude AS PatientLongitude
	, O.Latitude AS ServiceLatitude
	, O.Longitude AS ServiceLongitude
	, [dbo].[fnCalcDistanceMiles](AR.Latitude, AR.Longitude, AR.Latitude, O.Longitude) 
	+  [dbo].[fnCalcDistanceMiles](AR.Latitude, AR.Longitude, O.Latitude, AR.Longitude) 
	AS Distance
	, AR.ReferralName
	, AR.Referral AS TotalReferrals

FROM
	(
	-- Service Nudged(PSI 5,6)
	SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		, PSI.ServiceId
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, PSI.UserId
		, PSI.PatientId
		, NULL AS Latitude
		, NULL AS Longitude
		, 'ServNudged' AS ReferralName
		, 1 AS Referral
	FROM fact.PatientSiteInteraction PSI
	WHERE PSI.InteractionTypeId IN (5,6)
	UNION ALL

	-- TrackedReferrals(fact.Referral)
	SELECT CAST(CONVERT(VARCHAR(8), TR.CreatedDate,112) AS INT) AS DateKey
		, TR.ServiceId 
		, TR.MakerOrganizationId AS OrganizationId
		, TR.CareOrganizationDatabaseId
		, TR.MakerUserID
		, TR.MakerPatientId
		, TR.PatientLatitudeForReferral AS Latitude
		, TR.PatientLongitudeForReferral AS Longitude
		, 'TrackedRef' AS ReferralName
		, TR.TrackedReferralQuantity AS Referral 
	FROM fact.TrackedReferral TR
	UNION ALL

	-- Referrals on Shared Erx(fact.ErxService)
	SELECT CAST(CONVERT(VARCHAR(8), ES.CreatedDate,112) AS INT) DateKey
		, ES.ServiceId
		, ES.OrganizationId
		, ES.CareOrganizationDatabaseId
		, ES.UserId
		, ES.PatientId
		, EL.Latitude
		, EL.Longitude
		, 'SharedErx' AS ReferralName
		, ES.ErxServiceQuantity AS Referral
	FROM fact.ErxService ES
		INNER JOIN dim.eRxLocation AS EL
		ON ES.ErxId = EL.PrescriptionId
		AND ES.CareOrganizationDatabaseId = EL.CareOrganizationDatabaseId
		INNER JOIN (
			SELECT PSI.eRxId, PSI.CareOrganizationDatabaseId, 1 AS CheckCol
			FROM fact.PatientSiteInteraction PSI
			WHERE PSI.InteractionTypeId in(3,4,11)
			) PSI
			ON ES.eRxId = PSI.eRxId	
			AND ES.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
	UNION ALL 

	-- 
	SELECT CAST(CONVERT(VARCHAR(8), Uact.UserActivityActionDate, 112) AS INT) DateKey
		, CASE
			WHEN UAct.UserActivityAction = 'Printed Service' THEN Uact.UserActivityReferenceId
			ELSE NULL 
		END AS ServiceId
		, Uact.ContactOrganizationId AS OrganziationId
		, Uact.CareOrganizationDatabaseId
		, Uact.UserId
		, NULL AS PatientId
		, NULL AS Latitude
		, NULL AS Longitude
		, 'ServPrint' AS ReferralName
		, 1 AS Referral
	FROM fact.UserActivity Uact
	WHERE UAct.UserActivityAction like 'print%'
	) AR

	LEFT JOIN [dim].[User] AS U
	ON AR.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON AR.PatientId = P.PatientId 
		AND AR.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
	LEFT JOIN [dim].[Organization] AS O
	ON AR.OrganizationId = O.OrganizationId
	LEFT JOIN [dim].[Service] AS S
	ON AR.ServiceId = S.ServiceId
	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)