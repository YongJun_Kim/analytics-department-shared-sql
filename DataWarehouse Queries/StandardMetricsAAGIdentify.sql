SELECT
	SRPSI.DateKey
	, SRPSI.OrganizationId
	, SRPSI.CareOrganizationDatabaseId
	, SUM(ScreeningsCompleted) AS ScreeningsCompleted
	, SUM(ScreeningsIdentifyingNeeds) AS ScreeningsIdentifyingNeeds
	, SUM(NeedsIdentified) AS NeedsIdentified

FROM (

	SELECT
		CAST(CONVERT(VARCHAR(8), SR.CreatedDate, 112) AS INT) AS DateKey
		, dimSite.OrganizationId
		, CC.UserId
		, SR.CareOrganizationDatabaseId
		, SR.PatientId
		, CASE
			WHEN SR.CompletedDate IS NOT NULL THEN 1
			ELSE 0
		  END AS ScreeningsCompleted
		, 0 AS ScreeningsIdentifyingNeeds
		, 0 AS NeedsIdentified

	FROM dim.ScreeningResponse AS SR
	LEFT JOIN dim.[Site] as dimSite
	ON SR.SiteId = dimSite.SiteId
	LEFT JOIN [dim].[CareCoordinator] AS CC
	ON CC.CareCoordinatorId = SR.CareCoordinatorId AND CC.CareOrganizationDatabaseId = SR.CareOrganizationDatabaseId

	UNION ALL

	SELECT
		CAST(CONVERT(VARCHAR(8), PSI.InteractionDate, 112) AS INT) AS DateKey
		, PSI.OrganizationId
		, PSI.UserId
		, PSI.CareOrganizationDatabaseId
		, PSI.PatientId
		, 0 AS ScreeningsCompleted
		, CASE
			WHEN PSI.InteractionTypeId in (25) THEN 1
			ELSE 0
		  END AS ScreeningsIdentifyingNeeds
		, CASE
			WHEN PSI.InteractionTypeId in (24, 25) THEN 1
			ELSE 0
		  END AS NeedsIdentified
	
	FROM fact.PatientSiteInteraction AS PSI

) AS SRPSI

LEFT JOIN [dim].[User] AS U 
ON SRPSI.UserId = U.UserId
LEFT JOIN [dim].[Patient] AS P 
ON SRPSI.PatientId = P.PatientId AND SRPSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

WHERE (U.[ExcludeFromReports] <> 1 OR 
	   U.[ExcludeFromReports] IS NULL) AND 
	   (P.LastName NOT LIKE '%Zzztest%' OR P.LastName IS NULL)
GROUP BY SRPSI.DateKey
		, SRPSI.OrganizationId
		, SRPSI.CareOrganizationDatabaseId