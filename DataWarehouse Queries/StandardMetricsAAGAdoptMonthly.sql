--Last Step Query for StandardMetricsAAGAdoptMonthly

IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsAAGAdoptMonthly')				
DROP TABLE fact.StandardMetricsAAGAdoptMonthly;

CREATE TABLE fact.StandardMetricsAAGAdoptMonthly(
	OrganizationId BIGINT
	, MonthLastDay Date
	, CareOrganizationDatabaseId BIGINT
	, TotalUsers BIGINT
	, EngagedUsers BIGINT
	, ActiveUsers BIGINT
	, ModerateUsers BIGINT
	, InfrequentUsers BIGINT
	, InactiveUsers BIGINT
	);
INSERT INTO fact.StandardMetricsAAGAdoptMonthly
SELECT U.OrganizationId
	, U.MonthLastDay
	, U.CareOrganizationDatabaseId
	, U.TotalUsers
	, CASE
		WHEN  - L.NegativeEngagedUsers IS NULL THEN 0
		ELSE - L.NegativeEngagedUsers 
	END AS EngagedUsers
	, CASE
		WHEN L.Active IS NULL THEN 0
		ELSE L.Active 
	END AS ActiveUsers
	, CASE
		WHEN L.Moderate IS NULL THEN 0
		ELSE L.Moderate 
	END AS ModerateUsers
	, CASE 
		WHEN L.Infrequent IS NULL THEN 0
		ELSE L.Infrequent 
	END AS InfrequentUsers
	, CASE
		WHEN ((L.NegativeEngagedUsers IS NULL) OR (U.TotalUsers IS NULL)) THEN 0
		WHEN L.NegativeEngagedUsers + U.TotalUsers < 0 THEN  0
		ELSE L.NegativeEngagedUsers + U.TotalUsers
	END AS InactiveUsers

FROM (
	SELECT D.MonthLastDay
		, D.YearNumber 
		, D.YearMonthNumber
		, U.OrganizationId
		, U.CareOrganizationDatabaseId
		, U.TotalUsers
	FROM fact.ProcessCumulativeTotalUsersByDayStep2 U
		INNER JOIN 
			(
			SELECT DISTINCT D.MonthLastDay
			, D.YearNumber 
			, D.YearMonthNumber
			FROM dim.[Date] D
			) D
		ON U.DateValue = D.MonthLastDay
	) U
	LEFT JOIN
		(
		SELECT U.OrganizationId
			, L.YearNumber
			, L.YearMonthNumber
			, SUM(CASE WHEN L.SuccessfulLogin >= 11.5 THEN 1 ELSE 0 END) AS Active
			, SUM(CASE WHEN L.SuccessfulLogin < 11.5 AND L.SuccessfulLogin>= 3.5 THEN 1 ELSE 0 END) AS Moderate
			, SUM(CASE WHEN L.SuccessfulLogin < 3.5 AND L.SuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS Infrequent
			, - SUM(CASE WHEN L.SuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS NegativeEngagedUsers
		FROM fact.ProcessMonthlyLogin AS L
			LEFT JOIN dim.[User]U
			ON L.UserId = U.UserId
		GROUP BY U.OrganizationId
			, L.YearNumber
			, L.YearMonthNumber
		) AS L
	ON U.OrganizationId = L.OrganizationId
	AND U.YearNumber  = L.YearNumber
	AND U.YearMonthNumber = L.YearMonthNumber