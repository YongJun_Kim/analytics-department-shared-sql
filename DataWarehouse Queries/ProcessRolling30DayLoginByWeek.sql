IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessRolling30DayLoginByWeek')				
DROP TABLE fact.ProcessRolling30DayLoginByWeek;	

CREATE TABLE fact.ProcessRolling30DayLoginByWeek (
	UserId BIGINT
	, WeekLastDay Date
	, CareOrganizationDatabaseId BIGINT
	, RollingThirtyDaySuccessfulLogin BIGINT
	);

INSERT INTO fact.ProcessRolling30DayLoginByWeek

SELECT R1.UserId
	, R1.WeekLastDay AS LastDayOfWeekDate
	, R1.CareOrganizationDatabaseId
	, Sum(R2.SuccessfulLogin) AS RollingThirtyDaySuccessfulLogin

FROM(
	--This part creates a cartesian product list of users with any login actvities and
	--the list of last days of weeks that could had any login activties
	SELECT UsersWithActivity.UserId
	, UsersWithActivity.CareOrganizationDatabaseId
	, LastDaysOfWeeks.WeekLastDay
	FROM 
		--Get a list of users with any login activities
		(SELECT 
			DISTINCT Uact.UserId 
			, U.CareOrganizationDatabaseId
			, CAST(U.CreatedDate AS Date) As CreatedDate
		FROM [Fact].[ExternalUserActivity] Uact
			INNER JOIN [Dim].[User] U
			ON Uact.UserId = U.UserId
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
			AND Uact.ExternalUserActivityTypeId = 0
		) UsersWithActivity

		INNER JOIN (
			--Get a list of the last days of weeks for any possible days that logins could happen
			SELECT DISTINCT D.WeekLastDay
			FROM Dim.[Date] AS D
			WHERE D.DateKey >= 20150000  --Starting Day
			AND D.DateValue < DateADD(year,1,GetDate()) --1 year from the query execution date
			) LastDaysOfWeeks
			ON UsersWithActivity.CreatedDate <= LastDaysOfWeeks.WeekLastDay
	) AS R1
	INNER JOIN (
		--Create a daily aggregate sum of logins for users
		SELECT Uact.UserId
		, CAST(UAct.CreatedDate AS Date) AS CreatedDate
		, COUNT(*) AS SuccessfulLogin
		FROM [Fact].[ExternalUserActivity] as Uact
		WHERE Uact.ExternalUserActivityTypeId = 0
		GROUP BY Uact.UserId
		, CAST(UAct.CreatedDate AS Date)
		) AS R2
	ON R1.UserId = R2.UserId
	--Do a cumulative join so the sum can be cumulative
	AND R1.WeekLastDay >= R2.CreatedDate
	AND DateADD(day,-30,R1.WeekLastDay) < R2.CreatedDate  -- rolling 30 days
GROUP BY R1.UserId
	, R1.WeekLastDay
	, R1.CareOrganizationDatabaseId