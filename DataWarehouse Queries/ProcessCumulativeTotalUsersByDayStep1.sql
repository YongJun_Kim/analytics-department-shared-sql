--USE NowPow-Prod_DataWarehouse


IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessCumulativeTotalUsersByDayStep1')				
DROP TABLE fact.ProcessCumulativeTotalUsersByDayStep1;	

CREATE TABLE fact.ProcessCumulativeTotalUsersByDayStep1 (
	DateValue Date,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	NewUsers BIGINT
	);

	/*Create a raw table where it has following rows: 
		1. Rows that show cumulative total users for any day that users were added
		2. Place holder rows that show everday of all orgs with users with 0 as Total Users
	These rows will be aggregated in the select statement to calculate the cumulative total users for any organization on any day*/

INSERT INTO fact.ProcessCumulativeTotalUsersByDayStep1
	--This part calculates rows that show cumulative total users for any day that users were added.

-- V1.1 - Since we want PlaceHolder rows with 0 total users, but we also still want 
-- each tuple of the form (DateValue, OrganizationId, CareOrganizationDatabaseId) to be unique (with exactly one TotalUsers value)
-- just take the max totalUsers value for a given trio. This ensures we keep the placeholders we want,
-- and get rid of the ones that are giving near-duplicate and potentially confusing data.

-- V1.2 - To ensure cumulative totals aren't done twice through, don't take cumulative totals in step1.
-- Specifically, when users are added on a date, 'TotalUsers' will effectively be the number of newUsers.


SELECT 
	DateValue
		, OrganizationId
		, CareOrganizationDatabaseId
		, Max(NewUsers) as NewUsers
FROM (		
		SELECT CAST(U.CreatedDate AS Date) AS DateValue
		,U.OrganizationId
		,U.CareOrganizationDatabaseId
		, COUNT(*) AS NewUsers
		FROM dim.[User] U
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		GROUP BY CAST(U.CreatedDate AS Date)
		,U.OrganizationId
		,U.CareOrganizationDatabaseId
	UNION ALL
	--This part caculates the Place holder rows that have 0 as Total Users 
	SELECT Dates.DateValue
		, Orgs.OrganizationId
		, Orgs.CareOrganizationDatabaseId
		, 0 As NewUsers
	FROM(SELECT DISTINCT D.DateValue
		FROM dim.[Date] D
		WHERE D.DateKey >= 20150929
		AND D.DateValue < DateADD(year,1,GetDate())
		) AS Dates 
		,(SELECT Distinct U.OrganizationId
			, U.CareOrganizationDatabaseId
		FROM Dim.[User] U
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		) AS Orgs ) x
GROUP BY DateValue, OrganizationId, CareOrganizationDatabaseId

-- for testing
-- ORDER BY OrganizationId, CareOrganizationDatabaseId, DateValue 