SELECT psi.[PatientSiteInteractionId]
      ,u.[UserId]
      ,psi.[InteractionTypeId]
      ,psi.[InteractionDate]
      ,psi.[LanguageId]
      ,psi.[IsDeleted]
      ,psi.[PatientId]
      ,psi.[OrganizationId]
      ,psi.[PrescriptionId] AS 'ErxId'
      ,psi.[ServiceId]
      ,psi.[TextMessageId]
      ,psi.[EmailId]
      ,psi.[IsCompleted]
      ,psi.[WasvCardAttached]
      ,psi.[ReferralId]
      ,psi.[ScreeningResponseId]
      ,psi.[CaseId]
      ,psi.[CareOrganizationDatabaseId]
	  , PSISOR.ServiceOrganizationId AS NudgedServiceOrganizationId
	  ,1 AS 'PatientSiteInteractionQuantity'
FROM [dim].[PatientSiteInteraction] psi
LEFT JOIN [dim].[CareCoordinator] u 
	ON u.CareCoordinatorId = psi.CareCoordinatorId 
	AND u.CareOrganizationDatabaseId = psi.CareOrganizationDatabaseId
LEFT JOIN [dim].[PatientSiteInteractionServiceOrganizationRelation] PSISOR
	ON psi.[PatientSiteInteractionId] = PSISOR.[PatientSiteInteractionId]
	AND psi.[CareOrganizationDatabaseId] = PSISOR.[CareOrganizationDatabaseId]
ORDER BY psi.[CareOrganizationDatabaseId],psi.[PatientSiteInteractionId]