/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [OrganizationId]
      ,[WeekLastDay]
      ,[CareOrganizationDatabaseId]
      ,[TotalUsers]
      ,[EngagedUsers]
      ,[ActiveUsers]
      ,[ModerateUsers]
      ,[InfrequentUsers]
      ,[InactiveUsers]
  FROM [fact].[StandardMetricsAAGAdoptWeekly]
  WHERE TotalUsers != EngagedUsers + InactiveUsers