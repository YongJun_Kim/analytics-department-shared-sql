	SELECT DISTINCT CONCAT(D.YearNumber, RIGHT('00'+CAST(D.YearWeekNumber AS VARCHAR(2)),2)) AS YearWeek
	FROM dim.[Date] D
	WHERE D.DateKey >= 20150000
	AND D.DateValue < DateADD(year,1,GetDate())