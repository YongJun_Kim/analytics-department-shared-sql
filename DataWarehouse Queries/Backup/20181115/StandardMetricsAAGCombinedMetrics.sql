/*
Main Query for the Combined metrics for the At-A-Glance Reports. 
Total Referrals=
	Service Nudged(PSI 5,6): added
	TrackedReferrals(fact.Referral): added
	Referrals on Shared Erx(fact.ErxService): added
	Service Prints(UACT %Print%): added
*/
IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsAAGCombinedMetrics')				
DROP TABLE fact.StandardMetricsAAGCombinedMetrics;	

CREATE TABLE fact.StandardMetricsAAGCombinedMetrics (
	DateKey BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, TotalReferrals BIGINT);

INSERT INTO fact.StandardMetricsAAGCombinedMetrics 
SELECT AR.DateKey AS DateKey
	, AR.OrganizationId
	, AR.CareOrganizationDatabaseId
	, SUM(AR.Referral) AS TotalReferrals

FROM
	(
	SELECT PSI.DateKey AS DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, PSI.SingleServiceReferralsShared AS Referral 
	FROM fact.StandardMetricsPSI PSI

	UNION ALL

	SELECT CAST(CONVERT(VARCHAR(8), TR.CreatedDate,112) AS INT) AS DateKey
		, TR.MakerOrganizationId AS OrganizationId
		, TR.CareOrganizationDatabaseId
		, TR.TrackedReferralQuantity AS Referral 
	FROM fact.TrackedReferral TR
			LEFT JOIN [dim].[User] AS U
			ON TR.MakerUserId = U.UserId
			LEFT JOIN [dim].[Patient] AS P
			ON TR.MakerPatientId = P.PatientId 
			AND TR.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
		AND (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)

	UNION ALL

	SELECT ES.DateKey
		, ES.OrganizationId
		, ES.CareOrganizationDatabaseId
		, ES.eRxReferralsShared AS Referral
	FROM fact.StandardMetricsErxService ES

	UNION ALL

	SELECT UA.DateKey
		, UA.OrganizationId
		, UA.CareOrganizationDatabaseId
		, UA.SingleServiceReferralPrinted AS Referral
	FROM fact.StandardMetricsUserActivity UA
	) AR

GROUP BY AR.DateKey 
	, AR.OrganizationId
	, AR.CareOrganizationDatabaseId