IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsPSI')				
DROP TABLE dbo.StandardMetricsPSI;	

CREATE TABLE fact.StandardMetricsPSI (
	DateKey BIGINT,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	TextNudges BIGINT,
	EmailNudges BIGINT,
	ScreengingsCompleted BIGINT,
	eRxsGeneratedFromScreening BIGINT,
	ScreeningsIdentifyingNeed BIGINT,
	NeedsClosed BIGINT,
	Nudges BIGINT,
	eRxsShared BIGINT,
	SingleServiceReferralsShared BIGINT,
	NudgesWithPatientResponse BIGINT,
	TrackedReferralsShared BIGINT,
	NeedsIdentified BIGINT,
	TotalInteractions BIGINT);

INSERT INTO fact.StandardMetricsPSI
SELECT 	PSI.DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, SUM(TextNudges) AS TextNudges
		, SUM(EmailNudges) AS EmailNudges
		, SUM(ScreengingsCompleted) AS ScreengingsCompleted
		, SUM(eRxsGeneratedFromScreening) AS eRxsGeneratedFromScreening
		, SUM(ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
		, SUM(NeedsClosed) AS NeedsClosed
		, SUM(Nudges) AS Nudges
		, SUM(eRxsShared) AS eRxsShared
		, SUM(SingleServiceReferralsShared) AS SingleServiceReferralsShared
		, SUM(NudgesWithPatientResponse) AS NudgesWithPatientResponse
		, SUM(TrackedReferralsShared) AS TrackedReferralsShared
		, SUM(NeedsIdentified) AS NeedsIdentified
		, SUM(TotalInteractions) AS TotalInteractions
FROM
	(SELECT 
		CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, PSI.UserId
		, PSI.PatientId
		, CASE
		WHEN PSI.InteractionTypeId = 1 THEN 1
		ELSE 0
		END AS TextNudges
		, CASE
		WHEN PSI.InteractionTypeId = 2 THEN 1
		ELSE 0
		END AS EmailNudges
		, CASE
		WHEN PSI.InteractionTypeId = 21 THEN 1
		ELSE 0
		END AS ScreengingsCompleted
		, CASE
		WHEN PSI.InteractionTypeId = 22 THEN 1
		ELSE 0
		END AS eRxsGeneratedFromScreening
		, CASE
		WHEN PSI.InteractionTypeId = 25 THEN 1
		ELSE 0
		END AS ScreeningsIdentifyingNeed
		, CASE
		WHEN PSI.InteractionTypeId = 26 THEN 1
		ELSE 0
		END AS NeedsClosed
		, CASE
		WHEN PSI.InteractionTypeId IN (1,2) THEN 1
		ELSE 0
		END AS Nudges
		, CASE
		WHEN PSI.InteractionTypeId IN (3,4,11) THEN 1
		ELSE 0
		END AS eRxsShared
		, CASE
		WHEN PSI.InteractionTypeId IN (5,6) THEN 1
		ELSE 0
		END AS SingleServiceReferralsShared
		, CASE
		WHEN PSI.InteractionTypeId IN (12,13) THEN 1
		ELSE 0
		END AS NudgesWithPatientResponse
		, CASE
		WHEN PSI.InteractionTypeId IN (15,23) THEN 1
		ELSE 0
		END AS TrackedReferralsShared
		, CASE
		WHEN PSI.InteractionTypeId IN (24,25) THEN 1
		ELSE 0
		END AS NeedsIdentified
		, CASE
		WHEN PSI.InteractionTypeId IN (1,2,5,6,7,8,9,10) THEN 1
		ELSE 0
		END AS TotalInteractions
	FROM fact.PatientSiteInteraction AS PSI) AS PSI
		LEFT JOIN [dim].[User] AS U
		ON PSI.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

	AND (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
	
GROUP BY PSI.DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId