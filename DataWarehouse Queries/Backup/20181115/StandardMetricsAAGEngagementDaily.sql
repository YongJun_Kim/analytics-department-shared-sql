IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsAAGEngagementDaily')
DROP TABLE fact.StandardMetricsAAGEngagementDaily;	

CREATE TABLE fact.StandardMetricsAAGEngagementDaily (
	DateKey BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, InteractionPatientCount BIGINT
	, ReferralPatientCount BIGINT
	);

INSERT INTO fact.StandardMetricsAAGEngagementDaily
SELECT PatCnt.DateKey
	, PatCnt.OrganizationId
	, PatCnt.CareOrganizationDatabaseId
	, SUM(PatCnt.InteractionPatientCount) AS InteractionPatientCount
	, SUM(PatCnt.ReferralPatientCount) AS ReferralPatientCount
FROM 
	(
	SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, COUNT(DISTINCT PSI.PatientId) AS InteractionPatientCount
		, 0 AS ReferralPatientCount
	
	FROM fact.PatientSiteInteraction AS PSI
		LEFT JOIN [dim].[User] AS U
		ON PSI.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId 
		AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
		AND PSI.InteractionTypeId IN (1,2,5,6,7,8,9,10)

	GROUP BY CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT)
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId

	UNION ALL

	SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, 0 AS InteractionPatientCount
		, COUNT(DISTINCT PSI.PatientId) AS ReferralPatientCount
	
	FROM fact.PatientSiteInteraction AS PSI
		LEFT JOIN [dim].[User] AS U
		ON PSI.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId 
		AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
		AND PSI.InteractionTypeId = 15

	GROUP BY CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT)
		, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
	) AS PatCnt
GROUP BY PatCnt.DateKey
	, PatCnt.OrganizationId
	, PatCnt.CareOrganizationDatabaseId