IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessCumulativeTotalUsersByDayStep2')				
DROP TABLE fact.ProcessCumulativeTotalUsersByDayStep2;	

CREATE TABLE fact.ProcessCumulativeTotalUsersByDayStep2 (
	DateValue Date,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	TotalUsers BIGINT
	);

INSERT INTO fact.ProcessCumulativeTotalUsersByDayStep2

SELECT R1.DateValue
		, R1.OrganizationId
		, R1.CareOrganizationDatabaseId
		, Sum(R2.TotalUsers)AS TotalUsers
FROM fact.ProcessCumulativeTotalUsersByDayStep1 R1
	INNER JOIN fact.ProcessCumulativeTotalUsersByDayStep1 R2
	ON R1.DateValue >= R2.DateValue
	AND (R1.OrganizationId = R2.OrganizationId OR (R1.OrganizationId IS NULL AND R2.OrganizationId IS NULL))
GROUP BY R1.DateValue
		, R1.OrganizationId
		, R1.CareOrganizationDatabaseId