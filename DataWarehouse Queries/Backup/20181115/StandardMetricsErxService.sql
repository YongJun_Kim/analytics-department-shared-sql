IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsErxService')				
DROP TABLE fact.StandardMetricsErxService;	

CREATE TABLE fact.StandardMetricsErxService (
	DateKey BIGINT,
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	eRxReferralsCreated BIGINT, 
	eRxReferralsShared BIGINT
	);

INSERT INTO fact.StandardMetricsErxService

SELECT ES.DateKey
	, ES.OrganizationId
	, ES.CareOrganizationDatabaseId
	, SUM(ES.eRxReferralsCreated)
	, SUM(ES.TotalReferralsShared)
	FROM (
	SELECT	CAST(CONVERT(VARCHAR(8), ES.CreatedDate,112) AS INT) AS DateKey
		, ES.OrganizationId
		, ES.CareOrganizationDatabaseId
		, 1 AS eRxReferralsCreated
		, CASE
			WHEN PSI.CheckCol = 1 THEN 1
			ELSE 0
		END AS TotalReferralsShared
	FROM fact.ErxService AS ES 
		LEFT JOIN (
			SELECT PSI.eRxId, PSI.CareOrganizationDatabaseId, 1 AS CheckCol
			FROM fact.PatientSiteInteraction PSI
			WHERE PSI.InteractionTypeId in(3,4,11)
			) PSI
			ON ES.eRxId = PSI.eRxId	
			AND PSI.CareOrganizationDatabaseId = ES.CareOrganizationDatabaseId
		LEFT JOIN [dim].[User] AS U
		ON ES.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON ES.PatientId = P.PatientId 
		AND ES.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
	) ES
	GROUP BY ES.DateKey
	, ES.OrganizationId
	, ES.CareOrganizationDatabaseId
