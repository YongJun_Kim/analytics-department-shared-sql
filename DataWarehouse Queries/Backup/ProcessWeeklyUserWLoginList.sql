IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessWeeklyUserWLoginList')				
DROP TABLE fact.ProcessWeeklyUserWLoginList;	

CREATE TABLE fact.ProcessWeeklyUserWLoginList (
	UserID BIGINT
	, CareOrganizationDatabaseId BIGINT
	, DateKey BIGINT
	);

INSERT INTO fact.ProcessWeeklyUserWLoginList
--This table creates a cartesian product between any user that had login activties
-- and a list of last days of each week after those users' creation day
SELECT UsersWithActivity.UserId
	, UsersWithActivity.CareOrganizationDatabaseId
	, LastDaysOfWeeks.DateKey
FROM 
	(SELECT 
		DISTINCT Uact.UserId 
		, U.CareOrganizationDatabaseId
		, U.CreatedDate
	FROM [Fact].[ExternalUserActivity] Uact
		INNER JOIN [Dim].[User] U
		ON Uact.UserId = U.UserId
	WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		AND Uact.ExternalUserActivityTypeId = 0
	) UsersWithActivity
	INNER JOIN ( 
		--List of the last days of weeks
		SELECT DISTINCT CAST(CONVERT(VARCHAR(8),D.WeekLastDay,112) AS INT) AS DateKey
		FROM Dim.[Date] AS D
		WHERE D.DateKey >= 20150000
		AND D.DateValue < DateADD(year,1,GetDate())
		) LastDaysOfWeeks
	ON CAST(CONVERT(VARCHAR(8),UsersWithActivity.CreatedDate,112) AS INT)  <= LastDaysOfWeeks.DateKey