/*
Main Query for the Tracked Referral Metrics for the At-A-Glance Reports
The logic of this query is based on the Executive Summary report's Tracked Referral section's "Attended" and "No Show" categorization
This query tracks the following metrics:
	# Tracked Service Referrals Closed Successfully
	# Tracked Service Referrals Closed Unsuccessfully
*/
IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsTrackedReferral')				
DROP TABLE fact.StandardMetricsTrackedReferral;	

CREATE TABLE fact.StandardMetricsTrackedReferral (
	DateKey BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, TrackedServiceReferralsClosedSuccessfully BIGINT
	, TrackedServiceReferralsClosedUnsuccessfully BIGINT);

--To be edited
SELECT Ref.Id AS ReferralId		
	, Ref.TakerOrganizationId	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(Ref.MakerPatientId AS CHAR) AS MakerPatientId
	, Ref.ContactResultTypeId
	, Ref.ContactedDate
	, Ref.CreatedDate AS RefCreatedDate	
	, RA.Id AS ReferralAppointmentId
	, RA.AppointmentStatusId
	, CASE WHEN (Ref.PatientLatitude IS NULL) OR (Ref.PatientLongitude IS NULL) THEN NULL
	ELSE (geography::Point(Ref.PatientLatitude, Ref.PatientLongitude, 4326).STDistance(geography::Point(Ref.PatientLatitude, ServLoc.LocationLongitude, 4326)) 
	   + geography::Point(Ref.PatientLatitude, Ref.PatientLongitude, 4326).STDistance(geography::Point(ServLoc.LocationLatitude, Ref.PatientLongitude, 4326)) )/ 1609.344  
	END AS Distance
	, Ref.CompletedFlag
	, Ref.ArchivedFlag
	, CASE 
		WHEN ((RA.AppointmentStatusId = '2') OR (Ref.ArchivedFlag = '1' AND Ref.CompletedFlag = '1' AND Ref.ContactResultTypeId = '9')) THEN 'Attended'
		WHEN (RA.AppointmentStatusId = '3') OR (Ref.ArchivedFlag = '1' AND Ref.CompletedFlag = '0' AND Ref.ContactResultTypeId = '9') THEN 'No Show'
		WHEN (RA.AppointmentStatusId NOT IN ('2','3')) OR (Ref.ArchivedFlag = '0' AND Ref.CompletedFlag = '0' AND Ref.ContactResultTypeId = '9') THEN 'Other'
	END AS WalkInAndAppointmentResult
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId

FROM nowpow.Referral Ref			
	LEFT JOIN nowpow.ReferralAppointment RA
	ON Ref.Id = RA.ReferralId
	JOIN [nowpow].[Service] AS Serv	
	ON Ref.ServiceId = Serv.Id	
		JOIN [nowpow].[ServiceType] AS ServType
		ON [Serv].[TypeId] = [ServType].[Id]
			JOIN nowpow.Organization ServOrg
			ON Serv.OrganizationId = ServOrg.Id
				JOIN dbo.LocationDistance ServLoc
				ON ServOrg.LocationId = ServLoc.LocationId
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN dbo.DIMOrganization AS Org		
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN dbo.DIMContact AS DIMContact
	ON Ref.MakerContactId= DIMContact.ContactId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	/*Table Joins for filtering referrals end*/

WHERE Ref.CreatedDate < @@@DATEEND
	AND @@@ORG
	AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
