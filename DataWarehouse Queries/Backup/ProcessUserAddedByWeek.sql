SELECT
	D.YearNumber AS Year
	, D.YearWeekNumber AS Week
	,U.OrganizationId
	, COUNT(*) AS Cnt
	FROM dim.[User] U
		LEFT JOIN dim.[Date] D
		ON CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT) = D.DateKey
	WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
	GROUP BY D.YearNumber
	, D.YearWeekNumber
	,U.OrganizationId