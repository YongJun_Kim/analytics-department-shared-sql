IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessWeeklyLogin')				
DROP TABLE fact.ProcessWeeklyLogin;	

CREATE TABLE fact.ProcessWeeklyLogin (
	UserId BIGINT
	, OrganizationId BIGINT
	, YearNumber BIGINT
	, YearWeekNumber INT 
	, YearMonthNumber INT
	, CareOrganizationDatabaseId BIGINT
	, TotalUsers BIGINT
	);

INSERT INTO fact.ProcessWeeklyLogin
		SELECT Uact.UserId
		, U.OrganizationId 
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
		, U.CareOrganizationDatabaseId
		, COUNT(*) AS SuccessfulLogin
		FROM [Fact].[ExternalUserActivity] as Uact
			JOIN [Dim].[User] U
			ON Uact.UserId = U.Id
			LEFT JOIN Dim.[Date] D
			ON CAST(CONVERT(VARCHAR(8),UAct.CreatedDate,112) AS INT) = D.DateKey
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		AND Uact.ExternalUserActivityTypeId = 0
		GROUP BY Uact.UserId
		, U.OrganizationId 
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
		, U.CareOrganizationDatabaseId