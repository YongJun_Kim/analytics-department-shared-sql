
	SELECT U.OrganizationId
		, L.WeekLastDay
		, SUM(CASE WHEN L.RollingThirtyDaySuccessfulLogin >= 11.5 THEN 1 ELSE 0 END) AS Active
		, SUM(CASE WHEN L.RollingThirtyDaySuccessfulLogin < 11.5 AND L.RollingThirtyDaySuccessfulLogin>= 3.5 THEN 1 ELSE 0 END) AS Moderate
		, SUM(CASE WHEN L.RollingThirtyDaySuccessfulLogin < 3.5 AND L.RollingThirtyDaySuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS Infrequent
		, - SUM(CASE WHEN L.RollingThirtyDaySuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS Active
	FROM fact.ProcessRolling30DayLoginByWeek AS L
		LEFT JOIN dim.[User]U
		ON L.UserId = U.UserId
	GROUP BY U.OrganizationId
		, L.WeekLastDay