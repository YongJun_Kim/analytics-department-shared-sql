WIth TBL AS
	(SELECT	CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT) AS DateKey
	,U.OrganizationId
	, COUNT(*) AS Cnt
	FROM dim.[User] U
	WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
	GROUP BY CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT)
	,U.OrganizationId)

SELECT t1.DateKey
	, t1.OrganizationId
	, Sum(t2.Cnt)AS TotalUsers

FROM TBL t1
	INNER JOIN TBL t2
	ON t1.DateKey >= t2.DateKey
	AND (t1.OrganizationId = t2.OrganizationId OR (t1.OrganizationId IS NULL AND t2.OrganizationId IS NULL))
GROUP BY t1.DateKey, t1.OrganizationId
ORDER BY t1.DateKey DESC