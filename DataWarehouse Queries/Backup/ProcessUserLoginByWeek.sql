With WeeklyUsage AS (
		SELECT Uact.UserId
		, U.OrganizationId 
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
		, COUNT(*) AS SuccessfulLogin
		FROM [Fact].[ExternalUserActivity] as Uact
			JOIN [Dim].[User] U
			ON Uact.UserId = U.Id
			LEFT JOIN Dim.[Date] D
			ON CAST(CONVERT(VARCHAR(8),UAct.CreatedDate,112) AS INT) = D.DateKey
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		AND Uact.ExternalUserActivityTypeId = 0
		GROUP BY Uact.UserId
		, U.OrganizationId 
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
	)
 , MonthlyUsage AS (
		SELECT Uact.UserId 
		, U.OrganizationId
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
		, COUNT(*) AS SuccessfulLogin
		FROM [Fact].[ExternalUserActivity] as Uact
			JOIN [Dim].[User] U
			ON Uact.UserId = U.Id
			LEFT JOIN Dim.[Date] D
			ON CAST(CONVERT(VARCHAR(8),UAct.CreatedDate,112) AS INT) = D.DateKey
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		AND Uact.ExternalUserActivityTypeId = 0
		GROUP BY Uact.UserId
		, U.OrganizationId 
		, D.YearNumber
		, D.YearWeekNumber
		, D.YearMonthNumber
	)

SELECT W.UserId
		, W.OrganizationId 
		, W.YearNumber
		, W.YearMonthNumber
		, W.YearWeekNumber
		, W.SuccessfulLogin AS WeeklySuccessfulLogin
		, M.SuccessfulLogin AS MonthlySuccessfulLogin
FROM  WeeklyUsage AS W
	LEFT JOIN MonthlyUsage AS M
	ON W.YearNumber = M.YearNumber
	AND W.YearMonthNumber = M.YearMonthNumber
	AND W.UserId = M.UserId