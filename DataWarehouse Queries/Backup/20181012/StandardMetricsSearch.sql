IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsSearch')				
DROP TABLE fact.StandardMetricsSearch;	

CREATE TABLE fact.StandardMetricsSearch (
	DateKey VARCHAR(8),
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	Search BIGINT
	);
INSERT INTO fact.StandardMetricsSearch

SELECT CAST(CONVERT(VARCHAR(8), S.CreatedDate,112) AS INT) AS DateKey
	, S.OrganizationId
	, S.CareOrganizationDatabaseId
	, COUNT(*) AS Search
	FROM fact.SearchCriteria AS S
		LEFT JOIN [dim].[User] AS U
		ON S.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON S.PatientId = P.PatientId 
		AND S.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)

GROUP BY CAST(CONVERT(VARCHAR(8), S.CreatedDate,112) AS INT)
	, S.OrganizationId
	, S.CareOrganizationDatabaseId