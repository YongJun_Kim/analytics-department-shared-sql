
IF EXISTS(select top 1 1 from sys.tables where name = 'ProcessCumulativeTotalUsersByDayStep1')				
DROP TABLE fact.ProcessCumulativeTotalUsersByDayStep1;	

CREATE TABLE fact.ProcessCumulativeTotalUsersByDayStep1 (
	DateKey VARCHAR(8),
	OrganizationId BIGINT,
	CareOrganizationDatabaseId BIGINT,
	TotalUsers BIGINT
	);

	/*Create a raw table where it has following rows: 
		1. Rows that show cumulative total users for any day that users were added
		2. Place holder rows that show everday of all orgs with users with 0 as Total Users
	These rows will be aggregated in the select statement to calculate the cumulative total users for any organization on any day*/

INSERT INTO fact.ProcessCumulativeTotalUsersByDayStep1
	--This part calculates rows that show cumulative total users for any day that users were added.
	SELECT t1.DateKey
		, t1.OrganizationId
		, t1.CareOrganizationDatabaseId
		, Sum(t2.Cnt)AS TotalUsers
	FROM (SELECT CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT) AS DateKey
		,U.OrganizationId
		,U.CareOrganizationDatabaseId
		, COUNT(*) AS Cnt
		FROM dim.[User] U
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
		GROUP BY CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT)
		,U.OrganizationId
		,U.CareOrganizationDatabaseId) t1
		
		INNER JOIN (SELECT	CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT) AS DateKey
			,U.OrganizationId
			, COUNT(*) AS Cnt
			FROM dim.[User] U
			WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
			GROUP BY CAST(CONVERT(VARCHAR(8),U.CreatedDate,112) AS INT)
			,U.OrganizationId) t2
		ON t1.DateKey >= t2.DateKey
		AND (t1.OrganizationId = t2.OrganizationId OR (t1.OrganizationId IS NULL AND t2.OrganizationId IS NULL))
	GROUP BY t1.DateKey, t1.OrganizationId, t1.CareOrganizationDatabaseId

	UNION ALL
	--This part caculates the Place holder rows that have 0 as Total Users 
	SELECT Dates.DateKey
		, Orgs.OrganizationId
		, Orgs.CareOrganizationDatabaseId
		, 0 As TotalUsers
	FROM(SELECT DISTINCT D.DateKey AS DateKey
		FROM dim.[Date] D
		WHERE D.DateKey >= 20150929
		AND D.DateValue < DateADD(year,1,GetDate())
		) AS Dates ,
		(SELECT Distinct U.OrganizationId
			, U.CareOrganizationDatabaseId
		FROM Dim.[User] U
		WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)) AS Orgs
