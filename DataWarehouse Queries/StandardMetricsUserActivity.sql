/*
Main Query for the User Activity for the At-A-Glance Reports

*/

IF EXISTS(select top 1 1 from sys.tables where name = 'StandardMetricsUserActivity')				
DROP TABLE fact.StandardMetricsUserActivity;	

CREATE TABLE fact.StandardMetricsUserActivity (
	DateKey BIGINT
	, OrganizationId BIGINT
	, CareOrganizationDatabaseId BIGINT
	, SingleServiceReferralPrinted BIGINT);

INSERT INTO fact.StandardMetricsUserActivity
SELECT CAST(CONVERT(VARCHAR(8), Uact.UserActivityActionDate,112) AS INT) AS DateKey
	, Uact.ContactOrganizationId AS OrganizationId
	, Uact.CareOrganizationDatabaseId
	,SUM
	(
		CASE 
		WHEN UAct.UserActivityAction like '%print%' THEN 1
		ELSE 0
		END
	) AS SingleServiceReferralPrinted

	FROM fact.UserActivity Uact

	GROUP BY CAST(CONVERT(VARCHAR(8), Uact.UserActivityActionDate,112) AS INT)
	, Uact.ContactOrganizationId
	, Uact.CareOrganizationDatabaseId