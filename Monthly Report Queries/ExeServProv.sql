/*  
* Montly Executive Reports Service Provider portion Query			
* This query pulls data for the Executive Reports Service Provider portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/2/2017
* Current Version: v1.6
* v1.1 4/6/2017:Enterprise Name has been added.		
* v1.2 4/12/2017: EnterpriseOrg changed to EntOrg Table
* v1.3 6/14/2017: PreCCId Tables replaced various tables.
* v1.4 7/24/2017: Checked for aggregation
* v1.5 10/09/2017: Patient Filtering added
* v1.6 01/04/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to use ExcludeFromReports column in DB to exclude users to exclude from reporting
*/


SELECT YEAR(PSI.InteractionDate) AS PSI_Year			
	, CONVERT(CHAR(3),DATENAME(MONTH, PSI.InteractionDate)) AS PSI_Month			
	, Serv.OrganizationId			
	, [PreCCId].[CCSiteId] AS Site_ID			
	, ServOrg.Id AS Org_ID		
	, '"' + ServOrg.[Name] + '"' AS Org_Name		
	, PSI.WasvCardAttached 	
	, '"' + [EntOrg].[Name] + '"' AS Enterprise_Name	
	, PSI.InteractionDate
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId

FROM nowpow.PatientSiteInteraction AS PSI
	LEFT JOIN nowpow.Service AS Serv
	ON Serv.Id = PSI.ServiceId
		LEFT JOIN [nowpow].[Organization] AS ServOrg	
		ON [Serv].[OrganizationId] = [ServOrg].[Id]	
	LEFT JOIN [dbo].[PreCCId] AS PreCCId
	ON [PSI].[CareCoordinatorId] = [PreCCId].[CCId]
			LEFT JOIN nowpow.Organization AS Org		
			ON [PreCCId].[CCOrgId] = Org.Id
			LEFT JOIN [nowpow].[Organization] AS EntOrg
			ON [PreCCId].[CCEntOrgId] = EntOrg.Id
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId

WHERE PSI.InteractionTypeId IN ('5','6')			
	AND PSI.InteractionDate < @@@DATEEND
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND @@@ORG
	AND (DimPatient.TestPatient = 'False' 
		OR 
		DimPatient.TestPatient IS NULL)
	AND [PSI].[IsDeleted] = 0