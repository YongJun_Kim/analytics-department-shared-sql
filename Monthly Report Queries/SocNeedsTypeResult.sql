/*  
* Monthly Social Needs Type Reulst Report Query			
* This query pulls the main data for the Social Needs Type Result portion of monthly NowPow reports.
* 			
* Yong Jun Kim 2/20/2018
*/	

SELECT  PSI.InteractionDate		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PSI].[Id] AS CHAR) AS PatientSiteInteractionId	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([DimPatient].[PatientId] AS Char) AS PatientID	
	, PSI.SiteId	
	, CASE 	
		WHEN PSI.InteractionTypeId = '24' THEN 'Manual'
		WHEN PSI.InteractionTypeId = '25' THEN 'Screening'
		ELSE 'Etc'
	 END AS IdentificationMethod	
	, PSIType.Name NeedInteractionTypeName	
	, PSINR.CloseId	
	, CASE	
		WHEN PSINR.CloseId IS NULL THEN 'Pending'
		ELSE NCR.Name 
	END AS CloseName	
	 , Need.Id AS NeedId	
	 , Need.Name AS NeedName	
	 --The following org Id is to be used to generate lower hierarchy organizations.
	 , DimCC.NetworkOrgId AS DimNetOrgId
	 , DimCC.EnterpriseOrgId AS DimEntOrgId
	 , DimCC.OrgId AS DimOrgId
	 , DimSite.NetworkOrgId AS DimSiteNetOrgId
	 , DimSite.EnterpriseOrgId AS DimSiteEntOrgId
	 , DimSite.OrgId AS DimSiteOrgId
		
FROM [nowpow].PatientSiteInteraction AS PSI		
	JOIN  [nowpow].[PatientSiteInteractionNeedRelation] AS PSINR	
	ON PSI.Id = PSINR.PatientSiteInteractionId	
		LEFT JOIN pristine.NeedCloseResult AS NCR
		ON PSINR.CloseId = NCR.Id
		LEFT JOIN nowpow.Need AS Need
		ON PSINR.NeedId = Need.Id
		LEFT JOIN pristine.PatientInteractionType PSIType
		ON PSINR.NeedInteractionTypeId = PSIType.Id
	/*Common Filtering Block*/
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId
	LEFT JOIN DIMCareCoordinator AS DimCC
	ON PSI.CareCoordinatorId = DimCC.CareCoordinatorId
	LEFT JOIN [dbo].[DIMSite] AS DimSite
	ON PSI.SiteId = [DIMSite].SiteId

/*		
*Restrict the PSI types to 24 for manual and 25 for screening		
*If a new way to create a social need is added to the system,		
* that PSI type id is required to be added as well.		
*/	
WHERE PSI.InteractionTypeId IN ('24','25')		
	AND PSI.InteractionDate < @@@DATEEND
	AND (DimCC.[ExcludeFromReports] <> 1 OR DimCC.[ExcludeFromReports] IS NULL)
	AND @@@PSIORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND [PSI].[IsDeleted] = 0