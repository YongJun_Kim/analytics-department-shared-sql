/*  
* Monthly Social Needs Client Report Query			
* This query pulls the main data for the Social Needs report's client portion of monthly NowPow reports.
* 			
* Yong Jun Kim 2/21/2018
*/	
SELECT  PSI.InteractionDate		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PSI].[Id] AS CHAR) AS PatientSiteInteractionId		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([DimPatient].[PatientId] AS Char) AS PatientID	
	, PSI.SiteId	
	 , Need.Id AS NeedId	
	 , Need.Name AS NeedName	
	, [DimPatient].[PatientGender]  AS Gender	
	, [DimPatient].[PatientAgeGroup] AS AgeGroup	
	, [DimPatient].[PatientRace] AS Race	
	, [DimPatient].[PatientEthnicity] AS Ethnicity	
	, [DimPatient].[PatientInsurancePlans]  AS InsurancePlans	
	, [DimPatient].[PatientInsuranceTypes]  AS InsuranceTypes 	
	, [DimPatient].[PatientPreferredLanguage]  AS PreferredLanguage 	
		
FROM [nowpow].PatientSiteInteraction AS PSI		
	JOIN  [nowpow].[PatientSiteInteractionNeedRelation] AS PSINR	
	ON PSI.Id = PSINR.PatientSiteInteractionId	
		LEFT JOIN nowpow.Need AS Need
		ON PSINR.NeedId = Need.Id
	/*Common Filtering Block*/
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId
	LEFT JOIN DIMCareCoordinator AS DimCC
	ON PSI.CareCoordinatorId = DimCC.CareCoordinatorId
	LEFT JOIN [dbo].[DIMSite] AS DimSite
	ON PSI.SiteId = [DIMSite].SiteId

/*		
*Restrict the PSI types to 24 for manual and 25 for screening		
*If a new way to create a social need is added to the system,		
* that PSI type id is required to be added as well.		
*/		
WHERE PSI.InteractionTypeId IN ('24','25')		
	AND PSI.InteractionDate < @@@DATEEND
	AND (DimCC.[ExcludeFromReports] <> 1 OR DimCC.[ExcludeFromReports] IS NULL)
	AND @@@PSIORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND [PSI].[IsDeleted] = 0