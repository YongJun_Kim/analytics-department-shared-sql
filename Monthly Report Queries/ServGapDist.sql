
/*  								
* Monthly Service Gap Reports Distance portion Query								
* 								
* Yong Jun Kim 5/14/2017								
* v1.1: The query is revised to use spatial join.								
* v1.2: The query is revised to be used in conjunction with ServLoc and ErxServLoc tables utilizing spatial index.
* v1.3: ServiceType Table now joins on subqueries before UNION ALL
* v1.4: 4/12/2017 EntOrg Table has been added.
* v1.5: 5/13/2017 ValidErx Table has been added to the DB.  The query has been adjusted to accept POC.
* v1.6: 9/8/2017 User filtering added
* v1.7: 10/09/2017 Patient Filtering added
* v1.8: 01/06/2018 Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. * * v1.9: 06/10/2018 DISTINCT was added to the preference table to prevent duplicate if preference was set at org level and the query is at higher level.
Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtional org columns for classification purpose.
*/

SELECT Agg.ServiceTypeId								
	, '"' + Agg.NameDefaultText + '"' As ServType							
	, CAST(SUM(Agg.AvgDistance) AS float) As SumAvgAlleRxDist			
	, CAST(SUM(Agg.MinDistance) AS float) As SumAvgClosestDist		
	, CAST(SUM(Agg.AvgPrefDist) AS float) As SumAvgPrefDist			
	, CAST(SUM(Agg.LessThan1) AS float) As SumLessThan1			
	, CAST(SUM(Agg.LessThan5) AS float) As SumLessThan5			
	, CAST(SUM(Agg.LessThan10) AS float) As SumLessThan10			
	, CAST(SUM(Agg.LessThan25) AS float) As SumLessThan25			
	, COUNT(DISTINCT Agg.eRxId) AS eRxCNT	
	, Agg.CCNetOrgId AS DimNetOrgId
	, Agg.CCEntOrgId AS DimEntOrgId
	, Agg.CCOrgId AS DimOrgId 
	, Agg.ErxSiteNetOrgId AS DimSiteNetOrgId
	, Agg.ErxSiteEntOrgId AS DimSiteEntOrgId
	, Agg.ErxSiteOrgId AS DimSiteOrgId
FROM	(				
	(							
		SELECT  CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(Pre.Id AS VARCHAR(MAX)) AS ErxId		
			, [Serv].[TypeId] AS ServiceTypeId			
			, [ServType].[NameDefaultText]		
			, ValidErx.ErxSiteOrgId
			, ValidErx.ErxSiteEntOrgId
			, ValidErx.ErxSiteNetOrgId
			, ValidErx.CCOrgId
			, ValidErx.CCEntOrgId
			, ValidErx.CCNetOrgId
			, (SUM((geography::Point(PPL.Latitude, PPL.Longitude, 4326).STDistance(geography::Point(PPL.Latitude, ServLoc.LocationLongitude, 4326)) 					
			   + geography::Point(PPL.Latitude, PPL.Longitude, 4326).STDistance(geography::Point(ServLoc.LocationLatitude, PPL.Longitude, 4326)) )/ 1609.344 ))/COUNT(Serv.Id) AS AvgDistance					
			, SUM(CASE 					
					WHEN OC.ConfigurationType in (1,2) 			
					THEN ((geography::Point(PPL.Latitude, PPL.Longitude, 4326).STDistance(geography::Point(PPL.Latitude, ServLoc.LocationLongitude, 4326)) 			
						+ geography::Point(PPL.Latitude, PPL.Longitude, 4326).STDistance(geography::Point(ServLoc.LocationLatitude, PPL.Longitude, 4326)) )/ 1609.344)		
					ELSE 0 			
					END			
				)/COUNT(Serv.Id) AS AvgPrefDist				
			, COUNT(Serv.Id) As CntServId					
			, SUM(CASE WHEN OC.ConfigurationType in (1,2) THEN 1 ELSE 0 END) AS CntPrefPriServId					
			, 0 AS LessThan1					
			, 0 AS LessThan5					
			, 0 AS LessThan10					
			, 0 AS LessThan25					
			, 0 AS MinDistance					
			FROM [log].[Prescription] AS Pre	
				JOIN [dbo].[ValidErx] AS ValidErx
				ON [Pre].[Id] = [ValidErx].[ErxId]
				LEFT JOIN [log].[PrescriptionService] AS PS				
				ON [Pre].[Id] = [PS].[PrescriptionId]				
					LEFT JOIN [log].[PrescriptionPatientLocation] AS PPL			
					ON [Pre].[Id] = PPL.PrescriptionId			
					JOIN [nowpow].[Service] AS Serv			
					ON [PS].[ServiceID] = [Serv].[Id]			
						JOIN [nowpow].[Organization] AS ServOrg		
						ON [Serv].[OrganizationId] = [ServOrg].[Id]		
							JOIN dbo.LocationDistance ServLoc	
               				ON ServOrg.LocationId = ServLoc.LocationId				
						/*Preferred Status Table */		
						LEFT JOIN  (		
							SELECT DISTINCT OC.ProviderOrganizationId , OC.ConfigurationType	
							FROM [nowpow].[OrganizationConfiguration] AS OC	
								JOIN nowpow.Organization AS Org
								ON OC.OrganizationId = Org.Id
									LEFT JOIN [nowpow].[Organization] AS EntOrg
									ON [Org].EnterpriseOrganizationId = EntOrg.Id
							WHERE @@@PREFORG
							) AS OC	
						ON Serv.OrganizationId = OC.ProviderOrganizationId
				JOIN [nowpow].[ServiceType] AS ServType							
				ON [Serv].[TypeId] = [ServType].[Id]		
				LEFT JOIN [dbo].[PreCCId] AS PreCCId
				ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
				LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
				ON [Pre].[Id] = [PreOrgId].[ErxId]	
				LEFT JOIN [dbo].[DIMPatient] AS DimPatient
				ON Pre.PatientId = DimPatient.PatientId
				/*Exclude Test User from the result Set*/				
			WHERE [Pre].[CreatedDate] <  @@@DATEEND	
				AND [Pre].[CreatedDate] >= @@@DATEBEG							
				AND @@@ORG
				@@@SCTYPE
				AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
				AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
				AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
			GROUP BY CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(Pre.Id AS VARCHAR(MAX))
			, [Serv].[TypeId]
			, [ServType].[NameDefaultText]
			, ValidErx.ErxSiteOrgId
			, ValidErx.ErxSiteEntOrgId
			, ValidErx.ErxSiteNetOrgId
			, ValidErx.CCOrgId
			, ValidErx.CCEntOrgId
			, ValidErx.CCNetOrgId
	)							
	UNION ALL							
	(							
		SELECT CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST([ServDist].PrescriptionId AS VARCHAR(MAX)) AS ErxId					
		, [ServDist].ServiceTypeId	
		, [ServType].[NameDefaultText]		
		, [ServDist].ErxSiteOrgId
		, [ServDist].ErxSiteEntOrgId
		, [ServDist].ErxSiteNetOrgId
		, [ServDist].CCOrgId
		, [ServDist].CCEntOrgId
		, [ServDist].CCNetOrgId
		, 0 AS AvgDistance						
		, 0 AS AvgPrefDist						
		, 0 As CntServId						
		, 0 AS CntPrefPriServId						
		, SUM(CASE WHEN [ServDist].[Distance] < 1 THEN 1 ELSE 0 END) AS LessThan1						
		, SUM(CASE WHEN [ServDist].[Distance] < 5 THEN 1 ELSE 0 END) AS LessThan5						
		, SUM(CASE WHEN [ServDist].[Distance] < 10 THEN 1 ELSE 0 END) AS LessThan10						
		, SUM(CASE WHEN [ServDist].[Distance] < 25 THEN 1 ELSE 0 END) AS LessThan25						
		, MIN([ServDist].[Distance]) AS MinDistance						
								
		FROM (						
			SELECT DISTINCT					
				[ErxServLoc].PrescriptionId
				,[ErxServLoc].ServiceTypeId
				, [ErxServLoc].ErxSiteOrgId
				, [ErxServLoc].ErxSiteEntOrgId
				, [ErxServLoc].ErxSiteNetOrgId
				, [ErxServLoc].CCOrgId
				, [ErxServLoc].CCEntOrgId
				, [ErxServLoc].CCNetOrgId
				,[ServLoc].[ServId] AS ServId
				,  (geography::Point(ErxServLoc.Latitude, ErxServLoc.Longitude, 4326).STDistance(geography::Point(ErxServLoc.Latitude, ServLoc.Longitude, 4326)) 				
							+ geography::Point(ErxServLoc.Latitude, ErxServLoc.Longitude, 4326).STDistance(geography::Point(ServLoc.Latitude, ErxServLoc.Longitude, 4326)) )/ 1609.344  AS Distance	
								
			FROM (					
				SELECT ErxServLoc.PrescriptionId				
					, ErxServLoc.ServiceTypeId	
					, ValidErx.ErxSiteOrgId
					, ValidErx.ErxSiteEntOrgId
					, ValidErx.ErxSiteNetOrgId
					, ValidErx.CCOrgId
					, ValidErx.CCEntOrgId
					, ValidErx.CCNetOrgId
					, ErxServLoc.Latitude
					, ErxServLoc.Longitude		
					, ErxServLoc.GeographyColumn			
				FROM dbo.ErxServLoc AS ErxServLoc
					JOIN [log].[Prescription] AS Pre
					ON [ErxServLoc].[PrescriptionId] = [Pre].[Id]
						JOIN [dbo].[ValidErx] AS ValidErx
						ON [Pre].[Id] = [ValidErx].[ErxId]
						LEFT JOIN [dbo].[PreCCId] AS PreCCId
						ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
						LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
						ON [Pre].[Id] = [PreOrgId].[ErxId]
						LEFT JOIN [dbo].[DIMPatient] AS DimPatient
						ON Pre.PatientId = DimPatient.PatientId	
				WHERE [ErxServLoc].[CreatedDate] < @@@DATEEND				
					AND [ErxServLoc].[CreatedDate] >= @@@DATEBEG		
					AND @@@ORG
					@@@SCTYPE
					AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
					AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
					AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
				) AS ErxServLoc
				JOIN [dbo].[ServLoc] 
				ON (ErxServLoc.GeographyColumn.STDistance([ServLoc].geographyColumn)/1609.344) < 25
				AND ErxServLoc.ServiceTypeId = ServLoc.ServiceTypeId
					JOIN [nowpow].[ServiceType] AS [ServType]				
					ON [ErxServLoc].[ServiceTypeId] = [ServType].[ID]				
		) AS ServDist	
			JOIN [nowpow].[ServiceType] AS ServType							
			ON [ServDist].[ServiceTypeId] = [ServType].[Id]					

	GROUP BY	CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST([ServDist].PrescriptionId AS VARCHAR(MAX))						
		, [ServDist].ServiceTypeId	
		, [ServType].[NameDefaultText]	
		, [ServDist].ErxSiteOrgId
		, [ServDist].ErxSiteEntOrgId
		, [ServDist].ErxSiteNetOrgId
		, [ServDist].CCOrgId
		, [ServDist].CCEntOrgId
		, [ServDist].CCNetOrgId					
	)
)Agg														
GROUP BY [Agg].[ServiceTypeId]								
	, [Agg].[NameDefaultText]	
	, Agg.ErxSiteOrgId
	, Agg.ErxSiteEntOrgId
	, Agg.ErxSiteNetOrgId
	, Agg.CCOrgId
	, Agg.CCEntOrgId
	, Agg.CCNetOrgId	