/*  		
* Montly Screening Reports Query		
* This query pulls the main data for the Screening NowPow reports.		
* 		
* Yong Jun Kim 8/24/2017		
* v1.0		
* v1.1: Simplified and used more DimPatient columns
* v1.2 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/		
		
SELECT SR.Id		
	, SR.CompletedDate	
	, YEAR(SR.CompletedDate) as CompletedYear	
	, convert(char(3),datename(MONTH, SR.CompletedDate)) as CompletedMonth	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(SR.PatientId AS CHAR) AS PatientId
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(SR.PrescriptionId	AS CHAR) AS PrescriptionId
	, SR.SiteId	
	, SR.ScreeningTemplateId	
	, RIGHT('0000' + CAST(SR.ScreeningTemplateId AS VARCHAR(4)), 4) + ': '+ SRTemplate.[Name]	AS [Name]
	, Results.Summary	
	, Results.Score	
	, PostalCode.PostalCode	
	, [DimPatient].[PatientGender]  AS Gender
	, [DimPatient].[PatientAgeGroup] AS AgeGroup	
	, [DimPatient].[PatientRace] AS Race
	, [DimPatient].[PatientEthnicity] AS Ethnicity
	, [DimPatient].[PatientInsurancePlans]  AS InsurancePlans
	, [DimPatient].[PatientInsuranceTypes]  AS InsuranceTypes 
	, [DimPatient].[PatientPreferredLanguage]  AS PreferredLanguage 
	, '"' + [contact].[LastName] + ', ' + [contact].[FirstName] + '"' AS Users	
	, [Org].[Id] AS OrganizationId	
	, '"' + [Org].[Name] + '"' AS OrganizationName	
	, REPLACE(CAST(SR.ScreeningTemplateId AS CHAR)+ ';', ' ', '')  + CAST(Results.Summary AS VARCHAR(Max)) AS SummaryKey
	, REPLACE(STR(SR.ScreeningTemplateId) + ';' + STR(Results.Score), ' ', '') AS ScoreKey
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
		
FROM nowpow.ScreeningResponse AS SR		
	OUTER APPLY openjson(Results)	
		WITH (Score BIGINT
		, Summary NVArCHAR(MAX) ) AS Results
	OUTER APPLY openjson(Address)	
		WITH (PostalCode NVArCHAR(MAX) '$.location."postalCode"') AS PostalCode
	JOIN [nowpow].[ScreeningTemplate] AS SRTemplate	
	ON SR.ScreeningTemplateId = SRTemplate.Id	
	/*Joins for Patient Profiles*/	
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON SR.PatientId = DimPatient.PatientId
	/*Organizations Filtering*/	
	LEFT JOIN [dbo].[PreCCId] AS PreCCId	
	ON [SR].[CareCoordinatorId] = [PreCCId].[CCId]	
		LEFT JOIN nowpow.Contact AS contact
		ON PreCCId.CCContactId = contact.Id           
		LEFT JOIN [nowpow].[Organization] AS Org
		ON [PreCCId].[CCOrgId] = Org.Id
	LEFT JOIN [dbo].[ScreeningOrgId] AS ScreeningOrgId	
	ON SR.[Id] = ScreeningOrgId.ScreeningResponseId	

WHERE SR.CompletedDate IS NOT NULL		
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND(DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)	
	AND SR.CompletedDate < @@@DATEEND
	AND @@@ORG
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)