/*  
* Montly Engagement Reports Summary Query			
* This query pulls data for the Engagement summary portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/2/2017
* v1.3
* v1.1 4/12/2017: EntOrg Table has been added.
* v1.2 6/14/2017: PreCCId Tables replaced various tables.
* v1.3 10/09/2017: Patient Filtering added
* v1.4 12/16/2017: @@EngagementORG has been implemented.
* v1.5 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/	

SELECT	YEAR([PSI].[InteractionDate]) as InteractionYear		
	, convert(char(3),datename(MONTH, [PSI].[InteractionDate])) as InteractionMonth		
	, [PSI].[InteractionTypeId] 		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PSI].[PatientID]	AS Char) AS [PatientID]
	, case		
		when PSI.InteractionTypeId in (1,2,5,6,7,8,9,10)	
		then 'Y'	
		else 'N'	
		end as 'Interaction'	
	, case		
		when PSI.InteractionTypeId in (1,2,5,6,7,9)	
		then 'Y'	
		else 'N'	
		end as 'SuccessInteraction'	
	, case		
		when PSI.InteractionTypeId = 9 then 'Called'	
		when PSI.InteractionTypeId = 7 then 'Met'	
		when PSI.InteractionTypeId in (1,2) then 'Nudged'	
		when PSI.InteractionTypeId in (5,6) then 'Nudged a Service'	
		when PSI.InteractionTypeId in (10,8) then 'Attempted Interaction'	
		else 'N/A'	
		end as 'SuccessCat'	
	, PSI.Wasvcardattached 	
	, case		
		when PSI.InteractionTypeId in (12,13) then 'ResposeInteraction'	
		else 'NonResponse'	
		end as 'ResponseCat'	
	, [DimCC].[SiteId] AS Site_ID			
	, [DimCC].[OrgId] AS Org_ID		
	, '"' + [DimCC].[OrgName] + '"' AS Org_Name		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PSI].[Id] AS CHAR) AS IntId
	/* Added for CSV Split by Org*/
	, [DimCC].NetworkOrgId AS DimNetOrgId
	, [DimCC].EnterpriseOrgId AS DimEntOrgId
	, [DimCC].OrgId AS DimOrgId
	, DimSite.NetworkOrgId AS DimSiteNetOrgId
	, DimSite.EnterpriseOrgId AS DimSiteEntOrgId
	, DimSite.OrgId AS DimSiteOrgId

FROM [nowpow].[PatientSiteInteraction] AS PSI		
	LEFT JOIN dbo.DIMCareCoordinator AS DimCC
	ON PSI.CareCoordinatorId = DimCC.CareCoordinatorId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId
	LEFT JOIN [dbo].[DIMSite] AS DimSite
	ON PSI.SiteId = [DIMSite].SiteId

WHERE  [PSI].[InteractionDate] < @@@DATEEND
	AND ([DimCC].[ExcludeFromReports] <> 1 OR [DimCC].[ExcludeFromReports] IS NULL)
	AND @@@PSIORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND [PSI].[IsDeleted] = 0