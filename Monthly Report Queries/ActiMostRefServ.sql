/*
*Most Referred Service Types by Service Provider
*This query gets referral table data for the referred service types by service providers for Activation reports.
*Created: 01/05/2018		
* v1.1	DIM views implemented
* v1.2 10/09/2017: Patient Filtering added
* v1.3 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/	

SELECT 		
	[Serv].[TypeId] AS ServiceTypeId	
	, CASE 
		WHEN OC.ConfigurationType = 1 THEN 'Prioritized'	
		WHEN OC.ConfigurationType = 2 THEN 'Preferred'	
		ELSE 'N/A' END AS 'PreferredStatus'	
	, [TakerOrg].[OrgName] AS TakerOrgName	
	, [TakerOrg].[OrgId] AS TakerOrgId	
	, [Org].[OrgName] AS MakerOrgName	
	, [Org].[OrgId] AS MakerOrgId	
	, [L].[PostalCode] AS TakerZIPCode	
	, [TakerOrg].[OrgSubTypeId] AS TakerOrg
	, [TakerOrg].[OrgTypeName] AS TakerOrgType	
	, [ST].[NameDefaultText] AS TakerServType	
	, count([REF].[Id]) AS RefCnt
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId

FROM [nowpow].[Referral] AS REF		
	JOIN [nowpow].[Service] AS Serv	
	ON [REF].[ServiceID] = [Serv].[Id]	
		JOIN [nowpow].[ServiceType] AS ST	
		ON Serv.TypeId = ST.Id	
		LEFT JOIN (	
		SELECT OC.OrganizationId, OC.ConfigurationType
		FROM [nowpow].[OrganizationConfiguration] AS OC
		/*Only Include the preferred status for 1077836(Health Corner)*/
		WHERE OC.ProviderOrganizationId = '1077836'
		) AS OC
		ON Serv.OrganizationId = OC.OrganizationId	
	LEFT JOIN [dbo].[DIMOrganization] AS TakerOrg	
	ON [REF].[TakerOrganizationId] = [TakerOrg].[OrgId]
		JOIN [nowpow].[location] AS L	
		ON TakerOrg.OrgLocationId = L.Id
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN [dbo].[DIMOrganization] AS Org	
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	LEFT JOIN dbo.DIMContact AS DIMContact
	ON Ref.MakerContactId= DIMContact.ContactId
	/*Table Joins for filtering referrals end*/

WHERE Ref.CreatedDate < @@@DATEEND
	AND Ref.CreatedDate >= @@@DATEBEG
	AND @@@ORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)

GROUP BY 		
	[Serv].[TypeId]
	, CASE 
		WHEN OC.ConfigurationType = 1 THEN 'Prioritized'	
		WHEN OC.ConfigurationType = 2 THEN 'Preferred'	
		ELSE 'N/A' END 
	, [TakerOrg].[OrgName] 
	, [TakerOrg].[OrgId]
	, [Org].[OrgName] 
	, [Org].[OrgId] 
	, [L].[PostalCode] 
	, [TakerOrg].[OrgSubTypeId] 
	, [TakerOrg].[OrgTypeName]
	, [ST].[NameDefaultText]
	, Org.NetworkOrgId 
	, Org.EnterpriseOrgId 
	, Org.OrgId 
