/* 
*Count the unique users for a designated organization.
*2/27/2018
*Yong Jun Kim
*/

SELECT count(distinct(u.id)) AS TotalUser
	, DIMCC.OrgId AS DimOrgId
	, DIMCC.EnterpriseOrgId AS DimEntOrgId
	, DIMCC.NetworkOrgId AS DimNetOrgId

FROM nowpow.CareCoordinator CC
	JOIN nowpow.Contact Cont
	ON CC.ContactId = Cont.Id
		JOIN nowpow.[user] U
		ON U.ContactId = Cont.Id
	LEFT JOIN [nowpow].[Site] AS [Site]
	ON CC.SiteId = [Site].Id
		LEFT JOIN [dbo].DimOrganization AS Org
		ON [Site].OrganizationId = Org.OrgId
	LEFT JOIN dbo.DimCareCoordinator AS DIMCC
	ON CC.ID = DIMCC.CareCoordinatorID

WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
	AND Cont.IsDeleted = 0
	AND U.IsActive = 1
	AND @@@ORG

GROUP BY DIMCC.OrgId 
	, DIMCC.EnterpriseOrgId
	, DIMCC.NetworkOrgId