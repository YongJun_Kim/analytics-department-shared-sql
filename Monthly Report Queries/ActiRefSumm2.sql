/*	
*The Referral Summary SQL Query2	
*This query gets referral appointment table data for the Referral Summary part of the Referral monthly report.	
*It joins the referral table to the referral appointment table.	
*Created: 4/1/2017	
* v1.0	
* v1.1 Added @@@EXCC, @@@ORG, @@@DATEBEG, @@@DATEEND
* v1.2 Dimension tables have been added
* v1.3 10/09/2017: Patient Filtering added
* v1.4 11/29/2017: WalkInAndAppointmentResult, Ref.CompletedFlag, Ref.ArchivedFlag added
* v1.5 12/16/2017: Query symantic changed to be consistent with ActiRefSumm2.sql. Function stayed the same.
*/	
	
SELECT Ref.Id AS ReferralId	
	, Ref.TakerOrganizationId
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(Ref.MakerPatientId AS Char) AS MakerPatientId	
	, Ref.CreatedDate AS RefCreatedDate
	, RA.Id
	, RA.AppointmentStatusId
	, Ref.CompletedFlag
	, Ref.ArchivedFlag
	, CASE 
		WHEN ((RA.AppointmentStatusId = '2') OR (Ref.ArchivedFlag = '1' AND Ref.CompletedFlag = '1' AND Ref.ContactResultTypeId = '9')) THEN 'Attended'
		WHEN (RA.AppointmentStatusId = '3') OR (Ref.ArchivedFlag = '1' AND Ref.CompletedFlag = '0' AND Ref.ContactResultTypeId = '9') THEN 'No Show'
		WHEN (RA.AppointmentStatusId NOT IN ('2','3')) OR (Ref.ArchivedFlag = '0' AND Ref.CompletedFlag = '0' AND Ref.ContactResultTypeId = '9') THEN 'Other'
	END AS WalkInAndAppointmentResult
	/* Added for CSV Split by Org*/
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId

FROM nowpow.Referral Ref	
	LEFT JOIN nowpow.ReferralAppointment RA
	ON Ref.Id = RA.ReferralId
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN dbo.DIMOrganization AS Org
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN dbo.DIMContact AS DIMContact
	ON Ref.MakerContactId = DIMContact.ContactId 
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	/*Table Joins for filtering referrals end*/


WHERE Ref.CreatedDate < @@@DATEEND
AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)
AND @@@ORG
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)