/*  
* Montly Executive Reports Service portion Query			
* This query pulls data for the Executive Reports Service portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/2/2017
* Current Version: v1.4
* v1.1 4/6/2017:Enterprise Name has been added.	
* v1.2 4/12/2017: Enterprise Org table changed to EntOrg Table
* v1.3 6/14/2017: PreCCId PreOrgId Tables replaced various tables.
* v1.4 10/09/2017: Patient Filtering added
* v1.5 01/04/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to use ExcludeFromReports column in DB to exclude users to exclude from reporting
*/

SELECT YEAR([Pre].[CreatedDate]) AS eRx_Year
	, CONVERT(CHAR(3),DATENAME(MONTH, [Pre].[CreatedDate])) AS eRx_Month
	, CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(Pre.Id AS VARCHAR(MAX)) AS eRx_Id --DB specific
	, Serv.OrganizationId AS SP_ID
	, CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(Serv.OrganizationId AS VARCHAR(MAX)) + ';' + CAST(Pre.ID AS VARCHAR(MAX)) AS SP_ID_erxID --DB Specific
	, PS.ServiceId
	, CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(PS.ServiceId AS VARCHAR(MAX)) + ';' + CAST(Pre.ID AS VARCHAR(MAX)) AS Serv_ID_erxID --DB specific
	, Serv.TypeId AS ServTypeId		
	, '"' + ST.NameDefaultText + '"' AS ServTypeName		
	, [PreCCId].[CCSiteId] AS Site_ID			
	, (geography::Point(PrePatLoc.Latitude, PrePatLoc.Longitude, 4326).STDistance(geography::Point(PrePatLoc.Latitude, ServLoc.LocationLongitude, 4326)) 
	   + geography::Point(PrePatLoc.Latitude, PrePatLoc.Longitude, 4326).STDistance(geography::Point(ServLoc.LocationLatitude, PrePatLoc.Longitude, 4326)) )/ 1609.344  AS Distance
	, '"' +ServOrg.[Name] + '"' AS Serv_Org_Name		
	, CAST(DB_ID(DB_NAME()) AS VARCHAR(MAX)) + ';' + CAST(Serv.TypeId AS VARCHAR(MAX)) + ';' + CAST(Pre.ID AS VARCHAR(MAX)) AS ServType_ID_erxID	--DB specific
	, '"' + [EntOrg].[Name] + '"' AS Enterprise_Name
	, [Pre].[CreatedDate] AS eRxCreateDate
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, PreOrgId.ErxSiteNetOrgId AS DimSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId AS DimSiteEntOrgId
	, PreOrgId.ErxSiteOrgId AS DimSiteOrgId
	  
FROM [log].Prescription AS Pre			
	JOIN [log].PrescriptionService AS PS
	ON PS.PrescriptionId = Pre.Id
		JOIN nowpow.Service AS Serv		
		ON Serv.Id = PS.ServiceId	
			JOIN nowpow.ServiceType AS ST		
			ON Serv.TypeId = ST.Id	
			JOIN nowpow.Organization ServOrg
			ON Serv.OrganizationId = ServOrg.Id
				JOIN dbo.LocationDistance ServLoc
				ON ServOrg.LocationId = ServLoc.LocationId
	JOIN [log].PrescriptionPatientLocation PrePatLoc
	ON Pre.Id = PrePatLoc.PrescriptionId

	LEFT JOIN [dbo].[PreCCId] AS [PreCCId]
	ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
		LEFT JOIN [nowpow].[Organization] AS EntOrg
		ON [PreCCId].[CCEntOrgId] = [EntOrg].[Id]
	LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
	ON [Pre].[Id] = [PreOrgId].[ErxId]
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Pre.PatientId = DimPatient.PatientId

WHERE [PRE].[CreatedDate] < @@@DATEEND
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND @@@ORG
	@@@SCTYPE	
	AND (DimPatient.TestPatient = 'False' 
		OR 
		DimPatient.TestPatient IS NULL)
	AND PrePatLoc.Latitude IS NOT NULL
	AND PrePatLoc.Longitude IS NOT NULL
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)