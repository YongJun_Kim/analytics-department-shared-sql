/*		
*The Referral Summary SQL Query1		
*This query gets referral table data for the Referral Summary part of the Referral monthly report.		
*It does not join to the Referral appointment table.		
*Created: 4/1/2017		
* v1.0	
* v1.1 Added EXCC, ORG, DATEBEG, DATEEND variables
* v1.2 Added Dimension tables
* v1.3 10/09/2017: Patient Filtering added
* v1.4 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/		
		
SELECT Ref.Id AS ReferralId	
	, Ref.TakerOrganizationId
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(Ref.MakerPatientId AS Char) AS MakerPatientId	
	, Ref.ContactResultTypeId	
	, Ref.CreatedDate AS RefCreatedDate	
	, EarliestApptDt.AppointmentDate	
	, ISNULL(CAST(DATEDIFF(day, Ref.CreatedDate, EarliestApptDt.AppointmentDate) AS VARCHAR(10)), '') AS TimeBetRefAppt	
	, Ref.ContactedDate	
	, ISNULL(CAST(DATEDIFF(day, Ref.CreatedDate, Ref.ContactedDate) AS VARCHAR(10)) , '') AS TimeBetRefCont	
	, NULL AS DistClieServ
	, [ServType].[NameDefaultText] AS ServiceType	
	, Ref.MakerOrganizationId
	/* Added for CSV Split by Org*/	
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId
		
FROM nowpow.Referral Ref			
	LEFT JOIN 	
		(SELECT RA.ReferralId
		, MIN(RA.AppointmentDate) AS AppointmentDate
		FROM nowpow.ReferralAppointment RA
		GROUP BY RA.ReferralId) EarliestApptDt
		ON Ref.Id = EarliestApptDt.ReferralId
	JOIN [nowpow].[Service] AS Serv	
	ON Ref.ServiceId = Serv.Id	
		JOIN [nowpow].[ServiceType] AS ServType
		ON [Serv].[TypeId] = [ServType].[Id]
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN dbo.DIMOrganization AS Org		
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN dbo.DIMContact AS DIMContact
	ON Ref.MakerContactId= DIMContact.ContactId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	/*Table Joins for filtering referrals end*/


WHERE Ref.CreatedDate < @@@DATEEND
AND @@@ORG
AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
