/*
*Monthly Report Query
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*/
SELECT 
 CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
ELSE 'N/A'
END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS DimOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR)
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS DimEntOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS DimNetOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgName
, Raw.*
, ISNULL(Raw.ServiceNudged,0) + ISNULL(Raw.TrackedReferrals,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) AS TotalSharedReferrals

FROM
(
(
/**eRx Created**/              
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'                
, 1 AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, Pre.PatientId

FROM log.Prescription AS Pre
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
SELECT PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date) AS 'Date'                
, NULL AS eRxCreated
, COUNT(*) AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
JOIN log.Prescription AS Pre
ON PSI.PrescriptionId = Pre.Id
LEFT JOIN log.PrescriptionService AS PreServ
ON Pre.Id = PreServ.PrescriptionId
WHERE [PSI].[InteractionTypeId] in ('3','4','11') 
AND [PSI].IsDeleted = 0

GROUP BY PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date)         
, PSI.PatientId
)
UNION ALL
(
/**Tracked Referrals**/              
SELECT CC.Id AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, 1 AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, Ref.MakerPatientId AS PatientId

FROM nowpow.CareCoordinator CC
INNER JOIN nowpow.Contact Contact
ON CC.ContactId = Contact.Id
INNER JOIN nowpow.Referral AS Ref
ON Contact.Id = Ref.MakerContactId
)
UNION ALL
(
/**eRxShared**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS CCSiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, 1 AS eRxShared    
, NULL AS ServiceNudged   
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
WHERE [PSI].[InteractionTypeId] in ('3','4','11')    
AND [PSI].IsDeleted = 0
)
UNION ALL
(
/**Service Nudged**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS CCSiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared   
, 1 AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*5&6 for Service Nudged*/        
WHERE [PSI].[InteractionTypeId] in ('5','6')    
AND [PSI].IsDeleted = 0
)
UNION ALL
(
/**Login Activities**/            
SELECT CC.Id AS CareCoordinatorId           
,  NULL AS SiteId
, CAST(Uact.CreatedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared    
, NULL AS ServiceNudged   
, 1 AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS PatientId

/*Join from CareCoordinator Table Since External User Activity Table has only User ID and no CareCordinator Id*/
/*This also excludes any untrackable login activities that do not have UserId*/
FROM nowpow.CareCoordinator CC             
 JOIN nowpow.Contact Contact            
  ON CC.ContactId = Contact.Id           
JOIN nowpow.[User] U             
ON Contact.Id = U.Contactid           
JOIN [log].[ExternalUserActivity] UAct            
ON Uact.userid = U.Id  
WHERE UAct.Activity = 0
)
UNION ALL
(
/**Searches**/            
SELECT lsc.CareCoordinatorId AS CareCoordinatorId           
,  lsc.SiteId AS SiteId
, CAST(lsc.CreatedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared  
, NULL AS ServiceNudged     
, NULL AS Logins
, 1 AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, lsc.PatientId AS PatientId

/*SearchType 1 = Service Search*/
FROM log.SearchCriteria lsc
WHERE lsc.SearchType = 1  
AND lsc.CareCoordinatorId IS NOT NULL
)
UNION ALL
(
/**Favorites**/            
SELECT CC.Id AS CareCoordinatorId           
, NULL AS SiteId
, CAST(SSF.DateAdded as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, 1 AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS PatientId

FROM nowpow.CareCoordinator CC             
JOIN nowpow.Contact contact            
ON CC.ContactId = contact.Id           
JOIN nowpow.[User] U             
ON contact.Id = U.Contactid           
JOIN nowpow.SubscriberServiceFavorite AS SSF            
ON U.Id = SSF.SubscriberUserid     
)
UNION ALL
(
/**Screenings**/            
SELECT SR.CareCoordinatorId AS CareCoordinatorId           
, SR.SiteId AS SiteId
, CAST(SR.CompletedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, 1 AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS PatientId

FROM nowpow.ScreeningResponse AS SR
WHERE SR.CompletedDate IS NOT NULL
)
UNION ALL
(
/**eRxLookups and ServicePrints**/            
SELECT CC.Id AS CareCoordinatorId           
, UAct.SiteId AS SiteId
, CAST(UAct.ActionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL   AS Screenings
, CASE WHEN UAct.Action like '%view%' THEN 1 END AS eRxLookups
, CASE WHEN UAct.Action like '%print%' THEN 1 END AS ServicePrints
, NULL AS PatientId

FROM nowpow.CareCoordinator CC             
JOIN nowpow.Contact contact            
ON CC.ContactId = contact.Id           
JOIN nowpow.[User] U             
ON contact.Id = U.Contactid           
JOIN [log].UserActivity AS UAct           
ON U.Id = UAct.UserId
WHERE UAct.Action like '%view%'
OR UAct.Action like '%print%'
)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
LEFT JOIN DIMPatient AS DimPatient
ON Raw.PatientId = DimPatient.PatientId

WHERE [raw].[Date] >= @@@DATEBEG
AND [raw].[Date] < @@@DATEEND
AND @@@ORG
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/

AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)

/*Exclude any activity related to the test patient*/

AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)