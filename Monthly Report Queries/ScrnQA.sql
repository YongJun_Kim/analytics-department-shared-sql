/*  		
* Montly Screening Reports Query		
* This query pulls the main data for the Screening NowPow reports.		
* 		
* Yong Jun Kim 8/24/2017		
* v1.0		
* v1.1 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/		


SELECT Raw.*		
FROM (		
SELECT SR.Id		
	, SR.CompletedDate	
	, YEAR(SR.CompletedDate) as CompletedYear	
	, convert(char(3),datename(MONTH, SR.CompletedDate)) as CompletedMonth	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(SR.PatientId AS CHAR) As PatientId	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(SR.PrescriptionId	AS CHAR) AS PrescriptionId
	, SR.SiteId	
	, RIGHT('0000' + CAST(SR.ScreeningTemplateId AS VARCHAR(4)), 4) + ': '+ SRTemplate.Name AS SRTemplate	
	, '"' + [contact].[LastName] + ', ' + [contact].[FirstName] + '"' AS Users	
	, [Org].[Id] AS OrganizationId	
    , '"' + [Org].[Name] + '"' AS OrganizationName		
	, RIGHT('0000' + ISNULL(JSON_VALUE (SRQuestions.value, '$.Id'), ''), 4) + ': ' + JSON_VALUE (SRQuestions.value, '$.Text') AS Question	
	, JSON_VALUE (SRAnswers.value, '$.Response') AS Answer	
	, SRTemplate.IsActive
	, RIGHT('0000' + CAST(SR.ScreeningTemplateId AS VARCHAR(4)), 4) + ': '+ SRTemplate.Name	 + ';' + RIGHT('0000' + ISNULL(JSON_VALUE (SRQuestions.value, '$.Id'), ''), 4) + ': ' + JSON_VALUE(SRQuestions.value, '$.Text') + ';' + JSON_VALUE(SRAnswers.value, '$.Response') AS RepKey
	, RIGHT('0000' + CAST(SR.ScreeningTemplateId AS VARCHAR(4)), 4) + ': '+ SRTemplate.Name	 + ';' + RIGHT('0000' + ISNULL(JSON_VALUE (SRQuestions.value, '$.Id'), ''), 4) + ': ' + JSON_VALUE(SRQuestions.value, '$.Text') AS RepKeyQOnly
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId

FROM nowpow.ScreeningResponse AS SR		
         OUTER APPLY OPENJSON(SR.Answers) AS SRPages		
          OUTER APPLY OPENJSON(SRPages.Value, '$.Sections') AS SRSections		
           OUTER APPLY OPENJSON(SRSections.Value, '$.Questions') AS SRQuestions		
		    OUTER APPLY OPENJSON(SRQuestions.Value, '$.Answers') AS SRAnswers
	JOIN [nowpow].[ScreeningTemplate] AS SRTemplate	
	ON SR.ScreeningTemplateId = SRTemplate.Id	
	/*Joins for Patient Profiles*/	
		
	/*Organizations Filtering*/	
	LEFT JOIN [dbo].[PreCCId] AS PreCCId	
	ON [SR].[CareCoordinatorId] = [PreCCId].[CCId]	
		LEFT JOIN nowpow.Contact AS contact
		ON PreCCId.CCContactId = contact.Id           
		LEFT JOIN [nowpow].[Organization] AS Org
		ON [PreCCId].[CCOrgId] = Org.Id
	LEFT JOIN [dbo].[ScreeningOrgId] AS ScreeningOrgId	
	ON SR.[Id] = ScreeningOrgId.ScreeningResponseId	
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient	
	ON SR.PatientId = DimPatient.PatientId	

WHERE SR.CompletedDate IS NOT NULL	
	AND SR.CompletedDate >= @@@DATEBEG	
	AND SR.CompletedDate < @@@DATEEND	
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND @@@ORG	
	AND(DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	) Raw	
WHERE Raw.Answer IS NOT NULL	
/*Exclude any answers that list addresses*/	
AND Raw.Answer NOT LIKE '%{"address"%'		
/*Exclude any question that asks for names */
AND Raw.Question NOT LIKE '%: Name'
/*Exclude any screening date question*/
AND RAW.Question NOT LIKE '%Date of screen%'
AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)