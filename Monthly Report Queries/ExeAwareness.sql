/*  
* Monthly Executive Awareness Reports Mode Query
* This query pulls data for the Executive Awareness portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/2/2017
* Current Version: v1.5
* v1.1 4/6/2017:Enterprise Name has been added.
* v1.2 4/12/2017: EnterpriseOrg changed to EntOrg
* v1.3 6/14/2017: PreCCId PreOrgId Tables replaced various tables.
* v1.4 10/09/2017: Patient Filtering added
* v1.5 01/04/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users from reporting
*/

SELECT	YEAR([Pre].[CreatedDate]) as eRxCreateYear	
	, convert(char(3),datename(MONTH, [Pre].[CreatedDate])) as eRxCreateMonth	
	, [PS].[OntologyId] AS ConditionID	
	, [PreLoc].[PostalCode] AS ZipCode	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PS].[PrescriptionId] AS Char) AS eRxID	--Customer DB specific	
	, [PreCCId].[CCSiteId] AS Site_ID		
	, Org.Id AS Org_ID	
	, '"' + Org.[Name] + '"' AS Org_Name	
 	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([Pre].[PatientId] AS Char) AS ClientID	--Customer DB specific
	, '"' + [EntOrg].[Name] + '"' AS Enterprise_Name	
	, [Pre].[CreatedDate] as eRxCreateDate
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, PreOrgId.ErxSiteNetOrgId AS DimSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId AS DimSiteEntOrgId
	, PreOrgId.ErxSiteOrgId AS DimSiteOrgId

FROM [log].[PrescriptionService] AS PS		
	LEFT JOIN [log].[Prescription] AS Pre		
	ON [PS].[PrescriptionId] = [Pre].[Id]	
		Left JOIN [log].[PrescriptionPatientLocation] AS PreLoc		
		ON [PS].[PrescriptionId] = [PreLoc].[PrescriptionId]	
		LEFT JOIN [dbo].[PreCCId] AS PreCCId
		ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
				LEFT JOIN nowpow.Organization AS Org	
				ON [PreCCId].[CCOrgId] = Org.Id
					LEFT JOIN [nowpow].[Organization] AS EntOrg
					ON [Org].EnterpriseOrganizationId = EntOrg.Id
		LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
		ON [Pre].[Id] = [PreOrgId].[ErxId]
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Pre.PatientId = DimPatient.PatientId

WHERE [PRE].[CreatedDate] < @@@DATEEND
	AND @@@ORG
	@@@SCTYPE	
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)