/*  
* Monthly Service Provider Reports Type portion Query			
* This query pulls data for the Service Provider Reports Type portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/3/2017
* v1.1
* v1.1 4/12/2017 EntOrg Table has been added.
* v1.2 6/14/2017 PreCCId PreOrgId Tables replaced various tables.
* v1.3 10/09/2017 Patient Filtering added
* v1.4 01/06/2018 Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtional org columns for classification purpose.
* v1.5 06/10/2018 Revised to add distinct to the preference table for excluding duplicates.
*/	

SELECT [Serv].[TypeId] AS ServiceTypeId	
	, CASE 
		WHEN OC.ConfigurationType = 1 THEN 'Prioritized'	
		WHEN OC.ConfigurationType = 2 THEN 'Preferred'	
	ELSE 'N/A' END AS 'PreferredStatus'	
	, '"' + [SOrg].[Name] + '"' AS OrgName	
	, [Serv].[OrganizationId] AS OrgId	
	, [L].[PostalCode] AS ZIPCode	
	, '"' + OT.Name + '"' AS OrgType	
	, '"' + ST.NameDefaultText + '"' AS ServType	
	, [PreCCId].[CCOrgId] AS OrganizationId
	, YEAR([Pre].[CreatedDate]) AS CreatedYear
	, CONVERT(CHAR(3), DATENAME(MONTH, [Pre].[CreatedDate])) AS CreatedMonth
	, COUNT(DISTINCT[Pre].[Id]) AS PreCnt
	/* Added for CSV Split by Org*/
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, PreOrgId.ErxSiteNetOrgId AS DimSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId AS DimSiteEntOrgId
	, PreOrgId.ErxSiteOrgId AS DimSiteOrgId

FROM[log].[Prescription] AS Pre		
	LEFT JOIN [log].[PrescriptionService] AS PS	
	ON [Pre].[Id] = [PS].[PrescriptionId]	
		JOIN [nowpow].[Service] AS Serv	
		ON [PS].[ServiceID] = [Serv].[Id]
			LEFT JOIN (	
				SELECT DISTINCT OC.ProviderOrganizationId, OC.ConfigurationType
				FROM [nowpow].[OrganizationConfiguration] AS OC
					JOIN nowpow.Organization Org
					ON OC.OrganizationId = Org.Id
						LEFT JOIN [nowpow].[Organization] AS EntOrg
						ON [Org].EnterpriseOrganizationId = EntOrg.Id
				/*Only Include the preferred status for a given org*/
				WHERE @@@PREFORG
			) AS OC
			ON Serv.OrganizationId = OC.ProviderOrganizationId	
			JOIN [nowpow].[Organization] AS SOrg
			ON [Serv].[OrganizationId] = [SOrg].[Id]
				JOIN [nowpow].[location] AS L
				ON SOrg.LocationId = L.Id
				JOIN [pristine].[OrganizationSubType] AS OST	
				ON SOrg.OrganizationSubTypeId = OST.Id	
					JOIN [pristine].[OrganizationType] AS OT	
					ON OST.OrganizationTypeId = OT.ID	
			JOIN [nowpow].[ServiceType] AS ST	
			ON Serv.TypeId = ST.Id	
	/*Report Filtering*/
	LEFT JOIN [dbo].[PreCCId] AS PreCCId
	ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Pre.PatientId = DimPatient.PatientId
	LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
	ON [Pre].[Id] = [PreOrgId].[ErxId]

WHERE 	[Pre].[CreatedDate] < @@@DATEEND
	AND @@@ORG
	@@@SCTYPE	
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	/*Exclude Test User from the result Set*/
	AND (DimPatient.TestPatient = 'False' OR	DimPatient.TestPatient IS NULL)
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)

GROUP BY [Serv].[TypeId] 	
	, CASE 
		WHEN OC.ConfigurationType = 1 THEN 'Prioritized'	
		WHEN OC.ConfigurationType = 2 THEN 'Preferred'	
		ELSE 'N/A' 
	END	
	, [SOrg].[Name]	
	, [Serv].[OrganizationId] 	
	, [L].[PostalCode] 	
	, OT.[Name]	
	, ST.NameDefaultText	
	, [PreCCId].CCOrgId 	
	, YEAR([Pre].[CreatedDate]) 
	, CONVERT(CHAR(3), DATENAME(MONTH, [Pre].[CreatedDate]))
	, PreCCId.CCNetOrgId 
	, PreCCId.CCEntOrgId 
	, PreCCId.CCOrgId 
	, PreOrgId.ErxSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId 
	, PreOrgId.ErxSiteOrgId 
	