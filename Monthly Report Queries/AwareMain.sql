/*  
* Montly Awareness Reports Query			
* This query pulls the main data for the Awareness portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 10/31/2016
* v1.1 4/12/2017: EntOrg Table has been added.
* v1.2 6/14/2017: PreCCId PreOrgId Tables replaced various tables.
* v1.3 10/09/2017: Patient Filtering added
* v1.4 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/			
			
SELECT 			
    cast ([PRE].[CreatedDate] as date) as CreatedDate			
	, YEAR([PRE].[CreatedDate]) as CreatedYear		
	, convert(char(3),datename(MONTH, [PRE].[CreatedDate])) as CreatedMonth		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([Pre].[Id] AS Char) AS PrescriptionID		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([DimPatient].[PatientId] AS Char) AS PatientID		
	, [DimPatient].[PatientGender]  AS Gender
	, [DimPatient].[PatientAgeGroup] AS AgeGroup	
	, [DimPatient].[PatientRace] AS Race
	, [DimPatient].[PatientEthnicity] AS Ethnicity
	, [DimPatient].[PatientInsurancePlans]  AS InsurancePlans
	, [DimPatient].[PatientInsuranceTypes]  AS InsuranceTypes 
	, [DimPatient].[PatientPreferredLanguage]  AS PreferredLanguage 
	, [PL].[PostalCode] As ZipCode 
	, '"' + [contact].[LastName] + ', ' + [contact].[FirstName] + '"' AS Users		
	, [Org].[Id] AS OrganizationId		
	, '"' + [Org].[Name] + '"' AS OrganizationName	
	/* Added for CSV Split by Org*/
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, PreOrgId.ErxSiteNetOrgId AS DimSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId AS DimSiteEntOrgId
	, PreOrgId.ErxSiteOrgId AS DimSiteOrgId
			
FROM [log].[Prescription] AS PRE			
	/*Patient Information Block*/
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Pre.PatientId = DimPatient.PatientId
	LEFT JOIN [log].[PrescriptionPatientLocation] AS PL			
	ON [PRE].[Id] = [PL].[PrescriptionId]
	/*Org Classification*/
	LEFT JOIN [dbo].[PreCCId] AS PreCCId
	ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
		LEFT JOIN nowpow.Contact AS contact
		ON PreCCId.CCContactId = contact.Id           
		LEFT JOIN [nowpow].[Organization] AS Org			
		ON [PreCCId].[CCOrgId] = Org.Id
	LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
	ON [Pre].[Id] = [PreOrgId].[ErxId]

WHERE [PRE].[CreatedDate] < @@@DATEEND
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND @@@ORG
	@@@SCTYPE	
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)