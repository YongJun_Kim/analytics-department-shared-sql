/***  
* Montly Service Provider Reports Nudge portion Query			
* 			
* Yong Jun Kim 3/3/2017
* v1.1 4/12/2017 EntOrg Table has been added.		
* v1.2 6/14/2017 PreCCId Tables replaced various tables.
* v1.3 10/09/2017 Patient Filtering added
* v1.4 01/07/2018 Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtional org columns for classification purpose.
* v1.5 06/10/2018 DISTINCT was added to the preference table to prevent duplicate if preference was set at org level and the query is at higher level.
***/

SELECT
	YEAR([PSI].[InteractionDate]) as InteractionYear			
	, convert(char(3),datename(MONTH, [PSI].[InteractionDate])) as InteractionMonth			
	, PSI.WasvCardAttached
	, PSI.InteractionTypeId		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.ID AS CHAR) AS Id
	, OT.Name AS OrgType	
	, '"' + [ServOrg].[Name] + '"' As OrgName	
	, [Serv].[OrganizationId] AS OrgId	
	, '"' + OT.Name + '"' AS OrgType	
	, [L].[PostalCode] AS ZIPCode	
	, CASE WHEN OC.ConfigurationType = 1 THEN 'Prioritized'	
	WHEN OC.ConfigurationType = 2 THEN 'Preferred'	
	ELSE 'N/A' END AS 'PreferredStatus'
	/* Added for CSV Split by Org*/
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, DimSite.NetworkOrgId AS DimSiteNetOrgId
	, DimSite.EnterpriseOrgId AS DimSiteEntOrgId
	, DimSite.OrgId AS DimSiteOrgId

FROM [nowpow].[PatientSiteInteraction] AS PSI			
	LEFT JOIN [dbo].[PreCCId] AS PreCCId
	ON [PSI].[CareCoordinatorId] = [PreCCId].[CCId]
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId
	LEFT JOIN [dbo].[DIMSite] AS DimSite
	ON PSI.SiteId = [DIMSite].SiteId
	JOIN [nowpow].[Service] AS Serv	
	on [PSI].[ServiceID] = [Serv].[Id]	
		JOIN [nowpow].[Organization] AS ServOrg	
		on [Serv].[OrganizationId] = [ServOrg].[Id]	
			JOIN [pristine].[OrganizationSubType] AS OST	
			ON ServOrg.OrganizationSubTypeId = OST.Id	
				JOIN [pristine].[OrganizationType] AS OT	
				ON OST.OrganizationTypeId = OT.ID	
			JOIN [nowpow].[location] AS L	
			ON ServOrg.LocationId = L.Id	
		LEFT JOIN (	
		SELECT DISTINCT OC.ProviderOrganizationId, OC.ConfigurationType
		FROM [nowpow].[OrganizationConfiguration] AS OC
			JOIN nowpow.Organization Org
			ON OC.OrganizationId = Org.Id
				LEFT JOIN [nowpow].[Organization] AS EntOrg
				ON [Org].EnterpriseOrganizationId = EntOrg.Id
		/*Only Include the preferred status for Organizations that you are running the report for*/
		WHERE @@@PREFORG 
		) AS OC
		ON Serv.OrganizationId = OC.ProviderOrganizationId

WHERE [PSI].[InteractionDate] < @@@DATEEND
	AND @@@ORG
	AND [PSI].[InteractionTypeId] in ('5','6')	
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND(DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND [PSI].[IsDeleted] = 0