/***			
* Monthly Service Gap Reports Supply & Demand portion Query					
* 
* Date: 2016/11/16			
* Version: 1.4
* v1.1: The demand zip code will be calculated by the location of the prescrition no the location of the service providers			
* v1.2 4/12/2017: EntOrg Table has been added.
* v1.3 6/14/2017: PreCCId PreOrgId Tables replaced various tables.
* v1.4 10/09/2017 Patient Filtering added
***/			
			
SELECT ServPre.ServiceTypeId			
	, ServPre.ZIPCode		
	, '"' + ServPre.ServType + '"' AS ServType
	, ServPre.PreCnt		
	, ServOrg.OrgCnt		
	, ServPre.DimNetOrgId
	, ServPre.DimEntOrgId
	, ServPre.DimOrgId
	, ServPre.POCDimNetOrgId AS DimSiteNetOrgId
	, ServPre.POCDimEntOrgId AS DimSiteEntOrgId
	, ServPre.POCDimOrgId AS DimSiteOrgId
FROM			
(
	SELECT 	[Serv].[TypeId] AS ServiceTypeId	
		, [PPL].[PostalCode] AS ZIPCode	
		, ST.NameDefaultText AS ServType
		, PreCCId.CCNetOrgId AS DimNetOrgId
		, PreCCId.CCEntOrgId AS DimEntOrgId
		, PreCCId.CCOrgId AS DimOrgId
		, PreOrgId.ErxSiteNetOrgId AS POCDimNetOrgId
		, PreOrgId.ErxSiteEntOrgId AS POCDimEntOrgId
		, PreOrgId.ErxSiteOrgId AS POCDimOrgId
		, count(Distinct [Pre].[Id]) AS PreCnt
	FROM [log].[Prescription] AS Pre
		LEFT JOIN [log].[PrescriptionPatientLocation] AS PPL	
		ON [Pre].[Id] = PPL.PrescriptionId			
		LEFT JOIN [log].[PrescriptionService] AS PS	
		ON [Pre].[Id] = [PS].[PrescriptionId]	
			JOIN [nowpow].[Service] AS Serv	
			ON [PS].[ServiceID] = [Serv].[Id]	
				JOIN [nowpow].[Organization] AS ServOrg	
				ON [Serv].[OrganizationId] = [ServOrg].[Id]	
				JOIN [nowpow].[ServiceType] AS ST	
				ON Serv.TypeId = ST.Id	
		LEFT JOIN [dbo].[PreCCId] AS PreCCId
		ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
		LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
		ON [Pre].[Id] = [PreOrgId].[ErxId]
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Pre.PatientId = DimPatient.PatientId

	/*Exclude Test User from the result Set*/			
	WHERE [PRE].[CreatedDate] < @@@DATEEND
		AND [PRE].[CreatedDate] >= @@@DATEBEG
		AND @@@ORG
		@@@SCTYPE	
		AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
		AND (DimPatient.TestPatient = 'False' OR	DimPatient.TestPatient IS NULL)
		AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)

	GROUP BY 		
		[Serv].[TypeId] 	
		, [PPL].[PostalCode] 	
		, ST.NameDefaultText 	
		, PreCCId.CCNetOrgId 
		, PreCCId.CCEntOrgId 
		, PreCCId.CCOrgId
		, PreOrgId.ErxSiteNetOrgId 
		, PreOrgId.ErxSiteEntOrgId 
		, PreOrgId.ErxSiteOrgId 
) AS ServPre			
LEFT JOIN (			
	/*Existing Organizations by service type and zip codes */		
	SELECT S.TypeId AS ServiceTypeId		
		, L.PostalCode AS ZIPCode	
		, ST.NameDefaultText AS ServType	
		, COUNT(DISTINCT Org.Id) AS OrgCnt	
	FROM nowpow.organization AS Org		
		JOIN nowpow.location AS L		
		ON Org.LocationId = L.Id	
			JOIN nowpow.Service AS S
			ON Org.Id = S.OrganizationId	
				JOIN [nowpow].[ServiceType] AS ST		
				ON S.TypeId = ST.Id	
	/*Filter for Active(status id =1) organizations*/		
	WHERE Org.OrganizationStatusId  = '1'	
	GROUP BY S.TypeId 		
		, L.PostalCode 	
		, ST.NameDefaultText) AS ServOrg	
 ON (ServPre.ServiceTypeId = ServOrg.ServiceTypeId			
	AND ServPre.ZIPCode = ServOrg.ZIPCode		
	AND ServPre.ServType = ServOrg.ServType)		