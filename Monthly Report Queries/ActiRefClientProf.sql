/*  Monthly Activation Reports Query		
* This query pulls data for the Activation - Referrals by Client.		
* 		
* Yong Jun Kim 10/31/2016 rev by Chevy 12/12/2016	
* v1.4	
* v1.1 Added @@EXCC, @@ORG, @@DATEBEG, @@DATEEND	
* v1.2 10/09/2017: Patient Filtering adde
* v1.3 12/16/2017: Profile will be calculated using a view
* v1.4 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/		
		
SELECT 		
    cast ([REF].[CreatedDate] as date) AS CreatedDate		
	, YEAR([REF].[CreatedDate]) AS CreatedYear	
	, convert(char(3),datename(MONTH, [REF].[CreatedDate])) AS CreatedMonth	
	, [REF].[Id] AS ReferralID	
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PAT].[PatientId] AS CHAR) AS PatientID	
	, [PAT].[PatientGender] AS Gender	
	, [PAT].[PatientAgeGroup] AS AgeGroup
	, [PAT].[PatientRace] AS Race
	, [PAT].[PatientEthnicity] AS Ethnicity
	, [PAT].[PatientInsurancePlans] AS InsurancePlans
	, [PAT].[PatientInsuranceTypes] AS InsuranceTypes
	, [PAT].[PatientPreferredLanguage] As PreferredLanguage	
	, [REF].[PatientPostalCode] As ZipCode	
	, [DIMContact].[ContactLastName] + ', ' + [DIMContact].[ContactFirstName] AS Users	
	, [Org].[OrgId] AS OrganizationId	
    	, [Org].[OrgName] AS OrganizationName
	/* Added for CSV Split by Org*/	
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId	
		
FROM [nowpow].[Referral] AS REF		
	/*Patient Information Block*/
	LEFT JOIN [dbo].[DimPatient] AS PAT	
	ON [REF].[MakerPatientId] = [PAT].[PatientId]	
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN [dbo].[DIMOrganization] AS Org		
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN [dbo].[DIMContact] AS DIMContact
	ON Ref.MakerContactId= DIMContact.ContactId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	/*Table Joins for filtering referrals end*/


WHERE Ref.CreatedDate < @@@DATEEND
AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)
AND @@@ORG
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)