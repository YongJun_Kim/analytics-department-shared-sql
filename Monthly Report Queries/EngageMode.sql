/*  
* Monthly Engagement Reports Mode Query			
* This query pulls data for the Engagement portion of monthly NowPow reports.			
* 			
* Yong Jun Kim 3/2/2017
* v1.1 
* v1.1	4/12/2017: EntOrg Table has been added.
* v1.2 6/14/2017: PreCCId Tables replaced various tables.
* v1.3 10/09/2017: Patient Filtering added
* v1.4 12/16/2017: Query Simplified for DIM patients.
* v1.5 12/19/2017: ID => InteractionId
* v1.6 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
* v1.7 02/20/2018: @@@Org->@@@EngagementOrg to filter by Site Org ID as well as CC Org Id
*/			
SELECT			
  YEAR([PSI].[InteractionDate]) as InteractionYear			
  , convert(char(3),datename(MONTH, [PSI].[InteractionDate])) as InteractionMonth			
	, [DimPatient].[PatientGender]  AS Gender
	, [DimPatient].[PatientAgeGroup] AS AgeGroup	
	, [DimPatient].[PatientRace] AS Race
	, [DimPatient].[PatientEthnicity] AS Ethnicity
	, [DimPatient].[PatientInsurancePlans]  AS InsurancePlans
	, [DimPatient].[PatientInsuranceTypes]  AS InsuranceTypes 
	, [DimPatient].[PatientPreferredLanguage]  AS PreferredLanguage 	
	, PSI.InteractionTypeId
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.ID AS Char) AS InteractionId
	/* Added for CSV Split by Org*/
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST([PSI].[CareCoordinatorId] AS Char) AS CareCoordinatorId
	/* Added for CSV Split by Org*/
	, DimCC.NetworkOrgId AS DimNetOrgId
	, DimCC.EnterpriseOrgId AS DimEntOrgId
	, DimCC.OrgId AS DimOrgId
	, DimSite.NetworkOrgId AS DimSiteNetOrgId
	, DimSite.EnterpriseOrgId AS DimSiteEntOrgId
	, DimSite.OrgId AS DimSiteOrgId

FROM [nowpow].[PatientSiteInteraction] AS PSI			
	LEFT JOIN dbo.DIMCareCoordinator AS DimCC
	ON PSI.CareCoordinatorId = DimCC.CareCoordinatorId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON PSI.PatientId = DimPatient.PatientId
	LEFT JOIN [dbo].[DIMSite] AS DimSite
	ON PSI.SiteId = [DIMSite].SiteId

WHERE [PSI].[InteractionTypeId] in ('1','2')			
	AND [PSI].[InteractionDate] < @@@DATEEND
	AND ([DimCC].[ExcludeFromReports] <> 1 OR [DimCC].[ExcludeFromReports] IS NULL)
	AND @@@PSIORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND [PSI].IsDeleted = 0
