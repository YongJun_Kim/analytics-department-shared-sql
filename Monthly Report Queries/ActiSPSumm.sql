/*		
*The Referral Service Provider Summary SQL Query		
*This query gets service provider data for the Service Porvider Summary part of the Referral monthly report.		
*Created: 4/1/2017		
* v1.0	
* v1.1 Added @@EXCC, @@ORG, @@DATEBEG, @@DATEEND
* v1.2 DIM tables have been added.
* v1.3 10/09/2017: Patient Filtering added
* v1.4 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*/		

SELECT	'"' + TakerOrg.OrgName + '"' AS TakerOrganizationName		
	, Ref.TakerOrganizationId		
	, COUNT(DISTINCT Ref.Id) AS ReferrralsReceived		
	, COUNT(DISTINCT Ref.MakerPatientId) AS ClientsReferred		
	, ISNULL(CAST(AVG(CAST(DATEDIFF(day, Ref.CreatedDate, EarliestApptDt.AppointmentDate) AS FLOAT)) AS VARCHAR(10)),'') AS AVGTimeBetRefAppt		
	, SUM(CASE 		
		WHEN Ref.ContactResultTypeId = 5 THEN 1	
		ELSE 0	
		END) AS NoApptAvail	
	, ISNULL(SUM(AttendedReferrals.Result),0) AS AttendedReferrals		
	, SUM(CASE 		
		WHEN Ref.ContactResultTypeId = 6 THEN 1	
		ELSE 0	
		END) AS IneligibleReferrals	
	, NULL AS PatientRefDist
	/* Added for CSV Split by Org*/
	, Org.NetworkOrgId AS DimNetOrgId
	, Org.EnterpriseOrgId AS DimEntOrgId
	, Org.OrgId AS DimOrgId
			
FROM nowpow.Referral Ref			
	JOIN dbo.DIMOrganization TakerOrg		
	ON Ref.TakerOrganizationId = TakerOrg.OrgId		
	LEFT JOIN 		
		(SELECT Ref.Id	
		, MIN(RA.AppointmentDate) AS AppointmentDate	
		FROM nowpow.Referral Ref	
			JOIN nowpow.ReferralAppointment RA
			ON Ref.Id = RA.ReferralId
		GROUP BY Ref.Id) EarliestApptDt	
		ON Ref.Id = EarliestApptDt.Id	
	LEFT JOIN		
		(SELECT DISTINCT Ref.Id	
		, 1 AS Result	
		FROM nowpow.Referral Ref	
			JOIN nowpow.ReferralAppointment RA
			ON Ref.Id = RA.ReferralId
		WHERE RA.AppointmentStatusId = 2) AttendedReferrals	
	ON Ref.Id = AttendedReferrals.Id	
	/*Table Joins for filtering referrals begin*/
	LEFT JOIN dbo.DIMOrganization AS Org
	ON Ref.MakerOrganizationId = Org.OrgId
	LEFT JOIN dbo.DIMContact AS DIMContact
	ON Ref.MakerContactId= DIMContact.ContactId
	LEFT JOIN [dbo].[DIMPatient] AS DimPatient
	ON Ref.MakerPatientId = DimPatient.PatientId
	/*Table Joins for filtering referrals end*/


WHERE Ref.CreatedDate < @@@DATEEND
	AND Ref.CreatedDate >= @@@DATEBEG
	AND (DIMContact.[ExcludeFromReports] <> 1 OR DIMContact.[ExcludeFromReports] IS NULL)
	AND @@@ORG
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)

GROUP BY '"' + TakerOrg.OrgName + '"' 			
	, Ref.TakerOrganizationId	
	, Org.NetworkOrgId 
	, Org.EnterpriseOrgId
	, Org.OrgId
