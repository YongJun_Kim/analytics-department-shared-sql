SELECT O.Id
	, O.Name 
	, CASE WHEN O.IsNetwork = 1 THEN 'Network'
	WHEN O.IsEnterprise = 1 THEN 'Enterprise'
	ELSE 'Organization'
	END AS OrgLevel
	, SS.[Name] AS SubscriberName
	, CODB.ConnectionString
	, CODB.Id
	, O.SubscriberStatusId
  FROM [nowpow].[Site] AS S
	LEFT JOIN [nowpow].[Organization] AS O
	ON S.OrganizationId = O.Id
	LEFT JOIN [nowpow].[CareOrganizationDatabaseSiteRelation] AS CODBS
	ON S.Id = CODBS.SiteId
		LEFT JOIN [nowpow].[CareOrganizationDatabase] AS CODB
		ON CODBS.CareOrganizationDatabaseId = CODB.Id
	LEFT JOIN [pristine].[SubscriberStatus] AS SS
	ON O.SubscriberStatusId = SS.ID

	WHERE CODB.ConnectionString IS NOT NULL
	AND SS.ID NOT IN (2,8,7)
	--AND CODB.Id=100 