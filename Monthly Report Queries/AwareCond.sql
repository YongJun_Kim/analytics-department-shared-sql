/*
*Monthly Awareness Reports Query
*This query pulls the data with ontology conditions for the Awareness portion of monthly NowPow reports.
*
*Yong Jun Kim 3/2/2017
*v1.0
*v1.1 4/12/2017: EntOrg Table has been added
*v1.2 6/14/2017: PreCCID PreOrgID replaced various tables
*v1.3 10/09/2017: Patient Filtering added
*v1.4 01/05/2018: Revised to handle DB specific Ids concatenated with DBID, so data can be aggregated across DBs. Revised to scan ExcludeFromReports to exclude users to exclude from reporting.  Added 3 addtion org columns for classification purpose.
*v1.5 02/20/2018: Changed the path
*/

SELECT [PS].[OntologyId]			
	, [Org].Id AS OrganizationId		
	, '"' + [Org].Name + '"' AS OrganizationName		
	, '"' + Ont.Name + '"' AS COND		
	,	cast ([Pre].[CreatedDate] as date) as erxCreateDate	
	, YEAR([Pre].[CreatedDate]) as erxCreateYear		
	, convert(char(3),datename(MONTH, [Pre].[CreatedDate])) as erxCreateMonth		
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' +  CAST([PS].[PrescriptionId] AS CHAR) AS PrescriptionId
	, CAST(DB_ID(DB_NAME()) AS CHAR) + ';' +  CAST([Pre].[PatientId] AS CHAR) AS PatientId
	/* Added for CSV Split by Org*/
	, PreCCId.CCNetOrgId AS DimNetOrgId
	, PreCCId.CCEntOrgId AS DimEntOrgId
	, PreCCId.CCOrgId AS DimOrgId
	, PreOrgId.ErxSiteNetOrgId AS DimSiteNetOrgId
	, PreOrgId.ErxSiteEntOrgId AS DimSiteEntOrgId
	, PreOrgId.ErxSiteOrgId AS DimSiteOrgId
			
FROM [log].[PrescriptionService] AS PS			
	LEFT JOIN nowpow.Ontology AS Ont 			
	ON PS.OntologyId = Ont.Id			
	LEFT JOIN [log].[Prescription] AS Pre			
	ON [PS].[PrescriptionId] = [Pre].[Id]		
	LEFT JOIN [dbo].[PreCCId] AS PreCCId
	ON [Pre].[CareCoordinatorId] = [PreCCId].[CCId]
	LEFT JOIN [dbo].[PreOrgId] AS PreOrgId
	ON [Pre].[Id] = [PreOrgId].[ErxId]
		LEFT JOIN [nowpow].[Organization] AS Org
		ON [PreCCId].CCOrgId = Org.Id
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Pre.PatientId = DimPatient.PatientId

WHERE [PRE].[CreatedDate] < @@@DATEEND
	AND ([PreCCId].ExcludeFromReports <> 1 OR [PreCCId].[ExcludeFromReports] IS NULL)
	AND @@@ORG
	@@@SCTYPE	
	AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)