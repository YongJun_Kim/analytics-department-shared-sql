--USE ALL
--GO


/****QUERY TO GENERATE USER LIST***/
/****ADAPTED FROM ALLUSERS_LASTLOGIN QUERY*****/
/****V.1. UPDATED TO ADD DBNAME, USER TITLE AND DEPARTMENT*****/
/****V.2. UPDATED TO ADD CONTACT PHONE NUMBER AND TYPE****/
/****V.3. UPDATED TO ADD ISDEMO AND ISLIVE FLAGS FOR DATABASES****/
/****V.4. UPDATED TO ADD FLAG FOR ISFLOATING*****/

;
WITH CTE (UserId, LastLoginDate)
AS 
(
	SELECT	Uact.UserId,
			MAX([UAct].[CreatedDate]) AS 'LastLoginDate' 
	FROM	[log].[ExternalUserActivity] UAct  
	GROUP BY 
			UAct.UserId          
)

select	c.ID as ContactId, 
		U.Id as UserId, 
		O.Id as OrganizationId,
		COALESCE(NET.Name, COALESCE(ENT_NET.Name, ' - ')) as 'Network Name',
		COALESCE(ENT.Name, ' - ') as 'Enterprise Name',
		s.id as Siteid, 
		CASE WHEN o.IsNetwork is null OR o.IsNetwork = 0 THEN 'No' ELSE 'Yes' END as 'Is Network?',
		CASE WHEN o.IsEnterprise is null OR o.IsEnterprise = 0 THEN 'No' ELSE 'Yes' END as 'Is Enterprise?',
		o.Name as OrgName,
		concat(loc.number, loc.fraction, ' ', loc.Street, ' ', loc.Unit) as 'Address',
		loc.City,
		loc.[State],
		loc.PostalCode, 
		c.LastName,
		c.FirstName,
		c.Email,
		u.UserName,
		c.Title,
		c.Department,
		concat (cpn.AreaCode, cpn.Prefix, cpn.Suffix, cpn.Extension) as 'Phone Number',
		CASE WHEN cpn.PhoneNumberTypeId = 1 Then 'Work Phone' when cpn.PhoneNumberTypeId = 2 then 'Mobile Phone' when cpn.PhoneNumberTypeId = 3 then 'Fax' end as 'PhoneType',
		U.IsActive as UserIsActive, 
		C.IsDeleted as ContactIsDeleted,
		ur.roleid,
		r.Name as 'Role Name',
		l.Name as 'License',
		cast (C.CreatedDate as date) as DateAdded,
		cast (CTE.LastLoginDate as date) as LastLoginDate,
		u.ExcludeFromReports,
		u.IsFloating,
		substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1) AS 'DBName',
		cod.IsDemo,
		cod.IsLive
from	nowpow.contact C 
			LEFT JOIN nowpow.ContactPhoneNumber cpn
				on c.Id = cpn.ContactId

			LEFT JOIN nowpow.[user] u 
				LEFT JOIN CTE 
				ON u.Id = Cte.UserId
				ON u.ContactId = C.ID

			LEFT JOIN nowpow.OrganizationContact OC 
				LEFT JOIN nowpow.Organization O 
					
					LEFT JOIN nowpow.[site] s 
					on s.organizationid=o.id
						LEFT JOIN nowpow.CareOrganizationDatabaseSiteRelation codsr
						on s.Id = codsr.SiteId
						LEFT JOIN nowpow.CareOrganizationDatabase cod
						on codsr.CareOrganizationDatabaseId = cod.Id
					
					JOIN nowpow.[Location] loc
					on O.LocationId = loc.Id

					LEFT JOIN nowpow.organizationlicenserelation olr 
						LEFT JOIN nowpow.license l 
						on olr.licenseid=l.id
					on o.id = olr.organizationid

					LEFT JOIN nowpow.Organization ENT 
						LEFT JOIN nowpow.Organization ENT_NET 
						ON ENT.NetworkOrganizationid = ENT_NET.ID
					ON O.enterpriseorganizationid = ENT.ID

					LEFT JOIN nowpow.Organization NET 
					ON O.NetworkOrganizationid = NET.ID

				ON O.Id = OC.OrganizationId
			ON C.Id = OC.ContactId

			JOIN nowpow.UserRole ur
			    ON u.Id = ur.UserId
				and o.Id = ur.OrganizationId

			LEFT JOIN pristine.[Role] r
			    ON ur.RoleId = r.Id

order by 
	COALESCE(NET.Name, COALESCE(ENT_NET.Name, ' - ')) DESC, 
	ENT.Name, 
	O.IsNetwork DESC, 
	O.IsEnterprise DESC, 
	O.Name
