/*
*Calculate the users by activity categories
*
*Calculate the number of users of a given organization by their active levels.
*To Add: Unified way to set date range, Unified way to filter by Org
*Written By: Yong Jun Kim
*Date: 5/11/2018
*/

--Declare the variables for starting and end dates
--Test Value is for Allina
DECLARE @StartDate datetime = @@@DATEBEG
	, @EndDate   datetime = @@@DATEEND
;
--Create UsersByMonth and BlanksByMonth tables which will be used to calculate the 
WITH UsersByMonth AS (
	SELECT DATEADD(MONTH, DATEDIFF(MONTH, 0, C.CreatedDate), 0) As YearMonth
		, COUNT(*) AS Cnt
	FROM dbo.DIMCareCoordinator DIMCC
		LEFT JOIN nowpow.Contact C
		ON DIMCC.ContactId = C.Id
	WHERE (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		AND @@@ORG
	GROUP BY DATEADD(MONTH, DATEDIFF(MONTH, 0, C.CreatedDate), 0)
	)
	, BlanksByMonth AS
     (
	 SELECT DATEADD(DAY,0,@StartDate) as YearMonth, 0 AS Cnt
      UNION ALL
      SELECT DATEADD(month, 1, YearMonth), 0 AS Cnt
        FROM BlanksByMonth
		WHERE YearMonth <  @EndDate
     )

SELECT 0 AS Blank
	, Raw3.YearMonth
	,SUM(Raw3.Active) AS Active
	,SUM(Raw3.Moderate) AS Moderate
	,SUM(Raw3.Infrequent) AS Infrequent
	,SUM(Raw3.Inactive) AS Inactive
FROM (
	SELECT Raw2.YearMonth
		, 0 AS Active
		, 0 AS Moderate
		, 0 AS Infrequent
		, Max(Raw2.Cnt) AS Inactive
	FROM
	(
		SELECT Raw.YearMonth
			,Sum(Raw.Cnt) OVER (order by Raw.YearMonth rows unbounded preceding) as Cnt
		FROM (SELECT * FROM UsersByMonth 
			UNION ALL
			SELECT * FROM BlanksByMonth) AS Raw
	) Raw2
	WHERE Raw2.YearMonth >= @StartDate
	AND Raw2.YearMonth < @EndDate
	GROUP BY Raw2.YearMonth

	UNION ALL

	SELECT	UGROUP.YearMonth
		, SUM(CASE WHEN UGROUP.SuccessfulLogin >= 11.5 THEN 1 ELSE 0 END) AS Active
		, SUM(CASE WHEN UGROUP.SuccessfulLogin < 11.5 AND UGROUP.SuccessfulLogin >= 3.5 THEN 1 ELSE 0 END) AS Moderate
		, SUM(CASE WHEN UGROUP.SuccessfulLogin < 3.5 AND UGROUP.SuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS Infrequent
		, - SUM(CASE WHEN UGROUP.SuccessfulLogin >= 0.5 THEN 1 ELSE 0 END) AS Inactive

	FROM (
			SELECT Uact.UserId as ActivityUserId
			, DATEADD(MONTH, DATEDIFF(MONTH, 0, Uact.CreatedDate), 0) As YearMonth 
			, SUM(CASE WHEN Uact.Activity = 0 THEN 1
					ELSE 0
					END) AS SuccessfulLogin
			FROM [log].[ExternalUserActivity] as Uact
				JOIN nowpow.[User] U
				ON Uact.UserId = U.Id
					JOIN nowpow.Contact C
					ON U.ContactId = C.Id
						JOIN dbo.DIMCareCoordinator DIMCC
						ON C.Id = DIMCC.ContactId 
			WHERE (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
			AND @@@ORG
			AND Uact.[CreatedDate] >= @StartDate
			AND Uact.[CreatedDate] < @EndDate
			GROUP BY Uact.UserId
			, DATEADD(MONTH, DATEDIFF(MONTH, 0, Uact.CreatedDate), 0)
		) UGROUP
	GROUP BY UGROUP.YearMonth
	) Raw3
GROUP BY Raw3.YearMonth
HAVING 	SUM(Raw3.Active) 
	+ SUM(Raw3.Moderate) 
	+ SUM(Raw3.Infrequent)   > 0