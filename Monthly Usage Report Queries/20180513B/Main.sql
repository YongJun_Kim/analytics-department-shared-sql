/*
*Daily Report Query
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*v1.3: Days to Service and Days to Close added
*/
WITH cteRA AS (
		/* Generate table with Tracked Referral Appointments Attended */
		SELECT 
			RA.[ReferralId] as ReferralId
			, RA.[AppointmentStatusId]
			, RA.[AppointmentDate]
			, rank() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]
		FROM [nowpow].[ReferralAppointment] AS RA
		WHERE RA.AppointmentStatusId = 2 AND RA.IsDeleted <> 1
		)
	,
	cte AS (
	/*Select the earliest appointment using the rank*/
		SELECT 
			cteRA.[ReferralId] as ReferralId
			, cteRA.[AppointmentStatusId]
			, cteRA.AppointmentDate AS AppointmentDate
		FROM cteRA
		WHERE [r] = 1
	)

SELECT 
 CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
ELSE 'N/A'
END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS OrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR)
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgName
, Raw.*
, CASE
WHEN ISNULL(Raw.ServiceNudged,0) + ISNULL(Raw.TrackedReferrals,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) = 0 THEN NULL
ELSE ISNULL(Raw.ServiceNudged,0) + ISNULL(Raw.TrackedReferrals,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) 
END AS TotalSharedReferrals

FROM
(
(
/**eRx Created**/              
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'                
, 1 AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, Pre.PatientId

FROM log.Prescription AS Pre
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
SELECT PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date) AS 'Date'                
, NULL AS eRxCreated
, COUNT(*) AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
JOIN log.Prescription AS Pre
ON PSI.PrescriptionId = Pre.Id
LEFT JOIN log.PrescriptionService AS PreServ
ON Pre.Id = PreServ.PrescriptionId
WHERE [PSI].[InteractionTypeId] in ('3','4','11') 
	AND (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
GROUP BY PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date)         
, PSI.PatientId
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, COUNT(*) AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, Pre.PatientId

FROM log.Prescription AS Pre
	LEFT JOIN log.PrescriptionService AS PreServ
	ON Pre.Id = PreServ.PrescriptionId
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
GROUP BY Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date)         
, Pre.PatientId
)
UNION ALL
(
/**Tracked Referrals**/              
SELECT CC.Id AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, 1 AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, Ref.MakerPatientId AS PatientId

FROM nowpow.CareCoordinator CC
INNER JOIN nowpow.Contact Contact
ON CC.ContactId = Contact.Id
INNER JOIN nowpow.Referral AS Ref
ON Contact.Id = Ref.MakerContactId
)
UNION ALL
(
/**eRxShared**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS CCSiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, 1 AS eRxShared    
, NULL AS ServiceNudged   
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
WHERE [PSI].[InteractionTypeId] in ('3','4','11')    
)
UNION ALL
(
/**Service Nudged**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS CCSiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared   
, 1 AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*5&6 for Service Nudged*/        
WHERE [PSI].[InteractionTypeId] in ('5','6')    
)
UNION ALL
(
/**Login Activities**/            
SELECT CC.Id AS CareCoordinatorId           
,  NULL AS SiteId
, CAST(Uact.CreatedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared    
, NULL AS ServiceNudged   
, 1 AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, NULL AS PatientId

/*Join from CareCoordinator Table Since External User Activity Table has only User ID and no CareCordinator Id*/
/*This also excludes any untrackable login activities that do not have UserId*/
FROM nowpow.CareCoordinator CC             
 JOIN nowpow.Contact Contact            
  ON CC.ContactId = Contact.Id           
JOIN nowpow.[User] U             
ON Contact.Id = U.Contactid           
JOIN [log].[ExternalUserActivity] UAct            
ON Uact.userid = U.Id  
WHERE UAct.Activity = 0
)
UNION ALL
(
/**Searches**/            
SELECT lsc.CareCoordinatorId AS CareCoordinatorId           
,  lsc.SiteId AS SiteId
, CAST(lsc.CreatedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared  
, NULL AS ServiceNudged     
, NULL AS Logins
, 1 AS Searches
, NULL AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, lsc.PatientId AS PatientId

/*SearchType 1 = Service Search*/
FROM log.SearchCriteria lsc
WHERE lsc.SearchType = 1  
AND lsc.CareCoordinatorId IS NOT NULL
)
UNION ALL
(
/**Favorites**/            
SELECT CC.Id AS CareCoordinatorId           
, NULL AS SiteId
, CAST(SSF.DateAdded as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, 1 AS Favorites
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, NULL AS PatientId

FROM nowpow.CareCoordinator CC             
JOIN nowpow.Contact contact            
ON CC.ContactId = contact.Id           
JOIN nowpow.[User] U             
ON contact.Id = U.Contactid           
JOIN nowpow.SubscriberServiceFavorite AS SSF            
ON U.Id = SSF.SubscriberUserid     
)
UNION ALL
(
/**Screenings**/            
SELECT SR.CareCoordinatorId AS CareCoordinatorId           
, SR.SiteId AS SiteId
, CAST(SR.CompletedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, 1 AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, NULL AS PatientId

FROM nowpow.ScreeningResponse AS SR
WHERE SR.CompletedDate IS NOT NULL
)
UNION ALL
(
/**eRxLookups and ServicePrints**/            
SELECT CC.Id AS CareCoordinatorId           
, UAct.SiteId AS SiteId
, CAST(UAct.ActionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS ReferralsOnCreatedErx
, NULL AS TrackedReferrals
, NULL AS eRxShared     
, NULL AS ServiceNudged  
, NULL AS Logins
, NULL AS Searches
, NULL AS Favorites
, NULL   AS Screenings
, CASE WHEN UAct.Action like '%view%' THEN 1 END AS eRxLookups
, CASE WHEN UAct.Action like '%print%' THEN 1 END AS ServicePrints
, NULL AS DaysToService
, NULL AS DaysToClose
, NULL AS PatientId

FROM nowpow.CareCoordinator CC             
JOIN nowpow.Contact contact            
ON CC.ContactId = contact.Id           
JOIN nowpow.[User] U             
ON contact.Id = U.Contactid           
JOIN [log].UserActivity AS UAct           
ON U.Id = UAct.UserId
WHERE UAct.Action like '%view%'
OR UAct.Action like '%print%'
)
UNION ALL
(
SELECT  Raw.CareCoordinatorId
	, Raw.SiteId
	, Raw.Date             
	, NULL AS eRxCreated
	, NULL AS ReferralsOnSharedErx
	, NULL AS ReferralsOnCreatedErx
	, NULL AS TrackedReferrals
	, NULL AS eRxShared     
	, NULL AS ServiceNudged  
	, NULL AS Logins
	, NULL AS Searches
	, NULL AS Favorites
	, NULL AS Screenings
	, NULL AS eRxLookups
	, NULL AS ServicePrints
	, CASE
		WHEN Raw.DaysToService < 0 THEN NULL
		ELSE Raw.DaysToService
	END AS DaysToService
	, CASE
		WHEN Raw.DaysToClose < 0 THEN NULL
		ELSE Raw.DaysToClose
	END AS DaysToClose
	, Raw.PatientId
FROM
	(
		SELECT CC.Id AS CareCoordinatorId
		, Ref.MakerSiteId AS SiteId
		, CAST(Ref.CreatedDate as date) AS 'Date'                
		/*Calculate the days to the contacted and closed successful date*/
		, DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) as DaysToClose
		/*Calculate the days to service as specified in the introduction*/
		, CASE
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) IS NULL THEN NULL
			WHEN DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) <= DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) THEN DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) < DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
		END AS DaysToService
		, Ref.MakerPatientId AS PatientId
	FROM nowpow.CareCoordinator CC
		INNER JOIN nowpow.Contact Contact
		ON CC.ContactId = Contact.Id
			INNER JOIN nowpow.Referral AS Ref
			ON Contact.Id = Ref.MakerContactId	
	/*Table Joins for filtering referrals begin*/
				LEFT JOIN cte
				ON Ref.Id = cte.ReferralId
	) AS Raw
	WHERE (Raw.DaysToService IS NOT NULL OR Raw.DaysToClose IS NOT NULL)
)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
LEFT JOIN DIMPatient AS DimPatient
ON Raw.PatientId = DimPatient.PatientId

WHERE [raw].[Date] >= @@@DATEBEG
AND [raw].[Date] < @@@DATEEND
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
/*Exclude any activity related to the test patient*/
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
AND @@@ORG