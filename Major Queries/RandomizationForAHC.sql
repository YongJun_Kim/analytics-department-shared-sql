/***************DAILY REPORT ON RANDOMIZATION COUNT*************************/
/************************Jan 23, 2018 - Feb 23, 2018************************/
/******************************Allina Pilot*********************************/

SELECT psi.[Id] as 'PSIID'
      ,psi.[CareCoordinatorId]
      ,psi.[SiteId]
	  ,org.[Name] as 'Org Name'
	  ,p.[Id] as 'PatientId'
	  ,ps.CustomField4 as 'Group'

FROM [nowpow].[PatientSiteInteraction] psi
	LEFT JOIN [nowpow].[Patient] p
	ON psi.PatientId = p.Id
    LEFT JOIN nowpow.PatientSupplemental ps
	ON p.Id = ps.PatientId
	LEFT JOIN nowpow.CareCoordinator cc
	ON psi.CareCoordinatorId = cc.Id
	/*Sites can technicaly be NULL on [nowpow].[PatientSiteInteraction] table.
	Therefore, it would be better to left join them.*/
	LEFT JOIN nowpow.[Site] s
	ON cc.SiteId = s.Id
	LEFT JOIN nowpow.Organization org
	ON s.OrganizationId = org.Id
	LEFT JOIN dbo.DIMCareCoordinator AS DIMCareCoordinator
	ON PSI.[CareCoordinatorId] = DIMCareCoordinator.CareCoordinatorId

  WHERE psi.interactiontypeid = 21
  AND (P.LASTNAME not LIKE '%test%' OR P.LASTNAME IS NULL)  --"P.LASTNAME not LIKE '%test%'" would actually filter out any P.LASTNAME that is null, so it's important to  include "P.LASTNAME IS NULL" here.
  AND (DIMCareCoordinator.[ExcludeFromReports] <> 1 OR DIMCareCoordinator.[ExcludeFromReports] IS NULL)
  AND PSI.InteractionDate > '01/22/2018'
  AND PSI.InteractionDate < '02/24/2018'
