/*
	Script 6
	DROP Custom tables
	CREATE Custom tables
*/

/*Check if there is an existing LocationDistance table, and delete if it exists.*/				
IF EXISTS(select top 1 1 from sys.tables where name = 'LocationDistance')				
DROP TABLE dbo.LocationDistance;
	
CREATE TABLE LocationDistance
(
LocationId bigint
, LocationLatitude float
, LocationLongitude float
)

INSERT INTO LocationDistance
SELECT Loc.Id AS LocationId
, Loc.Latitude AS LocationLatitude
, Loc.Longitude AS LocationLongitude
FROM nowpow.[Location] Loc
WHERE Loc.Latitude IS NOT NULL
AND Loc.Longitude IS NOT NULL
;

/*Check if there is an existing ServLoc table, and delete if it exists.*/				
IF EXISTS(select top 1 1 from sys.tables where name = 'ServLoc')				
DROP TABLE dbo.ServLoc;				
/*Create ServLoc*/				
CREATE TABLE dbo.ServLoc				
(Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY				
, ServId BIGINT NOT NULL				
, ServiceTypeId BIGINT NOT NULL				
, OrgId BIGINT NOT NULL				
, Latitude FLOAT NOT NULL				
, Longitude FLOAT NOT NULL				
, GeographyColumn GEOGRAPHY NOT NULL);				
				
/*Create the spatial index for the ServLoc for better spatial join time */				
CREATE SPATIAL INDEX IX_ServLoc_SpatialLocation				
ON dbo.ServLoc(GeographyColumn)				
USING GEOGRAPHY_GRID				
WITH (DATA_COMPRESSION = PAGE);				
				
/*Fill the ServLoc Table*/        				
INSERT INTO dbo.ServLoc				
SELECT Serv.Id AS ServId                                        				
, Serv.TypeId AS ServiceTypeId				
, Org.Id AS OrgId				
, LD.LocationLatitude AS Latitude				
, LD.LocationLongitude AS Longitude				
, geography::STGeomFromText('POINT('+convert(varchar(20),LD.LocationLongitude)+' '+convert(varchar(20), LD.LocationLatitude)+')',4326)				
				
FROM [nowpow].[Service] AS Serv				
JOIN [nowpow].[Organization] AS Org				
ON [Serv].[OrganizationId] = [Org].[Id]				
JOIN [dbo].[LocationDistance] AS LD				
ON [Org].[LocationId] = [LD].[LocationId];				

				
/*Check if there is an existing ErxServLoc table, and delete if it exists.*/				
IF EXISTS(select top 1 1 from sys.tables where name = 'ErxServLoc')				
DROP TABLE dbo.ErxServLoc;				
/*Create ErxServLoc*/				
CREATE TABLE dbo.ErxServLoc				
(Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY				
, PrescriptionId BIGINT NOT NULL				
, ServiceTypeId BIGINT NOT NULL				
, CreatedDate DATETIME2(7) NOT NULL				
, CcOrgId BIGINT NOT NULL				
, ContactId BIGINT NOT NULL				
, UserName nvarchar(100)				
, Latitude FLOAT NOT NULL				
, Longitude FLOAT NOT NULL				
, GeographyColumn GEOGRAPHY NOT NULL);				
				
/*Create the spatial index for the ErxServLoc for better spatial join time */				
CREATE SPATIAL INDEX IX_ErxServLoc_SpatialLocation				
ON dbo.ErxServLoc(GeographyColumn)				
USING GEOGRAPHY_GRID				
WITH (DATA_COMPRESSION = PAGE);				
				
/*Fill the ErxServLoc Table*/				
INSERT INTO dbo.ErxServLoc				
SELECT PreServType.PrescriptionId                        				
, PreServType.ServiceTypeId                 				
, Pre.CreatedDate				
, Org.Id AS CcOrgId				
, Cont.Id AS ContactId				
, U.UserName				
, PPL.Latitude                				
, PPL.Longitude        				
, geography::STGeomFromText('POINT('+convert(varchar(20),PPL.Longitude)+' '+convert(varchar(20), PPL.Latitude)+')',4326)				

FROM [log].[Prescription] AS Pre				
JOIN (				
		SELECT DISTINCT PreServ.PrescriptionId                        		
		, Serv.TypeId AS ServiceTypeId                        		
		FROM [log].[Prescription] AS Pre		
			JOIN [log].[PrescriptionService] AS PreServ	
			ON [Pre].[Id] = [PreServ].[PrescriptionId]	
				JOIN [nowpow].[Service] AS Serv
				ON [PreServ].[ServiceId] = [Serv].[Id]
	) AS PreServType			
	ON Pre.Id = PreServType.PrescriptionId			
		JOIN [log].[PrescriptionPatientLocation] AS PPL		
		ON [Pre].[Id] = [PPL].[PrescriptionId]		
	JOIN nowpow.CareCoordinator AS CC			
	ON Pre.CareCoordinatorId = CC.Id                                        			
		LEFT JOIN [nowpow].[site] AS S                                		
		ON CC.SiteId = S.Id                                		
			JOIN nowpow.Organization Org                        	
			ON S.OrganizationId = Org.Id                        	
		JOIN nowpow.Contact Cont                        		
		ON CC.ContactId = Cont.Id                        		
		JOIN nowpow.[User] U                		
		ON Cont.Id = U.Contactid; 		

/*Create OrgLoc*/				
DROP TABLE IF EXISTS dbo.OrgLoc
CREATE TABLE dbo.OrgLoc
(Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY				
, OrgId BIGINT NOT NULL				
, Latitude FLOAT NOT NULL				
, Longitude FLOAT NOT NULL				
, GeographyColumn GEOGRAPHY NOT NULL);				
				
/*Create the spatial index for the OrgLoc for better spatial join time */				

CREATE SPATIAL INDEX IX_OrgLoc_SpatialLocation				
ON dbo.OrgLoc(GeographyColumn)				
USING GEOGRAPHY_GRID				
WITH (DATA_COMPRESSION = PAGE);				
				
/*Fill the OrgLoc Table*/        				
INSERT INTO dbo.OrgLoc				
SELECT Org.Id AS OrgId				
, LD.LocationLatitude AS Latitude				
, LD.LocationLongitude AS Longitude				
, geography::STGeomFromText('POINT('+convert(varchar(20),LD.LocationLongitude)+' '+convert(varchar(20), LD.LocationLatitude)+')',4326)				

FROM  [nowpow].[Organization] AS Org						
JOIN [dbo].[LocationDistance] AS LD				
ON [Org].[LocationId] = [LD].[LocationId];	


DROP TABLE IF EXISTS dbo.ValidErx                
CREATE TABLE dbo.ValidErx
(
       ErxId BIGINT NOT NULL
       , ErxSiteOrgId BIGINT NULL
       , ErxSiteEntOrgId BIGINT NULL
       , ErxSiteNetOrgId BIGINT NULL
       , CCOrgId BIGINT NULL
       , CCEntOrgId BIGINT NULL
       , CCNetOrgId BIGINT NULL
       , SearchType TINYINT NULL
)
;

INSERT INTO dbo.ValidErx
SELECT Pre.Id AS ErxId
       , ErxSiteOrg.Id 
       , ErxSiteEntOrg.Id 
       --Since network orgs don't have enterprise Id associated with them,
       -- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
       , CASE 
              WHEN ErxSiteOrg.IsNetwork = '1' THEN ErxSiteOrg.Id
              ELSE ErxSiteNetOrg.Id 
       END
       , Org.Id 
       , EntOrg.Id 
       , EntOrg.NetworkOrganizationId 
       , SC.SearchType

FROM log.Prescription AS Pre
       LEFT JOIN [nowpow].[Site] AS ErxSite
       ON [Pre].[SiteId] = [ErxSite].[Id]
              LEFT JOIN [nowpow].[Organization] AS ErxSiteOrg
              ON [ErxSite].[OrganizationId] = [ErxSiteOrg].[Id]
                     LEFT JOIN [nowpow].[Organization] AS ErxSiteEntOrg
                     ON [ErxSiteOrg].[EnterpriseOrganizationId] = [ErxSiteEntOrg].[Id]
                           LEFT JOIN [nowpow].[Organization] AS ErxSiteNetOrg
                           ON [ErxSiteEntOrg].[NetworkOrganizationId] = [ErxSiteNetOrg].[Id]
       LEFT JOIN nowpow.CareCoordinator AS CC
       ON [Pre].[CareCoordinatorId] = [CC].[ID]
              LEFT JOIN [nowpow].[site] AS S           
              ON [CC].[SiteId] = [S].[Id] 
                     LEFT JOIN [nowpow].[Organization] AS Org        
                     ON [S].[OrganizationId] = [Org].[Id]
                           LEFT JOIN [nowpow].[Organization] AS EntOrg
                           ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
       LEFT JOIN (SELECT PrescriptionId, SearchType FROM [Log].[SearchCriteria] WHERE SearchType = 5) AS SC
       ON [Pre].[Id] = [SC].[PrescriptionId]