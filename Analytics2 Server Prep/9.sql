CREATE VIEW DIMPatient
AS
SELECT [PAT].[Id] AS PatientID	
	, [GEN].[Gender] AS PatientGender
	, case	
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) <= 2 then '0 to 2'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 3 and 12 then '3 to 12'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 13 and 18 then '13 to 18'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 19 and 29 then '19 to 29'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 30 and 64 then '30 to 64'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) > 64 then '65+'
		else 'N/A'
		END as PatientAgeGroup
	, ISNULL([RefR].[RepName], 'Unknown') AS PatientRace /*Report Name*/	
	, ISNULL([RefE].[RepName], 'Unknown')	AS PatientEthnicity /*Report Name*/	
	, [PAT].[InsurancePlans] AS PatientInsurancePlans
	, ISNULL([INTY].[Name], 'Unknown') AS PatientInsuranceTypes
	, ISNULL([RefL].[RepName], 'Unknown') As PatientPreferredLanguage	
	, CASE
	WHEN [PAT].[LASTNAME] LIKE '%Zzztest%' THEN 'True'
	ELSE 'False'
	END AS TestPatient
	, [PAT].[Race] AS CustDBPatientRace 
	, [PAT].[Ethnicity]	AS CustDBPatientEthnicity
	, [SL].[Name] As CustDBPatientPreferredLanguage	

FROM [nowpow].[Patient] AS PAT
	LEFT JOIN [dbo].[RefRace] AS RefR
	ON [PAT].[Race] = [RefR].[Name]

	LEFT JOIN [dbo].[RefEthnicity] AS RefE
	ON [PAT].[Ethnicity] = [RefE].[Name]

	LEFT JOIN [pristine].[Gender] as GEN
	ON [PAT].[GenderId] = [GEN].[Id]

	LEFT JOIN [pristine].[SupportedLanguage] as SL
	ON [PAT].[PreferredLanguageId] = [SL].[Id]
		LEFT JOIN [dbo].[RefLanguage] AS RefL
		ON SL.[NAME] = RefL.[Name]

	LEFT JOIN [nowpow].[PatientInsuranceTypeRelation] as INPRL	
	ON [PAT].[Id] = [INPRL].[PatientId]	
		LEFT JOIN [pristine].[InsuranceType] as INTY
		ON [INPRL].[InsuranceTypeId] = [INTY].[ID]