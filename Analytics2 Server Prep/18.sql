CREATE VIEW DIMUsageSearch			
		AS			
					SELECT U.Id as UserId            
					 , (Cont.LastName + ', ' + Cont.FirstName) AS UserName            
					 , CAST(lsc.CreatedDate as date) AS 'Date'            
					 , COUNT(lsc.Id) AS Searches            
					 FROM log.SearchCriteria lsc            
					  JOIN nowpow.CareCoordinator cc           
					   ON lsc.CareCoordinatorId = cc.Id          
					  LEFT JOIN nowpow.Contact Cont           
					   ON cc.ContactId = Cont.Id          
					  LEFT JOIN nowpow.[User] U           
					   ON CC.ContactId = u.ContactId          
					 WHERE lsc.SearchType = 1            
					 GROUP BY U.Id            
					 , (cont.LastName + ', ' + Cont.FirstName)            
					 , CAST(lsc.CreatedDate as date)            
					 HAVING U.Id IS NOT NULL
					