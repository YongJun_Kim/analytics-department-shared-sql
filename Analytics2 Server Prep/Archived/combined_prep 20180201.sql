CREATE EXTERNAL DATA SOURCE Automation
WITH
(
	TYPE=RDBMS,
	LOCATION='nowpow-sql-analytics2.database.windows.net',
	DATABASE_NAME='Analytics_Automation',
	CREDENTIAL= npAnalytics
);

/*The following queries makes external tables to be available to the customer databases.*/

/*This query makes External User Activity Table available to the customer DBs as an external table*/
Go
IF EXISTS(select top 1 1 from sys.external_tables where name = 'ExternalUserActivityType')				
DROP EXTERNAL TABLE [pristine].[ExternalUserActivityType];	

GO
CREATE EXTERNAL TABLE [pristine].[ExternalUserActivityType](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
)	
WITH (	
  DATA_SOURCE=Prod	
);

/*Create Need table as an external table*/
GO
IF EXISTS(select top 1 1 from sys.external_tables where name = 'Need')				
DROP EXTERNAL TABLE [nowpow].[Need];	

GO
CREATE EXTERNAL TABLE [nowpow].[Need](
	[Id] [bigint]  NOT NULL,
	[SiteId] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
WITH (	
  DATA_SOURCE=Prod	
);	

/*Create External tables of Reference Reports from Analytics Automation*/
GO
IF EXISTS(select top 1 1 from sys.external_tables where name = 'RefEthnicity')				
DROP EXTERNAL TABLE [dbo].[RefEthnicity];	

GO
CREATE EXTERNAL TABLE [dbo].[RefEthnicity](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);	
GO
IF EXISTS(select top 1 1 from sys.external_tables where name = 'RefLanguage')				
DROP EXTERNAL TABLE [dbo].[RefLanguage];	

GO
CREATE EXTERNAL TABLE [dbo].[RefLanguage](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);	
GO
IF EXISTS(select top 1 1 from sys.external_tables where name = 'RefRace')				
DROP EXTERNAL TABLE [dbo].[RefRace];	

GO
CREATE EXTERNAL TABLE [dbo].[RefRace](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);	



/*The following Queries create Dimension Tables and Views used for monthly and other reports. */
GO
DROP VIEW IF EXISTS DIMContact;

GO
CREATE VIEW DIMContact
AS

SELECT Contact.Id AS ContactId
	, U.Id AS UserId
	, Contact.FirstName AS ContactFirstName
	, Contact.LastName AS ContactLastName
	, U.UserName AS UserName
	, U.ExcludeFromReports  --Added 1/4/2018

FROM nowpow.Contact contact
	LEFT JOIN nowpow.[User] U
	ON contact.Id = U.Contactid;

GO
DROP VIEW IF EXISTS DIMOrganization;

GO
CREATE VIEW DIMOrganization
AS
SELECT Org.Id AS OrgId
	, Org.Name AS OrgName
	, EntOrg.Id AS EnterpriseOrgId
	, EntOrg.Name AS EnterpriseOrgName
	--Since network orgs don't have enterprise Id associated with them,
	-- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS NetworkOrgId
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Name
		ELSE NetOrg.Name 
	END AS NetworkOrgName
	, OrgSubtype.Id AS OrgSubtypeId
	, OrgSubtype.Name AS OrgSubtypeName
	, OrgType.Id AS OrgTypeId
	, OrgType.Name AS OrgTypeName
	, Org.LocationId AS OrgLocationId

FROM [nowpow].[Organization] AS Org
	LEFT JOIN [nowpow].[Organization] AS EntOrg
	ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
		LEFT JOIN [nowpow].[Organization] AS NetOrg
		ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]
	LEFT JOIN [pristine].[OrganizationSubtype] AS OrgSubtype
	ON [Org].[OrganizationSubtypeId] = [OrgSubtype].[Id]
		LEFT JOIN [pristine].[OrganizationType] AS OrgType
		ON [OrgSubtype].[OrganizationTypeId] = [OrgType].[Id]

GO
DROP VIEW IF EXISTS DIMPatient;

GO
CREATE VIEW DIMPatient
AS
SELECT [PAT].[Id] AS PatientID	
	, [GEN].[Gender] AS PatientGender
	, case	
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) <= 2 then '0 to 2'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 3 and 12 then '3 to 12'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 13 and 18 then '13 to 18'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 19 and 29 then '19 to 29'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) between 30 and 64 then '30 to 64'
		when convert(int, DATEDIFF(d , [PAT].[DateOfBirth], getdate())/365.25) > 64 then '65+'
		else 'N/A'
		END as PatientAgeGroup
	, ISNULL([RefR].[RepName], 'Unknown') AS PatientRace /*Report Name*/	
	, ISNULL([RefE].[RepName], 'Unknown')	AS PatientEthnicity /*Report Name*/	
	, [PAT].[InsurancePlans] AS PatientInsurancePlans
	, ISNULL([INTY].[Name], 'Unknown') AS PatientInsuranceTypes
	, ISNULL([RefL].[RepName], 'Unknown') As PatientPreferredLanguage	
	, CASE
	WHEN [PAT].[LASTNAME] LIKE '%Zzztest%' THEN 'True'
	ELSE 'False'
	END AS TestPatient
	, [PAT].[Race] AS CustDBPatientRace 
	, [PAT].[Ethnicity]	AS CustDBPatientEthnicity
	, [SL].[Name] As CustDBPatientPreferredLanguage	

FROM [nowpow].[Patient] AS PAT
	LEFT JOIN [dbo].[RefRace] AS RefR
	ON [PAT].[Race] = [RefR].[Name]

	LEFT JOIN [dbo].[RefEthnicity] AS RefE
	ON [PAT].[Ethnicity] = [RefE].[Name]

	LEFT JOIN [pristine].[Gender] as GEN
	ON [PAT].[GenderId] = [GEN].[Id]

	LEFT JOIN [pristine].[SupportedLanguage] as SL
	ON [PAT].[PreferredLanguageId] = [SL].[Id]
		LEFT JOIN [dbo].[RefLanguage] AS RefL
		ON SL.[NAME] = RefL.[Name]

	LEFT JOIN [nowpow].[PatientInsuranceTypeRelation] as INPRL	
	ON [PAT].[Id] = [INPRL].[PatientId]	
		LEFT JOIN [pristine].[InsuranceType] as INTY
		ON [INPRL].[InsuranceTypeId] = [INTY].[ID]

GO
DROP VIEW IF EXISTS DIMSite;

GO
CREATE VIEW DIMSite
AS
SELECT Site.Id AS SiteId
	, Org.Id AS OrgId
	, Org.Name AS OrgName
	, EntOrg.Id AS EnterpriseOrgId
	, EntOrg.Name AS EnterpriseOrgName
	--Since network orgs don't have enterprise Id associated with them,
	-- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
	-- Revised 1/4/2018
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS NetworkOrgId
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.[Name]
		ELSE NetOrg.[Name]
	END AS NetworkOrgName
	, OrgSubtype.Id AS OrgSubtypeId
	, OrgSubtype.[Name] AS OrgSubtypeName
	, OrgType.Id AS OrgTypeId
	, OrgType.[Name] AS OrgTypeName

FROM [nowpow].[Site] AS Site
	LEFT JOIN [nowpow].[Organization] AS Org
	ON [Site].[OrganizationId] = [Org].[Id]
		LEFT JOIN [nowpow].[Organization] AS EntOrg
		ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
			LEFT JOIN [nowpow].[Organization] AS NetOrg
			ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]
		LEFT JOIN [pristine].[OrganizationSubtype] AS OrgSubtype
		ON [Org].[OrganizationSubtypeId] = [OrgSubtype].[Id]
			LEFT JOIN [pristine].[OrganizationType] AS OrgType
			ON [OrgSubtype].[OrganizationTypeId] = [OrgType].[Id]

GO
DROP VIEW IF EXISTS DIMCareCoordinator;

GO
CREATE VIEW DIMCareCoordinator
AS

SELECT CC.Id AS CareCoordinatorId
	, CC.ContactId AS ContactId
	, U.Id AS UserId
	, U.UserName AS UserName
	, Contact.FirstName
	, Contact.LastName
	, CONCAT (contact.LastName, ', ', contact.FirstName) AS FullName
	, [S].Id AS SiteId
	, Org.Id AS OrgId
	, Org.Name AS OrgName
	, EntOrg.Id AS EnterpriseOrgId
	, EntOrg.Name AS EnterpriseOrgName
	--Since network orgs don't have enterprise Id associated with them,
	-- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
	-- Revised 1/4/2018
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS NetworkOrgId
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.[Name]
		ELSE NetOrg.[Name]
	END AS NetworkOrgName
	, U.IsActive
	, U.ExcludeFromReports

FROM nowpow.CareCoordinator AS CC
		LEFT JOIN nowpow.Contact AS contact
		ON [CC].[ContactId] = [contact].[Id]
				LEFT JOIN nowpow.[User] U
				ON [contact].[Id] = [U].[Contactid]  
	LEFT JOIN [nowpow].[site] AS S		
	ON [CC].[SiteId] = [S].[Id]	
		LEFT JOIN [nowpow].[Organization] AS Org		
		ON [S].[OrganizationId] = [Org].[Id]
			LEFT JOIN [nowpow].[Organization] AS EntOrg
			ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS NetOrg
				ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]
			
/*
*The query below will make special reference tables that will be used for monthly reports.
*Please minimize the use of these reference tables, and use the above dimension tables.
*These tables are specialized for a specific purpose.
*/
/*Table similar to DIMCareCoordinator.  Please use DimCareCoordinator instead.*/
GO 
DROP VIEW IF EXISTS PreCCId;

GO
CREATE VIEW PreCCId
AS 

SELECT CC.Id AS CCId
	, CC.ContactId AS CCContactId
	, U.Id AS CCUserId
	, U.UserName AS CCUserName
	, [S].Id AS CCSiteId
	, Org.Id AS CCOrgId
	, EntOrg.Id AS CCEntOrgId
	, CASE  --Added 1/4/2018
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS CCNetOrgId
	, U.ExcludeFromReports

FROM nowpow.CareCoordinator AS CC
		LEFT JOIN nowpow.Contact AS contact
		ON [CC].[ContactId] = [contact].[Id]
				LEFT JOIN nowpow.[User] U
				ON [contact].[Id] = [U].[Contactid]  
	LEFT JOIN [nowpow].[site] AS S		
	ON [CC].[SiteId] = [S].[Id]	
		LEFT JOIN [nowpow].[Organization] AS Org		
		ON [S].[OrganizationId] = [Org].[Id]
			LEFT JOIN [nowpow].[Organization] AS EntOrg
			ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS NetOrg
				ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]

/*Table similar to DIMOrganization.  Only contains organizations that have eRx on the DB you run on.  Only use when you deal with large quantity of Prescriptions(eRx).*/
GO 
DROP VIEW IF EXISTS PreOrgId;

GO
CREATE VIEW PreOrgId
AS

SELECT Pre.Id AS ErxId
	, ErxSiteOrg.Id AS ErxSiteOrgId
	, ErxSiteEntOrg.Id AS ErxSiteEntOrgId
	, CASE  --Added 1/4/2018
		WHEN ErxSiteOrg.IsNetwork = '1' THEN ErxSiteOrg.Id
		ELSE ErxSiteNetOrg.Id 
	END AS ErxSiteNetOrgId
	, SC.SearchType AS SearchType

FROM log.Prescription AS Pre
	LEFT JOIN [nowpow].[Site] AS ErxSite
	ON [Pre].[SiteId] = [ErxSite].[Id]
		LEFT JOIN [nowpow].[Organization] AS ErxSiteOrg
		ON [ErxSite].[OrganizationId] = [ErxSiteOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS ErxSiteEntOrg
				ON [ErxSiteOrg].[EnterpriseOrganizationId] = [ErxSiteEntOrg].[Id]
					LEFT JOIN [nowpow].[Organization] AS ErxSiteNetOrg
					ON [ErxSiteEntOrg].[NetworkOrganizationId] = [ErxSiteNetOrg].[Id]
	LEFT JOIN nowpow.CareCoordinator AS CC
	ON [Pre].[CareCoordinatorId] = [CC].[ID]
	LEFT JOIN (SELECT PrescriptionId, SearchType FROM [Log].[SearchCriteria] WHERE SearchType = 5) AS SC
	ON [Pre].[Id] = [SC].[PrescriptionId]


/*Table similar to DIMOrganization.  Only contains organizations that have screenings on the DB you run on.  Only use when you deal with large quantity of Screenings.*/
GO	
DROP VIEW IF EXISTS ScreeningOrgId;

GO
CREATE VIEW ScreeningOrgId
AS

SELECT SR.Id AS ScreeningResponseId
	, SRSiteOrg.Id AS ScreeningResponseSiteOrgId
	, SRSiteEntOrg.Id AS ScreeningResponseSiteEntOrgId
	, SRSiteEntOrg.NetworkOrganizationId AS ScreeningResponseSiteNetOrgId

FROM nowpow.ScreeningResponse AS SR
	LEFT JOIN [nowpow].[Site] AS SRSite
	ON [SR].[SiteId] = SRSite.[Id]
		LEFT JOIN [nowpow].[Organization] AS SRSiteOrg
		ON SRSite.[OrganizationId] = SRSiteOrg.[Id]
				LEFT JOIN [nowpow].[Organization] AS SRSiteEntOrg
				ON SRSiteOrg.[EnterpriseOrganizationId] = SRSiteEntOrg.[Id]
	LEFT JOIN nowpow.CareCoordinator AS CC
	ON [SR].[CareCoordinatorId] = [CC].[ID]

GO	


/*This query makes Screening Template Table available to the customer DBs as an external table*/
IF EXISTS(select top 1 1 from sys.external_tables where name = 'ScreeningTemplate')				
DROP EXTERNAL TABLE [nowpow].[ScreeningTemplate];	

GO
CREATE EXTERNAL TABLE [nowpow].[ScreeningTemplate](	
	[Id] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[DisplayOrder] [tinyint] NULL,
	[SiteId] [bigint] NULL,
	[Workflow] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
)	
WITH (	
  DATA_SOURCE=Prod	
)	

/*
*The queries below are reference tables for Service Gap reports.
*They create reference tables to calculate distances of the services on eRx and available services that are near them
*/
/*Check if there is an existing ServLoc table, and delete if it exists.*/				
GO
IF EXISTS(select top 1 1 from sys.tables where name = 'OrgLoc')				
DROP TABLE dbo.OrgLoc;				
				
/*Create ServLoc*/				
GO
CREATE TABLE dbo.OrgLoc
(Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY				
, OrgId BIGINT NOT NULL				
, Latitude FLOAT NOT NULL				
, Longitude FLOAT NOT NULL				
, GeographyColumn GEOGRAPHY NOT NULL);				
				
/*Create the spatial index for the ServLoc for better spatial join time */				
GO
CREATE SPATIAL INDEX IX_OrgLoc_SpatialLocation				
ON dbo.OrgLoc(GeographyColumn)				
USING GEOGRAPHY_GRID				
WITH (DATA_COMPRESSION = PAGE);				
				
/*Fill the ServLoc Table*/        				
GO
INSERT INTO dbo.OrgLoc				
SELECT Org.Id AS OrgId				
, LD.LocationLatitude AS Latitude				
, LD.LocationLongitude AS Longitude				
, geography::STGeomFromText('POINT('+convert(varchar(20),LD.LocationLongitude)+' '+convert(varchar(20), LD.LocationLatitude)+')',4326)				

FROM  [nowpow].[Organization] AS Org						
JOIN [dbo].[LocationDistance] AS LD				
ON [Org].[LocationId] = [LD].[LocationId];	

IF EXISTS(select top 1 1 from sys.external_tables where name = 'UserActivity')				
DROP EXTERNAL TABLE [log].[UserActivity];	

GO
CREATE EXTERNAL TABLE [log].[UserActivity](
	[Id] [bigint] NOT NULL,
	[Action] [nvarchar](100) NULL,
	[UserId] [bigint] NOT NULL,
	[ContactId] [bigint] NULL,
	[ContactOrganizationId] [bigint] NULL,
	[SiteId] [bigint] NULL,
	[ReferenceTableName] [nvarchar](100) NULL,
	[ReferenceId] [bigint] NULL,
	[ActionDate] [datetime2](7) NOT NULL,
)	
WITH (	
	DATA_SOURCE=Prod
);

/*Similar to PreOrgId*/
GO
/*Check if there is an existing ValidErx table, and delete if it exists.*/                      
IF EXISTS (select top 1 1 from sys.tables where name = 'ValidErx')                        
DROP TABLE dbo.ValidErx;                        
GO                         
CREATE TABLE dbo.ValidErx
(
       ErxId BIGINT NOT NULL
       , ErxSiteOrgId BIGINT NULL
       , ErxSiteEntOrgId BIGINT NULL
       , ErxSiteNetOrgId BIGINT NULL
       , CCOrgId BIGINT NULL
       , CCEntOrgId BIGINT NULL
       , CCNetOrgId BIGINT NULL
       , SearchType TINYINT NULL
)
;
GO
INSERT INTO dbo.ValidErx
SELECT Pre.Id AS ErxId
       , ErxSiteOrg.Id 
       , ErxSiteEntOrg.Id 
       --Since network orgs don't have enterprise Id associated with them,
       -- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
       , CASE 
              WHEN ErxSiteOrg.IsNetwork = '1' THEN ErxSiteOrg.Id
              ELSE ErxSiteNetOrg.Id 
       END
       , Org.Id 
       , EntOrg.Id 
       , EntOrg.NetworkOrganizationId 
       , SC.SearchType

FROM log.Prescription AS Pre
       LEFT JOIN [nowpow].[Site] AS ErxSite
       ON [Pre].[SiteId] = [ErxSite].[Id]
              LEFT JOIN [nowpow].[Organization] AS ErxSiteOrg
              ON [ErxSite].[OrganizationId] = [ErxSiteOrg].[Id]
                     LEFT JOIN [nowpow].[Organization] AS ErxSiteEntOrg
                     ON [ErxSiteOrg].[EnterpriseOrganizationId] = [ErxSiteEntOrg].[Id]
                           LEFT JOIN [nowpow].[Organization] AS ErxSiteNetOrg
                           ON [ErxSiteEntOrg].[NetworkOrganizationId] = [ErxSiteNetOrg].[Id]
       LEFT JOIN nowpow.CareCoordinator AS CC
       ON [Pre].[CareCoordinatorId] = [CC].[ID]
              LEFT JOIN [nowpow].[site] AS S           
              ON [CC].[SiteId] = [S].[Id] 
                     LEFT JOIN [nowpow].[Organization] AS Org        
                     ON [S].[OrganizationId] = [Org].[Id]
                           LEFT JOIN [nowpow].[Organization] AS EntOrg
                           ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
       LEFT JOIN (SELECT PrescriptionId, SearchType FROM [Log].[SearchCriteria] WHERE SearchType = 5) AS SC
       ON [Pre].[Id] = [SC].[PrescriptionId]

/*
*Monthly Usage Reference views
*Please do not use them.  They will be depreciated in near future.
*
*/
GO					
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'DIMUsagePrescription' and type = 'v')					
DROP VIEW DIMUsagePrescription;					
GO					
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'DIMUsageLoginfo' and type = 'v')					
DROP VIEW DIMUsageLoginfo;					
GO					
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'DIMUsagePSIInfo' and type = 'v')					
DROP VIEW DIMUsagePSIInfo;					
GO					
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'DIMUsageSearch' and type = 'v')					
DROP VIEW DIMUsageSearch;					
GO					
IF EXISTS(SELECT 1 FROM sys.views WHERE NAME = 'DIMUsageFavorite' and type = 'v')					
DROP VIEW DIMUsageFavorite;					
 					
		GO 			
		CREATE VIEW DIMUsagePrescription			
		AS			
					
					/**nowrx eRxs generated by user by day**/            
					 SELECT             
					  U.ID AS UserId           
					  , (Cont.LastName + ', ' + Cont.FirstName) AS UserName            
					  , cast (erx.CreatedDate as date) AS 'Date'           
					   , count(distinct erx.Id) AS eRxCreated            
					   , count(distinct (case when PSID.Downloaded > 0 then erx.Id end)) AS eRxDownloaded            
					   , count(distinct (case when PSIN.Nudged > 0 then erx.Id end)) AS eRxNudged              
					
					 FROM [log].Prescription AS erx            
					LEFT JOIN nowpow.CareCoordinator AS CC           
					ON erx.CareCoordinatorId = CC.Id          
					LEFT JOIN nowpow.Contact AS Cont           
					ON CC.ContactId = Cont.Id          
					LEFT JOIN nowpow.[User] AS U           
					ON CC.ContactId = U.ContactId          
					   LEFT JOIN             
					   /*** Getting the Nudge information***/            
					  (           
					   SELECT DISTINCT [PSI].[PrescriptionId]           
					     , 1 AS Nudged          
					    FROM [nowpow].[PatientSiteInteraction] AS PSI           
					    /*** 3&4 are interaction types with eRx ***/           
					    WHERE [PSI].[InteractionTypeId] in ('3','4')           
					    ) PSIN           
					   ON [erx].[Id] = [PSIN].[PrescriptionId]            
					   /*** Getting the eRx download information ***/            
					   LEFT JOIN            
					  (           
					     SELECT DISTINCT [PSI].[PrescriptionId]           
					   , 1 AS Downloaded          
					    FROM [nowpow].[PatientSiteInteraction] AS PSI           
					    /*** 11 is downloaded eRx interactions ***/           
					    WHERE [PSI].[InteractionTypeId] in ('11')                 
					  ) PSID           
					  ON [erx].[Id] = [PSID].[PrescriptionId]           
					 GROUP BY  U.ID
					  , (Cont.LastName + ', ' + Cont.FirstName)            
		  , cast (erx.CreatedDate as date)     			
		  			
		GO 			
		CREATE VIEW DIMUsageLoginfo			
		AS			
					 SELECT U.Id AS UserId           
					 ,CONCAT (contact.LastName, ', ', contact.FirstName) AS UserName            
					 ,CAST ([UAct].[CreatedDate] AS date) AS Date            
					 ,COUNT(DISTINCT UAct.Id) AS Logins            
					 FROM nowpow.CareCoordinator CC             
					 LEFT JOIN nowpow.Contact contact            
					  ON CC.ContactId = contact.Id           
					 LEFT JOIN nowpow.[User] U             
					  ON contact.Id = U.Contactid           
					 LEFT JOIN [log].[ExternalUserActivity] UAct            
					  ON Uact.userid = U.Id   
					 WHERE
						Uact.Activity = 0
					 GROUP BY U.Id            
					  , CONCAT(contact.LastName, ', ', contact.FirstName)           
					  , CAST([UAct].[CreatedDate] AS date)    
					
		GO			
		CREATE VIEW DIMUsagePSIInfo			
		AS			
					 SELECT U.Id AS UserId           
					 ,CONCAT (Cont.LastName, ', ', Cont.FirstName) AS UserName            
					 ,CAST (PSI.InteractionDate AS date) AS Date            
					 ,COUNT(DISTINCT PSI.Id) AS ServicesNudged
					 FROM nowpow.PatientSiteInteraction AS PSI            
					  JOIN nowpow.CareCoordinator AS CC           
					   ON PSI.CareCoordinatorId = CC.Id          
					  LEFT JOIN nowpow.Contact AS Cont           
					   ON CC.ContactId = Cont.Id          
					  LEFT JOIN nowpow.[User] AS U           
					   ON CC.ContactId = U.ContactId          
					 WHERE PSI.InteractionTypeId IN ('5','6')                      
					 GROUP BY U.Id             
					 ,CONCAT (Cont.LastName, ', ', Cont.FirstName)            
		 ,CAST (PSI.InteractionDate AS date)      			
					
		GO			
		CREATE VIEW DIMUsageSearch			
		AS			
					SELECT U.Id as UserId            
					 , (Cont.LastName + ', ' + Cont.FirstName) AS UserName            
					 , CAST(lsc.CreatedDate as date) AS 'Date'            
					 , COUNT(lsc.Id) AS Searches            
					 FROM log.SearchCriteria lsc            
					  JOIN nowpow.CareCoordinator cc           
					   ON lsc.CareCoordinatorId = cc.Id          
					  LEFT JOIN nowpow.Contact Cont           
					   ON cc.ContactId = Cont.Id          
					  LEFT JOIN nowpow.[User] U           
					   ON CC.ContactId = u.ContactId          
					 WHERE lsc.SearchType = 1            
					 GROUP BY U.Id            
					 , (cont.LastName + ', ' + Cont.FirstName)            
					 , CAST(lsc.CreatedDate as date)            
					 HAVING U.Id IS NOT NULL
					
		GO			
		CREATE VIEW DIMUsageFavorite			
		AS			
					SELECT U.Id AS UserId           
					 ,CONCAT (contact.LastName, ', ', contact.FirstName) AS UserName            
					 ,CAST ([SSF].[DateAdded] AS date) AS Date            
					 ,COUNT(DISTINCT SSF.Id) AS Favorites            
					 FROM nowpow.CareCoordinator CC             
					 JOIN nowpow.Contact contact            
					  ON CC.ContactId = contact.Id           
					 JOIN nowpow.[User] U             
					  ON contact.Id = U.Contactid           
					 JOIN nowpow.SubscriberServiceFavorite AS SSF            
					  ON SSF.SubscriberUserid = U.Id           
					 GROUP BY U.Id            
					  , CONCAT(contact.LastName, ', ', contact.FirstName)           
  , CAST([SSF].[DateAdded] AS date);
GO