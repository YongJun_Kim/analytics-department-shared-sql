CREATE VIEW DIMUsageLoginfo			
		AS			
					 SELECT U.Id AS UserId           
					 ,CONCAT (contact.LastName, ', ', contact.FirstName) AS UserName            
					 ,CAST ([UAct].[CreatedDate] AS date) AS Date            
					 ,COUNT(DISTINCT UAct.Id) AS Logins            
					 FROM nowpow.CareCoordinator CC             
					 LEFT JOIN nowpow.Contact contact            
					  ON CC.ContactId = contact.Id           
					 LEFT JOIN nowpow.[User] U             
					  ON contact.Id = U.Contactid           
					 LEFT JOIN [log].[ExternalUserActivity] UAct            
					  ON Uact.userid = U.Id   
					 WHERE
						Uact.Activity = 0
					 GROUP BY U.Id            
					  , CONCAT(contact.LastName, ', ', contact.FirstName)           
					  , CAST([UAct].[CreatedDate] AS date)    
					
					