		CREATE VIEW DIMUsageFavorite			
		AS			
					SELECT U.Id AS UserId           
					 ,CONCAT (contact.LastName, ', ', contact.FirstName) AS UserName            
					 ,CAST ([SSF].[DateAdded] AS date) AS Date            
					 ,COUNT(DISTINCT SSF.Id) AS Favorites            
					 FROM nowpow.CareCoordinator CC             
					 JOIN nowpow.Contact contact            
					  ON CC.ContactId = contact.Id           
					 JOIN nowpow.[User] U             
					  ON contact.Id = U.Contactid           
					 JOIN nowpow.SubscriberServiceFavorite AS SSF            
					  ON SSF.SubscriberUserid = U.Id           
					 GROUP BY U.Id            
					  , CONCAT(contact.LastName, ', ', contact.FirstName)           
  , CAST([SSF].[DateAdded] AS date);