CREATE VIEW PreOrgId
AS

SELECT Pre.Id AS ErxId
	, ErxSiteOrg.Id AS ErxSiteOrgId
	, ErxSiteEntOrg.Id AS ErxSiteEntOrgId
	, CASE  --Added 1/4/2018
		WHEN ErxSiteOrg.IsNetwork = '1' THEN ErxSiteOrg.Id
		ELSE ErxSiteNetOrg.Id 
	END AS ErxSiteNetOrgId
	, SC.SearchType AS SearchType

FROM log.Prescription AS Pre
	LEFT JOIN [nowpow].[Site] AS ErxSite
	ON [Pre].[SiteId] = [ErxSite].[Id]
		LEFT JOIN [nowpow].[Organization] AS ErxSiteOrg
		ON [ErxSite].[OrganizationId] = [ErxSiteOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS ErxSiteEntOrg
				ON [ErxSiteOrg].[EnterpriseOrganizationId] = [ErxSiteEntOrg].[Id]
					LEFT JOIN [nowpow].[Organization] AS ErxSiteNetOrg
					ON [ErxSiteEntOrg].[NetworkOrganizationId] = [ErxSiteNetOrg].[Id]
	LEFT JOIN nowpow.CareCoordinator AS CC
	ON [Pre].[CareCoordinatorId] = [CC].[ID]
	LEFT JOIN (SELECT PrescriptionId, SearchType FROM [Log].[SearchCriteria] WHERE SearchType = 5) AS SC
	ON [Pre].[Id] = [SC].[PrescriptionId]