CREATE VIEW DIMOrganization
AS
SELECT Org.Id AS OrgId
	, Org.Name AS OrgName
	, EntOrg.Id AS EnterpriseOrgId
	, EntOrg.Name AS EnterpriseOrgName
	--Since network orgs don't have enterprise Id associated with them,
	-- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS NetworkOrgId
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Name
		ELSE NetOrg.Name 
	END AS NetworkOrgName
	, OrgSubtype.Id AS OrgSubtypeId
	, OrgSubtype.Name AS OrgSubtypeName
	, OrgType.Id AS OrgTypeId
	, OrgType.Name AS OrgTypeName
	, Org.LocationId AS OrgLocationId

FROM [nowpow].[Organization] AS Org
	LEFT JOIN [nowpow].[Organization] AS EntOrg
	ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
		LEFT JOIN [nowpow].[Organization] AS NetOrg
		ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]
	LEFT JOIN [pristine].[OrganizationSubtype] AS OrgSubtype
	ON [Org].[OrganizationSubtypeId] = [OrgSubtype].[Id]
		LEFT JOIN [pristine].[OrganizationType] AS OrgType
		ON [OrgSubtype].[OrganizationTypeId] = [OrgType].[Id]