/*
	Script 1
	Drop All External Tables and data sources
	Add User Credential for the DB
*/

--Drop External tables
DECLARE @sql NVARCHAR(max)=''

SELECT @sql += ' DROP EXTERNAL TABLE ' + QUOTENAME(S.name) + '.' + QUOTENAME(E.name) + '; '
FROM sys.external_tables  E
LEFT JOIN sys.schemas S
ON E.schema_id = S.schema_id

EXEC sp_executesql @sql;

--Drop external data sources
IF EXISTS ( SELECT TOP 1 1 FROM sys.external_data_sources WHERE name = 'Automation' )
    DROP EXTERNAL DATA SOURCE Automation;

IF EXISTS ( SELECT TOP 1 1 FROM sys.external_data_sources WHERE name = 'Prod' )
    DROP EXTERNAL DATA SOURCE Prod;

--Drop npAnalytics User Credential
IF EXISTS ( SELECT TOP 1 1 FROM sys.database_scoped_credentials WHERE name = 'npAnalytics' )
    DROP DATABASE SCOPED CREDENTIAL npAnalytics;

--Create npAnalytics User Crednetial
CREATE DATABASE SCOPED CREDENTIAL npAnalytics
	WITH IDENTITY = 'npAnalytics',
	SECRET = 'Mb6GlXIKwerz';

IF EXISTS ( SELECT TOP 1 1 FROM sys.database_principals WHERE name = 'npAnalytics' )
    DROP USER [npAnalytics];

CREATE USER [npAnalytics] FOR LOGIN [npAnalytics];
EXEC sp_addrolemember N'db_owner', N'npAnalytics';