CREATE VIEW DIMCareCoordinator
AS

SELECT CC.Id AS CareCoordinatorId
	, CC.ContactId AS ContactId
	, U.Id AS UserId
	, U.UserName AS UserName
	, Contact.FirstName
	, Contact.LastName
	, CONCAT (contact.LastName, ', ', contact.FirstName) AS FullName
	, [S].Id AS SiteId
	, Org.Id AS OrgId
	, Org.Name AS OrgName
	, EntOrg.Id AS EnterpriseOrgId
	, EntOrg.Name AS EnterpriseOrgName
	--Since network orgs don't have enterprise Id associated with them,
	-- org->ent->net joins do not work for network orgs, you have to specify them separately like below.
	-- Revised 1/4/2018
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.Id
		ELSE NetOrg.Id 
	END AS NetworkOrgId
	, CASE 
		WHEN Org.IsNetwork = '1' THEN Org.[Name]
		ELSE NetOrg.[Name]
	END AS NetworkOrgName
	, U.IsActive
	, U.ExcludeFromReports

FROM nowpow.CareCoordinator AS CC
		LEFT JOIN nowpow.Contact AS contact
		ON [CC].[ContactId] = [contact].[Id]
				LEFT JOIN nowpow.[User] U
				ON [contact].[Id] = [U].[Contactid]  
	LEFT JOIN [nowpow].[site] AS S		
	ON [CC].[SiteId] = [S].[Id]	
		LEFT JOIN [nowpow].[Organization] AS Org		
		ON [S].[OrganizationId] = [Org].[Id]
			LEFT JOIN [nowpow].[Organization] AS EntOrg
			ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS NetOrg
				ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]