CREATE VIEW PreCCId
AS 

SELECT CC.Id AS CCId
	, CC.ContactId AS CCContactId
	, U.Id AS CCUserId
	, U.UserName AS CCUserName
	, [S].Id AS CCSiteId
	, Org.Id AS CCOrgId
	, EntOrg.Id AS CCEntOrgId
	, CASE  --Added 1/4/2018
		WHEN Org.IsNetwork = 1 THEN Org.Id
		ELSE NetOrg.Id 
	END AS CCNetOrgId
	, U.ExcludeFromReports

FROM nowpow.CareCoordinator AS CC
		LEFT JOIN nowpow.Contact AS contact
		ON [CC].[ContactId] = [contact].[Id]
				LEFT JOIN nowpow.[User] U
				ON [contact].[Id] = [U].[Contactid]  
	LEFT JOIN [nowpow].[site] AS S		
	ON [CC].[SiteId] = [S].[Id]	
		LEFT JOIN [nowpow].[Organization] AS Org		
		ON [S].[OrganizationId] = [Org].[Id]
			LEFT JOIN [nowpow].[Organization] AS EntOrg
			ON [Org].[EnterpriseOrganizationId] = [EntOrg].[Id]
				LEFT JOIN [nowpow].[Organization] AS NetOrg
				ON [EntOrg].[NetworkOrganizationId] = [NetOrg].[Id]