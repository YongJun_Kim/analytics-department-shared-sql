/*
	Script 4
	Create npServiceInfo user and credential
*/

--Drop npServiceInfo user and credential
IF EXISTS(select top 1 1 from sys.[sysusers] where name like 'npServiceInfo')
	DROP USER npServiceInfo

IF EXISTS(select top 1 1 from sys.database_scoped_credentials where credential_identity = 'npServiceInfo')				
	DROP DATABASE SCOPED CREDENTIAL npServiceInfo  

--Create Credential
CREATE DATABASE SCOPED CREDENTIAL npServiceInfo
WITH IDENTITY = 'npServiceInfo',
SECRET = 'Cj!2Df3s$wnR';

--Create Database User
CREATE USER npServiceInfo FOR LOGIN npServiceInfo;

--Give User Access To Things
GRANT SELECT ON [log].[OrganizationHistory] to npServiceInfo;
GRANT SELECT ON [nowpow].[Service] to npServiceInfo;
GRANT SELECT ON [nowpow].[ServiceCategory] to npServiceInfo;
GRANT SELECT ON [nowpow].[Organization] to npServiceInfo;
GRANT SELECT ON [nowpow].[Contact] to npServiceInfo;
GRANT SELECT ON [nowpow].[Site] to npServiceInfo;
GRANT SELECT ON [nowpow].[ServiceTypeCategoryRelation] to npServiceInfo;
GRANT SELECT ON [nowpow].[ServiceType] to npServiceInfo;
GRANT SELECT ON [nowpow].[Ontology] to npServiceInfo;
GRANT SELECT ON [nowpow].[Location] to npServiceInfo;
GRANT SELECT ON [nowpow].[OrganizationContact] to npServiceInfo;
GRANT SELECT ON [pristine].[OrganizationSubtype] to npServiceInfo;
GRANT SELECT ON [pristine].[OrganizationType] to npServiceInfo;
GRANT SELECT ON [pristine].[AuditStatus] to npServiceInfo;