/*
	Script 5
	Drop Views that were created by the scipts
	--Consider dropping all views
*/

DROP VIEW IF EXISTS DIMContact;

DROP VIEW IF EXISTS ScreeningOrgId;				
					
DROP VIEW IF EXISTS DIMUsagePrescription;					
									
DROP VIEW IF EXISTS DIMUsageLoginfo;					
							
DROP VIEW IF EXISTS DIMUsagePSIInfo;					
						
DROP VIEW IF EXISTS DIMUsageSearch;					
									
DROP VIEW IF EXISTS DIMUsageFavorite;					

DROP VIEW IF EXISTS DIMOrganization;

DROP VIEW IF EXISTS DIMPatient;

DROP VIEW IF EXISTS DIMSite;

DROP VIEW IF EXISTS DIMCareCoordinator;

DROP VIEW IF EXISTS PreCCId;

DROP VIEW IF EXISTS PreOrgId;