CREATE VIEW DIMUsagePSIInfo			
		AS			
					 SELECT U.Id AS UserId           
					 ,CONCAT (Cont.LastName, ', ', Cont.FirstName) AS UserName            
					 ,CAST (PSI.InteractionDate AS date) AS Date            
					 ,COUNT(DISTINCT PSI.Id) AS ServicesNudged
					 FROM nowpow.PatientSiteInteraction AS PSI            
					  JOIN nowpow.CareCoordinator AS CC           
					   ON PSI.CareCoordinatorId = CC.Id          
					  LEFT JOIN nowpow.Contact AS Cont           
					   ON CC.ContactId = Cont.Id          
					  LEFT JOIN nowpow.[User] AS U           
					   ON CC.ContactId = U.ContactId          
					 WHERE PSI.InteractionTypeId IN ('5','6')                      
					 GROUP BY U.Id             
					 ,CONCAT (Cont.LastName, ', ', Cont.FirstName)            
		 ,CAST (PSI.InteractionDate AS date)      			
					