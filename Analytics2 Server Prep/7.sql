CREATE VIEW DIMContact
AS

SELECT Contact.Id AS ContactId
	, U.Id AS UserId
	, Contact.FirstName AS ContactFirstName
	, Contact.LastName AS ContactLastName
	, U.UserName AS UserName
	, U.ExcludeFromReports  --Added 1/4/2018

FROM nowpow.Contact contact
	LEFT JOIN nowpow.[User] U
	ON contact.Id = U.Contactid;