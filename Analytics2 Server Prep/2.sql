
/*
	Script 2 
	Drop all External Tables from N_NowPow-Prod(Prod)
	Drop External Data Source Prod
	Create External Data Source for Prod
	Create External Tables from Prod
*/

/*Drop all External Tables from N_NowPow-Prod(Prod)*/
DECLARE @sql NVARCHAR(max)=''''

SELECT @sql += '' DROP EXTERNAL TABLE '' + QUOTENAME(S.name) + ''.'' + QUOTENAME(E.name) + ''; ''
FROM sys.external_tables  E
	INNER JOIN sys.external_data_sources d
	ON E.data_source_id = d.data_source_id
	LEFT JOIN sys.schemas S
	ON E.schema_id = S.schema_id
WHERE d.name = ''Prod''
EXEC sp_executesql @sql;

/*Drop External Data Source Prod*/
IF EXISTS ( SELECT * FROM sys.external_data_sources WHERE name = ''Prod'' )
    DROP EXTERNAL DATA SOURCE Prod;

/*Create External Data Source for Prod*/
CREATE EXTERNAL DATA SOURCE Prod
WITH
(
	TYPE=RDBMS,
	LOCATION=''nowpow-sql-analytics2.database.windows.net'',
	DATABASE_NAME=''<MASTER_DB_NAME>'',  --N_NowPow-Prod for local execution <MASTER_DB_NAME> for Azure Automation
	CREDENTIAL= npAnalytics
)

/*Create External Tables from Prod DB*/
CREATE EXTERNAL TABLE [nowpow].[Service](
	[Id] [bigint] NOT NULL,
	[TypeId] [bigint] NOT NULL,
	[NameTextId] [bigint] NOT NULL,
	[NameDefaultText] [nvarchar](200) NULL,
	[DescriptionTextId] [bigint] NOT NULL,
	[DescriptionDefaultText] [nvarchar](2000) NULL,
	[ComputedAverageRating] [float] NULL,
	[LocationId] [bigint] NULL,
	[OrganizationId] [bigint] NOT NULL,
	[OgranizationPriority] [smallint] NULL,
	[LastModifiedById] [bigint] NULL,
	[PhoneArea] [nchar](3) NULL,
	[PhonePrefix] [nchar](3) NULL,
	[PhoneSuffix] [nchar](4) NULL,
	[Email] [nvarchar](100) NULL,
	[Website] [nvarchar](250) NULL,
	[InternalNotes] [nvarchar](2000) NULL,
	[ModifiedDate] [datetime2](2) NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[PhoneExtension] [nvarchar](10) NULL,
	[IsDeleted] [bit] NOT NULL
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[ServiceCategory](
	[Id] [bigint] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[NameTextId] [bigint] NOT NULL,
	[NameDefaultText] [nvarchar](50) NULL,
	[IsActive] [bit] NOT NULL
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[ServiceTagRelation](
	[Id] [bigint] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[TagRestrictionType] [smallint] NULL
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[Tag](
	[Id] [bigint] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[NameTextId] [bigint] NOT NULL,
	[NameDefaultText] [nvarchar](500) NULL,
        [CategoryId] [bigint] NOT NULL,
	[InternalDescription] [nvarchar](500) NULL,
	[ExcludeFilterFlag] [bit] NOT NULL,
	[FilterTextOverride] [nvarchar](500) NULL,
	[TagRestrictionType] [smallint] NULL
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[TagCategory](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[TypeId] [bigint] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[ApplyOnlyAssociatedTags] [bit] NOT NULL,
	[TagCategoryRestrictionType] [smallint] NULL
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[Organization](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[LocationId] [bigint] NULL,
	[OrganizationStatusId] [bigint] NOT NULL,
	[OrganizationStructureId] [bigint] NOT NULL,
	[OrganizationSubtypeId] [bigint] NULL,
	[SubscriberStatusId] [bigint] NOT NULL,
	[AuditStatusId] [bigint] NOT NULL,
	[DataSourceId] [bigint] NOT NULL,
	[LastModifiedById] [bigint] NULL,
	[SurveyAssigneeId] [bigint] NULL,
	[PhoneArea] [nchar](3) NULL,
	[PhonePrefix] [nchar](3) NULL,
	[PhoneSuffix] [nchar](4) NULL,
	[FaxArea] [nchar](3) NULL,
	[FaxPrefix] [nchar](3) NULL,
	[FaxSuffix] [nchar](4) NULL,
	[Email] [nvarchar](100) NULL,
	[Website] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[InternalNotes] [nvarchar](max) NULL,
	[PriorityLevel] [bigint] NOT NULL,
	[IsPrioritized] [bit] NOT NULL,
	[ImportId] [bigint] NULL,
	[ModifiedDate] [datetime2](2) NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[Rating] [float] NULL,
	[IsEnterprise] [bit] NOT NULL,
	[EnterpriseOrganizationId] [bigint] NULL,
	[PhoneExtension] [nvarchar](10) NULL,
	[IsPublished] [bit] NOT NULL,
	[VerifiedLocationId] [bigint] NULL,
	[AddressVerifiedDate] [datetime2](2) NULL,
	[IsNetwork] [bit] NOT NULL,
	[NetworkOrganizationId] [bigint] NULL,
	[VerifiedDate] [datetime] NULL,
) 
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[Contact](
	[Id] [bigint] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[Notes] [nvarchar](500) NULL,
	[Email] [nvarchar](100) NULL,
	[EmailOptIn] [bit] NOT NULL,
	[PreferredContactMethodId] [bigint] NOT NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Department] [nvarchar](100) NULL
)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[Site](
	[Id] [bigint] NOT NULL,
	[OrganizationId] [bigint] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[NameTextId] [bigint] NOT NULL,
	[CommunityInfo] [nvarchar](500) NULL,
	[UseEnterpriseConfig] [bit] NOT NULL,
	[AllowPatientVisibilityToAllUsers] [bit] NOT NULL,
	[CommunityInfoNameTextId] [bigint] NULL,
	[CareProfessionalToolPrescriptionVersionId] [bigint] NULL,
	[PointOfCarePrescriptionVersionId] [bigint] NULL
)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[User](
	[Id] [bigint] NOT NULL,
	[ContactId] [bigint] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[DefaultLanguageId] [bigint] NOT NULL ,
	[IsActive] [bit] NOT NULL ,
	[LastLoginDate] [datetime2](2) NULL,
	[UniqueId] [uniqueidentifier] NOT NULL ,
	[Version] [timestamp] NOT NULL,
	[ExcludeFromReports] [bit] NOT NULL

)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [log].[UserActivity](
	[Id] [bigint]  NOT NULL,
	[Action] [nvarchar](100) NULL,
	[UserId] [bigint] NOT NULL,
	[ContactId] [bigint] NULL,
	[ContactOrganizationId] [bigint] NULL,
	[SiteId] [bigint] NULL,
	[ReferenceTableName] [nvarchar](100) NULL,
	[ReferenceId] [bigint] NULL,
	[ActionDate] [datetime2](7) NOT NULL,
)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[UserUserTagRelation](
	[Id] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[UserTagId] [bigint] NOT NULL
)
WITH
( 
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [pristine].[UserTag](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NULL
)
WITH
(
	DATA_Source = Prod
)



CREATE EXTERNAL TABLE [nowpow].[ServiceTypeCategoryRelation](
	[TypeId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL
)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[ServiceType](
	[Id] [bigint] NOT NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
	[Version] [timestamp] NOT NULL,
	[NameTextId] [bigint] NOT NULL,
	[NameDefaultText] [nvarchar](100) NULL,
	[AliasTextId] [bigint] NULL,
	[AliasDefaultText] [nvarchar](1000) NULL,
	[IsActive] [bit] NOT NULL,
	[PrimaryServiceCategoryId] [bigint] NULL
	)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[Ontology](
	[Id] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[OntologyTypeId] [bigint] NOT NULL,
	[OntologyTriggerTypeId] [bigint] NOT NULL,
	[GenderId] [int] NULL,
	[MonthsMinimum] [smallint] NULL,
	[MonthsMaximum] [smallint] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [nowpow].[OntologySiteProduct](
	[Id] [bigint] NOT NULL,
	[OntologyId] [bigint] NOT NULL,
	[SiteId] [bigint] NOT NULL,
	[ProductId] [bigint] NULL,
	[IsFallback] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [pristine].[OrganizationSubtype](

  [Id] [bigint]  NOT NULL,
  [Name] [nvarchar](100) NOT NULL,
  [OrganizationTypeId] [bigint] NOT NULL,
  [SubTypeNumber] [bigint] NOT NULL,
  [IsNowPowType] [bit] NOT NULL,
  [RankingWeight] [float] NOT NULL,
  [UniqueId] [uniqueidentifier] NOT NULL,
  [Version] [timestamp] NOT NULL,
  )
WITH
(
	DATA_SOURCE = Prod
)

CREATE EXTERNAL TABLE [pristine].[OrganizationType](
    [Id] [bigint] NOT NULL,
    [Name] [nvarchar](100) NOT NULL,
    [UniqueId] [uniqueidentifier] NOT NULL,
    [Version] [timestamp] NOT NULL,)WITH 
(
DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[Location](
    [Id] [bigint] NOT NULL,
    [UniqueId] [uniqueidentifier] NOT NULL,
    [Version] [timestamp] NOT NULL,
    [Block] [nvarchar](10) NULL,
    [Number] [nvarchar](15) NOT NULL,
    [Fraction] [nvarchar](10) NULL,
    [Street] [nvarchar](100) NOT NULL,
    [Unit] [nvarchar](100) NULL,
    [CommunityArea] [nvarchar](100) NULL,
    [City] [nvarchar](100) NOT NULL,
    [State] [nchar](2) NOT NULL,
    [PostalCode] [nvarchar](10) NOT NULL,
    [CountryCode] [nvarchar](3) NOT NULL,
    [Latitude] [float] NULL,
    [Longitude] [float] NULL,
    [IsExactMatch] [bit] NULL,
)
WITH 
(
  DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[OrganizationConfiguration](
    [Id] [bigint] NOT NULL,
    [OrganizationId] [bigint] NOT NULL,
    [ProviderOrganizationId] [bigint] NOT NULL,
    [ConfigurationType] [tinyint] NULL,
    [IsActive] [bit] NOT NULL,
    [ModifiedDate] [datetime2](2) NOT NULL,
    [CreatedDate] [datetime2](2) NOT NULL,
    [IsReferralTaker] [bit] NOT NULL,
)
WITH (
 DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[SubscriberServiceFavorite](
    [Id] [bigint] NOT                                                                                                                                                                                                   NULL,
    [SubscriberUserId] [bigint] NOT NULL,
    [ServiceId] [bigint] NOT NULL,
    [DateAdded] [datetime2](2) NOT NULL,
)
WITH (
 DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[OrganizationContact](
       [Id] [bigint] NOT NULL,
       [OrganizationId] [bigint] NOT NULL,
       [ContactId] [bigint] NOT NULL,
       [Website] [nvarchar](250) NULL,
       [IsActive] [bit] NOT NULL,
       [UniqueId] [uniqueidentifier] NOT NULL,
       [Version] [timestamp] NOT NULL,
)
WITH (
 DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[OrganizationLicenseRelation](
       [Id] [bigint] NOT NULL,
       [OrganizationId] [bigint] NOT NULL,
       [LicenseId] [bigint] NOT NULL,
)
WITH (
 DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[License](
       [Id] [bigint] NOT NULL,
       [Name] [varchar](50) NOT NULL,
       [RoleIds] [varchar](75) NULL,
)
WITH (
 DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[ReferralAppointment](	
	[Id] [bigint] NOT NULL,
	[ReferralId] [bigint] NOT NULL,
	[AppointmentDate] [date] NOT NULL,
	[LastReminderDate] [datetime2](7) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[AppointmentStatusId] [bigint] NOT NULL,
	[Notes] [varchar](max) NULL,
	[CreatedByContactId] [bigint] NOT NULL,
	[AppointmentTime] [time](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[OutcomeDate] [datetime2](7) NULL,
)	
WITH (	
	DATA_SOURCE=Prod
)	

CREATE EXTERNAL TABLE [nowpow].[Referral](	
	[Id] [bigint] NOT NULL,
	[MakerOrganizationId] [bigint] NOT NULL,
	[MakerSiteId] [bigint] NOT NULL,
	[MakerContactId] [bigint] NOT NULL,
	[MakerPatientId] [bigint] NOT NULL,
	[MakerLastModifiedByContactId] [bigint] NOT NULL,
	[MakerModifiedDate] [datetime2](7) NOT NULL,
	[TakerOrganizationId] [bigint] NOT NULL,
	[TakerContactId] [bigint] NULL,
	[TakerLastModifiedByContactId] [bigint] NULL,
	[TakerModifiedDate] [datetime2](7) NULL,
	[ServiceId] [bigint] NOT NULL,
	[PatientFirstName] [varchar](100) NOT NULL,
	[PatientLastName] [varchar](100) NOT NULL,
	[PatientHomePhone] [varchar](25) NULL,
	[PatientMobilePhone] [varchar](25) NULL,
	[PatientEmailAddress] [varchar](300) NULL,
	[PatientPreferredContactMethod] [varchar](50) NULL,
	[PatientStreetAddress] [nvarchar](100) NULL,
	[PatientCity] [nvarchar](100) NULL,
	[PatientState] [nvarchar](100) NULL,
	[PatientPostalCode] [nvarchar](100) NULL,
	[PatientCountryCode] [nvarchar](3) NULL,
	[Notes] [varchar](max) NULL,
	[ContactedDate] [datetime2](7) NULL,
	[ContactResultTypeId] [bigint] NOT NULL,
	[CompletedFlag] [bit] NOT NULL,
	[CompletedDate] [datetime2](7) NULL,
	[SourceType] [tinyint] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ArchivedFlag] [bit] NOT NULL,
	[ArchivedDate] [datetime2](7) NULL,
	[UrgentFlag] [bit] NOT NULL,
	[AreMessagesUnread] [bit] NOT NULL,
	[Outcome] [varchar](max) NULL,
	[MakerHasUnreadMessages] [bit] NOT NULL,
	[TakerHasUnreadMessages] [bit] NOT NULL,
	[ReferralConsentTypeId] [bigint] NULL,
	[ArchivedByContactId] [bigint] NULL,
	[ArchivedByOrganizationId] [bigint] NULL,
	[PatientLatitude] [float] NULL,
	[PatientLongitude] [float] NULL

)	
WITH (	
  DATA_SOURCE=Prod	
)

CREATE EXTERNAL TABLE [nowpow].[ScreeningTemplate](	
	[Id] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[DisplayOrder] [tinyint] NULL,
	[SiteId] [bigint] NULL,
	[Workflow] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[ScreeningType] varchar(10) NULL
)	
WITH (	
  DATA_SOURCE=Prod	
)	

CREATE EXTERNAL TABLE [nowpow].[Need](
	[Id] [bigint]  NOT NULL,
	[SiteId] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
WITH (	
  DATA_SOURCE=Prod	
)	

CREATE EXTERNAL TABLE [log].[ExternalUserActivity](
	[Id] [bigint] NOT NULL,
	[UserId] [bigint] NULL,
	[Activity] [bigint] NOT NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[IPAddress] [nvarchar](50) NULL,
	[UserName] [nvarchar](100) NULL,
	[OrganizationId] [bigint] NULL
	)
WITH (
	DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[NeedServiceTypeRelation](
	[Id] [bigint] NOT NULL,
	[NeedId] [bigint] NOT NULL,
	[ServiceTypeId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
WITH (
	DATA_SOURCE=Prod	
)

CREATE EXTERNAL TABLE [pristine].[ContactResultType](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DisplayOrder] [tinyint] NOT NULL
)
WITH (
	DATA_SOURCE=Prod
)

CREATE EXTERNAL TABLE [nowpow].[OntologyProtocol] (
	[Id] [bigint] NOT NULL,
	[OntologyId] [bigint] NOT NULL,
	[ServiceCategoryId] [bigint] NOT NULL,
	[ServiceTypeId] [bigint] NOT NULL,
	[Priority] [smallint] NOT NULL,
	[WhyTextId] [bigint] NOT NULL,
	[WhyDefaultText] [nvarchar](1000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ServiceTypePriority] [int] NOT NULL
)
WITH (
	DATA_SOURCE=Prod
);
