/*
	Script 3
	Drop all External Tables from Analytics_Automation database 
	Drop Automation Data Source
	Create Automation Data Source
	Create all automation External Tables
*/

/*Drop all External Tables from Analytics_Automation database*/
DECLARE @sql NVARCHAR(max)=''

SELECT @sql += ' DROP EXTERNAL TABLE ' + QUOTENAME(S.name) + '.' + QUOTENAME(E.name) + '; '
FROM sys.external_tables  E
	INNER JOIN sys.external_data_sources d
	ON E.data_source_id = d.data_source_id
LEFT JOIN sys.schemas S
ON E.schema_id = S.schema_id
WHERE d.name = 'Automation'
EXEC sp_executesql @sql;

/*Drop Automation Data Source*/
IF EXISTS ( SELECT * FROM sys.external_data_sources WHERE name = 'Automation' )
    DROP EXTERNAL DATA SOURCE Automation;

/*Create Automation Data Source*/
CREATE EXTERNAL DATA SOURCE Automation
WITH
(
	TYPE=RDBMS,
	LOCATION='nowpow-sql-analytics2.database.windows.net',
	DATABASE_NAME='Analytics_Automation',
	CREDENTIAL= npAnalytics
);

/*Create all automation External Tables*/
CREATE EXTERNAL TABLE [dbo].[RefEthnicity](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);	

CREATE EXTERNAL TABLE [dbo].[RefLanguage](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);	

CREATE EXTERNAL TABLE [dbo].[RefRace](
	[Name] [varchar](122) NULL,
	[RepName] [varchar](42) NULL
	)
WITH (	
  DATA_SOURCE=Automation	
);