CREATE VIEW ScreeningOrgId
AS

SELECT SR.Id AS ScreeningResponseId
	, SRSiteOrg.Id AS ScreeningResponseSiteOrgId
	, SRSiteEntOrg.Id AS ScreeningResponseSiteEntOrgId
	, SRSiteEntOrg.NetworkOrganizationId AS ScreeningResponseSiteNetOrgId

FROM nowpow.ScreeningResponse AS SR
	LEFT JOIN [nowpow].[Site] AS SRSite
	ON [SR].[SiteId] = SRSite.[Id]
		LEFT JOIN [nowpow].[Organization] AS SRSiteOrg
		ON SRSite.[OrganizationId] = SRSiteOrg.[Id]
				LEFT JOIN [nowpow].[Organization] AS SRSiteEntOrg
				ON SRSiteOrg.[EnterpriseOrganizationId] = SRSiteEntOrg.[Id]
	LEFT JOIN nowpow.CareCoordinator AS CC
	ON [SR].[CareCoordinatorId] = [CC].[ID]