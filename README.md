# README #

* Analytics Department's Shared SQL repository.  Please save any SQL query that will be used in a cyclical reports.

### What is this repository for? ###

* This repository is to save Analytics department queries that will be used more than once.

* v 0.1

### How do I get set up? ###

* Please download this repository to your local drive and use the queries.  If your application uses dynamic queries that are generated from a template query, please save the template queries in the repository.

### Contribution guidelines ###

* Make separate folders for each of your report or usage.
* If you made a new cyclical report query, please review it with the entire department.
* Do not edit queries of the reports or usage that are not yours.

### Who do I talk to? ###

* If you have any question please ask the Analytics department: Chevy Williams or Yong Jun Kim.