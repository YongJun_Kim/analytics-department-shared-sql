/********QUERY TO OUTPUT REPORT DOWNLOADS***********/


SELECT 
    net.Name as Network
	,ent.Name as Enterprise
	,org.Name as Organization
	,[user].UserName
	,concat(contact.LastName, ', ', contact.FirstName) as Name
	,ua.UserId
	,ua.ContactId
	,ua.Action
	,arc.Name as CategoryName
	,ars.Name as SubCategoryName
	,arf.FileNamePdf
	,arf.FileNameExcel
	,art.Name as Tool
	,arp.Name as ReportingPeriod
	,ua.ActionDate
 FROM [log].[UserActivity] ua
	 join [nowpow].[AnalyticsReportFile] arf on ua.ReferenceId=arf.Id
	 left join nowpow.Organization org on arf.OrganizationId = org.Id
			LEFT JOIN nowpow.Organization ENT 
				ON org.enterpriseorganizationid = ENT.ID
			LEFT JOIN nowpow.Organization NET 
				ON org.NetworkOrganizationid = NET.ID
	 left join pristine.AnalyticsReportSubCategory ars on arf.SubCategoryId = ars.Id
	 left join pristine.AnalyticsReportCategory arc on arf.CategoryId = arc.Id
	 join nowpow.[user] on ua.UserId = [user].Id
	 left join nowpow.Contact on [user].ContactId = Contact.Id
	 left join pristine.AnalyticsReportTool art on arf.ToolId = art.Id
	 left join pristine.AnalyticsReportPeriod arp on arf.PeriodId = arp.Id
where contact.Email not like '%nowpow%'
	and [user].Username not like '%nowpow%'
	and org.Name not like '%corner%'
    and ua.[Action] like '%Analytics%'