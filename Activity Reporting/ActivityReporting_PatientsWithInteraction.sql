--USE ALL
--go

/*
Count of Unique Patients with Interaction, by Year-Month
author: joanna.tung
version: 2.0

2.1: removed join to dimpatient view table and utilized standard patient and pristine "state" tables to get patient state and filter for test patients. this speeds up the query.
2.2: converted zip codes to varchar. need to import csv files as "data/text" file in Excel to ensure that leading zeroes are preserved in the Excel output.
 
*/
DECLARE @fromdate date = '2019-01-01'
DECLARE @todate date = '2019-03-31'

;
with cte as (
 	select CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.[PatientId] AS Char) AS PatientID
	, PSI.PatientId as 'PSIPatientId'
	, PSI.Id
	, cast(PSI.InteractionDate as date) as InteractionDate
	, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, PSI.InteractionDate), 0) as date) as YearMonth
	, DIMCC.UserId
	, DIMSite.SiteId
	, DIMCC.CareCoordinatorId
	, DIMCC.ExcludeFromReports
	--, DIMPatient.TestPatient
	, case when PAT.LastName like '%Zzztest%' then 'TRUE' else 'FALSE' end as 'TestPatient'
	, DIMSite.OrgId
	, DIMSite.OrgName
	, DIMSite.EnterpriseOrgId
	, DIMSite.EnterpriseOrgName
	, DIMSIte.NetworkOrgId
	, DIMSite.NetworkOrgName
	, LOC.[State] as OrgState
	, CAST(LOC.[PostalCode] AS VARCHAR(11)) as OrgZip
	, pState.[Name] as PatientState
	, CAST(PAT.PostalCode AS VARCHAR(11)) as PatientZip
	, row_number() over (partition by CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.[PatientId] AS Char), DATEADD(MONTH, DATEDIFF(MONTH, 0, PSI.InteractionDate), 0) order by PSI.InteractionDate) as [r]


	FROM [nowpow].[PatientSiteInteraction] AS PSI
		LEFT JOIN DIMCareCoordinator AS DIMCC
		ON PSI.CareCoordinatorId = DIMCC.CareCoordinatorID
		LEFT JOIN DIMSite AS DIMSite
		ON PSI.SiteId = DIMSite.SiteId
			LEFT JOIN nowpow.[Organization] as O
				LEFT JOIN nowpow.[Location] as LOC
				ON O.LocationId = LOC.Id
			ON DIMCC.OrgId = O.Id
		--LEFT JOIN DIMPatient AS DimPatient
		--ON PSI.PatientId = DimPatient.PatientId
		LEFT JOIN nowpow.Patient as PAT
		ON PSI.PatientId = PAT.Id
		LEFT JOIN pristine.[State] as pState
		ON PAT.StateId = pState.Id
		--LEFT JOIN (
		--	Select pState.[Name] as PatientState, Patient.Id
		--	from nowpow.[Patient] as Patient 
		--	LEFT JOIN pristine.[State] as pState
		--	ON Patient.StateId = pState.Id
		--	) as pPatient	
		--ON pPatient.Id = PSI.PatientId


	WHERE PSI.[InteractionDate] >= DATEADD(yy, DATEDIFF(yy, 0, @fromdate), 0) 
		AND PSI.[InteractionDate] <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
		AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		AND (PAT.LastName not like '%Zzztest%' OR PAT.LastName IS NULL)
		--AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
		AND PSI.PatientId IS NOT NULL
		AND PSI.IsDeleted <> 1

	--ORDER BY PatientID, InteractionDate, [r]
) 

SELECT *
	FROM cte
	WHERE PatientId is not NULL
	and [r] = 1