Activity Reporting

Getting data:
- Use ScriptRunner to run queries in analytics2 
- Activity Reporting queries are stored in analytics-department-shared-sql > Activity Reporting
	- "prod" folder queries run on N_NowPow-Prod
	- all other queries should be run on all customer DBs

Deliverable:
1) Snapshot (pdf) 
2) Snapshot (xlsx)
3) Full (xlsx)

For Items 1 and 2:
- send email with 1 and 2 attached to: 
	Palicha, Roshni <roshni.palicha@nowpow.com>, Richardson-Deppe, Louisa <louisa.richardson-deppe@nowpow.com>; Feldmeth, Gillian <gillian.feldmeth@nowpow.com>; Nanquil, Abigail <abigail.nanquil@nowpow.com>; Loyd, Amy <Amy.Loyd@nowpow.com>; Kim, Yong <yong.kim@nowpow.com>; Kohler, Rachel <Rachel.Kohler@nowpow.com>; Nicotra Reilly, Andrew <Andrew.nicotrareilly@nowpow.com>; Goldstein, Jake <jake.goldstein@nowpow.com>; Curtin, Pat <pat.curtin@nowpow.com>; Davis, Alyssa <alyssa.davis@nowpow.com>
- upload 1 and 2 to:
	L:\Activity Reports
	G:\Customer Success\Analytics\ActivityReporting
	
For Item 3:
- upload to Azure Storage Explorer > nowpowanalytics > File Shares > analytics > Activity Reports 