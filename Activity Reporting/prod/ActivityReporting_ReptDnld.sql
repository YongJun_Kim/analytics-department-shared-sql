--USE N_NowPow-Prod
--go

DECLARE @todate date = '2019-03-31'

select
		C.Id AS ContactId
		, UA.UserId AS UserId
		, S.Id as SiteId
		, Org.[Name] AS OrgName
		, UA.ContactOrganizationId AS OrgId
		, EntOrg.[Name] AS EnterpriseOrgName
		, CASE WHEN Org.IsNetwork = '1' THEN Org.[Name] ELSE NetOrg.[Name] END AS NetworkOrgName

		, 1 as 'ReportDownloaded'
		--, UA.ActionDate
		, ua.Action
		, arc.Name as CategoryName
		, ars.Name as SubCategoryName
		, art.Name as Tool
		,cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, UA.ActionDate), 0) as date) as YearMonth

		, U.Username AS Username
		, C.FirstName
		, C.LastName
		, C.Email AS Email
		, U.IsActive AS UserIsActive
		, C.IsDeleted AS ContactIsDeleted
		, u.ExcludeFromReports AS ExcludeFromReports
		, c.CreatedDate AS ContactCreatedDate
		, Loc.[State] AS OrgState
		, Loc.[PostalCode] AS PostalCode
		, COALESCE(
				SUBSTRING(
				(
					Select	', ' + cast(R.[Name] as varchar(2000))
					From	nowpow.UserRole UR 
							LEFT JOIN pristine.[Role] as R
							ON UR.RoleId = R.Id
					Where	UR.UserId = U.Id
					ORDER BY UR.RoleId
					For XML PATH ('')
				), 3, 2000),
				''
				) as 'Role Name'
		, case when (C.Email LIKE '%nowpow%' OR U.Username LIKE '%nowpow%') then 'NP_flag' else 'Noflag' end as 'NowPowUserFlag'
		, substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1) AS 'DBName'
		, COD.IsDemo 
		, COD.IsLive
	
	FROM [log].[UserActivity] AS UA
	
		JOIN [nowpow].[AnalyticsReportFile] arf 
		ON ua.ReferenceId=arf.Id
		LEFT JOIN pristine.AnalyticsReportSubCategory ars 
		ON arf.SubCategoryId = ars.Id
		LEFT JOIN pristine.AnalyticsReportCategory arc 
		ON arf.CategoryId = arc.Id
		LEFT JOIN pristine.AnalyticsReportTool art 
		ON arf.ToolId = art.Id
		LEFT JOIN pristine.AnalyticsReportPeriod arp 
		ON arf.PeriodId = arp.Id

		JOIN nowpow.[User] AS U
		ON U.Id = UA.UserId
		JOIN nowpow.[Contact] as C
		ON C.Id = U.ContactId

		/* This join is causing floating user activity to "duplicate" across all of their provisioned orgs. Remove this join and assign organization based on the ContactOrganizationId available in the log.UserActivity table.
		LEFT JOIN nowpow.OrganizationContact AS OC
		ON C.Id = OC.ContactId */

		LEFT JOIN nowpow.[Organization] as Org
		ON UA.ContactOrganizationId = Org.Id
		LEFT JOIN nowpow.[Site] as S
		ON S.OrganizationId = Org.Id
		LEFT JOIN nowpow.CareOrganizationDatabaseSiteRelation codsr
		on s.Id = codsr.SiteId
		LEFT JOIN nowpow.CareOrganizationDatabase cod
		on codsr.CareOrganizationDatabaseId = cod.Id
		LEFT JOIN nowpow.[Location] as Loc
		ON Org.LocationId = Loc.Id
		LEFT JOIN nowpow.[Organization] as EntOrg
		ON Org.EnterpriseOrganizationId = EntOrg.Id
		LEFT JOIN nowpow.[Organization] as NetOrg 
		ON EntOrg.NetworkOrganizationId = NetOrg.Id

	WHERE UA.[Action] like '%analytics%' and UA.ActionDate <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))

	--GROUP BY UA.UserId, UA.[Action], Arc.[Name], Ars.[Name], Art.[Name]
	--	,cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, UA.ActionDate), 0) as date)

	ORDER BY UA.UserId