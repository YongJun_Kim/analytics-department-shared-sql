--USE ALL
--GO

/*
*ACTIVITY REPORT - ADAPTED FROM MONTHLY USAGE REPORT*
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*v1.3 (3/13/2018): Revised to 1) exclude usernames, 2) exclude favorites and 3) add PoC eRx and PoC eRx referrals
*v1.4 (4/19/2018): Revised to include 1) Nudges by User, 2) Report Downloads, and 3) Tracked Referrals Closed. 
				   Raw table column "TrackedReferrals" renamed "TrackedReferralsCreated" in order to differentiate created versus closed tracked referrals.
*v1.5 (5/1/2018): Removed Report Downloads from the query. Need to generate a separate query for that counts by user.
				  Added TrackedReferralsClosedSuccessfully query, adapted from Yong's Exec Dashboard query for the same metric
*v1.6 (5/31/2018): Removed Patients with Interaction query to own query. 
*v1.7 (6/14/2018): Added exclusion parameters for DIMCC.ExcludeFromReports and DIMPatient.TestPatient to the TrackedReferralsSuccessfullyClosed "RA" table creation --> this ensures that the earliest record selected is "eligible" for reporting purposes
	   (Else, we may encounter the case where the earliest ReferralAppointment record is tied to an "ExcludeFromReport" CareCoordinator or to a "TestPatient", and incorrectly removed from the final export with the
	   exclusion parameters set at final WHERE statement)
					Also set VARCHAR(200) for OrgName, EnterpriseOrgName and NetworkOrgName in the SELECT statement to ensure full name is included in the export (mirrors limit set in DIMSite table)
 */
DECLARE @fromdate date = '2019-01-01'
DECLARE @todate date = '2019-03-31'


SELECT 
 CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
--, CASE 
--WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
--ELSE 'N/A'
--END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS OrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR(200))
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR(200))
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR(200))
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR(200))
ELSE 'N/A'
END AS NetworkOrgName
, Raw.*
, ISNULL(Raw.SingleServiceReferral,0) + ISNULL(Raw.TrackedReferralsCreated,0) + ISNULL(Raw.ReferralsOnSharedErx, 0) + ISNULL(Raw.ServicePrints, 0) AS TotalSharedReferrals
, LOC.[State] as 'CC_State'
, CAST(LOC.[PostalCode] AS VARCHAR(11)) AS 'CC_ZipCode'
, LOCSite.[State] AS 'Org_State'
, CAST(LOCSite.[PostalCode] AS VARCHAR(11)) AS 'Org_ZipCode'
, CASE 
	WHEN Raw.CareCoordinatorId IS NOT NULL THEN LOC.[State] 
	WHEN Raw.CareCoordinatorId IS NULL THEN LOCSite.[State] 
	WHEN LOC.[State] IS NOT NULL THEN LOC.[State]
	ELSE LOCSite.[State]
  END AS 'Final_State'
, CASE 
	WHEN Raw.CareCoordinatorId IS NOT NULL THEN CAST(LOC.[PostalCode] AS VARCHAR(11))
	WHEN Raw.CareCoordinatorId IS NULL THEN CAST(LOCSite.[PostalCode] AS VARCHAR(11))
	WHEN LOC.[PostalCode] IS NOT NULL THEN CAST(LOC.[PostalCode] AS VARCHAR(11))
	ELSE CAST(LOCSite.[PostalCode] AS VARCHAR(11))
  END AS 'Final_ZipCode'

FROM
(
(
/**eRx Created**/              
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'                
, 1 AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared     
, NULL AS PoCeRx 
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Pre.PatientId

FROM log.Prescription AS Pre
WHERE (Pre.PrescriptionSourceTypeId <> 1 OR Pre.PrescriptionSourceTypeId IS NULL)
)
UNION ALL

/**PoC eRx Created**/  
(
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'      
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared  
, 1 AS PoCeRx  
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Pre.PatientId

FROM log.Prescription AS Pre
WHERE (Pre.PrescriptionSourceTypeId = 1)
)
UNION ALL
(
/**PoC eRx referrals**/
SELECT Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date) AS 'Date'      
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared  
, NULL AS PoCeRx   
, COUNT(*) AS ReferralsOnPoCErx
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Pre.PatientId

FROM log.Prescription AS Pre
	LEFT JOIN log.PrescriptionService AS PreServ
	ON Pre.Id = PreServ.PrescriptionId
WHERE (Pre.PrescriptionSourceTypeId = 1)
GROUP BY Pre.CareCoordinatorID 
, Pre.SiteId       
, CAST(Pre.CreatedDate as date)         
, Pre.PatientId
)
UNION ALL
(
/**Referrals On Erx that were shared**/              
SELECT PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date) AS 'Date'                
, NULL AS eRxCreated
, COUNT(*) AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared  
, NULL AS PoCeRx     
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
JOIN log.Prescription AS Pre
ON PSI.PrescriptionId = Pre.Id
LEFT JOIN log.PrescriptionService AS PreServ
ON Pre.Id = PreServ.PrescriptionId
WHERE [PSI].[InteractionTypeId] in ('3','4','11')  
	AND PSI.IsDeleted <> 1

GROUP BY PSI.CareCoordinatorID 
, PSI.SiteId       
, CAST(PSI.InteractionDate as date)         
, PSI.PatientId
)
UNION ALL
(
/**Tracked Referrals Created**/              
SELECT 
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, 1 AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared   
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 
)
UNION ALL
(
/**Tracked Referrals Closed**/              
SELECT 
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.ArchivedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, 1 AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared   
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 
WHERE Ref.ArchivedFlag = 1
)
UNION ALL
(
/**Tracked Referrals Closed with CreatedDate**/              
SELECT 
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, 1 AS TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared   
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 
WHERE Ref.ArchivedFlag = 1
)
UNION ALL
(
/**Tracked Referrals Closed Successfully**/              
SELECT 
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.ArchivedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, 1 AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared   
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 

LEFT JOIN
	(
	SELECT DISTINCT RA.ReferralId, 'Attended' AS Status, MIN(RA.AppointmentDate) AS AppointmentDate
	FROM nowpow.ReferralAppointment RA
		LEFT JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.ContactId = RA.CreatedByContactId
		LEFT JOIN nowpow.Referral as Ref
		ON Ref.Id = RA.ReferralId
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Ref.MakerPatientId = DimPatient.PatientId
	WHERE (RA.AppointmentStatusId = '2' AND RA.IsDeleted <> 1)
		/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
		AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		/*Exclude any activity related to the test patient*/
		AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	GROUP BY RA.ReferralId
		) RA
ON 	Ref.Id = RA.ReferralId
WHERE (RA.[Status] = 'Attended' OR Ref.ContactResultTypeId = 8) AND Ref.ArchivedFlag = 1 
)
UNION ALL
(
/**Tracked Referrals Closed Successfully with CreatedDate**/              
SELECT 
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, Ref.MakerSiteId AS SiteId
, CAST(Ref.CreatedDate as date) AS 'Date'                
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, 1 AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared   
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx  
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, Ref.MakerPatientId AS PatientId

FROM nowpow.Referral AS Ref
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
LEFT JOIN (
	SELECT
		Ref.*
		, CC.Id AS CareCoordinatorId
		, CC.SiteId
	FROM nowpow.Referral AS Ref
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	JOIN nowpow.CareCoordinator AS CC
	ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
) AS fullmatch
ON Ref.Id = fullmatch.Id
/* Use these join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.Contact AS Contact
ON Contact.Id = Ref.MakerContactId
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id) 

LEFT JOIN
	(
	SELECT DISTINCT RA.ReferralId, 'Attended' AS Status, MIN(RA.AppointmentDate) AS AppointmentDate
	FROM nowpow.ReferralAppointment RA
		LEFT JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.ContactId = RA.CreatedByContactId
		LEFT JOIN nowpow.Referral as Ref
		ON Ref.Id = RA.ReferralId
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Ref.MakerPatientId = DimPatient.PatientId
	WHERE (RA.AppointmentStatusId = '2' AND RA.IsDeleted <> 1)
		/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
		AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		/*Exclude any activity related to the test patient*/
		AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
	GROUP BY RA.ReferralId
		) RA
ON 	Ref.Id = RA.ReferralId
WHERE (RA.Status = 'Attended' OR Ref.ContactResultTypeId = 8) AND Ref.ArchivedFlag = 1 
)
UNION ALL
(
/**eRxShared**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS SiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, 1 AS eRxShared    
, NULL AS PoCeRx  
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral   
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*3&4 for eRx Nudged and 11 for eRx downloaded*/        
WHERE [PSI].[InteractionTypeId] in ('3','4','11')    
	AND PSI.IsDeleted <> 1
)
UNION ALL
(
/**Single Service Referral**/            
SELECT PSI.CareCoordinatorID AS CareCoordinatorId           
,  PSI.SiteID AS SiteId                     
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared 
, NULL AS PoCeRx 
, NULL AS ReferralsOnPoCErx    
, 1 AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*5&6 for Single Service Referral*/        
WHERE [PSI].[InteractionTypeId] in ('5','6')    
	AND PSI.IsDeleted <> 1 
)
UNION ALL
(
/**Login Activities**/            
SELECT
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, NULL AS SiteId
, CAST(Uact.CreatedDate as date) AS 'Date' 
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared 
, NULL AS PoCeRx 
, NULL AS ReferralsOnPoCErx    
, NULL AS SingleServiceReferral  
, 1 AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, NULL AS PatientId

FROM [log].[ExternalUserActivity] Uact
/* Append to Uact those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
left join (
	SELECT
		Uact.*
		, CC.Id AS CareCoordinatorId
		, S.Id AS SiteId
	FROM [log].[ExternalUserActivity] Uact
	JOIN nowpow.[site] AS S
	ON Uact.OrganizationId = S.OrganizationId
	JOIN nowpow.[user] U
	ON Uact.UserId = U.Id
	JOIN nowpow.Contact Contact
	ON Contact.Id = U.contactId
	JOIN nowpow.CareCoordinator CC
	ON CC.ContactId = Contact.Id and S.Id = CC.SiteId
	WHERE UAct.Activity = 0
) AS fullmatch
ON Uact.Id = fullmatch.Id
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
JOIN nowpow.[User] U
ON U.Id = Uact.UserId
JOIN nowpow.Contact Contact
ON Contact.Id = U.ContactId
/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.CareCoordinator CC1
ON CC1.ContactId = Contact.Id and CC1.Id = (SELECT MIN(Id) FROM nowpow.carecoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)
WHERE Uact.Activity = 0
)
UNION ALL
(
/**Searches**/            
SELECT lsc.CareCoordinatorId AS CareCoordinatorId           
,  lsc.SiteId AS SiteId
, CAST(lsc.CreatedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared 
, NULL AS PoCeRx   
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral     
, NULL AS Logins
, 1 AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, lsc.PatientId AS PatientId

/*SearchType 1 = Service Search*/
FROM log.SearchCriteria lsc
WHERE lsc.SearchType = 1  
AND lsc.CareCoordinatorId IS NOT NULL
)
UNION ALL
(
/**Screenings**/            
SELECT SR.CareCoordinatorId AS CareCoordinatorId           
, SR.SiteId AS SiteId
, CAST(SR.CompletedDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared     
, NULL AS PoCeRx  
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, 1 AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, NULL AS Nudges
, SR.PatientId AS PatientId

FROM nowpow.ScreeningResponse AS SR
WHERE SR.CompletedDate IS NOT NULL
)
UNION ALL
(
/**eRxLookups and ServicePrints**/            
SELECT 	
COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
, UAct.SiteId AS SiteId
, CAST(UAct.ActionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared     
, NULL AS PoCeRx  
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, CASE WHEN UAct.Action like '%view%' THEN 1 END AS eRxLookups
, CASE WHEN UAct.Action like '%print%' THEN 1 END AS ServicePrints
, NULL AS Nudges
, NULL AS PatientId

FROM [log].UserActivity AS UAct 
/* Append to Uact those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
LEFT JOIN (
	SELECT
		Uact.*
		, CC.Id AS CareCoordinatorId
	FROM [log].[UserActivity] AS Uact
	JOIN nowpow.[user] U
	ON Uact.UserId = U.Id
	JOIN nowpow.Contact Contact
	ON Contact.Id = U.contactId
	JOIN nowpow.CareCoordinator CC
	ON CC.ContactId = Contact.Id and Uact.SiteId = CC.SiteId
	WHERE UAct.Action like '%view%'
	OR UAct.Action like '%print%'
) AS fullmatch
ON Uact.Id = fullmatch.Id
/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
JOIN nowpow.[User] U
ON U.Id = Uact.UserId
JOIN nowpow.Contact Contact
ON Contact.Id = U.ContactId
/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
   has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
   to tie to the record.) */
JOIN nowpow.CareCoordinator AS CC1
ON CC1.ContactId = Contact.Id and CC1.Id = (select MIN(Id) from nowpow.carecoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)

WHERE UAct.Action like '%view%'
OR UAct.Action like '%print%'
)
UNION ALL
(
/**Nudges**/            
SELECT PSI.CareCoordinatorId AS CareCoordinatorId           
, PSI.SiteId AS SiteId
, CAST(PSI.InteractionDate as date) AS 'Date'                  
, NULL AS eRxCreated
, NULL AS ReferralsOnSharedErx
, NULL AS TrackedReferralsCreated
, NULL AS TrackedReferralsClosed
, NULL as TrackedReferralsClosed_CD
, NULL AS TrackedReferralsClosedSuccessfully
, NULL AS TrackedReferralsClosedSuccessfully_CD
, NULL AS eRxShared     
, NULL AS PoCeRx  
, NULL AS ReferralsOnPoCErx 
, NULL AS SingleServiceReferral  
, NULL AS Logins
, NULL AS Searches
, NULL AS Screenings
, NULL AS eRxLookups
, NULL AS ServicePrints
, 1 AS Nudges
, PSI.PatientId

FROM nowpow.PatientSiteInteraction AS PSI
/*1&2 for care provider email or text nudge*/        
WHERE [PSI].[InteractionTypeId] in ('1','2')  
	AND PSI.IsDeleted <> 1
)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
	LEFT JOIN nowpow.[Organization] as O
		JOIN nowpow.[Location] as LOC
		ON O.LocationId = LOC.Id
	ON DIMCC.OrgId = O.Id
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
	LEFT JOIN nowpow.[Organization] as OSite
		JOIN nowpow.[Location] as LOCSite
		ON OSite.LocationId = LOCSite.Id
	ON OSite.Id = DIMSite.OrgId
LEFT JOIN DIMPatient AS DimPatient
ON Raw.PatientId = DimPatient.PatientId

WHERE [raw].[Date] >= DATEADD(yy, DATEDIFF(yy, 0, @fromdate), 0) 
AND [raw].[Date] <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/

AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)

/*Exclude any activity related to the test patient*/

AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)

order by Raw.[Date]