--USE ALL
--go

/*
*Daily Report Query
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*v1.3: Days to Service and Days to Close added
*v1.4: Changed "rank() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]" to "row_number() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]" in the cteRA statement
	   For two records with the same value, "rank" classifies both records as [r] = "x" then classifies the next sequential record as [r] = "x+2". "Row_number()" avoids this by arbitrarily selecting a topmost record"
	   Added exclusion parameters for DIMCC.ExcludeFromReports and DIMPatient.TestPatient to the cteRA table creation --> this ensures that the earliest record selected is "eligible" for reporting purposes
	   (Else, we may encounter the case where the earliest ReferralAppointment record is tied to an "ExcludeFromReport" CareCoordinator or to a "TestPatient", and incorrectly removed from the final export with the
	   exclusion parameters set at final WHERE statement)
*v1.5: Revised date filtering to match ActivityReporting queries. Added Location table joins and ZipCode/State fields to the export.
*v1.6:  Modified JOIN and CareCoordinator logic in the inner Raw table to account for floating users, who have more than one CareCoordinatorId tied to the ContactId. This new logic allows us to tie the correct CarecoordinatorId (matched on UserId and SiteId)
		to the referral record, while also accounting for the situation in which a CareCoordinator has since been moved from the SiteId at which the activity was conducted. 
		Failure to use this join will result in duplicate row records for a single referral, where each referralid is tied incorrectly to each of the different CareCoordinatorIds assocaited with the ContactId logged to the referral. 
*/

DECLARE @fromdate date = '2016-10-01'
DECLARE @todate date = '2019-03-31'
;

WITH cteRA AS (
		/* Generate table with Tracked Referral Appointments Attended */
		SELECT 
			RA.[ReferralId] as ReferralId
			, RA.[AppointmentStatusId]
			, RA.[AppointmentDate]
			, row_number() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]
		FROM [nowpow].[ReferralAppointment] AS RA
		LEFT JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.ContactId = RA.CreatedByContactId
		LEFT JOIN nowpow.Referral as Ref
		ON Ref.Id = RA.ReferralId
		--LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		LEFT JOIN nowpow.Patient as PAT
		ON Ref.MakerPatientId = PAT.Id
		WHERE RA.AppointmentStatusId = 2 AND RA.IsDeleted <> 1
			/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
			AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
			/*Exclude any activity related to the test patient*/
			AND (PAT.LastName not like '%Zzztest%' AND PAT.LastName is not null)
		)
	,
	cte AS (
	/*Select the earliest appointment using the rank*/
		SELECT 
			cteRA.[ReferralId] as ReferralId
			, cteRA.[AppointmentStatusId]
			, cteRA.AppointmentDate AS AppointmentDate
		FROM cteRA
		WHERE [r] = 1
	)
	,
	cteContClose AS (
		SELECT Ref.Id, Ref.ContactedDate
		FROM nowpow.Referral Ref
		WHERE Ref.ContactResultTypeId = 8
		)

SELECT 
Raw.ReferralId
, CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
ELSE 'N/A'
END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS OrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR)
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgName
, Loc.[State] AS OrgState
, Loc.[PostalCode] AS PostalCode
, Raw.CareCoordinatorId
, Raw.SiteId
, Raw.Date
, Raw.ContactResultTypeId
, Raw.ArchivedFlag
, Raw.ApptDate
, Raw.DaysToService
, Raw.DaysToClose
, Raw.PatientId

FROM
(
SELECT  Raw.CareCoordinatorId
	, Raw.SiteId
	, Raw.Date
	, Raw.ContactResultTypeId
	, Raw.ReferralId
	, Raw.ArchivedFlag
	, Raw.ApptDate
	, CASE
		WHEN Raw.DaysToService < 0 THEN NULL
		ELSE Raw.DaysToService
	END AS DaysToService
	, CASE
		WHEN Raw.DaysToClose < 0 THEN NULL
		ELSE Raw.DaysToClose
	END AS DaysToClose
	, Raw.PatientId
FROM
	(
		SELECT 
		COALESCE(fullmatch.CareCoordinatorId, CC1.Id) AS CareCoordinatorId
		, Ref.MakerSiteId AS SiteId
		, CAST(Ref.CreatedDate as date) AS 'Date'
		, CAST(Ref.ArchivedDate as date) AS 'ArchivedDate'
		, Ref.ContactResultTypeId
		, Ref.Id as ReferralId
		, Ref.ArchivedFlag       
		, cte.AppointmentDate AS ApptDate         
		/*Calculate the days to the contacted and closed successful date*/
		, DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) as DaysToClose
		/*Calculate the days to service as specified in the introduction*/
		, CASE
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) IS NULL THEN NULL
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) <= DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) < DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
		END AS DaysToService
		, Ref.MakerPatientId AS PatientId
	FROM nowpow.Referral AS Ref
	/* Append to Ref those records that have a CareCoordinator match on both OrganizationId and UserId. Each CareCoordinatorId should be matched to a UNIQUE [ContactId and SiteId]. */
	LEFT JOIN (
		SELECT
			Ref.*
			, CC.Id AS CareCoordinatorId
			, CC.SiteId
		FROM nowpow.Referral AS Ref
		JOIN nowpow.Contact AS Contact
		ON Contact.Id = Ref.MakerContactId
		JOIN nowpow.CareCoordinator AS CC
		ON CC.ContactId = Contact.Id and Ref.MakerSiteId = CC.SiteId
	) AS fullmatch
	/* Use these inner joins to get only those records that were generated by CareCoordinators tied to the DB in question */
	ON Ref.Id = fullmatch.Id
	JOIN nowpow.Contact AS Contact
	ON Contact.Id = Ref.MakerContactId
	/* Use this join to return the earliest carecoordinator record match for the ContactId/UserId in a given row record. This join ensures that we match a CareCoordinatorId to the activity record even if the CareCoordinator
		has been moved Organizations, (i.e. where there are NO CareCoordinatorIds available in the DB on the day of the report run that match BOTH the activity record's UserId and the activity record's listed OrganizationId). 
	   Employ the second join condition to ensure that we match only ONE CareCoordinator match for a given activity record in the event that the user is floating. (The join condition below selects the earliest CareCoordinatorId record created
	   to tie to the record.) */
	JOIN nowpow.CareCoordinator AS CC1
	ON CC1.ContactId = Contact.Id AND CC1.Id = (SELECT MIN(Id) FROM nowpow.CareCoordinator AS CC2 WHERE CC2.ContactId = Contact.Id)
	/*Table Joins for filtering referrals begin*/
				LEFT JOIN cte
				ON Ref.Id = cte.ReferralId
				LEFT JOIN cteContClose
				ON Ref.Id = cteContClose.Id
	) AS Raw
	WHERE (Raw.DaysToService IS NOT NULL OR Raw.DaysToClose IS NOT NULL)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
	JOIN nowpow.[Organization] as O
		JOIN nowpow.[Location] as LOC
		ON O.LocationId = LOC.Id
	ON DIMCC.OrgId = O.Id
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
LEFT JOIN nowpow.Patient as PAT
ON Raw.PatientId = PAT.Id

WHERE [raw].[Date] >= DATEADD(yy, DATEDIFF(yy, 0, @fromdate), 0) 
AND [raw].[Date] <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
/*Exclude any activity related to the test patient*/
AND (PAT.LastName not like '%Zzztest%' OR PAT.LastName is null)

Order by referralid