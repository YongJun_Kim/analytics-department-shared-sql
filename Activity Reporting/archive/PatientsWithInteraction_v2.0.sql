--USE ALL
--go

/*
Count of Unique Patients with Interaction, by Year-Month
author: joanna.tung
version: 2.0


*/
DECLARE @fromdate date = '2018-06-01'
DECLARE @todate date = '2018-06-30'

;
with cte as (
 	select CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.[PatientId] AS Char) AS PatientID
	, PSI.PatientId as 'PSIPatientId'
	, PSI.Id
	, cast(PSI.InteractionDate as date) as InteractionDate
	, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, PSI.InteractionDate), 0) as date) as YearMonth
	, DIMCC.UserId
	, DIMSite.SiteId
	, DIMCC.CareCoordinatorId
	, DIMCC.ExcludeFromReports
	, DIMPatient.TestPatient
	, DIMSite.OrgId
	, DIMSite.OrgName
	, DIMSite.EnterpriseOrgId
	, DIMSite.EnterpriseOrgName
	, DIMSIte.NetworkOrgId
	, DIMSite.NetworkOrgName
	, LOC.[State] as OrgState
	, LOC.[PostalCode]
	, pPatient.PatientState
	, row_number() over (partition by CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PSI.[PatientId] AS Char), DATEADD(MONTH, DATEDIFF(MONTH, 0, PSI.InteractionDate), 0) order by PSI.InteractionDate) as [r]


	FROM [nowpow].[PatientSiteInteraction] AS PSI
		LEFT JOIN DIMCareCoordinator AS DIMCC
		ON PSI.CareCoordinatorId = DIMCC.CareCoordinatorID
		LEFT JOIN DIMSite AS DIMSite
		ON PSI.SiteId = DIMSite.SiteId
			LEFT JOIN nowpow.[Organization] as O
				LEFT JOIN nowpow.[Location] as LOC
				ON O.LocationId = LOC.Id
			ON DIMCC.OrgId = O.Id
		LEFT JOIN DIMPatient AS DimPatient
		ON PSI.PatientId = DimPatient.PatientId
		LEFT JOIN (
			Select pState.[Name] as PatientState, Patient.Id
			from nowpow.[Patient] as Patient 
			LEFT JOIN pristine.[State] as pState
			ON Patient.StateId = pState.Id
			) as pPatient	
		ON pPatient.Id = PSI.PatientId


	WHERE PSI.[InteractionDate] >= DATEADD(yy, DATEDIFF(yy, 0, @fromdate), 0) 
		AND PSI.[InteractionDate] <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
		AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
		AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
		AND PSI.PatientId IS NOT NULL
) 

SELECT *
	FROM cte
	WHERE PatientId is not NULL
	and [r] = 1