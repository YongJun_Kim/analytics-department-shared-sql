--USE N_NowPow-Prod
--go

/* 
This query is used to generate metrics for internal Activity reporting:
	- Count of Reports Downloaded by month
	- Total Number of Users Provisioned by month
	- Total Number of Enterprises
Revised from the userlist (version with database string field) and reportdownloads queries stored in Bitbucket.

UPDATES:
Replaced UserRole and OrgLicense mapping in the joins and moved it into the select statement
	-UserRole and OrgLicense are not a one-to-one match with User or Org, respectively
	-This enables us to map all roles and licenses into a single row record for a given contact id
Older user list metrics removed demo users through manual methods -- Added DemoDB flag for easy filtering
*/

DECLARE @todate date = '2018-04-30'

/* 
GENERAL NOTES:
CTE tables generate count of report downloads by the report month.
Count of report downloads, per month, are joined to the full contact list by contactid
This will result in duplicate counts of contactids because a given user/contact may have downloaded a report in more than one reporting month

USAGE:
MUST REMOVE DEMO DBs before usage to remove demo user activity.

To use the export for USERS provisioned in system reporting, you must count DISTINCT user/contact ids
	(This is also necessary as of 5/12/2018 because there is an error in the data which maps TWO site ids to ONE org id, causing the joins to return two row 
	records for Contact/User Ids in the export. must count distinct users for this reason)
To use the export for REPORT DOWNLOADS, count sum of reportdownloaded column by the ReptMonth. 
	Because of the multiple Site Ids mapped to Org Id issue, set pivot table to use distinct user/contacts 

Updates:

Date: 2018.05.31
author: joanna.tung

	- Replaced individual groupby statements by month with single CTE table that assigns a "YearMonth" field so that we can groupby both UserId and the relevant reporting month
*/
;
with cte as (
	select
		UA.UserId, COUNT(UA.ActionDate) as 'ReportDownloaded'
		, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, UA.ActionDate), 0) as date) as YearMonth
	FROM [log].[UserActivity] AS UA
	WHERE UA.[Action] like '%analytics%'
	GROUP BY UA.UserId, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, UA.ActionDate), 0) as date)
)

SELECT 
	C.Id AS ContactId
	, U.Id AS UserId
	, S.Id as SiteId
	, Org.[Name] AS OrgName
	, Org.Id AS OrgId
	, EntOrg.[Name] AS EnterpriseOrgName
	, CASE WHEN Org.IsNetwork = '1' THEN Org.[Name] ELSE NetOrg.[Name] END AS NetworkOrgName
	, U.Username AS Username
	, C.FirstName
	, C.LastName
	, C.Email AS Email
	, U.IsActive AS UserIsActive
	, C.IsDeleted AS ContactIsDeleted
	, u.ExcludeFromReports AS ExcludeFromReports
	, c.CreatedDate AS ContactCreatedDate
	, Loc.[State] AS OrgState
	, Loc.[PostalCode] AS PostalCode
	, COALESCE(
			SUBSTRING(
			(
				Select	', ' + cast(R.[Name] as varchar(2000))
				From	nowpow.UserRole UR 
						LEFT JOIN pristine.[Role] as R
						ON UR.RoleId = R.Id
				Where	UR.UserId = U.Id
				ORDER BY UR.RoleId
				For XML PATH ('')
			), 3, 2000),
			''
			) as 'Role Name'
	, COALESCE(
			SUBSTRING(
			(	
				Select ', ' + cast(L.[Name] as varchar(2000))
				From nowpow.organizationlicenserelation OLR
				LEFT JOIN nowpow.License L
				ON OLR.licenseId = L.Id
				WHERE OLR.OrganizationId = Org.Id
				ORDER BY OLR.LicenseId
				For XML PATH ('')
			), 3, 2000),
			''
			) as 'License'
	, Activity.LastLoginDate
	, cte.YearMonth
	, cte.ReportDownloaded AS ReportDownloaded
	, case when (C.Email LIKE '%nowpow%' OR U.Username LIKE '%nowpow%') then 'NP_flag' else 'Noflag' end as 'NowPowUserFlag'
	, substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1) AS 'DBName'
	, case when (substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1)) 
		in ('NowPow-Prod_CareOrgBase',
            'NowPow-Prod_CareOrgCareStopNYC',
            'NowPow-Prod_CareOrgDemo',             
            'NowPow-Prod_CareOrgDemoCT', 
            'NowPow-Prod_CareOrgDemoFamilyWellness',
            'NowPow-Prod_CareOrgDemoNJCares',
            'NowPow-Prod_CareOrgDemoNM_Pronto',
            'NowPow-Prod_CareOrgDemoUNC_HealthPartners',
            'NowPow-Prod_CareOrgNewYorkPinnacle',
            'NowPow-Prod_CareOrgNowPowOutreach',
            'NowPow-Prod_CareOrgQA',
            'NowPow-Prod_Taker_Demo')
		then 'DemoDB' 
		when (substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1)) IS NULL then 'NoDB'
		ELSE 'NotDemoDB' END AS 'DemoDBFlag'
FROM
	nowpow.Contact AS C
		JOIN nowpow.[User] AS U
			LEFT JOIN CTE 
			ON u.Id = Cte.UserId
			LEFT JOIN (
			SELECT	Uact.UserId,
					MAX([UAct].[CreatedDate]) AS 'LastLoginDate' 
			FROM	[log].[ExternalUserActivity] as UAct
			Group By UAct.UserId
			) as Activity
		ON Activity.UserId = U.Id
	ON U.ContactId = C.Id

	LEFT JOIN nowpow.OrganizationContact AS OC
	ON C.Id = OC.ContactId
	LEFT JOIN nowpow.[Organization] as Org
	ON OC.OrganizationId = Org.Id
	LEFT JOIN nowpow.[Site] as S
	ON S.OrganizationId = Org.Id
	LEFT JOIN nowpow.CareOrganizationDatabaseSiteRelation codsr
	on s.Id = codsr.SiteId
	LEFT JOIN nowpow.CareOrganizationDatabase cod
	on codsr.CareOrganizationDatabaseId = cod.Id
	LEFT JOIN nowpow.[Location] as Loc
	ON Org.LocationId = Loc.Id
	LEFT JOIN nowpow.[Organization] as EntOrg
	ON Org.EnterpriseOrganizationId = EntOrg.Id
	LEFT JOIN nowpow.[Organization] as NetOrg 
	ON EntOrg.NetworkOrganizationId = NetOrg.Id

/* joins for getting license of organization, add as needed */
	--LEFT JOIN nowpow.organizationlicenserelation olr 
	--	LEFT JOIN nowpow.license l 
	--	on olr.licenseid=l.id
	--on Org.id=olr.organizationid


	
/* as needed: filter parameters for all users in the system. NP flag field in the export achieves the same. */
WHERE  c.CreatedDate <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
	--C.Email NOT LIKE '%nowpow%'
	--AND U.Username NOT LIKE '%nowpow%'

/* excludefromreports filter not used here: this include admin users who frequently download reports */

ORDER BY C.Id