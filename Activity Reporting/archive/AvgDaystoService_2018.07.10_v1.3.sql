--USE ALL
--go

/*
*Daily Report Query
*
*1/25/2018
*v1.0
*v1.1: POC Excluded, Referrals on erx -> Referrals on shared erx, Total Referrals -> Total Shared Referrals
*v1.2: Referrals On Erx that were shared were fixed to use PSI Date, CC, and patientID
*v1.3: Days to Service and Days to Close added
*v1.4: Changed "rank() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]" to "row_number() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]" in the cteRA statement
	   For two records with the same value, "rank" classifies both records as [r] = "x" then classifies the next sequential record as [r] = "x+2". "Row_number()" avoids this by arbitrarily selecting a topmost record"
	   Added exclusion parameters for DIMCC.ExcludeFromReports and DIMPatient.TestPatient to the cteRA table creation --> this ensures that the earliest record selected is "eligible" for reporting purposes
	   (Else, we may encounter the case where the earliest ReferralAppointment record is tied to an "ExcludeFromReport" CareCoordinator or to a "TestPatient", and incorrectly removed from the final export with the
	   exclusion parameters set at final WHERE statement)
*v1.5: Revised date filtering to match ActivityReporting queries. Added Location table joins and ZipCode/State fields to the export.
*/
DECLARE @fromdate date = '2016-06-01'
DECLARE @todate date = '2018-06-30'
;

WITH cteRA AS (
		/* Generate table with Tracked Referral Appointments Attended */
		SELECT 
			RA.[ReferralId] as ReferralId
			, RA.[AppointmentStatusId]
			, RA.[AppointmentDate]
			, row_number() over (partition by RA.ReferralId order by RA.AppointmentDate) as [r]
		FROM [nowpow].[ReferralAppointment] AS RA
		LEFT JOIN [dbo].[DIMCareCoordinator] AS DIMCC
		ON DIMCC.ContactId = RA.CreatedByContactId
		LEFT JOIN nowpow.Referral as Ref
		ON Ref.Id = RA.ReferralId
		LEFT JOIN [dbo].[DIMPatient] AS DimPatient
		ON Ref.MakerPatientId = DimPatient.PatientId
		WHERE RA.AppointmentStatusId = 2 AND RA.IsDeleted <> 1
			/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
			AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
			/*Exclude any activity related to the test patient*/
			AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)
		)
	,
	cte AS (
	/*Select the earliest appointment using the rank*/
		SELECT 
			cteRA.[ReferralId] as ReferralId
			, cteRA.[AppointmentStatusId]
			, cteRA.AppointmentDate AS AppointmentDate
		FROM cteRA
		WHERE [r] = 1
	)
	,
	cteContClose AS (
		SELECT Ref.Id, Ref.ContactedDate
		FROM nowpow.Referral Ref
		WHERE Ref.ContactResultTypeId = 8
		)

SELECT 
 CASE 
WHEN Raw.CareCoordinatorID IS NULL THEN 1
ELSE 0
END AS Blank
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.UserId AS VARCHAR)
ELSE 'N/A'
END AS UserId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.FullName AS VARCHAR)
ELSE 'N/A'
END AS UserName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.SiteId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(Raw.SiteId AS VARCHAR)
ELSE 'N/A'
END AS SiteId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgId AS VARCHAR)
ELSE 'N/A'
END AS OrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.OrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.OrgName AS VARCHAR)
ELSE 'N/A'
END AS OrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgId AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.EnterpriseOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.EnterpriseOrgName AS VARCHAR)
ELSE 'N/A'
END AS EnterpriseOrgName
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgId AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgId AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgId
, CASE 
WHEN Raw.CareCoordinatorID IS NOT NULL THEN CAST(DIMCC.NetworkOrgName AS VARCHAR)
WHEN Raw.SiteId IS NOT NULL THEN CAST(DIMSite.NetworkOrgName AS VARCHAR)
ELSE 'N/A'
END AS NetworkOrgName
, Loc.[State] AS OrgState
, Loc.[PostalCode] AS PostalCode
, Raw.*

FROM
(
SELECT  Raw.CareCoordinatorId
	, Raw.SiteId
	, Raw.Date
	, Raw.ContactResultTypeId
	, Raw.ArchivedFlag
	, Raw.ApptDate
	, CASE
		WHEN Raw.DaysToService < 0 THEN NULL
		ELSE Raw.DaysToService
	END AS DaysToService
	, CASE
		WHEN Raw.DaysToClose < 0 THEN NULL
		ELSE Raw.DaysToClose
	END AS DaysToClose
	, Raw.PatientId
FROM
	(
		SELECT CC.Id AS CareCoordinatorId
		, Ref.MakerSiteId AS SiteId
		, CAST(Ref.CreatedDate as date) AS 'Date'
		, CAST(Ref.ArchivedDate as date) AS 'ArchivedDate'
		, Ref.ContactResultTypeId
		, Ref.ArchivedFlag       
		, cte.AppointmentDate AS ApptDate         
		/*Calculate the days to the contacted and closed successful date*/
		, DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) as DaysToClose
		/*Calculate the days to service as specified in the introduction*/
		, CASE
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) IS NOT NULL THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) IS NULL AND DATEDIFF(day, Ref.CreatedDate, Ref.ArchivedDate) IS NULL THEN NULL
			WHEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) <= DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) THEN DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate)
			WHEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate) < DATEDIFF(day, Ref.CreatedDate, cteContClose.ContactedDate) THEN DATEDIFF(day, Ref.CreatedDate, cte.AppointmentDate)
		END AS DaysToService
		, Ref.MakerPatientId AS PatientId
	FROM nowpow.CareCoordinator CC
		INNER JOIN nowpow.Contact Contact
		ON CC.ContactId = Contact.Id
			INNER JOIN nowpow.Referral AS Ref
			ON Contact.Id = Ref.MakerContactId	
	/*Table Joins for filtering referrals begin*/
				LEFT JOIN cte
				ON Ref.Id = cte.ReferralId
				LEFT JOIN cteContClose
				ON Ref.Id = cteContClose.Id
	) AS Raw
	WHERE (Raw.DaysToService IS NOT NULL OR Raw.DaysToClose IS NOT NULL)
) Raw
LEFT JOIN DIMCareCoordinator AS DIMCC
	JOIN nowpow.[Organization] as O
		JOIN nowpow.[Location] as LOC
		ON O.LocationId = LOC.Id
	ON DIMCC.OrgId = O.Id
ON Raw.CareCoordinatorId = DIMCC.CareCoordinatorID
LEFT JOIN DIMSite AS DIMSite
ON Raw.SiteId = DIMSite.SiteId
LEFT JOIN DIMPatient AS DimPatient
ON Raw.PatientId = DimPatient.PatientId

WHERE [raw].[Date] >= DATEADD(yy, DATEDIFF(yy, 0, @fromdate), 0) 
AND [raw].[Date] <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
/*Exclude any activity related to the care coordinator with ExcludeFromReport Attribute True*/
AND (DIMCC.ExcludeFromReports <> 1 OR DIMCC.ExcludeFromReports IS NULL)
/*Exclude any activity related to the test patient*/
AND (DimPatient.TestPatient = 'False' OR DimPatient.TestPatient IS NULL)