--USE ALL
--GO

/*# of People in System. Aggregate As Needed.*/	

/* For Executive Dashboard, Benefit Chicago, At-A-Glance */
/* Count of Number of People (Patients) in System*/

/* Update by joanna.tung: Added DB Name to PatientId to distinguish counts */
	
--DECLARE @fromdate datetime,
--		@todate datetime;


SELECT	COUNT(CAST(DB_ID(DB_NAME()) AS CHAR) + ';' + CAST(PAT.[Id] AS Char)) AS CountPatientId
		,pState.[Name] AS STATE
		,CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, PAT.CreatedDate), 0) AS DATE) AS CreatedDateYearMonth

FROM nowpow.Patient AS PAT
	LEFT JOIN pristine.[State] AS pState
	ON pState.Id = PAT.StateId
			
WHERE [PAT].[LASTNAME] NOT LIKE '%Zzztest%' 
	--[PAT].[LASTNAME] LIKE 'Zzztest%' is the condition used in the Design of the DIMPatient table to trigger the "Test Patient" flag
	--AND (DimPatient.TestPatient = 'False') --OR DimPatient.TestPatient IS NULL)		
	
GROUP BY pState.[Name], CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, PAT.CreatedDate), 0) AS DATE)