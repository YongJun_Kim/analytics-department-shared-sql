--USE N_NowPow-Prod
--go

--DECLARE @fromdate date = '2018-06-01'
DECLARE @todate date = '2018-12-31'

/* Create CTE table with count of Successful Logins by UserId and YearMonth */

;
with cte as (
	select
		Uact.UserId
		, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, Uact.CreatedDate), 0) as date) as YearMonth
		, cast(sum(CASE WHEN Uact.Activity = 0 THEN 1 ELSE 0 END) as int) AS SuccessfulLogins
	from [log].[ExternalUserActivity] as Uact
	group by Uact.UserId, cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, Uact.CreatedDate), 0) as date)
)

select 
	C.Id as ContactId
	, U.Id as UserId
	, S.Id as SiteId
	, Org.[Name] as OrgName
	, EntOrg.[Name] AS EnterpriseOrgName
	, CASE WHEN Org.IsNetwork = '1' THEN Org.[Name] ELSE NetOrg.[Name] END AS NetworkOrgName
	, U.Username as Username
	, U.IsActive as UserIsActive
	, C.IsDeleted as ContactIsDeleted
	, u.ExcludeFromReports as ExcludeFromReports
	, case when cte.SuccessfulLogins >= 11.5 THEN 'Active'
		when (cte.SuccessfulLogins < 11.5 AND cte.SuccessfulLogins >= 3.5) THEN 'Moderate'
		when (cte.SuccessfulLogins < 3.5 AND cte.SuccessfulLogins >= 0.5) THEN 'Infrequent'
		when (cte.SuccessfulLogins < 0.5 OR cte.SuccessfulLogins is NULL) THEN 'Inactive'
		end as UserGroupbyActiveStatus
	, c.CreatedDate as ContactCreatedDate
	, Loc.[State] as OrgState
	, Loc.[PostalCode] as PostalCode
	, cte.YearMonth
	--, datepart(month, @fromdate) AS ReptMonth
	, cod.DatabaseName as 'DBName'
	, case when (C.Email LIKE '%nowpow%' OR U.Username LIKE '%nowpow%') then 'NP_flag' else 'Noflag' end as 'NowPowUserFlag'
	, case when cod.DatabaseName in ('NowPow-Prod_CareOrgBase',
            'NowPow-Prod_CareOrgCareStopNYC',
            'NowPow-Prod_CareOrgDemo',             
            'NowPow-Prod_CareOrgDemoCT', 
            'NowPow-Prod_CareOrgDemoFamilyWellness',
            'NowPow-Prod_CareOrgDemoNJCares',
            'NowPow-Prod_CareOrgDemoNM_Pronto',
            'NowPow-Prod_CareOrgDemoUNC_HealthPartners',
            'NowPow-Prod_CareOrgNewYorkPinnacle',
            'NowPow-Prod_CareOrgNowPowOutreach',
            'NowPow-Prod_CareOrgQA',
            'NowPow-Prod_Taker_Demo',
			'NowPow-Prod_CareOrgDEMOMS',
			'N_NowPow-Prod_CareOrgAllina_Copy',
			'N_NowPow-Prod_CareOrgAdvocate_Demo')
		then 'DemoDB' 
		when cod.DatabaseName IS NULL then 'NoDB'
		ELSE 'NotDemoDB' END AS 'DemoDBFlag'	
	--, case when (substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1)) 
	--	in ('NowPow-Prod_CareOrgBase',
 --           'NowPow-Prod_CareOrgCareStopNYC',
 --           'NowPow-Prod_CareOrgDemo',             
 --           'NowPow-Prod_CareOrgDemoCT', 
 --           'NowPow-Prod_CareOrgDemoFamilyWellness',
 --           'NowPow-Prod_CareOrgDemoNJCares',
 --           'NowPow-Prod_CareOrgDemoNM_Pronto',
 --           'NowPow-Prod_CareOrgDemoUNC_HealthPartners',
 --           'NowPow-Prod_CareOrgNewYorkPinnacle',
 --           'NowPow-Prod_CareOrgNowPowOutreach',
 --           'NowPow-Prod_CareOrgQA',
 --           'NowPow-Prod_Taker_Demo',
	--		'NowPow-Prod_CareOrgDEMOMS',
	--		'N_NowPow-Prod_CareOrgAllina_Copy',
	--		'N_NowPow-Prod_CareOrgAdvocate_Demo')
	--	then 'DemoDB' 
	--	when (substring(ConnectionString, charindex('NowPow-Prod_', ConnectionString), charindex('Persist', ConnectionString) - charindex('NowPow-Prod_', ConnectionString) -1)) IS NULL then 'NoDB'
	--	ELSE 'NotDemoDB' END AS 'DemoDBFlag'
		, COD.IsDemo 
		, COD.IsLive

from cte
	LEFT JOIN nowpow.[User] as U
	ON U.Id = cte.UserId
	LEFT JOIN nowpow.[Contact] as C
	ON U.ContactId = C.Id
	LEFT JOIN nowpow.OrganizationContact OC
	ON C.Id = OC.ContactId
	LEFT JOIN nowpow.[Organization] as Org
	ON Org.Id = OC.OrganizationId
	LEFT JOIN nowpow.[Site] as S
	ON S.OrganizationId = Org.Id
	LEFT JOIN nowpow.CareOrganizationDatabaseSiteRelation codsr
	on s.Id = codsr.SiteId
	LEFT JOIN nowpow.CareOrganizationDatabase cod
	on codsr.CareOrganizationDatabaseId = cod.Id
	LEFT JOIN nowpow.[Organization] as EntOrg
	ON Org.EnterpriseOrganizationId = EntOrg.Id
	LEFT JOIN nowpow.[Organization] as NetOrg 
	ON EntOrg.NetworkOrganizationId = NetOrg.Id
	LEFT JOIN nowpow.[Location] as Loc
	ON Org.LocationId = Loc.Id

WHERE (U.ExcludeFromReports <> 1 OR U.ExcludeFromReports IS NULL)
	/* Set date to end of reporting period to pull all users created in the system throughout the reporting period ONLY */
	AND c.CreatedDate <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@todate)+1,0))
	AND cte.SuccessfulLogins <> 0
order by UserGroupbyActiveStatus