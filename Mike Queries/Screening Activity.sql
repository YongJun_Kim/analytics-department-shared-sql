select --top 10
		SR.Id as ScreeningId,
		SR.CareCoordinatorId,
		C.Email,
		SR.PrescriptionId,
		SR.StartedDate,
		SR.CompletedDate,
		SR.SiteId,
		CASE 
			WHEN SR.Notes like 'Flowsheet sent from Epic' THEN 'Epic Flowsheet' 
			ELSE 'Manual Screening' 
		END AS Source,
		CASE 
			WHEN CompletedDate IS NULL THEN 'Not Completed'
			ELSE 'Completed'
		END as 'Completion Status',

		CASE
			WHEN Results like '%Patient has been identified for social resources%' THEN 'Should Gen'
			ELSE 'Should Not Gen'
		END AS 'Should Generate ERX',
		CASE 
			WHEN PrescriptionId IS NOT NULL THEN 'Did Gen'
			ELSE 'Did Not Gen'
		END AS 'Did Generate ERX'
		--,*
from	nowpow.ScreeningResponse SR with (NOLOCK)
			LEFT JOIN nowpow.CareCoordinator CC with (NOLOCK) 
				LEFT JOIN nowpow.Contact C --with (NOLOCK)
				ON CC.ContactId = C.Id
			ON CC.Id = SR.CareCoordinatorId