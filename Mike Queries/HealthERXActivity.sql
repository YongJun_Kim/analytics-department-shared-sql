select	P.Id as PrescriptionId,
		P.PatientId,
		P.CreatedDate,
		P.CareCoordinatorId,
		C.Email,
		Case 
			WHEN PrescriptionSourceTypeId = 1 THEN 'Point of Care'
			WHEN PrescriptionSourceTypeId = 2 THEN 'Automatic'
			WHEN PrescriptionSourceTypeId = 3 THEN 'Manual'
			WHEN PrescriptionSourceTypeId = 4 THEN 'Screening'
			ELSE NULL
		END as PrescriptionSource,
		CASE 
			WHEN SR.Notes like 'Flowsheet sent from Epic' THEN 'Epic Flowsheet' 
			WHEN SR.ID IS NOT NULL THEN 'Manual Screening' 
			ELSE NULL
		END AS ScreeningType
		--,P.*
from	log.prescription P with (nolock)
			LEFT JOIN nowpow.CareCoordinator CC with (NOLOCK) 
				LEFT JOIN nowpow.Contact C --with (NOLOCK)
				ON CC.ContactId = C.Id
			ON CC.Id = P.CareCoordinatorId

		LEFT JOIN nowpow.ScreeningResponse SR with (NOLOCK) 
		ON P.Id = SR.PrescriptionId