--USE ALL
--GO

;
WITH CTE (UserId, LastLoginDate)
AS 
(
	SELECT	Uact.UserId,
			MAX([UAct].[CreatedDate]) AS 'LastLoginDate' 
	FROM	[log].[ExternalUserActivity] UAct  
	GROUP BY 
			UAct.UserId          
)

select	c.ID as ContactId, 
		U.Id as UserId, 
		O.Id as OrganizationId,
		COALESCE(NET.Name, COALESCE(ENT_NET.Name, ' - ')) as 'Network Name',
		COALESCE(ENT.Name, ' - ') as 'Enterprise Name',
		s.id as siteid, 
		CASE WHEN o.IsNetwork is null OR o.IsNetwork = 0 THEN 'No' ELSE 'Yes' END as 'Is Network?',
		CASE WHEN o.IsEnterprise is null OR o.IsEnterprise = 0 THEN 'No' ELSE 'Yes' END as 'Is Enterprise?',
		o.Name as OrgName,
		concat(loc.number, loc.fraction, ' ', loc.Street, ' ', loc.Unit) as 'Address',
		loc.City,
		loc.[State],
		loc.PostalCode, 
		c.LastName,
		c.FirstName,
		c.Email,
		u.UserName,
		U.IsActive as UserIsActive, 
		C.IsDeleted as ContactIsDeleted,
		r.Name as 'Role Name',
		l.Name as 'License',
		cast(C.CreatedDate as date) as DateAdded,
		cast (CTE.LastLoginDate as date) as LastLoginDate,
		u.ExcludeFromReports
from	nowpow.contact C 
			JOIN nowpow.[user] u 
				LEFT JOIN CTE 
				ON u.Id = Cte.UserId

				ON u.ContactId = C.ID
				--AND (U.Username like 'PowRx_NN%' OR U.Username like 'SA_NN%')
			JOIN nowpow.UserRole ur
			    ON u.Id = ur.UserId

			JOIN pristine.[Role] r
			    ON ur.RoleId = r.Id

			LEFT JOIN nowpow.OrganizationContact OC 
				JOIN nowpow.Organization O 
					
					LEFT JOIN nowpow.site s 
					on s.organizationid=o.id
					
					LEFT JOIN nowpow.[Location] loc
					on O.LocationId = loc.Id

					LEFT JOIN nowpow.organizationlicenserelation olr 
						LEFT JOIN nowpow.license l 
						on olr.licenseid=l.id
					on o.id=olr.organizationid

					LEFT JOIN nowpow.Organization ENT 
						LEFT JOIN nowpow.Organization ENT_NET 
						ON ENT.NetworkOrganizationid = ENT_NET.ID
					ON O.enterpriseorganizationid = ENT.ID

					LEFT JOIN nowpow.Organization NET 
					ON O.NetworkOrganizationid = NET.ID

				ON O.Id = OC.OrganizationId
			ON C.Id = OC.ContactId
--WHERE 
--U.ID not in ( --Exclusions from CumUsers - not all Amy's users)
--946	,962	,965	,967	,969	,971	,973	,975	,977	,1252	,1255	,4312	,4385	,1950	,1950	,1950	,1559	,1559	,1559	,1559	,1560	,1560	,5234	,3644	,3644	,3644	,1947	,1947	,1947	,1312	,1312	,1312	,1566	,1566	,1556	,1556	,1557	,1557	,1567	,1567	,1558	,1558	,1564	,1564	,1949	,1949	,1949	,1320	,1320	,290	,290	,291	,291	,292	,292	,668	,668	,1022	,1022	,5513	,5514	,5508	,5512	,5515	,5516	,4981	,1405	,1458	,1431	,1391	,1433	,1483	,4746	,4744	,4745	,4301	,4301	,4302	,4302	,4130	,4129	,4902	,1321	,1321	,1321	,1321	,1122	,1122	,1122	,1122	,1161	,1161	,1161	,1071	,1071	,1071	,1072	,1072	,1072	,1072	,667	,4765	,4753	,4757	,4767	,4771	,4750	,4758	,4759	,4763	,4754	,4772	,4772	,5656	,4770	,4748	,4755	,4774	,4769	,4747	,4756	,4773	,4752	,4764	,4751	,4869	,4749	,2417	,756	,1355	,2385	,1575	,4307	,4308	,1118	,1571	,845	,845	,845	,845	,4080	,4127	,4126	,1572	,1573	,1574	,1568	,4722	,4715	,4709	,918	,1934	,1934	,2418	,5434	,1026	,1341	,4312	,4385	,4385	,4385	,846	,4188	,4189	,4190	,4193	,4194	,4186	,4187	,1953	,1954	,4236	,4236	,4236	,4236	,4967	,1318	,4191	,4192	,4197	,4205	,4201	,4195	,4196	,4137	,1108	,1216	,1216	,5648	,5648	,1214	,1214	,1215	,1215	,712	,712	,4661	,2384	,5655	,1397	,1554	,1554	,5511	,1555	,3647	,4945	,1344	,1344	,1344	,3648	,2346	,2347	,2000	,2001	,1994	,1995	,2004	,2005	,1992	,1993	,1996	,1997	,1998	,1999	,2002	,2003	,1113	,1113	,1951	,1951	,1951	,1951	,1951	,1951	,706	,706	,706	,1486	,3637	,3641	,3642	,3638	,3712	,3634	,3643	,920	,920	,830	,1114	,1116	,1287	,1451	,793	,1117	,1482	,954	,4313	,919	,4897	,5176	,4896	,4300	,4901	,4876	,1252	,969	,971	,965	,975	,967	,962	,931	,931	,931	,931	,769	,769	,769	,1107	,1317	,973	,977	,946	,2136	,2135	,2134	,2133	,2023	,4311	,4310	,1115	,1051	,1119	,5658	,5657	,4142	,4170	,4171	,4139	,5492	,4167	,4140	,4169	,4138	,5664	,4141	,4168	,4173	,1255	,5236	,4077	,4078	,1011	,794	,1286	,4299	,4136	,4134	,4134	,4154	,4154	,4208	,4208	,5420	,5415	,5433	,5409	,5427	,5413	,5425	,5416	,5450	,5406	,5414	,5428	,5489	,5410	,5432	,5412	,5447	,5430	,5429	,5424	,5411	,5407	,5423	,4306	,5421	,4304	,5490	,5488	,5487	,5491	,5453	,5426	,4303	,5449	,5659	,5422	,4305	,5485	,5486	,5452	,5408	,1334	,1372	,4743	,92	,184	,242	,243	,244	,245	,246	,248	,250	,251	,252	,254	,255	,256	,257	,258	,259	,260	,261	,357	,358	,389	,604	,611	,675	,676	,707	,760	,928	,929	,930	,947	,1024	,1254	,1488	,1948	,1952	,2066	,2120	,2121	,4658	,4892	,5154	,4553	,4557	,4561	,4648	,4649	,4650	,4651	,4652	,5226
--)

--Ent.Name IS NOT NULL
----AND u.IsActive = 1
--AND Ent.Name not like '%Wellness%'
--AND Ent.Name not like '%CareStop%'
--AND Ent.Name not like '%H3%'
--AND Ent.Name not like '%Northwestern%'
--AND Ent.Name not like '%Corner%'
--AND c.FirstName not like '%Account%'
order by 
	COALESCE(NET.Name, COALESCE(ENT_NET.Name, ' - ')) DESC, 
	ENT.Name, 
	O.IsNetwork DESC, 
	O.IsEnterprise DESC, 
	O.Name
