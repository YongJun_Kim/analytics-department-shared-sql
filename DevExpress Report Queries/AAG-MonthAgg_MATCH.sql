/*
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-12-31' 
DECLARE @OrgParam AS INT  = 1085389

;
WITH 

Dummydates AS (
	SELECT  
		
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalReferrals
		, YearMonthNumber AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue

	UNION ALL

	SELECT
		'YTD Total' AS 'DateHeader'
		, 'N/A' AS TotalReferrals
		, '14' AS sortkey

	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS TotalReferrals
		, '13' AS sortkey
)
,

Base AS (
	SELECT 
		
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, 'Dummy' AS Dummy
		, MonthLastDay
		, SUM(vfactStandardMetricsAAGCombinedMetrics.TotalReferrals) AS TotalReferrals		
		, dimDate.YearMonthNumber AS sortkey
		, ROW_NUMBER() OVER (ORDER BY dimDate.MonthLastDay DESC) AS [rPeriod]

	FROM vfact.StandardMetricsAAGCombinedMetrics AS vfactStandardMetricsAAGCombinedMetrics
		JOIN vdim.[Date] as dimDate
		ON dimDate.DateKey = vfactStandardMetricsAAGCombinedMetrics.DateKey
	WHERE dimDate.MonthLastDay > DATEADD(DAY, 0, @StartDate) AND dimDate.MonthLastDay <= DATEADD(DAY, 0, @EndDate)
		AND OrganizationId in (@orgparam)
	
	GROUP BY dimDate.MonthLastDay, dimDate.YearMonthNumber	
)
,
AddDelta AS (
	SELECT 
		'% Change' AS 'DateHeader',
		'13' AS sortkey, 
		/* Create conditions to account for situations in which:
			1) records may not exist for this week and/or last week 
				- Because we use a full outer join (see below), the resulting table returns "NULL" values for any week during which searches were not conducted.
			2) where divisor may be zero (records of searches do not exist for "last week"). */
		ThisMonth.TotalReferrals AS TTotalReferrals
		, PriorMonth.TotalReferrals AS PTotalReferrals,
		CASE 

			/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisMonth.TotalReferrals IS NOT NULL and PriorMonth.TotalReferrals IS NOT NULL and PriorMonth.TotalReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TotalReferrals) - CONVERT(FLOAT,PriorMonth.TotalReferrals))/CONVERT(FLOAT,PriorMonth.TotalReferrals),0)

			/* This second condition addresses the situation in which there are no records of searches for this week (ThisMonth.DateHeader is null), and
			   last week's values are neither null nor zero. */ 

			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TotalReferrals IS NOT NULL AND PriorMonth.TotalReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorMonth.TotalReferrals))/CONVERT(FLOAT, PriorMonth.TotalReferrals),0)

			/* Use condition "else NULL" for situations in which divisor is zero */
			ELSE NULL END AS TotalReferrals

	FROM (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy

)
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDelta.TotalReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.TotalReferrals) + '%' END AS 'TotalReferrals'
	FROM AddDelta
)

SELECT 
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TotalReferrals, DummyDates.TotalReferrals) AS 'TotalReferrals'
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey
FROM (
SELECT 
	 COALESCE(Base.DateHeader, YearSum.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
	, COALESCE(CAST(Base.TotalReferrals AS VARCHAR), CAST(YearSum.TotalReferrals AS VARCHAR), AddFormat.TotalReferrals) AS TotalReferrals
	, COALESCE(Base.sortkey, YearSum.sortkey, AddFormat.sortkey) AS sortkey
FROM Base
FULL OUTER JOIN (
	SELECT 
		'YTD Total' AS DateHeader
		, '14' AS sortkey
		, SUM(TotalReferrals) AS TotalReferrals
	FROM Base
	) AS YearSum
ON Base.DateHeader = YearSum.DateHeader
FULL OUTER JOIN AddFormat
ON Base.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON Biggie.DateHeader = DummyDates.DateHeader
ORDER BY sortkey