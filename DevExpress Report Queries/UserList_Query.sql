-- Log in as dwUser_Demo to run with this OrgParam

DECLARE @startdate AS DATETIME = '2018-08-04' 
DECLARE @enddate AS DATETIME = '2019-03-17'
DECLARE @orgparam AS INT = 1085389                                         
; 


SELECT 
u.UserId, 
cc.EnterpriseOrganizationName as 'Enterprise Name'
, cc.OrganizationName as 'Organization Name'
, cc.OrganizationId as 'Organization Id'
, u.LastName as 'Last Name'
, u.FirstName as 'First Name'
, u.UserName
, CASE u.ExcludeFromReports WHEN 1 THEN 'Yes' ELSE 'No' END as 'Exclude From Reports'
, CASE WHEN u.Department is NULL or u.Department LIKE 'NULL' THEN '' ELSE u.Department END as Department   -- format empty string instead of NULL
, CASE WHEN u.Title IS NULL or u.Title LIKE 'NULL' THEN '' ELSE u.Title END as Title          -- format empty string instead of NULL
, CASE WHEN ur.RoleId IS NULL THEN '' ELSE ur.RoleId END AS 'Role Id(s)'
, CASE WHEN ur.RoleName IS NULL THEN '' ELSE ur.RoleName END as 'Role Name(s)'
, CAST( u.CreatedDate AS date) as 'Date Added'-- truncate timestamp off 
, -- There are three cases for last login date:
  --       before start, between start and end, or after end.
  CASE WHEN (u.LastLoginDate > DATEADD(DAY, 1, @enddate)) THEN (     -- if the LastLoginDate from user table is after enddate
	COALESCE( CAST(                   
		(SELECT TOP (1) euact.[CreatedDate]           -- find most recent login date from extUserActivity that occurred before the enddate
		  FROM [vdim].[ExternalUserActivity] euact
		  WHERE euact.Activity = 0                  -- code 0 is login 
		  AND euact.UserId = u.UserId               -- for specific user
		  AND OrganizationId = cc.OrganizationId    -- for specific org
		  AND euact.CreatedDate <= DATEADD(DAY, 0, @enddate)          -- before endDate 
		  ORDER BY euact.CreatedDate desc)           -- grab the most recent logindate < enddate
		  AS Date) , ''))                              -- if there is no such date, format empty string instead of NULL  
    ELSE  COALESCE(CONVERT(varchar,u.LastLoginDate,23), '')
    END As 'Last Login Date' -- Otherwise, value from user table is before enddate (Or Is NULL), so project it

, CASE WHEN (ut.UserTagName) IS NULL THEN '' ELSE ut.UserTagName END AS  'User Tags'  -- format empty string instead of NULL
, CASE u.isActive WHEN 1 THEN 'Yes' ELSE 'No' END as 'Is Currently Active'
, CASE WHEN u.DeactivationDate IS NULL THEN NULL      -- If user not deactivated or not deactivated within date range, project empty string                   
       WHEN u.DeactivationDate > '0001-01-01'       -- If user deactivated within known date range, then project actual Deactivation date
	        THEN CAST(u.DeactivationDate as Date)
								 -- If user deactivated before implementation of slowly changing dimensions ( ETL '0001-01-01' placeholder)
       ELSE Cast('0001-01-01' as Date)         -- then project 'before 2018-10-12' (implementation date)
       END as 'DeactivationDate'

FROM vdim.[User] u   

LEFT JOIN vdim.CareCoordinator cc  
ON cc.UserId = u.UserId 

LEFT JOIN vdim.UserRoleCoalesced ur
ON u.UserId = ur.UserId
AND ur.OrganizationID = cc.OrganizationId
LEFT JOIN vdim.UserTagCoalesced ut
ON u.UserId = ut.UserId
WHERE 
EnterpriseOrganizationName IS NOT NULL
AND u.CreatedDate < DATEADD(DAY, 1, @enddate) 
AND cc.OrganizationId in (@orgparam)						            -- only pull users associated with org levels in param dropdown
AND u.UserName NOT LIKE 'xx%'											-- filter out known demo / support users and orgs
AND u.UserName NOT LIKE '%nowpow%'
AND cc.OrganizationName NOT LIKE '%Analytics Support'
ORDER BY cc.EnterpriseOrganizationName, cc.OrganizationName, u.LastName, u.FirstName

