/*
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-12-31' 
DECLARE @OrgParam AS INT = 1085389
;

WITH

/* The At-a-Glance report requires that all weeks in the current 4 week period appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all necessary weeks. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 4 weeks of the current 4 week period appear in the export. */

Dummydates AS (
	SELECT  
		
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS ScreeningsCompleted
		, CAST(0 AS VARCHAR) AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, CAST(0 AS VARCHAR) AS NeedsIdentified
		, YearMonthNumber AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue

	UNION ALL

	SELECT
		'YTD Total' AS 'DateHeader'
		, 'N/A' AS ScreeningsCompleted
		, 'N/A' AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, 'N/A' AS NeedsIdentified
		, '14' AS sortkey

	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS ScreeningsCompleted
		, 'N/A' AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, 'N/A' AS NeedsIdentified
		, '13' AS sortkey
)
,

/* This temp table returns aggregated metrics, by day of week. Query parameters are applied here to ensure that only the relevant
   row records are included in the aggregates. */

Base AS (
	SELECT
		dimDate.MonthLastDay
		, REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		,'Dummy' AS Dummy
		, SUM(vfactStandardMetricsPSI.ScreengingsCompleted) AS ScreeningsCompleted
		, SUM(vfactStandardMetricsPSI.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
		, SUM(vfactStandardMetricsPSI.NeedsIdentified) AS NeedsIdentified
		,CASE WHEN SUM(vfactStandardMetricsPSI.ScreengingsCompleted) > 0 THEN
		/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,SUM(ScreeningsIdentifyingNeed))/CONVERT(FLOAT,SUM(ScreengingsCompleted)),1))
			ELSE NULL END AS 'ScreeningsIdentifyingNeed_Perc'
		, dimDate.YearMonthNumber as sortkey
		, ROW_NUMBER() OVER (ORDER BY dimDate.MonthLastDay DESC) AS [rPeriod]

	FROM vfact.StandardMetricsPSI AS vfactStandardMetricsPSI
		JOIN vdim.[Date] as dimDate
		ON dimDate.DateKey = vfactStandardMetricsPSI.DateKey
	WHERE dimDate.MonthLastDay > DATEADD(DAY, 0, @StartDate) AND dimDate.MonthLastDay <= DATEADD(DAY, 0, @EndDate)
		AND OrganizationId in (@orgparam)
	
	GROUP BY dimDate.MonthLastDay, dimDate.YearMonthNumber
)
,
AddDelta AS (

	SELECT 
		'% Change' AS DateHeader,
		'13' AS sortkey,
		CASE 
			WHEN (ThisMonth.ScreeningsCompleted IS NOT NULL and PriorMonth.ScreeningsCompleted IS NOT NULL AND PriorMonth.ScreeningsCompleted <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ScreeningsCompleted - PriorMonth.ScreeningsCompleted))/CONVERT(FLOAT,PriorMonth.ScreeningsCompleted),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ScreeningsCompleted IS NOT NULL AND PriorMonth.ScreeningsCompleted <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorMonth.ScreeningsCompleted))/CONVERT(FLOAT, PriorMonth.ScreeningsCompleted),0)
			ELSE NULL END AS ScreeningsCompleted,

		CASE 
			WHEN (ThisMonth.ScreeningsIdentifyingNeed IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ScreeningsIdentifyingNeed - PriorMonth.ScreeningsIdentifyingNeed))/CONVERT(FLOAT,PriorMonth.ScreeningsIdentifyingNeed),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ScreeningsIdentifyingNeed IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ScreeningsIdentifyingNeed - PriorMonth.ScreeningsIdentifyingNeed))/CONVERT(FLOAT,PriorMonth.ScreeningsIdentifyingNeed),0)
			ELSE NULL END AS ScreeningsIdentifyingNeed,

		CASE 
			WHEN (ThisMonth.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ScreeningsIdentifyingNeed_Perc - PriorMonth.ScreeningsIdentifyingNeed_Perc))/CONVERT(FLOAT,PriorMonth.ScreeningsIdentifyingNeed_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorMonth.ScreeningsIdentifyingNeed_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ScreeningsIdentifyingNeed_Perc - PriorMonth.ScreeningsIdentifyingNeed_Perc))/CONVERT(FLOAT,PriorMonth.ScreeningsIdentifyingNeed_Perc),0)
			ELSE NULL END AS ScreeningsIdentifyingNeed_Perc,

		CASE 
			WHEN (ThisMonth.NeedsIdentified IS NOT NULL AND PriorMonth.NeedsIdentified IS NOT NULL AND PriorMonth.NeedsIdentified <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.NeedsIdentified - PriorMonth.NeedsIdentified))/CONVERT(FLOAT,PriorMonth.NeedsIdentified),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.NeedsIdentified IS NOT NULL AND PriorMonth.NeedsIdentified <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.NeedsIdentified - PriorMonth.NeedsIdentified))/CONVERT(FLOAT,PriorMonth.NeedsIdentified),0)
			ELSE NULL END AS NeedsIdentified

	FROM (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy

)
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDelta.ScreeningsCompleted IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ScreeningsCompleted) + '%' END AS 'ScreeningsCompleted'
		, CASE WHEN AddDelta.ScreeningsIdentifyingNeed IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ScreeningsIdentifyingNeed) + '%' END AS 'ScreeningsIdentifyingNeed'
		, CASE WHEN AddDelta.ScreeningsIdentifyingNeed_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ScreeningsIdentifyingNeed_Perc) + '%' END AS 'ScreeningsIdentifyingNeed_Perc'
		, CASE WHEN AddDelta.NeedsIdentified IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.NeedsIdentified) + '%' END AS 'NeedsIdentified'
	FROM AddDelta
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.ScreeningsCompleted, DummyDates.ScreeningsCompleted) AS ScreeningsCompleted
	, COALESCE(Biggie.ScreeningsIdentifyingNeed, DummyDates.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
	, COALESCE(Biggie.ScreeningsIdentifyingNeed_Perc, DummyDates.ScreeningsIdentifyingNeed_Perc) AS ScreeningsIdentifyingNeed_Perc
	, COALESCE(Biggie.NeedsIdentified, DummyDates.NeedsIdentified) AS NeedsIdentified
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey
FROM (
	SELECT 
		COALESCE(Base.DateHeader, YearSum.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(Base.ScreeningsCompleted AS VARCHAR), CAST(YearSum.ScreeningsCompleted AS VARCHAR), AddFormat.ScreeningsCompleted) AS ScreeningsCompleted
		, COALESCE(CAST(Base.ScreeningsIdentifyingNeed AS VARCHAR), CAST(YearSum.ScreeningsIdentifyingNeed AS VARCHAR), AddFormat.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
		, COALESCE(CAST(Base.NeedsIdentified AS VARCHAR), CAST(YearSum.NeedsIdentified AS VARCHAR), AddFormat.NeedsIdentified) AS NeedsIdentified
	/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		, COALESCE(CONVERT(VARCHAR, Base.ScreeningsIdentifyingNeed_Perc) + '%', CONVERT(VARCHAR, YearSum.ScreeningsIdentifyingNeed_Perc) + '%', AddFormat.ScreeningsIdentifyingNeed_Perc) AS ScreeningsIdentifyingNeed_Perc
		, COALESCE(Base.sortkey, YearSum.sortkey, AddFormat.sortkey) AS sortkey
		
	FROM Base
	FULL OUTER JOIN (
		SELECT 
			'YTD Total' AS DateHeader
			, '14' AS sortkey
			, SUM(ScreeningsCompleted) AS ScreeningsCompleted
			, SUM(ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
			, SUM(NeedsIdentified) AS NeedsIdentified
			, CASE WHEN SUM(ScreeningsCompleted) > 0 THEN
				/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
					CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,SUM(ScreeningsIdentifyingNeed))/CONVERT(FLOAT,SUM(ScreeningsCompleted)),1))
					ELSE NULL END AS 'ScreeningsIdentifyingNeed_Perc'
		FROM Base
	) AS YearSum
	ON Base.DateHeader = YearSum.DateHeader
	FULL OUTER JOIN AddFormat
	ON Base.DateHeader = AddFormat.DateHeader
	--order by sortkey
	) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY COALESCE(Biggie.sortkey, DummyDates.sortkey)