/*
At-A-Glance, Week Aggregation Query - ADOPT
author: joanna.tung
date: 2018.12.07

	This query returns an export that can produce the desired table format presented for the At-A-Glance, 4-week by week report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Last 4Wk Period" field), and 
			2) it requires a calculation of the percent change between the current 4Week period total and the prior 4Week period total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for week period for the current 4Week period 
			- There are row records for pre-calculated Prior 4Week Total, Current 4Week Total, and Percent Change between these 4Wk Date Periods
			* For the Adopt section, the "Current" and "Last" 4Week Date Period correspond to the LAST WEEK data for that 4week date period
				- These values will be used to calculate "Percent Change"
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) wek aggregation, 
			2) 4Wk Period aggregation, and 
			3) percent change between 4Week Period aggregations
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for each of the 4 weeks in the current 4Week period, where format lists
					  the start and end day for the week as follows: 'mm/dd - mm/dd'
					- one row record for the current 4Week period totals, prior 4Week period totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) NoPerc (temp):
			OUTPUT: aggregated sums for each week in the current 4Week period
			LOGIC: 1) contains necessary week aggregations for the current 4Week period and 2) serves as a base table from which to perform filters for prior
			vs. current 4-week date period totals

		3) AddPerc (temp):
			OUTPUT: full list of metrics for week totals for all weeks in the current and prior 4-week date period
			LOGIC:
				- adds "dummy" column so that join along horizontal axis can be performed in AddDeltaWeek temp table
				- adds "datePeriod" column and row ranking to distinguish:
					1) which week values belong to the current and prior 4-week date period
					2) which week values are the last week of the current and prior 4-week date period 
						- these values will also be used to perform the percent change calculations in the ADdDeltaWeek temp table

		4) AddDeltaWeek (temp):
			OUTPUT: percent change values for current 4Week period and the prior 4Week period
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the current and prior date period, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between the current and prior 4Week periods. Uses "dummy" field to
			perform 'arbitrary' join to append prior data to current data along the horizontal axis.

		5) AddFormat (temp):
			OUTPUT: percent change values for current 4week period and the prior 4week period, with '%' sign added for values that are not NULL, and 'N/A' 
			inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		6) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each week of the current 4week period, by the current 4week period, and by the prior 4week period, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from current to the prior 4week period.
			LOGIC: Perform full outer join to merge the individual week aggregations, 4week aggregations, and formatted percent change (AddFormat) values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-07-30' --DATEADD(DAY, -27, '2018-09-23') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-08-26' 
DECLARE @OrgParam AS INT  = 1085389

;
WITH
/* The At-a-Glance report requires that we display current week, prior week, and percent change columns, even if the fact table contains no data for the given date period.
   We use an outer join to the DummyDates temp table in the final select statement below in order to ensure that these row records appear in the export
   regardless if data exists. */

Dummydates AS (
	SELECT  
		
		WeekLastDay
		, CONCAT(CONVERT(VARCHAR(5), WeekLastDay, 101), ' End Date*') AS DateHeader
		, CAST(0 AS VARCHAR) AS TotalUsers
		, CAST(0 AS VARCHAR) AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, CAST(0 AS VARCHAR) AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, CAST(0 AS VARCHAR) AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, CAST(0 AS VARCHAR) AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, CAST(0 AS VARCHAR) AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)
	GROUP BY WeekLastDay, CONCAT(CONVERT(VARCHAR(5), WeekFirstDay, 101), ' - ' , CONVERT(VARCHAR(5), WeekLastDay, 101))
	
)
,
NoPerc AS (
	SELECT 
		 SUM(TotalUsers) AS TotalUsers
		, SUM(EngagedUsers) AS EngagedUsers
		, SUM(ActiveUsers) AS ActiveUsers
		, SUM(ModerateUsers) AS ModerateUsers
		, SUM(InfrequentUsers) AS InfrequentUsers
		, SUM(InactiveUsers) AS InactiveUsers
		, factStandardWeek.WeekLastDay AS WeekLastDay
		, CONCAT(CONVERT(VARCHAR(5), factStandardWeek.WeekLastDay, 101), ' End Date*') AS DateHeader
	FROM vfact.[StandardMetricsAAGAdoptWeekly] as factStandardWeek
	WHERE factStandardWeek.WeekLastDay >= DATEADD(DAY, 0, @StartDate) AND factStandardWeek.WeekLastDay < DATEADD(DAY, 6, @EndDate)
		AND OrganizationId IN (@orgparam) 
	GROUP BY factStandardWeek.WeekLastDay 

	UNION ALL

	SELECT 
		 SUM(TotalUsers) AS TotalUsers
		, SUM(EngagedUsers) AS EngagedUsers
		, SUM(ActiveUsers) AS ActiveUsers
		, SUM(ModerateUsers) AS ModerateUsers
		, SUM(InfrequentUsers) AS InfrequentUsers
		, SUM(InactiveUsers) AS InactiveUsers
		, factStandardWeek.WeekLastDay AS WeekLastDay
		, 'End of Prior Period*' AS DateHeader
	FROM vfact.[StandardMetricsAAGAdoptWeekly] as factStandardWeek
	WHERE factStandardWeek.WeekLastDay = DATEADD(DAY, -1, @StartDate)
		AND OrganizationId IN (@orgparam) 
	GROUP BY factStandardWeek.WeekLastDay 

)
,
AddPerc AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN "TotalUsers" > 0 THEN
		/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,EngagedUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'EngagedUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT, ActiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ActiveUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,ModerateUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ModerateUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InfrequentUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InfrequentUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InactiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InactiveUsers_Perc'
		/* Add Row Ranking to tag which records are the last week for each 4Week Date Period */
		, ROW_NUMBER() OVER (ORDER BY WeekLastDay) AS [rPeriod]
	FROM NoPerc
)
,
AddDelta AS (
	SELECT 
		'% Change' AS 'DateHeader',

		CASE 
			WHEN (ThisWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalUsers) - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers),0)
			ELSE NULL END AS TotalUsers,

		CASE 
			WHEN (ThisWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers <> 0) THEN 
				ROUND((100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers) - CONVERT(FLOAT,PriorWeek.EngagedUsers))/CONVERT(FLOAT,PriorWeek.EngagedUsers)),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers <> 0) THEN 
				ROUND((100 * (0 - CONVERT(FLOAT,PriorWeek.EngagedUsers))/CONVERT(FLOAT,PriorWeek.EngagedUsers)),0)
			ELSE NULL END AS EngagedUsers,
		
		CASE 
			WHEN (ThisWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers_Perc) - CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc))/CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc))/CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc),0)
			ELSE NULL END AS EngagedUsers_Perc,

		CASE 
			WHEN (ThisWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers) - CONVERT(FLOAT,PriorWeek.ActiveUsers))/CONVERT(FLOAT,PriorWeek.ActiveUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ActiveUsers))/CONVERT(FLOAT,PriorWeek.ActiveUsers),0)
			ELSE NULL END AS ActiveUsers,
				
		CASE 
			WHEN (ThisWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers_Perc) - CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc),0)
			ELSE NULL END AS ActiveUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers) - CONVERT(FLOAT,PriorWeek.ModerateUsers))/CONVERT(FLOAT,PriorWeek.ModerateUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ModerateUsers))/CONVERT(FLOAT,PriorWeek.ModerateUsers),0)
			ELSE NULL END AS ModerateUsers,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers_Perc) - CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc))/CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc))/CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc),0)
			ELSE NULL END AS ModerateUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers) - CONVERT(FLOAT,PriorWeek.InfrequentUsers))/CONVERT(FLOAT,PriorWeek.InfrequentUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InfrequentUsers))/CONVERT(FLOAT,PriorWeek.InfrequentUsers),0)
			ELSE NULL END AS InfrequentUsers,

		CASE 
			WHEN (ThisWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers_Perc) - CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc),0)
			ELSE NULL END AS InfrequentUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InactIveUsers) - CONVERT(FLOAT,PriorWeek.InactIveUsers))/CONVERT(FLOAT,PriorWeek.InactIveUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InactIveUsers))/CONVERT(FLOAT,PriorWeek.InactIveUsers),0)
			ELSE NULL END AS InactIveUsers,
		
		CASE 
			WHEN (ThisWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InactIveUsers_Perc) - CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc))/CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc))/CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc),0)
			ELSE NULL END AS InactIveUsers_Perc

	FROM (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 5
		) AS ThisWeek
	FULL OUTER JOIN (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 1
		) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDelta.TotalUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.TotalUsers) + '%' END AS 'TotalUsers'
		, CASE WHEN AddDelta.EngagedUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers) + '%' END AS 'EngagedUsers'
		, CASE WHEN AddDelta.EngagedUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers_Perc) + '%' END AS 'EngagedUsers_Perc'
		, CASE WHEN AddDelta.ActiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers) + '%' END AS 'ActiveUsers'
		, CASE WHEN AddDelta.ActiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers_Perc) + '%' END AS 'ActiveUsers_Perc'
		, CASE WHEN AddDelta.ModerateUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers) + '%' END AS 'ModerateUsers'
		, CASE WHEN AddDelta.ModerateUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers_Perc) + '%' END AS 'ModerateUsers_Perc'
		, CASE WHEN AddDelta.InfrequentUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers) + '%' END AS 'InfrequentUsers'
		, CASE WHEN AddDelta.InfrequentUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers_Perc) + '%' END AS 'InfrequentUsers_Perc'
		, CASE WHEN AddDelta.InactiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers) + '%' END AS 'InactiveUsers'
		, CASE WHEN AddDelta.InactiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers_Perc) + '%' END AS 'InactiveUsers_Perc'
	FROM AddDelta
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, DummyDates.sortkey AS sortkey
	, COALESCE(Biggie.TotalUsers, DummyDates.TotalUsers) AS TotalUsers
	, COALESCE(Biggie.EngagedUsers, DummyDates.EngagedUsers) AS EngagedUsers
	, COALESCE(Biggie.EngagedUsers_Perc, DummyDates.EngagedUsers_Perc) AS EngagedUsers_Perc
	, COALESCE(Biggie.ActiveUsers, DummyDates.ActiveUsers) AS ActiveUsers
	, COALESCE(Biggie.ActiveUsers_Perc, DummyDates.ActiveUsers_Perc) AS ActiveUsers_Perc
	, COALESCE(Biggie.ModerateUsers, DummyDates.ModerateUsers) AS ModerateUsers
	, COALESCE(Biggie.ModerateUsers_Perc, DummyDates.ModerateUsers_Perc) AS ModerateUsers_Perc
	, COALESCE(Biggie.InfrequentUsers, DummyDates.InfrequentUsers) AS InfrequentUsers
	, COALESCE(Biggie.InfrequentUsers_Perc, DummyDates.InfrequentUsers_Perc) AS InfrequentUsers_Perc
	, COALESCE(Biggie.InactiveUsers, DummyDates.InactiveUsers) AS InactiveUsers
	, COALESCE(Biggie.InactiveUsers_Perc, DummyDates.InactiveUsers_Perc) AS InactiveUsers_Perc

FROM (
	SELECT 
		COALESCE(WeekSum.DateHeader, AddFormat.DateHeader) AS DateHeader
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(WeekSum.TotalUsers AS VARCHAR), AddFormat.TotalUsers) AS TotalUsers
		,COALESCE(CAST(WeekSum.EngagedUsers AS VARCHAR), AddFormat.EngagedUsers) AS EngagedUsers
		/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR,WeekSum.EngagedUsers_Perc) + '%', AddFormat.EngagedUsers_Perc) AS EngagedUsers_Perc
		,COALESCE(CAST(WeekSum.ActiveUsers AS VARCHAR), AddFormat.ActiveUsers) AS ActiveUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.ActiveUsers_Perc) + '%', AddFormat.ActiveUsers_Perc) AS ActiveUsers_Perc
		,COALESCE(CAST(WeekSum.ModerateUsers AS VARCHAR), AddFormat.ModerateUsers) AS ModerateUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.ModerateUsers_Perc) + '%', AddFormat.ModerateUsers_Perc) AS ModerateUsers_Perc
		,COALESCE(CAST(WeekSum.InfrequentUsers AS VARCHAR), AddFormat.InfrequentUsers) AS InfrequentUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.InfrequentUsers_Perc) + '%', AddFormat.InfrequentUsers_Perc) AS InfrequentUsers_Perc
		,COALESCE(CAST(WeekSum.InactiveUsers AS VARCHAR), AddFormat.InactiveUsers) AS InactiveUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.InactiveUsers_Perc) + '%', AddFormat.InactiveUsers_Perc) AS InactiveUsers_Perc

	FROM (
		SELECT *
		FROM AddPerc
		) AS WeekSum

	FULL OUTER JOIN AddFormat
	ON WeekSum.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN (
	SELECT 
		DateHeader
		, TotalUsers
		, EngagedUsers
		, EngagedUsers_Perc
		, ActiveUsers
		, ActiveUsers_Perc
		, ModerateUsers
		, ModerateUsers_Perc
		, InfrequentUsers
		, InfrequentUsers_Perc
		,  InactiveUsers
		, InactiveUsers_Perc
		, ROW_NUMBER() OVER (ORDER BY WeekLastDay) AS sortkey
	FROM DummyDates

	UNION ALL

	SELECT
		'End of Prior Period*' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalUsers
		, CAST(0 AS VARCHAR) AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, CAST(0 AS VARCHAR) AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, CAST(0 AS VARCHAR) AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, CAST(0 AS VARCHAR) AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, CAST(0 AS VARCHAR) AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
		, '6' AS sortkey
		
	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A'AS TotalUsers
		, 'N/A' AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, 'N/A' AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, 'N/A' AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, 'N/A' AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, 'N/A' AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
		, '7' AS sortkey
) AS DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY sortkey