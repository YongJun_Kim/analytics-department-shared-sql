/*
Match Detail
author: joanna.tung
date: 2019.03.12

	This query returns an export that can produce the desired table format presented for the Match Detail, Service Providers Report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrTable and xRPivotGrid functionality.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)


	The output of "QueryBig" is used to generate the "Top 10" pivot grids on page 1 of the report. We cannot use the pre-aggregated, pre-averaged fields in the "Query" output to assess Total Referrals and Average Distance to 
	correctly assess the total referrals sent/shared and average distance by service provider and service type.
	
	The Match Detail DevExpress template utilizes custom scripting functionality to return the correct Average distance per Service Provider Organization, and Service Type for the "Top 10" pivot grids.

		***Why do we need custom scripting and a separate query for the "Top 10" pivot grids?***
			1) Not all referrals contain the patient location information necessary to calculate the "Distance" field. For these referrals, Distance IS NULL.
				"Average Distance to Service" should only include those referral records with available distance information.
					- "QueryBig" contains individual row records for each referral, allowing us to distinguish which of the "Total Referral" count should be included in the Average Distance to service count (the denominator)
					- Custom scripting allows us to selectively include only those referral records with available distance information in the final count.
			2) DevExpress provides default "error" value for calculations where divisor = 0 or numerator IS NULL. 
					- CUstom scripting allows us to utilize custom logic in the pivot grid, allowing us to return the friendlier "N/A" where divisor = 0 or numerator IS NULL
*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-09-16'
DECLARE @orgparam AS INT = 1085389

SELECT
	dOrgMaker.[Name] AS SelectedOrganizationName
	, factMatch.OrganizationId
	/* Some "prints" are service prints, others are organization prints. We have traditionally counted both as a service print. To differentiate the two printing activities on the report, we must add logic to return the 
	   printed organization's id when an organization is printed, and add logic to return "unspecified service" as the service ServiceTypeName and ServiceName when an organization is printed */
	, CASE
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.[Name]
		ELSE dService.OrganizationName
	  END AS ServiceProviderOrganizationName
	, CASE
		WHEN factMatch.ServiceId IS NULL OR factMatch.ServiceId = 0 THEN 'Unspecified Service'
		ELSE dService.[ServiceTypeName]
	  END AS ServiceTypeName
	/* Per Sylvie, assign Zip Code of the service provider organizaion if there is no zip code provided at the ServiceId level */
	, CASE 
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.PostalCode
		WHEN dService.PostalCode IS NULL THEN dOrgSP.PostalCode 
		ELSE dService.PostalCode
		END AS ZipCode
	, dService.ServiceName
	, CASE
		WHEN Distance IS NULL THEN 0 ELSE 1 END AS 'ReferralWithDistance'
	, TotalReferrals
	, Distance

  FROM [vfact].[StandardMetricsAAGMatchDetail] as factMatch
  LEFT JOIN vdim.[Service] as dService
  ON dService.ServiceId = factMatch.ServiceId
  LEFT JOIN vdim.[Organization] as dOrgMaker
  ON dOrgMaker.OrganizationId = factMatch.OrganizationId
  LEFT JOIN vdim.[Organization] as dOrgSP
  ON dService.OrganizationId = dOrgSP.OrganizationId
  LEFT JOIN vdim.[Organization] as dOrgNudgedSP
  ON dOrgNudgedSP.OrganizationId = factMatch.NudgedServiceOrganizationId

  WHERE CAST(factMatch.DateKey AS VARCHAR) >= DATEADD(DAY, 0, @StartDate) AND CAST(factMatch.DateKey AS VARCHAR) < DATEADD(DAY, 1, @EndDate)
	AND factMatch.OrganizationId in (@orgparam)
