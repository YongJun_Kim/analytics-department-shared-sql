/*
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-12-31' 
DECLARE @OrgParam AS INT  = 1085389

;
WITH 

Dummydates AS (
	SELECT  
		  
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsSent
		, CAST(0 AS VARCHAR) AS TrackedReferralClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedReferralClosedUnsuccessfully
		, YearMonthNumber AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue
	
	UNION ALL

	SELECT
		'YTD Total' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsSent
		, CAST(0 AS VARCHAR) AS TrackedReferralClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedReferralClosedUnsuccessfully
		, '14' AS sortkey

	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS TrackedReferralsSent
		, 'N/A' AS TrackedReferralClosedSuccessfully
		, 'N/A' AS TrackedReferralClosedUnsuccessfully
		, '13' AS sortkey
)
,
Base AS (

	SELECT 
		REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, 'Dummy' AS Dummy
		, dimDate.MonthLastDay			
		, SUM(factStandardMetricsTrackedReferral.TrackedReferralsSent) AS TrackedReferralsSent
		, SUM(factStandardMetricsTrackedReferral.TrackedReferralClosedSuccessfully) AS TrackedReferralClosedSuccessfully
		, SUM(factStandardMetricsTrackedReferral.TrackedReferralClosedUnsuccessfully) AS TrackedReferralClosedUnsuccessfully
		, dimDate.YearMonthNumber AS sortkey
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay DESC) AS [rPeriod]

	FROM vfact.StandardMetricsTrackedReferral AS factStandardMetricsTrackedReferral
		JOIN vdim.[Date] as dimDate
		ON dimDate.DateKey = factStandardMetricsTrackedReferral.DateKey
	WHERE dimDate.MonthLastDay > DATEADD(DAY, 0, @StartDate) AND dimDate.MonthLastDay <= DATEADD(DAY, 0, @EndDate)
		--AND OrganizationId in (@orgparam)
	
	GROUP BY dimDate.MonthLastDay, dimDate.YearMonthNumber

)
,
AddDeltaWeek AS (
	SELECT 
		'% Change' AS 'DateHeader',
		'13' AS sortkey,
		CASE 
			WHEN (ThisMonth.TrackedReferralsSent IS NOT NULL and PriorMonth.TrackedReferralsSent IS NOT NULL and PriorMonth.TrackedReferralsSent <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedReferralsSent) - CONVERT(FLOAT,PriorMonth.TrackedReferralsSent))/CONVERT(FLOAT,PriorMonth.TrackedReferralsSent),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedReferralsSent IS NOT NULL AND PriorMonth.TrackedReferralsSent <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorMonth.TrackedReferralsSent))/CONVERT(FLOAT, PriorMonth.TrackedReferralsSent),0)
			ELSE NULL END AS TrackedReferralsSent,

		CASE 
			WHEN (ThisMonth.TrackedReferralClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedSuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedReferralClosedSuccessfully) - CONVERT(FLOAT,PriorMonth.TrackedReferralClosedSuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedReferralClosedSuccessfully),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedReferralClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedSuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TrackedReferralClosedSuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedReferralClosedSuccessfully),0)
			ELSE NULL END AS TrackedReferralClosedSuccessfully,

		CASE 
			WHEN (ThisMonth.TrackedReferralClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedReferralClosedUnsuccessfully) - CONVERT(FLOAT,PriorMonth.TrackedReferralClosedUnsuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedReferralClosedUnsuccessfully),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedReferralClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedReferralClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TrackedReferralClosedUnsuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedReferralClosedUnsuccessfully),0)
			ELSE NULL END AS TrackedReferralClosedUnsuccessfully
	
	FROM (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDeltaWeek.TrackedReferralsSent IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedReferralsSent) + '%' END AS 'TrackedReferralsSent'
		, CASE WHEN AddDeltaWeek.TrackedReferralClosedSuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedReferralClosedSuccessfully) + '%' END AS 'TrackedReferralClosedSuccessfully'
		, CASE WHEN AddDeltaWeek.TrackedReferralClosedUnsuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedReferralClosedUnsuccessfully) + '%' END AS 'TrackedReferralClosedUnsuccessfully'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TrackedReferralsSent, DummyDates.TrackedReferralsSent) AS TrackedReferralsSent
	, COALESCE(Biggie.TrackedReferralClosedSuccessfully, DummyDates.TrackedReferralClosedSuccessfully) AS TrackedReferralClosedSuccessfully
	, COALESCE(Biggie.TrackedReferralClosedUnsuccessfully, DummyDates.TrackedReferralClosedUnsuccessfully) AS TrackedReferralClosedUnsuccessfully
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey

FROM (
	SELECT 
		 COALESCE(Base.DateHeader, YearSum.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(Base.TrackedReferralsSent AS VARCHAR), CAST(YearSum.TrackedReferralsSent AS VARCHAR), AddFormat.TrackedReferralsSent) AS TrackedReferralsSent
		, COALESCE(CAST(Base.TrackedReferralClosedSuccessfully AS VARCHAR), CAST(YearSum.TrackedReferralClosedSuccessfully AS VARCHAR), AddFormat.TrackedReferralClosedSuccessfully) AS TrackedReferralClosedSuccessfully
		, COALESCE(CAST(Base.TrackedReferralClosedUnsuccessfully AS VARCHAR), CAST(YearSum.TrackedReferralClosedUnsuccessfully AS VARCHAR), AddFormat.TrackedReferralClosedUnsuccessfully) AS TrackedReferralClosedUnsuccessfully
		, COALESCE(Base.sortkey, YearSum.sortkey, AddFormat.sortkey) AS sortkey
	FROM Base
	FULL OUTER JOIN (
		SELECT 
			'YTD Total' AS 'DateHeader'
			, SUM(TrackedReferralsSent) AS TrackedReferralsSent
			, SUM(TrackedReferralClosedSuccessfully) AS TrackedReferralClosedSuccessfully
			, SUM(TrackedReferralClosedUnsuccessfully) AS TrackedReferralClosedUnsuccessfully
			,'14' AS sortkey
	FROM Base
	) AS YearSum
	ON Base.DateHeader = YearSum.DateHeader
	FULL OUTER JOIN AddFormat
	ON Base.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY sortkey