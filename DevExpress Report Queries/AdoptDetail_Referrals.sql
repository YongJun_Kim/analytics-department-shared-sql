/*
Adopt-Detail: Referral Activities Query
author: joanna.tung
date: 2019.01.29

	This query returns an export that can produce the desired table format presented for the Adopt-Detail: Referral Activities in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrTable functionality.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Updates:

2019.03.27: per Analytics-496, changing join condition to Patient table to utilize AND condition rather than "Concat." This will speed up performance in DB.
2019.04.12: Removed CONCAT statements in the groupby and select statement from the following sections:
	 - /* By UserId, OrganizationId: Count of unique shares per eRxId */
	 - /* By UserId, OrganizationId: Count of referrals per eRxId */
*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-09-16'
DECLARE @orgparam AS INT = 1085389

SELECT 

       Base4.UserId
       ,CONCAT(dUSer.LastName, ', ',dUser.FirstName) AS 'UserName'
       ,dUser.Title
       ,dUser.Department
       ,Base4.OrganizationId
       ,dOrg.[Name] as OrganizationName
       ,Base4.ErxsCreated
       ,Base4.ErxShares
       ,CASE WHEN (Base4.ErxReferrals + Base4.TrackedShares + Base4.NudgeShares + Base4.PrintShares) IS NULL THEN 0 ELSE (Base4.ErxReferrals + Base4.TrackedShares + Base4.NudgeShares + Base4.PrintShares) END AS TotalReferrals --  
       ,Base4.ErxReferrals
       ,Base4.TrackedShares
       ,(Base4.NudgeShares + Base4.PrintShares) AS SingleServiceShares
FROM (
       SELECT
             COALESCE(Base3.UserId, PrintShares.UserId) AS UserId
             ,COALESCE(Base3.OrganizationId, PrintShares.OrganizationId) AS OrganizationId
             ,CASE WHEN Base3.ErxsCreated IS NULL THEN 0 ELSE Base3.ErxsCreated END AS 'ErxsCreated'
             ,CASE WHEN Base3.ErxShares IS NULL THEN 0 ELSE Base3.ErxShares END AS 'ErxShares'
             ,CASE WHEN Base3.ErxReferrals IS NULL THEN 0 ELSE Base3.ErxReferrals END AS 'ErxReferrals'
             ,CASE WHEN Base3.NudgeShares IS NULL THEN 0 ELSE Base3.NudgeShares END AS 'NudgeShares'
             ,CASE WHEN Base3.TrackedShares IS NULL THEN 0 ELSE Base3.TrackedShares END AS 'TrackedShares'
             ,CASE WHEN PrintShares.PrintShares IS NULL THEN 0 ELSE PrintShares.PrintShares END AS 'PrintShares'

       FROM (

             SELECT 
                    COALESCE(Base2.UserId, TrkShares.UserId) AS UserId
                    ,COALESCE(Base2.OrganizationId, TrkShares.OrganizationId) AS OrganizationId
                    ,Base2.ErxsCreated
                    ,Base2.ErxShares
                    ,Base2.ErxReferrals
                    ,Base2.NudgeShares
                    ,TrkShares.TrackedShares AS 'TrackedShares'
             FROM (
                    SELECT
                           COALESCE(Base1.UserId, NugShares.UserId) AS UserId
                           ,COALESCE(Base1.OrganizationId, NugShares.OrganizationId) AS OrganizationId
                           ,Base1.ErxsCreated
                           ,Base1.ErxShares
                           ,Base1.ErxReferrals
                           ,NugShares.NudgeShares

                    FROM (
                           SELECT
                                 COALESCE(eRxCreated.UserId, eRxShares.UserId) AS UserId
                                 ,COALESCE(eRxCreated.OrganizationId, eRxShares.OrganizationId) AS OrganizationId
                                 ,eRxCreated.ErxsCreated
                                 ,eRxShares.ErxShares
                                 ,eRxShares.ErxReferrals

                           FROM (
                                 /* By UserId, OrganizationId: Count of unique eRx's Created
                                 11/16/2018 Note: use "distinct" count here due to duplicates in the fact.eRx table. When duplicates are removed, revert to standard count */
                                 SELECT 
                                        factErx.UserId
                                        ,factErx.OrganizationId
                                        ,COUNT(DISTINCT CONCAT(factErx.ErxId, ';', factErx.CareOrganizationDatabaseId)) AS 'ErxsCreated'
                                 FROM vfact.Erx AS factErx
                                 LEFT JOIN vdim.Patient as dPat
                                 ON dPat.PatientId = factErx.PatientId AND dPat.CareOrganizationDatabaseId = factErx.CareOrganizationDatabaseId
                                 WHERE factErx.UserId IS NOT NULL
                                        AND factErx.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND factErx.CreatedDate < DATEADD(DAY, 1, @EndDate)
                                        AND factErx.OrganizationId IN (@orgparam)
                                        AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL)
                                 /* Historical Usage reports do not include POC eRx Creation. We exclude POC interactions to maintain consistency with historical reports. */
                                        AND (factErx.ErxSourceTypeId <> 1 OR factErx.ErxSourceTypeId IS NULL)
                                 GROUP BY factErx.UserId, factErx.OrganizationId
                                 ) AS eRxCreated
                           FULL OUTER JOIN (
                                 SELECT 
                                        PSI.UserId
                                        ,PSI.OrganizationId
                                        ,SUM(PSI.CountShares) AS 'ErxShares'
                                        ,SUM(ErxRefs.Referrals * PSI.CountShares) AS 'ErxReferrals'
                                 FROM (
                                        /* By UserId, OrganizationId: Count of unique shares per eRxId */
                                               SELECT
                                                     vfactPSI.ErxId AS 'ErxId'
                                                      ,vfactPSI.CareOrganizationDatabaseId AS 'CareOrganizationDatabaseId'
                                                      ,SUM(vfactPSI.PatientSiteInteractionQuantity) AS 'CountShares'
                                                     ,vfactPSI.UserId
                                                     ,vfactPSI.OrganizationId
                                               FROM vfact.PatientSiteInteraction AS vfactPSI
                                               LEFT JOIN vdim.Patient AS dPat
                                               ON dPat.PatientId = vfactPSI.PatientId AND dPat.CareOrganizationDatabaseId = vfactPSI.CareOrganizationDatabaseId
                                               LEFT JOIN vfact.Erx as vfactErx
                                               ON vfactPSI.ErxId = vfactErx.ErxId AND vfactPSI.CareOrganizationDatabaseId = vfactErx.CareOrganizationDatabaseId
                                               WHERE InteractionTypeId IN (3, 4, 11) AND vfactPSI.ErxId IS NOT NULL AND vfactPSI.UserId IS NOT NULL
                                                     AND vfactPSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND vfactPSI.InteractionDate < DATEADD(DAY, 1, @Enddate)
                                                     AND vfactPSI.OrganizationId IN (@orgparam)
                                                     AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
                                                     /* Historical Usage reports do not include POC eRx Creation. We exclude POC interactions to maintain consistency with historical reports. */
                                                     AND (vfactErx.ErxSourceTypeId <> 1 OR vfactErx.ErxSourceTypeId IS NULL)
                                               GROUP BY vfactPSI.ErxId, vfactPSI.CareOrganizationDatabaseId, vfactPSI.UserId, vfactPSI.OrganizationId
                                        ) AS PSI
                                 LEFT JOIN (
                                               /* By UserId, OrganizationId: Count of referrals per eRxId */
                                               SELECT 
                                                     vfactErxService.UserId
                                                      ,vfactErxService.OrganizationId
                                                     ,vfactErxService.ErxId AS 'ErxId'
                                                      ,vfactErxService.CareOrganizationDatabaseId AS 'CareOrganizationDatabaseId'
                                                      ,SUM(vfactErxService.ErxServiceQuantity) AS Referrals
                                               FROM vfact.ErxService AS vfactErxService
                                               WHERE UserId IS NOT NULL
                                                     /* DO NOT include a filter for date, orgid or test patients! Shared referrals can be made by and for anyone, but
                                                        shared to as a patient */
                                               GROUP BY vfactErxService.ErxId, vfactErxService.CareOrganizationDatabaseId, vfactErxService.UserId, vfactErxService.OrganizationId
                                        ) AS ErxRefs
                                 ON PSI.ErxId = ErxRefs.ErxId AND PSI.CareOrganizationDatabaseId = ErxRefs.CareOrganizationDatabaseId
                                 GROUP BY PSI.UserId, PSI.OrganizationId
                                 ) AS eRxShares
                           ON (eRxShares.UserId = eRxCreated.UserId AND eRxShares.OrganizationId = eRxCreated.OrganizationId)
                           ) AS Base1
                    FULL OUTER JOIN (
                           /* By UserId, OrganizationId: Count of Nudged Service Shares */
                           SELECT 
                                 vfactPSI.UserId
                                 ,vfactPSI.OrganizationId
                                  ,SUM(vfactPSI.PatientSiteInteractionQuantity) AS 'NudgeShares'
                           FROM vfact.PatientSiteInteraction as vfactPSI
                           LEFT JOIN vdim.Patient AS dPat
                           ON dPat.PatientId = vfactPSI.PatientId AND dPat.CareOrganizationDatabaseId = vfactPSI.CareOrganizationDatabaseId 
                           WHERE vfactPSI.interactiontypeid IN (5, 6) AND vfactPSI.UserId IS NOT NULL
                                 AND vfactPSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND vfactPSI.InteractionDate < DATEADD(DAY, 1, @Enddate)
                                 AND vfactPSI.OrganizationId IN (@orgparam)
                                 AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
                           GROUP BY vfactPSI.UserId, vfactPSI.OrganizationId
                           ) AS NugShares
                    ON (Base1.UserId = NugShares.UserId AND Base1.OrganizationId = NugShares.OrganizationId)
                    ) AS Base2
             FULL OUTER JOIN (
                    /* By UserId, OrganizationId: Count of Tracked Referrals Sent */
                    SELECT
                           vfactTrack.MakerUserId AS UserId
                           ,vfactTrack.MakerOrganizationId AS OrganizationId
                           ,SUM(vfactTrack.TrackedReferralQuantity) AS 'TrackedShares'
                    FROM vfact.TrackedReferral AS vfactTrack
                    LEFT JOIN vdim.Patient AS dPat
                           ON dPat.PatientId = vfactTrack.MakerPatientId AND dPat.CareOrganizationDatabaseId = vfactTrack.CareOrganizationDatabaseId
                    WHERE vfactTrack.MakerUserId IS NOT NULL
                           AND vfactTrack.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND vfactTrack.CreatedDate < DATEADD(DAY, 1, @Enddate)
                           AND vfactTrack.MakerOrganizationId IN (@orgparam)
                           AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
                    GROUP BY vfactTrack.MakerUserId, vfactTrack.MakerOrganizationId
                    ) AS TrkShares
             ON (Base2.UserId = TrkShares.UserId AND Base2.OrganizationId = TrkShares.OrganizationId)
             ) AS Base3
             FULL OUTER JOIN (
                    /* By UserId, OrganizationId: Count of Service Prints */
                    SELECT
                           vfactUact.UserId
                           ,vfactUact.ContactOrganizationId AS OrganizationId
                           ,SUM(vfactUact.UserActivityQuantity) AS 'PrintShares'
                    FROM vfact.UserActivity as vfactUact
                    WHERE vfactUact.UserActivityAction like '%print%'
                           AND vfactUact.UserActivityActionDate >= DATEADD(DAY, 0, @StartDate) AND vfactUact.UserActivityActionDate < DATEADD(DAY, 1, @Enddate)
                           AND vfactUact.ContactOrganizationId IN (@orgparam)
                    GROUP BY vfactUact.UserId, vfactUact.ContactOrganizationId
                    ) AS PrintShares
             ON (Base3.UserId = PrintShares.UserId AND Base3.OrganizationId = PrintShares.OrganizationId)
) as Base4
JOIN vdim.[User] as dUser
ON Base4.UserId = dUser.UserId 
JOIN vdim.Organization as dOrg
ON Base4.OrganizationId = dOrg.OrganizationId

WHERE (dUser.ExcludeFromReports <> 1 OR dUser.ExcludeFromReports IS NULL)

ORDER BY OrganizationName, UserName