/*
Match Detail
author: joanna.tung
date: 2019.03.12

	This query returns an export that can produce the desired table format presented for the Match Detail, Service Providers Report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrTable and xRPivotGrid functionality.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)


	The output of "Query" is used to generate the dynamic table in DetailReport band "Query." 
	Total Referral and Average Distance is pre-calculated and pre-aggregated on the query side to reduce report processing time.

	A separate query ("QueryBig") is used to generate the "Top 10" pivot grids on page 1 of the Match Deatil report.

Update:

2019.04.05: Removed conditional logic for "Referral without Distance" from query and group by statement. This logic is not needed for this part of the template and was causing errors in output.

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = '01/01/2018'
DECLARE @EndDate AS DATETIME = '01/31/2018'
DECLARE @orgparam AS INT = 1085389


SELECT
	dOrgMaker.[Name] AS SelectedOrganizationName
	, factMatch.OrganizationId
	/* Some "prints" are service prints, others are organization prints. We have traditionally counted both as a service print. To differentiate the two printing activities on the report, we must add logic to return the 
	   printed organization's id when an organization is printed, and add logic to return "unspecified service" as the service ServiceTypeName and ServiceName when an organization is printed */
	, CASE
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.[Name]
		--WHEN dService.OrganizationName IS NULL THEN 'Service Has Been Deleted'
		ELSE dService.OrganizationName
	  END AS ServiceProviderOrganizationName
	, CASE
		WHEN factMatch.ServiceId IS NULL OR factMatch.ServiceId = 0 THEN 'Unspecified Service'
		ELSE dService.[ServiceTypeName]
	  END AS ServiceTypeName
	/* Per Sylvie, assign Zip Code of the service provider organization if there is no zip code provided at the ServiceId level */
	, CASE 
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.PostalCode
		WHEN dService.PostalCode IS NULL THEN dOrgSP.PostalCode 
		ELSE dService.PostalCode
		END AS ZipCode
	, dService.ServiceName
	, SUM(TotalReferrals) as 'TotalReferrals'
	, CONVERT(DECIMAL(19,2),ROUND(SUM(Distance)/SUM(CASE WHEN Distance IS NULL THEN 0 ELSE TotalReferrals END),2)) AS 'AvgDistance'
  
  FROM [vfact].[StandardMetricsAAGMatchDetail] as factMatch
  LEFT JOIN vdim.[Service] as dService
  ON dService.ServiceId = factMatch.ServiceId
  LEFT JOIN vdim.[Organization] as dOrgMaker
  ON dOrgMaker.OrganizationId = factMatch.OrganizationId
  LEFT JOIN vdim.[Organization] as dOrgSP
  ON dService.OrganizationId = dOrgSP.OrganizationId
  LEFT JOIN vdim.[Organization] as dOrgNudgedSP
  ON dOrgNudgedSP.OrganizationId = factMatch.NudgedServiceOrganizationId

  WHERE CAST(factMatch.DateKey AS VARCHAR) >= DATEADD(DAY, 0, @StartDate) AND CAST(factMatch.DateKey AS VARCHAR) < DATEADD(DAY, 1, @EndDate)
	AND factMatch.OrganizationId in (@orgparam)

  GROUP BY 
	dOrgMaker.[Name]
	, factMatch.OrganizationId
	, CASE
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.[Name]
		--WHEN dService.OrganizationName IS NULL THEN 'Service Has Been Deleted'
		ELSE dService.OrganizationName END 
	, CASE
		WHEN factMatch.ServiceId IS NULL OR factMatch.ServiceId = 0 THEN 'Unspecified Service'
		ELSE dService.[ServiceTypeName] END 
	, CASE 
		WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND factMatch.NudgedServiceOrganizationId IS NOT NULL THEN dOrgNudgedSP.PostalCode
		WHEN dService.PostalCode IS NULL THEN dOrgSP.PostalCode 
		ELSE dService.PostalCode END
	, dService.ServiceName

  ORDER BY SelectedOrganizationName, ServiceProviderOrganizationName, TotalReferrals DESC, ServiceTypeName, ZipCode 