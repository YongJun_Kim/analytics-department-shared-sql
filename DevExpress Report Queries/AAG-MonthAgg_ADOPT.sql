/*
At-A-Glance, Month Aggregation Query - ADOPT
author: joanna.tung
date: 2018.12.11

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-12-31' 
DECLARE @OrgParam AS INT  = 1085389

;
WITH

Dummydates AS (
	SELECT  
		  
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalUsers
		, CAST(0 AS VARCHAR) AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, CAST(0 AS VARCHAR) AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, CAST(0 AS VARCHAR) AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, CAST(0 AS VARCHAR) AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, CAST(0 AS VARCHAR) AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay) AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue
	
	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A'AS TotalUsers
		, 'N/A' AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, 'N/A' AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, 'N/A' AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, 'N/A' AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, 'N/A' AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
		, '13' AS [sortkey]

)
,
NoPerc AS (
	SELECT 
		 SUM(TotalUsers) AS TotalUsers
		, SUM(EngagedUsers) AS EngagedUsers
		, SUM(ActiveUsers) AS ActiveUsers
		, SUM(ModerateUsers) AS ModerateUsers
		, SUM(InfrequentUsers) AS InfrequentUsers
		, SUM(InactiveUsers) AS InactiveUsers
		, MonthLastDay
		, REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		FROM vfact.[StandardMetricsAAGAdoptMonthly]
		WHERE MonthLastDay > DATEADD(DAY, 0, @StartDate) AND MonthLastDay <= DATEADD(DAY, 0, @EndDate)
			AND OrganizationId IN (@orgparam) 
		GROUP BY MonthLastDay

)
,
AddPerc AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN "TotalUsers" > 0 THEN
		/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,EngagedUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'EngagedUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT, ActiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ActiveUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,ModerateUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ModerateUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InfrequentUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InfrequentUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InactiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InactiveUsers_Perc'
		/* Add Row Ranking to tag which records are the last week for each 4Week Date Period */
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay DESC) AS [rPeriod]
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay) AS [sortkey]
	FROM NoPerc
)
,
AddDelta AS (
	SELECT 
		'% Change' AS 'DateHeader',
		'13' AS [sortkey],

		CASE 
			WHEN (ThisMonth.TotalUsers IS NOT NULL AND PriorMonth.TotalUsers IS NOT NULL AND PriorMonth.TotalUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TotalUsers) - CONVERT(FLOAT,PriorMonth.TotalUsers))/CONVERT(FLOAT,PriorMonth.TotalUsers),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TotalUsers IS NOT NULL AND PriorMonth.TotalUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TotalUsers))/CONVERT(FLOAT,PriorMonth.TotalUsers),0)
			ELSE NULL END AS TotalUsers,
		--ThisMonth.TotalUsers AS TTotalUsers,
		--PriorMonth.TotalUsers AS PTotalUsers,

		CASE 
			WHEN (ThisMonth.EngagedUsers IS NOT NULL AND PriorMonth.EngagedUsers IS NOT NULL AND PriorMonth.EngagedUsers <> 0) THEN 
				ROUND((100 * (CONVERT(FLOAT,ThisMonth.EngagedUsers) - CONVERT(FLOAT,PriorMonth.EngagedUsers))/CONVERT(FLOAT,PriorMonth.EngagedUsers)),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.EngagedUsers IS NOT NULL AND PriorMonth.EngagedUsers <> 0) THEN 
				ROUND((100 * (0 - CONVERT(FLOAT,PriorMonth.EngagedUsers))/CONVERT(FLOAT,PriorMonth.EngagedUsers)),0)
			ELSE NULL END AS EngagedUsers,
		--ThisMonth.EngagedUsers AS TEngagedUsers,
		--PriorMonth.EngagedUsers AS PEngagedUsers,
		
		CASE 
			WHEN (ThisMonth.EngagedUsers_Perc IS NOT NULL AND PriorMonth.EngagedUsers_Perc IS NOT NULL AND PriorMonth.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.EngagedUsers_Perc) - CONVERT(FLOAT,PriorMonth.EngagedUsers_Perc))/CONVERT(FLOAT,PriorMonth.EngagedUsers_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.EngagedUsers_Perc IS NOT NULL AND PriorMonth.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.EngagedUsers_Perc))/CONVERT(FLOAT,PriorMonth.EngagedUsers_Perc),0)
			ELSE NULL END AS EngagedUsers_Perc,
		--ThisMonth.EngagedUsers_Perc AS TEngagedUsers_Perc,
		--PriorMonth.EngagedUsers_Perc AS PEngagedUsers_Perc,

		CASE 
			WHEN (ThisMonth.ActiveUsers IS NOT NULL AND PriorMonth.ActiveUsers IS NOT NULL AND PriorMonth.ActiveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ActiveUsers) - CONVERT(FLOAT,PriorMonth.ActiveUsers))/CONVERT(FLOAT,PriorMonth.ActiveUsers),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ActiveUsers IS NOT NULL AND PriorMonth.ActiveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.ActiveUsers))/CONVERT(FLOAT,PriorMonth.ActiveUsers),0)
			ELSE NULL END AS ActiveUsers,
		--ThisMonth.ActiveUsers AS TActiveUsers,
		--PriorMonth.ActiveUsers AS PActiveUsers,
				
		CASE 
			WHEN (ThisMonth.ActiveUsers_Perc IS NOT NULL AND PriorMonth.ActiveUsers_Perc IS NOT NULL AND PriorMonth.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ActiveUsers_Perc) - CONVERT(FLOAT,PriorMonth.ActiveUsers_Perc))/CONVERT(FLOAT,PriorMonth.ActiveUsers_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ActiveUsers_Perc IS NOT NULL AND PriorMonth.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.ActiveUsers_Perc))/CONVERT(FLOAT,PriorMonth.ActiveUsers_Perc),0)
			ELSE NULL END AS ActiveUsers_Perc,
		--ThisMonth.ActiveUsers_Perc AS TActiveUsers_Perc,
		--PriorMonth.ActiveUsers_Perc AS PActiveUsers_Perc,
		
		CASE 
			WHEN (ThisMonth.ModerateUsers IS NOT NULL AND PriorMonth.ModerateUsers IS NOT NULL AND PriorMonth.ModerateUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ModerateUsers) - CONVERT(FLOAT,PriorMonth.ModerateUsers))/CONVERT(FLOAT,PriorMonth.ModerateUsers),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ModerateUsers IS NOT NULL AND PriorMonth.ModerateUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.ModerateUsers))/CONVERT(FLOAT,PriorMonth.ModerateUsers),0)
			ELSE NULL END AS ModerateUsers,
		--ThisMonth.ModerateUsers AS TModerateUsers,
		--PriorMonth.ModerateUsers AS PModerateUsers,
		
		CASE 
			WHEN (ThisMonth.ModerateUsers_Perc IS NOT NULL AND PriorMonth.ModerateUsers_Perc IS NOT NULL AND PriorMonth.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.ModerateUsers_Perc) - CONVERT(FLOAT,PriorMonth.ModerateUsers_Perc))/CONVERT(FLOAT,PriorMonth.ModerateUsers_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.ModerateUsers_Perc IS NOT NULL AND PriorMonth.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.ModerateUsers_Perc))/CONVERT(FLOAT,PriorMonth.ModerateUsers_Perc),0)
			ELSE NULL END AS ModerateUsers_Perc,
		--ThisMonth.ModerateUsers_Perc AS TModerateUsers_Perc,
		--PriorMonth.ModerateUsers_Perc AS PModerateUsers_Perc,
		
		CASE 
			WHEN (ThisMonth.InfrequentUsers IS NOT NULL AND PriorMonth.InfrequentUsers IS NOT NULL AND PriorMonth.InfrequentUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.InfrequentUsers) - CONVERT(FLOAT,PriorMonth.InfrequentUsers))/CONVERT(FLOAT,PriorMonth.InfrequentUsers),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.InfrequentUsers IS NOT NULL AND PriorMonth.InfrequentUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.InfrequentUsers))/CONVERT(FLOAT,PriorMonth.InfrequentUsers),0)
			ELSE NULL END AS InfrequentUsers,
		--ThisMonth.InfrequentUsers AS TInfrequentUsers,
		--PriorMonth.InfrequentUsers AS PInfrequentUsers, 

		CASE 
			WHEN (ThisMonth.InfrequentUsers_Perc IS NOT NULL AND PriorMonth.InfrequentUsers_Perc IS NOT NULL AND PriorMonth.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.InfrequentUsers_Perc) - CONVERT(FLOAT,PriorMonth.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorMonth.InfrequentUsers_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.InfrequentUsers_Perc IS NOT NULL AND PriorMonth.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorMonth.InfrequentUsers_Perc),0)
			ELSE NULL END AS InfrequentUsers_Perc,
		--ThisMonth.InfrequentUsers_Perc AS TInfrequentUsers_Perc,
		--PriorMonth.InfrequentUsers_Perc AS PInfrequentUsers_Perc,
		
		CASE 
			WHEN (ThisMonth.InactIveUsers IS NOT NULL AND PriorMonth.InactIveUsers IS NOT NULL AND PriorMonth.InactIveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.InactIveUsers) - CONVERT(FLOAT,PriorMonth.InactIveUsers))/CONVERT(FLOAT,PriorMonth.InactIveUsers),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.InactIveUsers IS NOT NULL AND PriorMonth.InactIveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.InactIveUsers))/CONVERT(FLOAT,PriorMonth.InactIveUsers),0)
			ELSE NULL END AS InactIveUsers,
		--ThisMonth.InactIveUsers AS TInactIveUsers,
		--PriorMonth.InactIveUsers AS PInactIveUsers,
		
		CASE 
			WHEN (ThisMonth.InactIveUsers_Perc IS NOT NULL AND PriorMonth.InactIveUsers_Perc IS NOT NULL AND PriorMonth.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.InactIveUsers_Perc) - CONVERT(FLOAT,PriorMonth.InactIveUsers_Perc))/CONVERT(FLOAT,PriorMonth.InactIveUsers_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.InactIveUsers_Perc IS NOT NULL AND PriorMonth.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.InactIveUsers_Perc))/CONVERT(FLOAT,PriorMonth.InactIveUsers_Perc),0)
			ELSE NULL END AS InactIveUsers_Perc
		--ThisMonth.InactIveUsers_Perc AS TInactIveUsers_Perc,
		--PriorMonth.InactIveUsers_Perc AS PInactIveUsers_Perc

	FROM (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy
)
,
AddFormat AS (
	SELECT 
		DateHeader
		, [sortkey]
		, CASE WHEN AddDelta.TotalUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.TotalUsers) + '%' END AS 'TotalUsers'
		, CASE WHEN AddDelta.EngagedUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers) + '%' END AS 'EngagedUsers'
		, CASE WHEN AddDelta.EngagedUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers_Perc) + '%' END AS 'EngagedUsers_Perc'
		, CASE WHEN AddDelta.ActiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers) + '%' END AS 'ActiveUsers'
		, CASE WHEN AddDelta.ActiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers_Perc) + '%' END AS 'ActiveUsers_Perc'
		, CASE WHEN AddDelta.ModerateUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers) + '%' END AS 'ModerateUsers'
		, CASE WHEN AddDelta.ModerateUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers_Perc) + '%' END AS 'ModerateUsers_Perc'
		, CASE WHEN AddDelta.InfrequentUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers) + '%' END AS 'InfrequentUsers'
		, CASE WHEN AddDelta.InfrequentUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers_Perc) + '%' END AS 'InfrequentUsers_Perc'
		, CASE WHEN AddDelta.InactiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers) + '%' END AS 'InactiveUsers'
		, CASE WHEN AddDelta.InactiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers_Perc) + '%' END AS 'InactiveUsers_Perc'
	FROM AddDelta
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TotalUsers, DummyDates.TotalUsers) AS TotalUsers
	, COALESCE(Biggie.EngagedUsers, DummyDates.EngagedUsers) AS EngagedUsers
	, COALESCE(Biggie.EngagedUsers_Perc, DummyDates.EngagedUsers_Perc) AS EngagedUsers_Perc
	, COALESCE(Biggie.ActiveUsers, DummyDates.ActiveUsers) AS ActiveUsers
	, COALESCE(Biggie.ActiveUsers_Perc, DummyDates.ActiveUsers_Perc) AS ActiveUsers_Perc
	, COALESCE(Biggie.ModerateUsers, DummyDates.ModerateUsers) AS ModerateUsers
	, COALESCE(Biggie.ModerateUsers_Perc, DummyDates.ModerateUsers_Perc) AS ModerateUsers_Perc
	, COALESCE(Biggie.InfrequentUsers, DummyDates.InfrequentUsers) AS InfrequentUsers
	, COALESCE(Biggie.InfrequentUsers_Perc, DummyDates.InfrequentUsers_Perc) AS InfrequentUsers_Perc
	, COALESCE(Biggie.InactiveUsers, DummyDates.InactiveUsers) AS InactiveUsers
	, COALESCE(Biggie.InactiveUsers_Perc, DummyDates.InactiveUsers_Perc) AS InactiveUsers_Perc
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey

FROM (
	SELECT 
		COALESCE(WeekSum.DateHeader, AddFormat.DateHeader) AS DateHeader
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(WeekSum.TotalUsers AS VARCHAR), AddFormat.TotalUsers) AS TotalUsers
		,COALESCE(CAST(WeekSum.EngagedUsers AS VARCHAR), AddFormat.EngagedUsers) AS EngagedUsers
		/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR,WeekSum.EngagedUsers_Perc) + '%', AddFormat.EngagedUsers_Perc) AS EngagedUsers_Perc
		,COALESCE(CAST(WeekSum.ActiveUsers AS VARCHAR), AddFormat.ActiveUsers) AS ActiveUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.ActiveUsers_Perc) + '%', AddFormat.ActiveUsers_Perc) AS ActiveUsers_Perc
		,COALESCE(CAST(WeekSum.ModerateUsers AS VARCHAR), AddFormat.ModerateUsers) AS ModerateUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.ModerateUsers_Perc) + '%', AddFormat.ModerateUsers_Perc) AS ModerateUsers_Perc
		,COALESCE(CAST(WeekSum.InfrequentUsers AS VARCHAR), AddFormat.InfrequentUsers) AS InfrequentUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.InfrequentUsers_Perc) + '%', AddFormat.InfrequentUsers_Perc) AS InfrequentUsers_Perc
		,COALESCE(CAST(WeekSum.InactiveUsers AS VARCHAR), AddFormat.InactiveUsers) AS InactiveUsers
		,COALESCE(CONVERT(VARCHAR,WeekSum.InactiveUsers_Perc) + '%', AddFormat.InactiveUsers_Perc) AS InactiveUsers_Perc
		,COALESCE(WeekSum.sortkey, AddFormat.sortkey) AS sortkey
		
	FROM (
		SELECT *
		FROM AddPerc
		) AS WeekSum
	FULL OUTER JOIN AddFormat
	ON WeekSum.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY COALESCE(Biggie.sortkey, DummyDates.sortkey)