/*
Adopt-Detail: Other Activities Query
author: joanna.tung
date: 2018.12.12

	This query returns an export that can produce the desired table format presented for the Adopt-Detail: Other Activities in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrTable functionality.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/



DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-09-16'
DECLARE @orgparam AS INT = 1085388

SELECT
	dOrg.[Name] AS OrganizationName
	, factMatch.OrganizationId
	--, dService.ServiceId  /* This should be Service Name!! */
	, CASE WHEN dService.PostalCode IS NULL THEN dOrg.PostalCode END AS ZipCode
	, dService.[ServiceTypeName]
	, SUM(TotalReferrals) as 'TotalReferrals'
	, SUM(Distance)/SUM(CASE WHEN Distance IS NULL THEN 0 ELSE TotalReferrals END) AS 'AvgDistance'
  
  FROM [vfact].[StandardMetricsAAGMatchDetail] as factMatch
  LEFT JOIN vdim.[Service] as dService
  ON dService.ServiceId = factMatch.ServiceId
  LEFT JOIN vdim.[Organization] as dOrg
  ON dOrg.OrganizationId = factMatch.OrganizationId

  WHERE CAST(factMatch.DateKey AS VARCHAR) >= @StartDate AND CAST(factMatch.DateKey AS VARCHAR) < DATEADD(DAY, 1, @EndDate)
	AND factMatch.OrganizationId in (@orgparam)

  GROUP BY dOrg.[Name]
	, factMatch.OrganizationId
	--, dService.ServiceId /* This should be Service Name!! */
	, CASE WHEN dService.PostalCode IS NULL THEN dOrg.PostalCode END
	, dService.[ServiceTypeName]

  ORDER BY OrganizationName, TotalReferrals DESC
