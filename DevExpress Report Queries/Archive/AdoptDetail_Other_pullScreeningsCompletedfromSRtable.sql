SELECT 
	
	Base3.OrganizationId
	,dOrg.[Name] as OrganizationName
	,Base3.UserId
	,CONCAT(dUSer.LastName, ', ',dUser.FirstName) AS 'UserName'
	,dUser.Title
	,dUser.Department
	,CASE WHEN Base3.SuccessfulLogins IS NULL THEN 0 ELSE Base3.SuccessfulLogins END AS SuccessfulLogins
	,CASE WHEN Base3.Nudges IS NULL THEN 0 ELSE Base3.Nudges END AS Nudges
	,CASE WHEN Base3.Searches IS NULL THEN 0 ELSE Base3.Searches END AS Searches
	,CASE WHEN Base3.ScreeningsCompleted IS NULL THEN 0 ELSE Base3.ScreeningsCompleted END AS ScreeningsCompleted

FROM (
	SELECT
		COALESCE(Base2.UserId, Search.Searches) AS UserId
		, COALESCE(Base2.OrganizationId, Search.OrganizationId) AS OrganizationId
		, Base2.SuccessfulLogins
		, Base2.ScreeningsCompleted
		, Base2.Nudges
		, Search.Searches

	FROM (
		SELECT
			COALESCE(Base1.UserId, PSINudges.UserId) AS UserId
			,COALESCE(Base1.OrganizationId, PSINudges.OrganizationId) AS OrganizationId
			, Base1.SuccessfulLogins
			, Base1.ScreeningsCompleted
			, PSINudges.Nudges
		FROM (
			SELECT
				COALESCE(Logins.UserId, Screenings.UserId) AS UserId
				,COALESCE(Logins.OrganizationId, Screenings.OrganizationId) AS OrganizationId
				,Logins.SuccessfulLogins
				,Screenings.ScreeningsCompleted

			FROM (
				/* By UserId, OrganizationId: Count of Logins */
					SELECT 
						UserId
						, CreatedByOrganizationId AS OrganizationId
						, COUNT(ExternalUserQuantity) AS SuccessfulLogins
					FROM [vFact].[ExternalUserActivity]
					WHERE ExternalUserActivityTypeId = 0
						AND CreatedByOrganizationId in (@orgparam)
						AND CreatedDate >= DATEADD(DAY, 0, @StartDate) AND CreatedDate < DATEADD(DAY, 1, @Enddate)
					GROUP BY UserId, CreatedByOrganizationId
					) AS Logins
			FULL OUTER JOIN (
				/* By UserId, OrganizationId: Count of Screenings Completed */
					SELECT
						SR.UserId
						, SR.OrganizationId
						, SUM(SR.ScreeningsCompleted) AS ScreeningsCompleted						
					FROM (
						SELECT 
							dimSR.ScreeningResponseId
							, dimSR.ScreeningTemplateId
							, dimSR.CareCoordinatorId
							, dimSR.CareorganizationDatabaseId
							, dimSR.CreatedDate
							, dimSR.CompletedDate
							, dimSR.SiteId
							, CC.UserId
							, dimSite.OrganizationId
							, 1 AS ScreeningsCompleted
						FROM vdim.ScreeningResponse as dimSR
						LEFT JOIN vdim.[Site] as dimSite
						ON dimSR.SiteId = dimSite.SiteId
						LEFT JOIN [vdim].[CareCoordinator] AS CC
						ON CC.CareCoordinatorId = dimSR.CareCoordinatorId AND CC.CareOrganizationDatabaseId = dimSR.CareOrganizationDatabaseId
						LEFT JOIN vdim.Patient as dPat
						ON CONCAT(dPat.PatientId, ';', dPat.CareOrganizationDatabaseId) = CONCAT(dimSR.PatientId, ';', dimSR.CareOrganizationDatabaseId)

						WHERE dimSR.CareCoordinatorId IS NOT NULL
							AND dimSR.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND dimSR.CreatedDate < DATEADD(DAY, 1, @Enddate)
							AND dimSR.CompletedDate IS NOT NULL
							AND dimSite.OrganizationId IN (@orgparam)
							AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL)
					) AS SR
					GROUP BY SR.UserId, SR.OrganizationId

				) AS Screenings		
				ON (Logins.UserId = Screenings.UserId AND Logins.OrganizationId = Screenings.OrganizationId)
			) AS Base1	
			FULL OUTER JOIN (
				/* By UserId, OrganizationId: Count of Nudges (non-Service Referral) */
				SELECT 
					factPSI.UserId
					, factPSI.OrganizationId
					, COUNT(factPSI.PatientSiteInteractionQuantity) AS Nudges
				FROM vfact.PatientSiteInteraction as factPSI
				LEFT JOIN vdim.Patient as dPat
				ON CONCAT(dPat.PatientId, ';', dPat.CareOrganizationDatabaseId) = CONCAT(factPSI.PatientId, ';', factPSI.CareOrganizationDatabaseId) 
				WHERE UserId IS NOT NULL
					AND factPSI.InteractionTypeId in (1,2)
					AND factPSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND factPSI.InteractionDate < DATEADD(DAY, 1, @Enddate)
					AND OrganizationId IN (@orgparam)
					AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
				GROUP BY factPSI.UserId, factPSI.OrganizationId
					) AS PSINudges
			ON (Base1.UserId = PSINudges.UserId AND Base1.OrganizationId = PSINudges.OrganizationId)
			) AS Base2
			FULL OUTER JOIN (
				/* By UserId, OrganizationId: Count of Searches */
				SELECT 
					factSC.UserId
					,factSC.OrganizationId
					,COUNT(factSC.SearchCriteriaQuantity) AS Searches
				FROM vfact.SearchCriteria AS factSC
				LEFT JOIN vdim.Patient AS dPat
				ON CONCAT(dPat.PatientId, ';', dPat.CareOrganizationDatabaseId) = CONCAT(factSC.PatientId, ';', factSC.CareOrganizationDatabaseId) 
				WHERE UserId IS NOT NULL
					AND factSC.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND factSC.CreatedDate < DATEADD(DAY, 1, @Enddate)
					AND OrganizationId IN (@orgparam)
					AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL)
					AND factSC.SearchType = 1
				GROUP BY factSC.UserId, factSC.OrganizationId
				) AS Search
			ON (Base2.UserId = Search.UserId AND Base2.OrganizationId = Search.OrganizationId)

		) AS Base3

JOIN vdim.[User] as dUser
ON Base3.UserId = dUser.UserId 
JOIN vdim.Organization as dOrg
ON Base3.OrganizationId = dOrg.OrganizationId

WHERE (dUser.ExcludeFromReports <> 1 OR dUser.ExcludeFromReports IS NULL) 

ORDER BY  UserName --OrganizationId,
