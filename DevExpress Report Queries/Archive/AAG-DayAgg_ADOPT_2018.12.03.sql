/*
At-A-Glance, Day Aggregation Query - ADOPT
author: joanna.tung
date: 2018.12.03

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid.
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires a calculation of the percent change between the selected week's total and the prior week's total
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) week aggregation, and 
			2) percent change between week aggregation
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) NoPerc (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses input from UI (user-selected week) to determine the WeekLastDay for the "Current Week" and "Prior Week" to select the correct records from
			the fact.StandardMetricsAAGAdoptWeekly table

		2) AddPerc (temp):
			OUTPUT: weekly values for any metrics that require the calculation of percentages
			LOGIC: The At-A-Glance report's Adopt section requires the calculation of each engagement category as a percentage of the total number of users.
			We cannot generate the week totals for this metric by simply summing all daily percentage values. I use this temp table to calculate this metric 
			for the weekly values, and will calculate the daily value in the "Base1" table in the main select statement.

		3) AddDelta (temp):  
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "AddPerc" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Perform full outer join to merge the week aggregation and percent change values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. NOTE: The "fact.StandardMetricsAAGAdopt..." tables differ from most other data mart tables in that
			it calculates engagement levels for every week of the year. There is thus no way for a user to get a "NULL" value for any given week of data. I therefore do not use 
			a "Dummydates" table as I do in other AAG queries.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @startdate AS DATETIME = DATEADD(DAY, -6, '2018-11-11') 
DECLARE @enddate AS DATETIME = '2018-11-11' 
--DECLARE @orgparam AS BIGINT = 1076764 --1100469, 1076764, 1106512, 1086675

;
WITH
NoPerc AS (
	SELECT 
		--, OrganizationId
		 SUM(TotalUsers) AS TotalUsers
		, SUM(EngagedUsers) AS EngagedUsers
		, SUM(ActiveUsers) AS ActiveUsers
		, SUM(ModerateUsers) AS ModerateUsers
		, SUM(InfrequentUsers) AS InfrequentUsers
		, SUM(InactiveUsers) AS InactiveUsers
		, WeekLastDay AS WeekLastDay
		, CASE WHEN WeekLastDay = @EndDate THEN 'Current Week'
			WHEN WeekLastDay = DATEADD(DAY, -1, @StartDate) THEN 'Last Week'
			ELSE 'Error' END AS 'DateHeader'
		FROM vfact.[StandardMetricsAAGAdoptWeekly]
		WHERE (WeekLastDay = @enddate OR WeekLastDay = DATEADD(DAY, -1, @startdate))
			AND OrganizationId IN (@orgparam) 
		GROUP BY WeekLastDay 
)
,
AddPerc AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,EngagedUsers))/CONVERT(FLOAT,TotalUsers))
			ELSE NULL END AS 'EngagedUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,ActiveUsers))/CONVERT(FLOAT,TotalUsers))
			ELSE NULL END AS 'ActiveUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,ModerateUsers))/CONVERT(FLOAT,TotalUsers))
			ELSE NULL END AS 'ModerateUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,InfrequentUsers))/CONVERT(FLOAT,TotalUsers))
			ELSE NULL END AS 'InfrequentUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,InactiveUsers))/CONVERT(FLOAT,TotalUsers))
			ELSE NULL END AS 'InactiveUsers_Perc'
	FROM NoPerc
)
,
AddDelta AS (
	SELECT 
		PriorWeek.WeekLastDay AS PWLD,
		ThisWeek.WeekLastDay AS TWLD,
		'Percent Change' AS 'DateHeader',

		CASE 
			WHEN (ThisWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.TotalUsers) - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers)))
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.TotalUsers) - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers)))
			ELSE NULL END AS TotalUsers,
		PriorWeek.TotalUsers AS PTotalUsers,
		ThisWeek.TotalUsers AS TTotalUsers,

		CASE 
			WHEN (ThisWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers) - CONVERT(FLOAT,PriorWeek.EngagedUsers))/CONVERT(FLOAT,PriorWeek.EngagedUsers)))
			ELSE NULL END AS EngagedUsers,
		PriorWeek.EngagedUsers AS PEngagedUsers,
		ThisWeek.EngagedUsers AS TEngagedUsers,
		
		CASE 
			WHEN (ThisWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers_Perc) - CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc))/CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc)))
			ELSE NULL END AS EngagedUsers_Perc,
		PriorWeek.EngagedUsers_Perc AS PEngagedUsers_Perc,
		ThisWeek.EngagedUsers_Perc AS TEngagedUsers_Perc,

		CASE 
			WHEN (ThisWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers) - CONVERT(FLOAT,PriorWeek.ActiveUsers))/CONVERT(FLOAT,PriorWeek.ActiveUsers)))
			ELSE NULL END AS ActiveUsers,
		PriorWeek.ActiveUsers AS PActiveUsers,
		ThisWeek.ActiveUsers AS TActiveUsers,
				
		CASE 
			WHEN (ThisWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers_Perc) - CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc)))
			ELSE NULL END AS ActiveUsers_Perc,
		PriorWeek.ActiveUsers_Perc AS PActiveUsers_Perc,
		ThisWeek.ActiveUsers_Perc AS TActiveUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers) - CONVERT(FLOAT,PriorWeek.ModerateUsers))/CONVERT(FLOAT,PriorWeek.ModerateUsers)))
			ELSE NULL END AS ModerateUsers,
		PriorWeek.ModerateUsers AS PModerateUsers,
		ThisWeek.ModerateUsers AS TModerateUsers,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers_Perc) - CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc))/CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc)))
			ELSE NULL END AS ModerateUsers_Perc,
		PriorWeek.ModerateUsers_Perc AS PModerateUsers_Perc,
		ThisWeek.ModerateUsers_Perc AS TModerateUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers) - CONVERT(FLOAT,PriorWeek.InfrequentUsers))/CONVERT(FLOAT,PriorWeek.InfrequentUsers)))
			ELSE NULL END AS InfrequentUsers,
		PriorWeek.InfrequentUsers AS PInfrequentUsers,
		ThisWeek.InfrequentUsers AS TInfrequentUsers,

		
		CASE 
			WHEN (ThisWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers_Perc) - CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc)))
			ELSE NULL END AS InfrequentUsers_Perc,
		PriorWeek.InfrequentUsers_Perc AS PInfrequentUsers_Perc,
		ThisWeek.InfrequentUsers_Perc AS TInfrequentUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InactiveUsers IS NOT NULL AND PriorWeek.InactiveUsers IS NOT NULL AND PriorWeek.InactiveUsers <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.InactiveUsers) - CONVERT(FLOAT,PriorWeek.InactiveUsers))/CONVERT(FLOAT,PriorWeek.InactiveUsers)))
			ELSE NULL END AS InactiveUsers,
		PriorWeek.InactiveUsers AS PInactiveUsers,
		ThisWeek.InactiveUsers AS TInactiveUsers,
		
		CASE 
			WHEN (ThisWeek.InactiveUsers_Perc IS NOT NULL AND PriorWeek.InactiveUsers_Perc IS NOT NULL AND PriorWeek.InactiveUsers_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.InactiveUsers_Perc) - CONVERT(FLOAT,PriorWeek.InactiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.InactiveUsers_Perc)))
			ELSE NULL END AS InactiveUsers_Perc,
		PriorWeek.InactiveUsers_Perc AS PInactiveUsers_Perc,
		ThisWeek.InactiveUsers_Perc AS TInactiveUsers_Perc

	FROM (
		SELECT * FROM AddPerc
		WHERE DateHeader = 'Current Week') AS ThisWeek
	FULL OUTER JOIN (
		SELECT * FROM AddPerc
		WHERE DateHeader = 'Last Week' ) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy --and ThisWeek.OrganizationId = PriorWeek.OrganizationId
)

SELECT 
	COALESCE(AddPerc.DateHeader, AddDelta.DateHeader) AS DateHeader
	,COALESCE(AddPerc.TotalUsers, AddDelta.TotalUsers) AS TotalUsers
	,COALESCE(AddPerc.EngagedUsers, AddDelta.EngagedUsers) AS EngagedUsers
	,COALESCE(AddPerc.EngagedUsers_Perc, AddDelta.EngagedUsers_Perc) AS EngagedUsers_Perc
	,COALESCE(AddPerc.ActiveUsers, AddDelta.ActiveUsers) AS ActiveUsers
	,COALESCE(AddPerc.ActiveUsers_Perc, AddDelta.ActiveUsers_Perc) AS ActiveUsers_Perc
	,COALESCE(AddPerc.ModerateUsers, AddDelta.ModerateUsers) AS ModerateUsers
	,COALESCE(AddPerc.ModerateUsers_Perc, AddDelta.ModerateUsers_Perc) AS ModerateUsers_Perc
	,COALESCE(AddPerc.InfrequentUsers, AddDelta.InfrequentUsers) AS InfrequentUsers
	,COALESCE(AddPerc.InfrequentUsers_Perc, AddDelta.InfrequentUsers_Perc) AS InfrequentUsers_Perc
	,COALESCE(AddPerc.InactiveUsers, AddDelta.InactiveUsers) AS InactiveUsers
	,COALESCE(AddPerc.InactiveUsers_Perc, AddDelta.InactiveUsers_Perc) AS InactiveUsers_Perc

FROM AddPerc
FULL OUTER JOIN AddDelta
ON AddPerc.DateHeader = AddDelta.DateHeader