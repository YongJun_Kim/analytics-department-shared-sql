/* 
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-08-31' 
DECLARE @OrgParam AS INT  = 1100668

;
WITH 

Dummydates AS (
	SELECT  
		
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS NudgeReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS ServicePrints 
		, CAST(0 AS VARCHAR) AS eRxReferralsShared
		, CAST(0 AS VARCHAR) AS TotalSharedSingleServiceReferrals
		, CAST(0 AS VARCHAR) AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay) AS [sortkey]

	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue

	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS NudgeReferralsShared
		, 'N/A' AS TrackedReferralsShared
		, 'N/A' AS ServicePrints 
		, 'N/A' AS eRxReferralsShared
		, 'N/A' AS TotalSharedSingleServiceReferrals
		, 'N/A' AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
		, '13' AS sortkey
)
,
Base AS (
	SELECT 
		COALESCE(vfactStandardMetricsPSI.MonthLastDay, vfactStandardMetricsUserActivity.MonthLastDay, vfactStandardMetricsErxService.MonthLastDay) AS MonthLastDay
		, COALESCE(vfactStandardMetricsPSI.DateHeader, vfactStandardMetricsUserActivity.DateHeader, vfactStandardMetricsErxService.DateHeader) AS DateHeader
		, CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END AS eRxReferralsShared
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)) AS TotalSharedSingleServiceReferrals
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)
		+ (CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END)) AS TotalSharedReferrals
		, ROW_NUMBER() OVER (ORDER BY COALESCE(vfactStandardMetricsPSI.MonthLastDay, vfactStandardMetricsUserActivity.MonthLastDay, vfactStandardMetricsErxService.MonthLastDay)) AS sortkey
		, ROW_NUMBER() OVER (ORDER BY COALESCE(vfactStandardMetricsPSI.MonthLastDay, vfactStandardMetricsUserActivity.MonthLastDay, vfactStandardMetricsErxService.MonthLastDay) DESC) AS [rPeriod]

			
	FROM (
	/* Metrics for electronic Single Service Nudges (Text/Email) and Tracked Referrals, by Day of Week */
		SELECT 
			dimDate.MonthLastDay
			, REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
			, SUM(factStandardMetricsPSI.SingleServiceReferralsShared) AS SingleServiceReferralsShared
		FROM vfact.StandardMetricsPSI AS factStandardMetricsPSI
			JOIN vdim.[Date] as dimDate
			ON dimDate.DateKey = factStandardMetricsPSI.DateKey
		WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
			AND OrganizationId in (@orgparam)
		GROUP BY MonthLastDay
		) AS vfactStandardMetricsPSI

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Single Service Prints, by Day of Week */
		SELECT 
			SUM(factStandardMetricsUserActivity.SingleServiceReferralPrinted) AS SingleServicePrints
			, REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
			, dimDate.MonthLastDay
		FROM vfact.StandardMetricsUserActivity AS factStandardMetricsUserActivity
			JOIN vdim.[Date] as dimDate
			ON dimDate.DateKey = factStandardMetricsUserActivity.DateKey
		WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
			AND OrganizationId in (@orgparam)
		GROUP BY MonthLastDay
		) AS vfactStandardMetricsUserActivity
	ON (vfactStandardMetricsUserActivity.MonthLastDay = vfactStandardMetricsPSI.MonthLastDay)

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Referrals on shared eRxs, by Day of Week */
		SELECT 
			dimDate.MonthLastDay
			, REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
			, SUM(factStandardMetricsErxService.eRxReferralsShared) AS eRxReferralsShared
		FROM vfact.StandardMetricsErxService AS factStandardMetricsErxService
			JOIN vdim.[Date] as dimDate
			ON dimDate.DateKey = factStandardMetricsErxService.DateKey
		WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
			AND OrganizationId in (@orgparam)
		GROUP BY MonthLastDay
		) AS vfactStandardMetricsErxService
	ON (vfactStandardMetricsErxService.MonthLastDay = vfactStandardMetricsPSI.MonthLastDay)

)
,
AddPerc AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN TotalSharedReferrals > 0 THEN
		/* Must convert to DECIMAL(19,3), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,TotalSharedSingleServiceReferrals)/CONVERT(FLOAT,TotalSharedReferrals),1))
			ELSE NULL END AS 'TotalSingleServiceReferralsShared_Perc'
		, CASE WHEN TotalSharedReferrals > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,eRxReferralsShared)/CONVERT(FLOAT,TotalSharedReferrals),1))
			ELSE NULL END AS 'eRxReferralsShared_Perc'
	FROM Base
)
,
AddDeltaWeek as (
	SELECT 
		'% Change' AS DateHeader,
		'13' AS sortkey,

		CASE 
			WHEN (ThisMonth.eRxReferralsShared IS NOT NULL AND PriorMonth.eRxReferralsShared IS NOT NULL AND PriorMonth.eRxReferralsShared <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.eRxReferralsShared) - CONVERT(FLOAT,PriorMonth.eRxReferralsShared))/CONVERT(FLOAT,PriorMonth.eRxReferralsShared),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.eRxReferralsShared IS NOT NULL AND PriorMonth.eRxReferralsShared <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.eRxReferralsShared))/CONVERT(FLOAT,PriorMonth.eRxReferralsShared),0)
			ELSE NULL END AS eRxReferralsShared,

		CASE 
			WHEN (ThisMonth.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorMonth.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorMonth.TotalSharedSingleServiceReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TotalSharedSingleServiceReferrals) - CONVERT(FLOAT,PriorMonth.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorMonth.TotalSharedSingleServiceReferrals),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorMonth.TotalSharedSingleServiceReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorMonth.TotalSharedSingleServiceReferrals),0)
			ELSE NULL END AS TotalSharedSingleServiceReferrals,

		CASE 
			WHEN (ThisMonth.TotalSharedReferrals IS NOT NULL AND PriorMonth.TotalSharedReferrals IS NOT NULL AND PriorMonth.TotalSharedReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TotalSharedReferrals) - CONVERT(FLOAT,PriorMonth.TotalSharedReferrals))/CONVERT(FLOAT,PriorMonth.TotalSharedReferrals),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TotalSharedReferrals IS NOT NULL AND PriorMonth.TotalSharedReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TotalSharedReferrals))/CONVERT(FLOAT,PriorMonth.TotalSharedReferrals),0)
			ELSE NULL end as TotalSharedReferrals,

		CASE 
			WHEN (ThisMonth.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorMonth.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorMonth.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TotalSingleServiceReferralsShared_Perc) - CONVERT(FLOAT,PriorMonth.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorMonth.TotalSingleServiceReferralsShared_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorMonth.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorMonth.TotalSingleServiceReferralsShared_Perc),0)
			ELSE NULL END AS TotalSingleServiceReferralsShared_Perc,

		CASE 
			WHEN (ThisMonth.eRxReferralsShared_Perc IS NOT NULL AND PriorMonth.eRxReferralsShared_Perc IS NOT NULL AND PriorMonth.eRxReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.eRxReferralsShared_Perc) - CONVERT(FLOAT,PriorMonth.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorMonth.eRxReferralsShared_Perc),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.eRxReferralsShared_Perc IS NOT NULL AND PriorMonth.eRxReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorMonth.eRxReferralsShared_Perc),0)
			ELSE NULL END AS eRxReferralsShared_Perc


	FROM (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM AddPerc
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDeltaWeek.eRxReferralsShared IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.eRxReferralsShared) + '%' END AS 'eRxReferralsShared'
		, CASE WHEN AddDeltaWeek.TotalSharedSingleServiceReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSharedSingleServiceReferrals) + '%' END AS 'TotalSharedSingleServiceReferrals'
		, CASE WHEN AddDeltaWeek.TotalSharedReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSharedReferrals) + '%' END AS 'TotalSharedReferrals'
		, CASE WHEN AddDeltaWeek.TotalSingleServiceReferralsShared_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSingleServiceReferralsShared_Perc) + '%' END AS 'TotalSingleServiceReferralsShared_Perc'
		, CASE WHEN AddDeltaWeek.eRxReferralsShared_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.eRxReferralsShared_Perc) + '%' END AS 'eRxReferralsShared_Perc'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.eRxReferralsShared, DummyDates.eRxReferralsShared) AS eRxReferralsShared
	, COALESCE(Biggie.TotalSharedSingleServiceReferrals, DummyDates.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
	, COALESCE(Biggie.TotalSharedReferrals, DummyDates.TotalSharedReferrals) AS TotalSharedReferrals
	, COALESCE(Biggie.TotalSingleServiceReferralsShared_Perc, DummyDates.TotalSingleServiceReferralsShared_Perc) AS TotalSingleServiceReferralsShared_Perc
	, COALESCE(Biggie.eRxReferralsShared_Perc, DummyDates.eRxReferralsShared_Perc) AS eRxReferralsShared_Perc
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey

FROM (
	SELECT 
		COALESCE(AddPerc.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(AddPerc.eRxReferralsShared AS VARCHAR), AddFormat.eRxReferralsShared) AS eRxReferralsShared
		,COALESCE(CAST(AddPerc.TotalSharedSingleServiceReferrals AS VARCHAR), AddFormat.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
		,COALESCE(CAST(AddPerc.TotalSharedReferrals AS VARCHAR), AddFormat.TotalSharedReferrals) AS TotalSharedReferrals
		/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR, AddPerc.TotalSingleServiceReferralsShared_Perc) + '%', AddFormat.TotalSingleServiceReferralsShared_Perc) AS TotalSingleServiceReferralsShared_Perc
		,COALESCE(CONVERT(VARCHAR, AddPerc.eRxReferralsShared_Perc) + '%', AddFormat.eRxReferralsShared_Perc) AS eRxReferralsShared_Perc
		, COALESCE(AddPerc.sortkey, AddFormat.sortkey) AS sortkey

	FROM AddPerc
	FULL OUTER JOIN AddFormat
	ON AddPerc.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY sortkey
