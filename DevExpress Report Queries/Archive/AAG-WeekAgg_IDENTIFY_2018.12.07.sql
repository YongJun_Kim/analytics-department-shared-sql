/*
At-A-Glance, Week Aggregation Query - IDENTIFY
author: joanna.tung
date: 2018.12.07

	This query returns an export that can produce the desired table format presented for the At-A-Glance, 4-week by week report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Last 4Wk Period" field), and 
			2) it requires a calculation of the percent change between the current 4Week period total and the prior 4Week period total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for week period for the current 4Week period 
			- There are row records for pre-calculated Prior 4Week Total, Current 4Week Total, and Percent Change between these 4Wk Date Periods
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) wek aggregation, 
			2) 4Wk Period aggregation, and 
			3) percent change between 4Week Period aggregations
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for each of the 4 weeks in the current 4Week period, where format lists
					  the start and end day for the week as follows: 'mm/dd - mm/dd'
					- one row record for the current 4Week period totals, prior 4Week period totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each week in the current 4Week period
			LOGIC: 1) contains necessary week aggregations for the current 4Week period and 2) serves as a base table from which to perform aggregations for
			the current 4Week period and the prior 4week period (see NoPercWeek table)

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the current 4Week period and the prior 4Week period
			LOGIC: uses Base (temp) table to produce necessary week aggregations to display in the "Current.." and "Prior.." columns of the final 
		
		4) AddPercWeek (temp):
			OUTPUT: percentages for 4week totals for current and last 4week date periods
			LOGIC: aggregate across the "DatePeriod" column from the NoPercWeek table

		5) AddDeltaWeek (temp):
			OUTPUT: percent change values for current 4Week period and the prior 4Week period
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the current and prior date period, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between the current and prior 4Week periods. Uses "dummy" field to
			perform 'arbitrary' join to append prior data to current data along the horizontal axis.

		6) AddFormat (temp):
			OUTPUT: percent change values for current 4week period and the prior 4week period, with '%' sign added for values that are not NULL, and 'N/A' 
			inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		7) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each week of the current 4week period, by the current 4week period, and by the prior 4week period, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from current to the prior 4week period.
			LOGIC: Perform full outer join to merge the individual week aggregations, 4week aggregations, and formatted percent change (AddFormat) values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = DATEADD(DAY, -27, '2019-01-20') 
DECLARE @EndDate AS DATETIME = '2019-01-20' 
DECLARE @OrgParam AS INT = 1077836
;

WITH

/* The At-a-Glance report requires that all weeks in the current 4 week period appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all necessary weeks. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 4 weeks of the current 4 week period appear in the export. */

Dummydates AS (
	SELECT  
		
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table. 
		   
		   NOTE: Must cast all integers as VARCHAR data type so that it will union properly with any 'N/A' values. */ 

		CASE 
			/* One case for each week in the Current 4Wk Period */
			WHEN DateValue >= @StartDate AND DateValue < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 7, @StartDate) AND DateValue < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 14, @StartDate) AND DateValue < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 21, @StartDate) AND DateValue < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
				END AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS ScreeningsCompleted
		, CAST(0 AS VARCHAR) AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, CAST(0 AS VARCHAR) AS NeedsIdentified
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)
	GROUP BY (CASE 
			/* One case for each week in the Current 4Wk Period */
			WHEN DateValue >= @StartDate AND DateValue < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 7, @StartDate) AND DateValue < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 14, @StartDate) AND DateValue < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 21, @StartDate) AND DateValue < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
			END)

	/* The AAG-day aggregation report requires us to provide values for "Current Week" totals, Prior Week" totals, and a separate column for "Percent Change" (betwen weeks). 
	   Create dummy rows to ensure that these column header fields are displayed, even when there are no available data. */
	
	UNION ALL

	SELECT
		'Last 4Wk Period' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS ScreeningsCompleted
		, CAST(0 AS VARCHAR) AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, CAST(0 AS VARCHAR) AS NeedsIdentified
		
	UNION ALL

	SELECT
		'Current 4Wk Period' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS ScreeningsCompleted
		, CAST(0 AS VARCHAR) AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, CAST(0 AS VARCHAR) AS NeedsIdentified
	
	UNION ALL

	SELECT
		'Percent Change' AS 'DateHeader'
		, 'N/A' AS ScreeningsCompleted
		, 'N/A' AS ScreeningsIdentifyingNeed
		, 'N/A' AS ScreeningsIdentifyingNeed_Perc
		, 'N/A' AS NeedsIdentified
)
,

/* This temp table returns aggregated metrics, by day of week. Query parameters are applied here to ensure that only the relevant
   row records are included in the aggregates. */

Base AS (
	SELECT
		/* Tag by Individual Weeks and Last 4 Wk Period */ 
		 CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 7, @StartDate) AND DateKey < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 14, @StartDate) AND DateKey < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 21, @StartDate) AND DateKey < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
				END AS 'DateHeader'
		/* Tag by Last 4Wk and Current 4Wk Period */
		, CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
				END AS 'DatePeriod'	 
		, SUM(vfactStandardMetricsPSI.ScreengingsCompleted) AS ScreeningsCompleted
		, SUM(vfactStandardMetricsPSI.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
		, SUM(vfactStandardMetricsPSI.NeedsIdentified) AS NeedsIdentified

	FROM vfact.StandardMetricsPSI AS vfactStandardMetricsPSI

	WHERE DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < DATEADD(DAY, 1, @EndDate)
		AND OrganizationId in (@orgparam)
	
	GROUP BY
		/* First Group By Last vs. Current 4Wk Period */ 
		(CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
			END),
		/* Then Group By Last 4Wk Period and Individual Weeks of the Current 4Wk Period */
		(CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 7, @StartDate) AND DateKey < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 14, @StartDate) AND DateKey < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 21, @StartDate) AND DateKey < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
			END)
)
,

/* The AAG Report requires us to display aggregated values for each metric, for the selected week and the prior week.
   This temp table returns the aggregated values for each metric, grouped by the selected week and the prior week. */


NoPercWeek AS (
	SELECT 	
		DatePeriod
		, SUM(Base.ScreeningsCompleted) AS ScreeningsCompleted
		, SUM(Base.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed 
		, SUM(Base.NeedsIdentified) AS NeedsIdentified
	FROM Base
	GROUP BY DatePeriod
)
,

/* The AAG Report's Identify section requires us to provide metrics that involve calculating percentage values by day, and by week. As discussed above, we cannot calculate the weekly
   value by summing over the percentage values calcluated for each day. I use this temp table to add this percent metric for this week and prior week totals. NOTE: I add the daily percentage
   values in the main select statement (see Base1 table). */

AddPercWeek AS (
	SELECT *
		,'Dummy' AS Dummy
		,CASE WHEN ScreeningsCompleted > 0 THEN
		/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,ScreeningsIdentifyingNeed)/CONVERT(FLOAT,ScreeningsCompleted),1))
			ELSE NULL END AS 'ScreeningsIdentifyingNeed_Perc'
	FROM NoPercWeek
)
,

/* The AAG report requires us to display the percent change in metrics for this week and the prior week. 
   This temp table returns the percent change between the aggregated sum of each metric recorded for this week and last week. */

AddDeltaWeek AS (

	SELECT 
		'Percent Change' AS DateHeader,

		/* Create conditions to account for situations in which:
			1) records may not exist for this week and/or last week 
				- Because we use a full outer join (see below), the resulting table returns "NULL" values for any week during which searches were not conducted.
			2) where divisor may be zero (records of searches do not exist for "last week"). */

		CASE 
		/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisWeek.ScreeningsCompleted IS NOT NULL and PriorWeek.ScreeningsCompleted IS NOT NULL AND PriorWeek.ScreeningsCompleted <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ScreeningsCompleted - PriorWeek.ScreeningsCompleted))/CONVERT(FLOAT,PriorWeek.ScreeningsCompleted),0)

		/* This second condition addresses the situation in which there are no records of searches for this week (ThisWeek.DatePeriod is null), and
		   last week's values are neither null nor zero. */ 
			WHEN (ThisWeek.DatePeriod IS NULL AND PriorWeek.ScreeningsCompleted IS NOT NULL AND PriorWeek.ScreeningsCompleted <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorWeek.ScreeningsCompleted))/CONVERT(FLOAT, PriorWeek.ScreeningsCompleted),0)
			
		/* Use condition "else NULL" for situations in which divisor is zero */	
			ELSE NULL END AS ScreeningsCompleted,

		CASE 
			WHEN (ThisWeek.ScreeningsIdentifyingNeed IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ScreeningsIdentifyingNeed - PriorWeek.ScreeningsIdentifyingNeed))/CONVERT(FLOAT,PriorWeek.ScreeningsIdentifyingNeed),0)
			WHEN (ThisWeek.DatePeriod IS NULL AND PriorWeek.ScreeningsIdentifyingNeed IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ScreeningsIdentifyingNeed - PriorWeek.ScreeningsIdentifyingNeed))/CONVERT(FLOAT,PriorWeek.ScreeningsIdentifyingNeed),0)
			ELSE NULL END AS ScreeningsIdentifyingNeed,

		CASE 
			WHEN (ThisWeek.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ScreeningsIdentifyingNeed_Perc - PriorWeek.ScreeningsIdentifyingNeed_Perc))/CONVERT(FLOAT,PriorWeek.ScreeningsIdentifyingNeed_Perc),0)
			WHEN (ThisWeek.DatePeriod IS NULL AND PriorWeek.ScreeningsIdentifyingNeed_Perc IS NOT NULL AND PriorWeek.ScreeningsIdentifyingNeed_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ScreeningsIdentifyingNeed_Perc - PriorWeek.ScreeningsIdentifyingNeed_Perc))/CONVERT(FLOAT,PriorWeek.ScreeningsIdentifyingNeed_Perc),0)
			ELSE NULL END AS ScreeningsIdentifyingNeed_Perc,

		CASE 
			WHEN (ThisWeek.NeedsIdentified IS NOT NULL AND PriorWeek.NeedsIdentified IS NOT NULL AND PriorWeek.NeedsIdentified <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.NeedsIdentified - PriorWeek.NeedsIdentified))/CONVERT(FLOAT,PriorWeek.NeedsIdentified),0)
			WHEN (ThisWeek.DatePeriod IS NULL AND PriorWeek.NeedsIdentified IS NOT NULL AND PriorWeek.NeedsIdentified <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.NeedsIdentified - PriorWeek.NeedsIdentified))/CONVERT(FLOAT,PriorWeek.NeedsIdentified),0)
			ELSE NULL END AS NeedsIdentified

	FROM (
		SELECT * FROM AddPercWeek
		WHERE DatePeriod = 'Current 4Wk Period'
			) AS ThisWeek

	/* In order to calculate the percent difference between this week and last week, we must move this week and last week's aggregated sum of searches into a single
	   row record. Using a full outer join allows us to do this. By joining the selected week data with the prior week data on a "dummy" value, 
	   we can collapse last week and this week's records into a single row. */

	FULL OUTER JOIN (
		SELECT * FROM AddPercWeek
		WHERE DatePeriod = 'Last 4Wk Period'
			) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDeltaWeek.ScreeningsCompleted IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.ScreeningsCompleted) + '%' END AS 'ScreeningsCompleted'
		, CASE WHEN AddDeltaWeek.ScreeningsIdentifyingNeed IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.ScreeningsIdentifyingNeed) + '%' END AS 'ScreeningsIdentifyingNeed'
		, CASE WHEN AddDeltaWeek.ScreeningsIdentifyingNeed_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.ScreeningsIdentifyingNeed_Perc) + '%' END AS 'ScreeningsIdentifyingNeed_Perc'
		, CASE WHEN AddDeltaWeek.NeedsIdentified IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.NeedsIdentified) + '%' END AS 'NeedsIdentified'
	FROM AddDeltaWeek
)
--SELECT * FROM DummyDates
SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.ScreeningsCompleted, DummyDates.ScreeningsCompleted) AS ScreeningsCompleted
	, COALESCE(Biggie.ScreeningsIdentifyingNeed, DummyDates.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
	, COALESCE(Biggie.ScreeningsIdentifyingNeed_Perc, DummyDates.ScreeningsIdentifyingNeed_Perc) AS ScreeningsIdentifyingNeed_Perc
	, COALESCE(Biggie.NeedsIdentified, DummyDates.NeedsIdentified) AS NeedsIdentified

FROM (
	SELECT 
		COALESCE(WeekDays.DateHeader, AddPercWeek.DatePeriod, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(WeekDays.ScreeningsCompleted AS VARCHAR), CAST(AddPercWeek.ScreeningsCompleted AS VARCHAR), AddFormat.ScreeningsCompleted) AS ScreeningsCompleted
		,COALESCE(CAST(WeekDays.ScreeningsIdentifyingNeed AS VARCHAR), CAST(AddPercWeek.ScreeningsIdentifyingNeed AS VARCHAR), AddFormat.ScreeningsIdentifyingNeed) AS ScreeningsIdentifyingNeed
		,COALESCE(CAST(WeekDays.NeedsIdentified AS VARCHAR), CAST(AddPercWeek.NeedsIdentified AS VARCHAR), AddFormat.NeedsIdentified) AS NeedsIdentified
	/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR, WeekDays.ScreeningsIdentifyingNeed_Perc) + '%', CONVERT(VARCHAR, AddPercWeek.ScreeningsIdentifyingNeed_Perc) + '%', AddFormat.ScreeningsIdentifyingNeed_Perc) AS ScreeningsIdentifyingNeed_Perc
		
	FROM (

		/* As stated above, the Identify section of the AAG report requires the calculation of "% Screenings Identifying a Need" by day, and by week. I calculate the Week value for this metric
		   in the "AddPercWeek" temp table above, and calculate the day value for this metric in the table below. */

			SELECT * 
			,CASE WHEN ScreeningsCompleted > 0 THEN
			/* Must convert to DECIMAL(19,1) before converting to varchar, else whole-value numbers like "100.0%" will show up as "100%." */
				CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,ScreeningsIdentifyingNeed)/CONVERT(FLOAT,ScreeningsCompleted),1))
				ELSE NULL END AS 'ScreeningsIdentifyingNeed_Perc'
			FROM Base
			WHERE Base.DatePeriod = 'Current 4Wk Period'

		) AS WeekDays
	FULL OUTER JOIN AddPercWeek
	ON AddPercWeek.DatePeriod = WeekDays.DateHeader
	FULL OUTER JOIN AddFormat
	ON AddPercWeek.DatePeriod = AddFormat.DateHeader
	) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY DateHeader