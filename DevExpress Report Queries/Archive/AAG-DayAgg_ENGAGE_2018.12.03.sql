/* 
At-A-Glance, Day Aggregation Query - ENGAGE
author: joanna.tung
date: 2018.11.30

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid.
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Prior Week Total" field), and 
			2) it requires a calculation of the percent change between the selected week's total and the prior week's total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for each day of the selected week (7 days), and are formatted as "mm/dd" 
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) day of week aggregation, 
			2) week aggregation, and 
			3) percent change between week aggregation
		- Create a temp tables to produce metrics for each of the three categories above
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.
		- Because the ENGAGE section requires a distinct count of patients by day and week periods, we cannot generate weekly values by simply summing across all 7 days of the
		  week. Use Data Marts generated for the required aggregation level (Daily, Weekly, etc.) in order to get an accurate count of distinct patients for each required date period.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT: table of all 7 days of the week with
				- the correct DateHeader formatting for all 7 days of the selected week
				- row records for the selected week's total and prior week totals
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each day in the selected week
			LOGIC: uses "fact.StandardMetricsAAGEngagementDaily" table to produce day aggregations for the selected week

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses "fact.StandardMetricsAAGEngagementWeekly" table to produce necessary week aggregations to display in the "Current Week" and "Prior Week" columns of the final report

		4) AddDelta (temp):
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each day of the selected week, by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Perform full outer join to merge the individual day aggregation (Base1), week aggregation (AddPercWeek), and percent change (AddDeltaWeek) 
			values into a single table.  Use "coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all
			required row records for "DateKey" exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = DATEADD(DAY, -6, '2018-11-11') 
DECLARE @EndDate AS DATETIME = '2018-11-11' 
DECLARE @OrgParam AS INT  = 1106525
;

WITH 
Dummydates AS (
	SELECT  
		  CONCAT(LEFT(RIGHT(dimDate.DateKey,4),2), '/', RIGHT(dimDate.DateKey,2)) AS 'DateHeader'
		, NULL AS InteractionPatientCount
		, NULL AS PatientsReceivingReferrals
		, NULL AS AvgReferralsReceivedByPatients
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)

	UNION ALL

	SELECT
		'Last Week' AS 'DateHeader'
		, NULL AS InteractionPatientCount
		, NULL AS PatientsReceivingReferrals
		, NULL AS AvgReferralsReceivedByPatients
		
	UNION ALL

	SELECT
		'Current Week' AS 'DateHeader'
		, NULL AS InteractionPatientCount
		, NULL AS PatientsReceivingReferrals
		, NULL AS AvgReferralsReceivedByPatients
)
,
Base AS (
	SELECT 
		 CONCAT(LEFT(RIGHT(factStandardMetricsAAGEngagementDaily.DateKey,4),2), '/', RIGHT(factStandardMetricsAAGEngagementDaily.DateKey,2)) AS 'DateHeader'
		, SUM(factStandardMetricsAAGEngagementDaily.InteractionPatientCount) AS InteractionPatientCount
		, SUM(factStandardMetricsAAGEngagementDaily.PatientsReceivingReferralsQty) AS PatientsReceivingReferrals
		, CASE WHEN SUM(factStandardMetricsAAGEngagementDaily.PatientsReceivingReferralsQty) > 0 THEN
			ROUND(CONVERT(FLOAT,SUM(factStandardMetricsAAGEngagementDaily.ReferralsReceivedByPatientsQty))/CONVERT(FLOAT,SUM(factStandardMetricsAAGEngagementDaily.PatientsReceivingReferralsQty)), 0)
			ELSE NULL END AS AvgReferralsReceivedByPatients
	FROM vfact.StandardMetricsAAGEngagementDaily AS factStandardMetricsAAGEngagementDaily
	WHERE CAST(factStandardMetricsAAGEngagementDaily.DateKey AS VARCHAR(8)) >= @StartDate
		AND CAST(factStandardMetricsAAGEngagementDaily.DateKey AS VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
		AND OrganizationId IN (@orgparam)
	GROUP BY DateKey
)
,
NoPercWeek AS (
	SELECT 
		'Dummy' AS Dummy
		, SUM(InteractionPatientCount) AS InteractionPatientCount
		, SUM(PatientsReceivingReferralsQty) AS PatientsReceivingReferrals
		, CASE WHEN SUM(PatientsReceivingReferralsQty) > 0 THEN
			ROUND(CONVERT(FLOAT,SUM(ReferralsReceivedByPatientsQty))/CONVERT(FLOAT,SUM(PatientsReceivingReferralsQty)) ,0)
			ELSE NULL END AS AvgReferralsReceivedByPatients
		,CASE WHEN WeekLastDay = @EndDate THEN 'Current Week'
			WHEN WeekLastDay = DATEADD(DAY, -1, @StartDate) THEN 'Last Week'
			ELSE 'Error' END AS 'DateHeader'
		/* To get the right count of distinct patients for the Week period, use the Weekly table */
		FROM [vfact].[StandardMetricsAAGEngagementWeekly]
		WHERE (WeekLastDay = @EndDate or WeekLastDay = DATEADD(DAY, -1, @StartDate)) 
			AND OrganizationId IN (@orgparam)
		GROUP BY WeekLastDay

)
, 
DeltaWeek AS (
	SELECT 
		PriorWeek.DateHeader AS PWeekKey
		, ThisWeek.DateHeader AS TWeekKey
		, 'Percent Change' AS DateHeader
		, CASE 
			WHEN (ThisWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InteractionPatientCount) - CONVERT(FLOAT,PriorWeek.InteractionPatientCount))/CONVERT(FLOAT,PriorWeek.InteractionPatientCount),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InteractionPatientCount) - CONVERT(FLOAT,PriorWeek.InteractionPatientCount))/CONVERT(FLOAT,PriorWeek.InteractionPatientCount),0)
			ELSE NULL END AS InteractionPatientCount
		, PriorWeek.InteractionPatientCount as PInteractionPatientCount
		, ThisWeek.InteractionPatientCount as TInteractionPatientCount
		
		, CASE 
			WHEN (ThisWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.PatientsReceivingReferrals) - CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals))/CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.PatientsReceivingReferrals) - CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals))/CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals),0)
			ELSE NULL END AS PatientsReceivingReferrals
		, PriorWeek.PatientsReceivingReferrals as PPatientsReceivingReferrals
		, ThisWeek.PatientsReceivingReferrals as TPatientsReceivingReferrals
		
		, CASE 
			WHEN (ThisWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.AvgReferralsReceivedByPatients) - CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients))/CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.AvgReferralsReceivedByPatients) - CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients))/CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients),0)
			ELSE NULL END AS AvgReferralsReceivedByPatients
		, PriorWeek.AvgReferralsReceivedByPatients as PAvgReferralsReceivedByPatients
		, ThisWeek.AvgReferralsReceivedByPatients as TAvgReferralsReceivedByPatients

	FROM (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Current Week'
			) AS ThisWeek
	FULL OUTER JOIN (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Last Week'
			) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)

SELECT
	COALESCE(WeekDays.DateHeader, NoPercWeek.DateHeader, DeltaWeek.DateHeader) AS DateHeader
	, COALESCE(WeekDays.InteractionPatientCount, NoPercWeek.InteractionPatientCount, DeltaWeek.InteractionPatientCount) AS InteractionPatientCount
	, COALESCE(WeekDays.PatientsReceivingReferrals, NoPercWeek.PatientsReceivingReferrals, DeltaWeek.PatientsReceivingReferrals) AS PatientsReceivingReferrals
	, COALESCE(WeekDays.AvgReferralsReceivedByPatients, NoPercWeek.AvgReferralsReceivedByPatients, DeltaWeek.AvgReferralsReceivedByPatients) AS AvgReferralsReceivedByPatients

FROM (
	SELECT 
		COALESCE(Base.DateHeader, Dummydates.DateHeader) AS DateHeader
		, COALESCE(Base.InteractionPatientCount, Dummydates.InteractionPatientCount) AS InteractionPatientCount
		, COALESCE(Base.PatientsReceivingReferrals, Dummydates.PatientsReceivingReferrals) AS PatientsReceivingReferrals
		, COALESCE(Base.AvgReferralsReceivedByPatients, Dummydates.AvgReferralsReceivedByPatients) AS AvgReferralsReceivedByPatients

	FROM Base
	RIGHT OUTER JOIN DummyDates
	ON Base.DateHeader = DummyDates.DateHeader
	) AS WeekDays
FULL OUTER JOIN NoPercWeek
ON NoPercWeek.DateHeader = WeekDays.DateHeader
FULL OUTER JOIN DeltaWeek
ON NoPercWeek.DateHeader = DeltaWeek.DateHeader
ORDER BY DateHeader