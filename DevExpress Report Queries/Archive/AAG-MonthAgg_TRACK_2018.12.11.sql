/*
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-08-31' 
DECLARE @OrgParam AS INT  = 1100668

;
WITH 

Dummydates AS (
	SELECT  
		  
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedUnsuccessfully
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay) AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue
	
	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS TrackedReferralsShared
		, 'N/A' AS TrackedServiceReferralsClosedSuccessfully
		, 'N/A' AS TrackedServiceReferralsClosedUnsuccessfully
		, '13' AS sortkey
)
,
Base AS (

	SELECT 
		COALESCE(vfactStandardMetricsTrackedReferral.DateHeader, vfactStandardMetricsPSI.DateHeader) AS DateHeader
		, COALESCE(vfactStandardMetricsTrackedReferral.MonthLastDay, vfactStandardMetricsPSI.MonthLastDay) AS MonthLastDay
		, 'Dummy' AS Dummy
		, (CASE WHEN vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully IS NULL THEN 0 
			ELSE vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully END) AS TrackedServiceReferralsClosedSuccessfully
		, (CASE WHEN vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully IS NULL THEN 0 
			ELSE vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully END) as TrackedServiceReferralsClosedUnsuccessfully
		, (CASE WHEN vfactStandardMetricsPSI.TrackedReferralsShared IS NULL THEN 0 
			ELSE vfactStandardMetricsPSI.TrackedReferralsShared END) AS TrackedReferralsShared
		, ROW_NUMBER() OVER (ORDER BY COALESCE(vfactStandardMetricsTrackedReferral.MonthLastDay, vfactStandardMetricsPSI.MonthLastDay)) AS sortkey
		, ROW_NUMBER() OVER (ORDER BY COALESCE(vfactStandardMetricsTrackedReferral.MonthLastDay, vfactStandardMetricsPSI.MonthLastDay) DESC) AS [rPeriod]

	FROM (
		SELECT 
			REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
			, dimDate.MonthLastDay			
			, SUM(factStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
			, SUM(factStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully

		FROM vfact.StandardMetricsTrackedReferral AS factStandardMetricsTrackedReferral
			JOIN vdim.[Date] as dimDate
			ON dimDate.DateKey = factStandardMetricsTrackedReferral.DateKey
		WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
			AND OrganizationId in (@orgparam)
	
		GROUP BY dimDate.MonthLastDay	
		) AS vfactStandardMetricsTrackedReferral

	FULL OUTER JOIN (
		SELECT 
			REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
			, dimDate.MonthLastDay
			, SUM(factStandardMetricsPSI.TrackedReferralsShared) AS TrackedReferralsShared

		FROM vfact.StandardMetricsPSI AS factStandardMetricsPSI
			JOIN vdim.[Date] as dimDate
			ON dimDate.DateKey = factStandardMetricsPSI.DateKey
		WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
			AND OrganizationId in (@orgparam)
		GROUP BY MonthLastDay
		) AS vfactStandardMetricsPSI
	ON vfactStandardMetricsTrackedReferral.DateHeader = vfactStandardMetricsPSI.DateHeader 
)
,
AddDeltaWeek AS (
	SELECT 
		'% Change' AS 'DateHeader',
		'13' AS sortkey,
		CASE 
			WHEN (ThisMonth.TrackedReferralsShared IS NOT NULL and PriorMonth.TrackedReferralsShared IS NOT NULL and PriorMonth.TrackedReferralsShared <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedReferralsShared) - CONVERT(FLOAT,PriorMonth.TrackedReferralsShared))/CONVERT(FLOAT,PriorMonth.TrackedReferralsShared),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedReferralsShared IS NOT NULL AND PriorMonth.TrackedReferralsShared <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorMonth.TrackedReferralsShared))/CONVERT(FLOAT, PriorMonth.TrackedReferralsShared),0)
			ELSE NULL END AS TrackedReferralsShared,

		CASE 
			WHEN (ThisMonth.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedSuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedServiceReferralsClosedSuccessfully) - CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedSuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedSuccessfully),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedSuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedSuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedSuccessfully),0)
			ELSE NULL END AS TrackedServiceReferralsClosedSuccessfully,

		CASE 
			WHEN (ThisMonth.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.TrackedServiceReferralsClosedUnsuccessfully) - CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedUnsuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedUnsuccessfully),0)
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorMonth.TrackedServiceReferralsClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedUnsuccessfully))/CONVERT(FLOAT,PriorMonth.TrackedServiceReferralsClosedUnsuccessfully),0)
			ELSE NULL END AS TrackedServiceReferralsClosedUnsuccessfully
	
	FROM (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDeltaWeek.TrackedReferralsShared IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedReferralsShared) + '%' END AS 'TrackedReferralsShared'
		, CASE WHEN AddDeltaWeek.TrackedServiceReferralsClosedSuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedServiceReferralsClosedSuccessfully) + '%' END AS 'TrackedServiceReferralsClosedSuccessfully'
		, CASE WHEN AddDeltaWeek.TrackedServiceReferralsClosedUnsuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedServiceReferralsClosedUnsuccessfully) + '%' END AS 'TrackedServiceReferralsClosedUnsuccessfully'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TrackedReferralsShared, DummyDates.TrackedReferralsShared) AS TrackedReferralsShared
	, COALESCE(Biggie.TrackedServiceReferralsClosedSuccessfully, DummyDates.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
	, COALESCE(Biggie.TrackedServiceReferralsClosedUnsuccessfully, DummyDates.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey

FROM (
	SELECT 
		 COALESCE(Base.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(Base.TrackedReferralsShared AS VARCHAR), AddFormat.TrackedReferralsShared) AS TrackedReferralsShared
		, COALESCE(CAST(Base.TrackedServiceReferralsClosedSuccessfully AS VARCHAR), AddFormat.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
		, COALESCE(CAST(Base.TrackedServiceReferralsClosedUnsuccessfully AS VARCHAR), AddFormat.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully
		, COALESCE(Base.sortkey, AddFormat.sortkey) AS sortkey
	FROM Base
	FULL OUTER JOIN AddFormat
	ON Base.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY sortkey