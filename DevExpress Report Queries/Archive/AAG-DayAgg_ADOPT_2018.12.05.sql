/*
At-A-Glance, Day Aggregation Query - ADOPT
author: joanna.tung
date: 2018.12.05

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires a calculation of the percent change between the selected week's total and the prior week's total
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

		In addition, the report requires the calculation of percentages in the final report. 
			- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign. 
			- For the metrics that utilize percentages, as per the existing weekly usage report, these values will be rounded to the nearest 0.1% and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) week aggregation, and 
			2) percent change between week aggregation
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record for the selected week's totals, prior week totals, and percent change fields
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) NoPerc (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses input from UI (user-selected week) to determine the WeekLastDay for the "Current Week" and "Prior Week" to select the correct records from
			the fact.StandardMetricsAAGAdoptWeekly table

		3) AddPerc (temp):
			OUTPUT: weekly values for any metrics that require the calculation of percentages
			LOGIC: The At-A-Glance report's Adopt section requires the calculation of each engagement category as a percentage of the total number of users.
			We cannot generate the week totals for this metric by simply summing all daily percentage values. I use this temp table to calculate this metric 
			for the weekly values, and will calculate the daily value in the "Base1" table in the main select statement.

		4) AddDelta (temp):  
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "AddPerc" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) AddFormat (temp):
			OUTPUT: percent change values for selected week and the prior week, with '%' sign added for values that are not NULL, and 'N/A' inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		6) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Perform full outer join to merge the week aggregation and percent change values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @startdate AS DATETIME = DATEADD(DAY, -6, '2018-11-11') 
DECLARE @enddate AS DATETIME = '2018-11-11' 
DECLARE @orgparam AS BIGINT = 1077836--1100469, 1076764, 1106512, 1086675

;
WITH
/* The At-a-Glance report requires that we display current week, prior week, and percent change columns, even if the fact table contains no data for the given date period.
   We use an outer join to the DummyDates temp table in the final select statement below in order to ensure that these row records appear in the export
   regardless if data exists. */

Dummydates AS (

	/* The AAG-day aggregation report requires us to provide values for "Current Week" totals, Prior Week" totals, and a separate column for "Percent Change" (betwen weeks). 
	   Create dummy rows to ensure that these column header fields are displayed, even when there are no available data. */

	SELECT
		'Last Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalUsers
		, CAST(0 AS VARCHAR) AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, CAST(0 AS VARCHAR) AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, CAST(0 AS VARCHAR) AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, CAST(0 AS VARCHAR) AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, CAST(0 AS VARCHAR) AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
		
	UNION ALL

	SELECT
		'Current Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalUsers
		, CAST(0 AS VARCHAR) AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, CAST(0 AS VARCHAR) AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, CAST(0 AS VARCHAR) AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, CAST(0 AS VARCHAR) AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, CAST(0 AS VARCHAR) AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
	
	UNION ALL

	SELECT
		'Percent Change' AS 'DateHeader'
		, 'N/A'AS TotalUsers
		, 'N/A' AS EngagedUsers
		, 'N/A' AS EngagedUsers_Perc
		, 'N/A' AS ActiveUsers
		, 'N/A' AS ActiveUsers_Perc
		, 'N/A' AS ModerateUsers
		, 'N/A' AS ModerateUsers_Perc
		, 'N/A' AS InfrequentUsers
		, 'N/A' AS InfrequentUsers_Perc
		, 'N/A' AS InactiveUsers
		, 'N/A' AS InactiveUsers_Perc
)
,

NoPerc AS (
	SELECT 
		 SUM(TotalUsers) AS TotalUsers
		, SUM(EngagedUsers) AS EngagedUsers
		, SUM(ActiveUsers) AS ActiveUsers
		, SUM(ModerateUsers) AS ModerateUsers
		, SUM(InfrequentUsers) AS InfrequentUsers
		, SUM(InactiveUsers) AS InactiveUsers
		, WeekLastDay AS WeekLastDay
		, CASE WHEN WeekLastDay = @EndDate THEN 'Current Week'
			WHEN WeekLastDay = DATEADD(DAY, -1, @StartDate) THEN 'Last Week'
			ELSE 'Error' END AS 'DateHeader'
		FROM vfact.[StandardMetricsAAGAdoptWeekly]
		WHERE (WeekLastDay = @enddate OR WeekLastDay = DATEADD(DAY, -1, @startdate))
			AND OrganizationId IN (@orgparam) 
		GROUP BY WeekLastDay 
)
,
AddPerc AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN "TotalUsers" > 0 THEN
		/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,EngagedUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'EngagedUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT, ActiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ActiveUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,ModerateUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'ModerateUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InfrequentUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InfrequentUsers_Perc'
		, CASE WHEN "TotalUsers" > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,InactiveUsers)/CONVERT(FLOAT,TotalUsers),1))
			ELSE NULL END AS 'InactiveUsers_Perc'
	FROM NoPerc
)
,
AddDelta AS (
	SELECT 
		'Percent Change' AS 'DateHeader',

		CASE 
			WHEN (ThisWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalUsers) - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalUsers IS NOT NULL AND PriorWeek.TotalUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalUsers))/CONVERT(FLOAT,PriorWeek.TotalUsers),0)
			ELSE NULL END AS TotalUsers,

		CASE 
			WHEN (ThisWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers <> 0) THEN 
				ROUND((100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers) - CONVERT(FLOAT,PriorWeek.EngagedUsers))/CONVERT(FLOAT,PriorWeek.EngagedUsers)),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.EngagedUsers IS NOT NULL AND PriorWeek.EngagedUsers <> 0) THEN 
				ROUND((100 * (0 - CONVERT(FLOAT,PriorWeek.EngagedUsers))/CONVERT(FLOAT,PriorWeek.EngagedUsers)),0)
			ELSE NULL END AS EngagedUsers,
		
		CASE 
			WHEN (ThisWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.EngagedUsers_Perc) - CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc))/CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.EngagedUsers_Perc IS NOT NULL AND PriorWeek.EngagedUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc))/CONVERT(FLOAT,PriorWeek.EngagedUsers_Perc),0)
			ELSE NULL END AS EngagedUsers_Perc,

		CASE 
			WHEN (ThisWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers) - CONVERT(FLOAT,PriorWeek.ActiveUsers))/CONVERT(FLOAT,PriorWeek.ActiveUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ActiveUsers IS NOT NULL AND PriorWeek.ActiveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ActiveUsers))/CONVERT(FLOAT,PriorWeek.ActiveUsers),0)
			ELSE NULL END AS ActiveUsers,
				
		CASE 
			WHEN (ThisWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ActiveUsers_Perc) - CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ActiveUsers_Perc IS NOT NULL AND PriorWeek.ActiveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc))/CONVERT(FLOAT,PriorWeek.ActiveUsers_Perc),0)
			ELSE NULL END AS ActiveUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers) - CONVERT(FLOAT,PriorWeek.ModerateUsers))/CONVERT(FLOAT,PriorWeek.ModerateUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ModerateUsers IS NOT NULL AND PriorWeek.ModerateUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ModerateUsers))/CONVERT(FLOAT,PriorWeek.ModerateUsers),0)
			ELSE NULL END AS ModerateUsers,
		
		CASE 
			WHEN (ThisWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.ModerateUsers_Perc) - CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc))/CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.ModerateUsers_Perc IS NOT NULL AND PriorWeek.ModerateUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc))/CONVERT(FLOAT,PriorWeek.ModerateUsers_Perc),0)
			ELSE NULL END AS ModerateUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers) - CONVERT(FLOAT,PriorWeek.InfrequentUsers))/CONVERT(FLOAT,PriorWeek.InfrequentUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InfrequentUsers IS NOT NULL AND PriorWeek.InfrequentUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InfrequentUsers))/CONVERT(FLOAT,PriorWeek.InfrequentUsers),0)
			ELSE NULL END AS InfrequentUsers,

		CASE 
			WHEN (ThisWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InfrequentUsers_Perc) - CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InfrequentUsers_Perc IS NOT NULL AND PriorWeek.InfrequentUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc))/CONVERT(FLOAT,PriorWeek.InfrequentUsers_Perc),0)
			ELSE NULL END AS InfrequentUsers_Perc,
		
		CASE 
			WHEN (ThisWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InactIveUsers) - CONVERT(FLOAT,PriorWeek.InactIveUsers))/CONVERT(FLOAT,PriorWeek.InactIveUsers),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InactIveUsers IS NOT NULL AND PriorWeek.InactIveUsers <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InactIveUsers))/CONVERT(FLOAT,PriorWeek.InactIveUsers),0)
			ELSE NULL END AS InactIveUsers,
		
		CASE 
			WHEN (ThisWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InactIveUsers_Perc) - CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc))/CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InactIveUsers_Perc IS NOT NULL AND PriorWeek.InactIveUsers_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc))/CONVERT(FLOAT,PriorWeek.InactIveUsers_Perc),0)
			ELSE NULL END AS InactIveUsers_Perc

	FROM (
		SELECT * FROM AddPerc
		WHERE DateHeader = 'Current Week') AS ThisWeek
	FULL OUTER JOIN (
		SELECT * FROM AddPerc
		WHERE DateHeader = 'Last Week' ) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDelta.TotalUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.TotalUsers) + '%' END AS 'TotalUsers'
		, CASE WHEN AddDelta.EngagedUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers) + '%' END AS 'EngagedUsers'
		, CASE WHEN AddDelta.EngagedUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.EngagedUsers_Perc) + '%' END AS 'EngagedUsers_Perc'
		, CASE WHEN AddDelta.ActiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers) + '%' END AS 'ActiveUsers'
		, CASE WHEN AddDelta.ActiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ActiveUsers_Perc) + '%' END AS 'ActiveUsers_Perc'
		, CASE WHEN AddDelta.ModerateUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers) + '%' END AS 'ModerateUsers'
		, CASE WHEN AddDelta.ModerateUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.ModerateUsers_Perc) + '%' END AS 'ModerateUsers_Perc'
		, CASE WHEN AddDelta.InfrequentUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers) + '%' END AS 'InfrequentUsers'
		, CASE WHEN AddDelta.InfrequentUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InfrequentUsers_Perc) + '%' END AS 'InfrequentUsers_Perc'
		, CASE WHEN AddDelta.InactiveUsers IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers) + '%' END AS 'InactiveUsers'
		, CASE WHEN AddDelta.InactiveUsers_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDelta.InactiveUsers_Perc) + '%' END AS 'InactiveUsers_Perc'
	FROM AddDelta
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TotalUsers, DummyDates.TotalUsers) AS TotalUsers
	, COALESCE(Biggie.EngagedUsers, DummyDates.EngagedUsers) AS EngagedUsers
	, COALESCE(Biggie.EngagedUsers_Perc, DummyDates.EngagedUsers_Perc) AS EngagedUsers_Perc
	, COALESCE(Biggie.ActiveUsers, DummyDates.ActiveUsers) AS ActiveUsers
	, COALESCE(Biggie.ActiveUsers_Perc, DummyDates.ActiveUsers_Perc) AS ActiveUsers_Perc
	, COALESCE(Biggie.ModerateUsers, DummyDates.ModerateUsers) AS ModerateUsers
	, COALESCE(Biggie.ModerateUsers_Perc, DummyDates.ModerateUsers_Perc) AS ModerateUsers_Perc
	, COALESCE(Biggie.InfrequentUsers, DummyDates.InfrequentUsers) AS InfrequentUsers
	, COALESCE(Biggie.InfrequentUsers_Perc, DummyDates.InfrequentUsers_Perc) AS InfrequentUsers_Perc
	, COALESCE(Biggie.InactiveUsers, DummyDates.InactiveUsers) AS InactiveUsers
	, COALESCE(Biggie.InactiveUsers_Perc, DummyDates.InactiveUsers_Perc) AS InactiveUsers_Perc

FROM (
	SELECT 
		COALESCE(AddPerc.DateHeader, AddFormat.DateHeader) AS DateHeader
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(AddPerc.TotalUsers AS VARCHAR), AddFormat.TotalUsers) AS TotalUsers
		,COALESCE(CAST(AddPerc.EngagedUsers AS VARCHAR), AddFormat.EngagedUsers) AS EngagedUsers
		/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR,AddPerc.EngagedUsers_Perc) + '%', AddFormat.EngagedUsers_Perc) AS EngagedUsers_Perc
		,COALESCE(CAST(AddPerc.ActiveUsers AS VARCHAR), AddFormat.ActiveUsers) AS ActiveUsers
		,COALESCE(CONVERT(VARCHAR,AddPerc.ActiveUsers_Perc) + '%', AddFormat.ActiveUsers_Perc) AS ActiveUsers_Perc
		,COALESCE(CAST(AddPerc.ModerateUsers AS VARCHAR), AddFormat.ModerateUsers) AS ModerateUsers
		,COALESCE(CONVERT(VARCHAR,AddPerc.ModerateUsers_Perc) + '%', AddFormat.ModerateUsers_Perc) AS ModerateUsers_Perc
		,COALESCE(CAST(AddPerc.InfrequentUsers AS VARCHAR), AddFormat.InfrequentUsers) AS InfrequentUsers
		,COALESCE(CONVERT(VARCHAR,AddPerc.InfrequentUsers_Perc) + '%', AddFormat.InfrequentUsers_Perc) AS InfrequentUsers_Perc
		,COALESCE(CAST(AddPerc.InactiveUsers AS VARCHAR), AddFormat.InactiveUsers) AS InactiveUsers
		,COALESCE(CONVERT(VARCHAR,AddPerc.InactiveUsers_Perc) + '%', AddFormat.InactiveUsers_Perc) AS InactiveUsers_Perc

	FROM AddPerc
	FULL OUTER JOIN AddFormat
	ON AddPerc.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader