/* 
At-A-Glance, Day Aggregation Query - SHARE
author: joanna.tung
date: 2018.11.30

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid.
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Prior Week Total" field), and 
			2) it requires a calculation of the percent change between the selected week's total and the prior week's total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for each day of the selected week (7 days), and are formatted as "mm/dd" 
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) day of week aggregation, 
			2) week aggregation, and 
			3) percent change between week aggregation
		- Create a temp tables to produce metrics for each of the three categories above
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT: table of all 7 days of the week with
				- the correct DateHeader formatting for all 7 days of the selected week
				- row records for the selected week's total and prior week totals
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each day in the selected week and the prior week
			LOGIC: 1) contains necessary day aggregations for the selected week and 2) serves as a base table from which to perform aggregations for
			the selected week and the prior week (see NoPercWeek table). The "Share" section of the AAG report requires metrics to be calculated from 3 different tables: fact.StandardMetricsPSI,
			fact.StandardMetricsErxService, and fact.UserActivity

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses Base (temp) table to produce necessary week aggregations to display in the "Current Week" and "Prior Week" columns of the final report

		4) AddPercWeek (temp):
			OUTPUT: weekly values for any metrics that require the calculation of percentages
			LOGIC: The At-A-Glance report's SHARE section requires the calculation of "% Single Service Referrals Shared" and "% eRx Referrals Shared." 
			We cannot generate the week totals for this metric by simply summing all daily percentage values. I use this temp table to calculate this metric 
			for the weekly value, and will calculate the daily value in the "Base1" table in the main select statement.

		4) AddDeltaWeek (temp):
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each day of the selected week, by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Add field to the "Base" temp table that returns daily values for "% Single Service Referrals Shared" and "% eRx Referrals Shared," and name this new table "Base1."
			Perform full outer join to merge the individual day aggregation (Base1), week aggregation (AddPercWeek), and percent change (AddDeltaWeek) 
			values into a single table.  Use "coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all
			required row records for "DateKey" exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = DATEADD(DAY, -6, '2018-09-12') 
DECLARE @EndDate AS DATETIME = '2018-09-12' 
DECLARE @OrgParam AS INT = 1106525
;

WITH 
Dummydates AS (
	SELECT  
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table. */ 
		
		 CONCAT(LEFT(RIGHT(dimDate.DateKey,4),2), '/', RIGHT(dimDate.DateKey,2)) AS 'DateHeader'
		, 0 AS NudgeReferralsShared
		, 0 AS TrackedReferralsShared
		, 0 AS ServicePrints 
		, 0 AS eRxReferralsShared
		, 0 AS TotalSharedSingleServiceReferrals
		, 0 AS TotalSharedReferrals
		, NULL AS TotalSingleServiceReferralsShared_Perc
		, NULL AS eRxReferralsShared_Perc
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)

	UNION ALL
	
	/* The AAG-day aggregation report requires us to provide aggregates for "ThisWeek" and "LastWeek." 
	   Create dummy rows to ensure that these column header fields are displayed, even when there is no available data. */

	SELECT
		'Last Week' AS 'DateHeader'
		, 0 AS NudgeReferralsShared
		, 0 AS TrackedReferralsShared
		, 0 AS ServicePrints 
		, 0 AS eRxReferralsShared
		, 0 AS TotalSharedSingleServiceReferrals
		, 0 AS TotalSharedReferrals
		, NULL AS TotalSingleServiceReferralsShared_Perc
		, NULL AS eRxReferralsShared_Perc
		
	UNION ALL

	SELECT
		'Current Week' AS 'DateHeader'
		, 0 AS NudgeReferralsShared
		, 0 AS TrackedReferralsShared
		, 0 AS ServicePrints 
		, 0 AS eRxReferralsShared
		, 0 AS TotalSharedSingleServiceReferrals
		, 0 AS TotalSharedReferrals
		, NULL AS TotalSingleServiceReferralsShared_Perc
		, NULL AS eRxReferralsShared_Perc
)
,

/* This temp table returns aggregated metrics, by day of week. Query parameters are applied here to ensure that only the relevant
   row records are included in the aggregates. */

Base AS (
	SELECT 
		 CAST(COALESCE(vfactStandardMetricsPSI.DateKey,vfactStandardMetricsUserActivity.DateKey, vfactStandardMetricsErxService.DateKey) AS VARCHAR(8)) AS DateKey
		, CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END AS eRxReferralsShared
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)) AS TotalSharedSingleServiceReferrals
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)
		+ (CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END)) AS TotalSharedReferrals
			
	FROM (
	/* Metrics for electronic Single Service Nudges (Text/Email) and Tracked Referrals, by Day of Week */
		SELECT 
			factStandardMetricsPSI.DateKey
			, SUM(factStandardMetricsPSI.SingleServiceReferralsShared) AS SingleServiceReferralsShared
		FROM vfact.StandardMetricsPSI AS factStandardMetricsPSI
		WHERE factStandardMetricsPSI.DateKey >= DATEADD(DAY, -7, @StartDate) and factStandardMetricsPSI.DateKey < DATEADD(DAY, 1, @EndDate)
			AND factStandardMetricsPSI.OrganizationId IN (@orgparam)
		GROUP BY DateKey
		) AS vfactStandardMetricsPSI

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Single Service Prints, by Day of Week */
		SELECT 
			SUM(factStandardMetricsUserActivity.SingleServiceReferralPrinted) AS SingleServicePrints
			, factStandardMetricsUserActivity.DateKey AS DateKey
		FROM vfact.StandardMetricsUserActivity AS factStandardMetricsUserActivity
		WHERE CAST(factStandardMetricsUserActivity.DateKey AS VARCHAR(8)) >=  DATEADD(DAY, -7, @StartDate) 
			AND CAST(factStandardMetricsUserActivity.DateKey AS VARCHAR(8)) < dateadd(day, 1, @EndDate)
			AND factStandardMetricsUserActivity.OrganizationId IN (@orgparam)
		GROUP BY DateKey
		) AS vfactStandardMetricsUserActivity
	ON (vfactStandardMetricsUserActivity.DateKey = vfactStandardMetricsPSI.DateKey)

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Referrals on shared eRxs, by Day of Week */
		SELECT 
			factStandardMetricsErxService.DateKey
			, SUM(factStandardMetricsErxService.eRxReferralsShared) AS eRxReferralsShared
		FROM vfact.StandardMetricsErxService AS factStandardMetricsErxService
		WHERE factStandardMetricsErxService.DateKey >=  DATEADD(DAY, -7, @StartDate) 
			AND factStandardMetricsErxService.DateKey < DATEADD(DAY, 1, @EndDate)
			AND factStandardMetricsErxService.OrganizationId IN (@orgparam)
		GROUP BY DateKey
		) AS vfactStandardMetricsErxService
	ON (vfactStandardMetricsErxService.DateKey = vfactStandardMetricsPSI.DateKey)

)
,

/* The AAG Report requires us to display aggregated values for each metric, for the selected week and the prior week.
   This temp table returns the aggregated values for each metric, grouped by the selected week and the prior week. */

NoPercWeek as (
	SELECT 	
		CASE WHEN (Base.DateKey >= @StartDate AND Base.DateKey < DATEADD(DAY, 1, @EndDate)) THEN 'Current Week'
			WHEN (Base.DateKey >= DATEADD(DAY, -7, @StartDate) AND Base.DateKey < @StartDate) THEN 'Last Week'
			ELSE 'Error' END AS DateHeader
		, SUM(Base.eRxReferralsShared) AS eRxReferralsShared
		, SUM(Base.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
		, SUM(Base.TotalSharedReferrals) AS TotalSharedReferrals
	FROM Base
	GROUP BY (CASE WHEN (Base.DateKey >= @StartDate AND Base.DateKey < DATEADD(DAY, 1, @EndDate)) THEN 'Current Week'
			WHEN (Base.DateKey >= DATEADD(DAY, -7, @StartDate) AND Base.DateKey < @StartDate) THEN 'Last Week'
			ELSE 'Error' END)
)
,

/* The AAG Report's SHARE section requires us to provide metrics that involve calculating percentage values by day, and by week. As discussed above, we cannot calculate the weekly
   value by summing over the percentage values calcluated for each day. I use this temp table to add this percent metric for this week and prior week totals. NOTE: I add the daily percentage
   values in the main select statement (see Base1 table). */
AddPercWeek AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN TotalSharedReferrals > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,TotalSharedSingleServiceReferrals)/CONVERT(FLOAT,TotalSharedReferrals)))
			ELSE NULL END AS 'TotalSingleServiceReferralsShared_Perc'
		, CASE WHEN TotalSharedReferrals > 0 THEN
			CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,eRxReferralsShared)/CONVERT(FLOAT,TotalSharedReferrals)))
			ELSE NULL END AS 'eRxReferralsShared_Perc'
	FROM NoPercWeek
)
,

/* The AAG report requires us to display the percent change in metrics for this week and the prior week. 
   This temp table returns the percent change between the aggregated sum of each metric recorded for this week and last week. */
AddDeltaWeek as (
	SELECT 
		'Percent Change' AS DateHeader,

		/* Create conditions to account for situations in which:
			1) records may not exist for this week and/or last week 
				- Because we use a full outer join (see below), the resulting table returns "NULL" values for any week during which searches were not conducted.
			2) where divisor may be zero (records of searches do not exist for "last week"). */

		CASE 
		/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.eRxReferralsShared) - CONVERT(FLOAT,PriorWeek.eRxReferralsShared))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared)))

		/* This second condition addresses the situation in which there are no records of searches for this week (ThisWeek.DateHeader is null), and
		   last week's values are neither null nor zero. */ 
		    WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (0 - CONVERT(FLOAT,PriorWeek.eRxReferralsShared))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared)))
		/* Use condition "else NULL" for situations in which divisor is zero */	
			ELSE NULL END AS eRxReferralsShared,

		CASE 
			WHEN (ThisWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.TotalSharedSingleServiceReferrals) - CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals)))
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals)))
			ELSE NULL END AS TotalSharedSingleServiceReferrals,

		CASE 
			WHEN (ThisWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.TotalSharedReferrals) - CONVERT(FLOAT,PriorWeek.TotalSharedReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedReferrals)))
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSharedReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedReferrals)))
			ELSE NULL end as TotalSharedReferrals,

		CASE 
			WHEN (ThisWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.TotalSingleServiceReferralsShared_Perc) - CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc)))
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc)))
			ELSE NULL END AS TotalSingleServiceReferralsShared_Perc,

		CASE 
			WHEN (ThisWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (CONVERT(FLOAT,ThisWeek.eRxReferralsShared_Perc) - CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc)))
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc <> 0) THEN 
				CONVERT(DECIMAL(19,3),(100 * (0 - CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc)))
			ELSE NULL END AS eRxReferralsShared_Perc


	FROM (
		SELECT * FROM AddPercWeek
		WHERE DateHeader = 'Current Week'
			) AS ThisWeek
	/* In order to calculate the percent difference between this week and last week, we must move this week and last week's aggregated sum of searches into a single
	   row record. Using a full outer join allows us to do this. By joining the selected week data with the prior week data on a "dummy" value, 
	   we can collapse last week and this week's records into a single row. */
	FULL OUTER JOIN (
		SELECT * FROM AddPercWeek
		WHERE DateHeader = 'Last Week'
			) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)

SELECT 
	COALESCE(WeekDays.DateHeader, AddPercWeek.DateHeader, AddDeltaWeek.DateHeader) AS 'DateHeader'
	,COALESCE(AddPercWeek.eRxReferralsShared, AddDeltaWeek.eRxReferralsShared,WeekDays.eRxReferralsShared) AS eRxReferralsShared
	,COALESCE(AddPercWeek.TotalSharedSingleServiceReferrals, AddDeltaWeek.TotalSharedSingleServiceReferrals,WeekDays.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
	,COALESCE(AddPercWeek.TotalSharedReferrals, AddDeltaWeek.TotalSharedReferrals,WeekDays.TotalSharedReferrals) AS TotalSharedReferrals
	,COALESCE(AddPercWeek.TotalSingleServiceReferralsShared_Perc, AddDeltaWeek.TotalSingleServiceReferralsShared_Perc, WeekDays.TotalSingleServiceReferralsShared_Perc) AS TotalSingleServiceReferralsShared_Perc
	,COALESCE(AddPercWeek.eRxReferralsShared_Perc, AddDeltaWeek.eRxReferralsShared_Perc, WeekDays.eRxReferralsShared_Perc) AS eRxReferralsShared_Perc

from (
	SELECT 
		COALESCE(Base1.eRxReferralsShared, DummyDates.eRxReferralsShared) AS eRxReferralsShared
		,COALESCE(Base1.TotalSharedSingleServiceReferrals, DummyDates.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
		,COALESCE(Base1.TotalSharedReferrals, DummyDates.TotalSharedReferrals) AS TotalSharedReferrals
		,Base1.eRxReferralsShared_Perc
		,Base1.TotalSingleServiceReferralsShared_Perc
		,DummyDates.DateHeader
	FROM (

	/* As stated above, the Identify section of the AAG report requires the calculation of "% Single Service Referrals Shared" and "% eRx Referrals Shared," by day, and by week. 
	   I calculate the Week value for this metric in the "AddPercWeek" temp table above, and calculate the day value for this metric using the "Base1" table below. */
		SELECT
			eRxReferralsShared
			, TotalSharedSingleServiceReferrals
			, TotalSharedReferrals
			, CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2)) AS 'DateHeader'
			, CASE WHEN CAST(TotalSharedReferrals AS INT) > 0 THEN
				CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,TotalSharedSingleServiceReferrals)/CONVERT(FLOAT,TotalSharedReferrals)))
				ELSE NULL END AS 'TotalSingleServiceReferralsShared_Perc'
			, CASE WHEN CAST(TotalSharedReferrals AS INT) > 0 THEN
				CONVERT(DECIMAL(19,3),(100 * CONVERT(FLOAT,eRxReferralsShared)/CONVERT(FLOAT,TotalSharedReferrals)))
				ELSE NULL END AS 'eRxReferralsShared_Perc'
		FROM Base
		WHERE Base.DateKey >= @StartDate and Base.DateKey < dateadd(day, 1, @EndDate)
		) AS Base1
	RIGHT OUTER JOIN DummyDates
	ON Base1.DateHeader = DummyDates.DateHeader
	) as WeekDays
FULL OUTER JOIN AddPercWeek
ON AddPercWeek.DateHeader = WeekDays.DateHeader
FULL OUTER JOIN AddDeltaWeek
ON AddPercWeek.DateHeader = AddDeltaWeek.DateHeader
ORDER BY DateHeader
