/* 
At-A-Glance, Day Aggregation Query - ENGAGE
author: joanna.tung
date: 2018.12.05

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Prior Week Total" field), and 
			2) it requires a calculation of the percent change between the selected week's total and the prior week's total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for each day of the selected week (7 days), and are formatted as "mm/dd" 
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) day of week aggregation, 
			2) week aggregation, and 
			3) percent change between week aggregation
		- Create a temp tables to produce metrics for each of the three categories above
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.
		- Because the ENGAGE section requires a distinct count of patients by day and week periods, we cannot generate weekly values by simply summing across all 7 days of the
		  week. Use Data Marts generated for the required aggregation level (Daily, Weekly, etc.) in order to get an accurate count of distinct patients for each required date period.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for all 7 days of the selected week
					- one row record for the selected week's totals, prior week totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each day in the selected week
			LOGIC: uses "fact.StandardMetricsAAGEngagementDaily" table to produce day aggregations for the selected week

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses "fact.StandardMetricsAAGEngagementWeekly" table to produce necessary week aggregations to display in the "Current Week" and "Prior Week" columns of the final report

		4) AddDelta (temp):
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) AddFormat (temp):
			OUTPUT: percent change values for selected week and the prior week, with '%' sign added for values that are not NULL, and 'N/A' inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		6) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each day of the selected week, by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Perform full outer join to merge the individual day aggregation (Base), week aggregation (NoPercWeek), and formatted percent change (AddFormat) 
			values into a single table.  Use "coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydatestable to ensure that all
			required row records for "DateHeader" exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = DATEADD(DAY, -6, '2018-09-23') 
DECLARE @EndDate AS DATETIME = '2018-09-23' 
DECLARE @OrgParam AS INT  = 1077836
;
WITH 

/* The At-a-Glance report requires that all 7 days of the week appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all 7 days of the week. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 7 days of the week appear in the export. */
Dummydates AS (
	SELECT  			
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table. 
		   
		   NOTE: Must cast all integers as VARCHAR data type so that it will union properly with any 'N/A' values. */ 

		  CONCAT(LEFT(RIGHT(dimDate.DateKey,4),2), '/', RIGHT(dimDate.DateKey,2)) AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS InteractionPatientCount
		, CAST(0 AS VARCHAR) AS PatientsReceivingReferrals
		, CAST(0 AS VARCHAR) AS AvgReferralsReceivedByPatients
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)

	UNION ALL

	SELECT
		'Last Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS InteractionPatientCount
		, CAST(0 AS VARCHAR) AS PatientsReceivingReferrals
		, CAST(0 AS VARCHAR) AS AvgReferralsReceivedByPatients
		
	UNION ALL

	SELECT
		'Current Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS InteractionPatientCount
		, CAST(0 AS VARCHAR) AS PatientsReceivingReferrals
		, CAST(0 AS VARCHAR) AS AvgReferralsReceivedByPatients
		
	UNION ALL

	SELECT
		'Percent Change' AS 'DateHeader'
		, 'N/A' AS InteractionPatientCount
		, 'N/A' AS PatientsReceivingReferrals
		, 'N/A' AS AvgReferralsReceivedByPatients
)
,
Ints AS (

	SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
		--, PSI.OrganizationId
		, PSI.CareOrganizationDatabaseId
		, PSI.PatientId
		, 0 AS PatientsReceivingReferralsQty
		, 0 AS ReferralsReceivedByPatientsQty
	
	FROM fact.PatientSiteInteraction AS PSI
		LEFT JOIN [dim].[User] AS U
		ON PSI.UserId = U.UserId
		LEFT JOIN [dim].[Patient] AS P
		ON PSI.PatientId = P.PatientId 
		AND PSI.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
		AND PSI.InteractionTypeId IN (1,2,5,6,7,8,9,10)
		--AND PSI.OrganizationId in (@orgparam)
		AND PSI.InteractionDate >= DATEADD(DAY, -7, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate)
)
,

Refs AS (

	SELECT
		RefBase.DateKey
		, RefBase.CareOrganizationDatabaseId
		, RefBase.PatientId
		, RefBase.Referrals
	FROM (
		/* Get PSI records and Referral Quantity for any Shared eRx received by a Patient, where PatientId IS NOT NULL */
		SELECT Eref.DateKey
				, Eref.UserId
				, Eref.CareOrganizationDatabaseId
				, Eref.PatientId
				, Eref.Referrals
				, Eref.ErxId 
				, 'eRx' AS 'ReferralType'
		FROM (
			SELECT 
				CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
				, PSI.UserId
				, PSI.CareOrganizationDatabaseId
				, PSI.[PatientId]
				, ServQty.Referrals
				, concat(PSI.ErxId,';',PSI.CareOrganizationDatabaseId) as ErxId
				, NULL AS ServiceId
					/* ErXIds are not unique ACROSS DBs. Must add the CareOrganizationDatabaseId to the ErxId to make this "ErxId value" truly unique.
						The same eRx can be shared with given patient multiple times. This row ranking allows us to select only the most recent event when a given eRx was shared. */
				, ROW_NUMBER() OVER (PARTITION BY CONCAT(PSI.PatientId, ';', PSI.CareOrganizationDatabaseId), concat(PSI.ErxId,';',PSI.CareOrganizationDatabaseId), CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) ORDER BY InteractionDate) AS [r] 

			FROM fact.[PatientSiteInteraction] AS PSI
			/* Get count of Referrals for each unique eRx Shared */
				LEFT OUTER JOIN (
					SELECT 
						PS.ErxId
						,PS.CareOrganizationDatabaseId
						,SUM(ErxServiceQuantity) as Referrals
					FROM [fact].ErxService as PS
					WHERE PatientId IS NOT NULL
						--AND PS.OrganizationId in (@orgparam)
						/* Do not add a filter for CreatedDate. An eRx might be created and shared on different dates */
					GROUP BY CareOrganizationDatabaseId, ErxId
					) AS ServQty
				ON concat(PSI.ErxId,';',PSI.CareOrganizationDatabaseId) = concat(ServQty.ErxId,';',ServQty.CareOrganizationDatabaseId)
				WHERE InteractionTypeId IN (3, 4, 11, 22)
					AND PatientId IS NOT NULL 
					--AND PSI.OrganizationId in (@orgparam)
					AND PSI.InteractionDate >= DATEADD(DAY, -7, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate)
			) as Eref
		WHERE Eref.[r] = 1 --Get only the earliest PSI interaction for which a given eRx was shared with a patient
		
		UNION ALL

		SELECT 
			Others.DateKey
			, Others.UserId
			, Others.CareOrganizationDatabaseId
			, Others.PatientId
			, Others.Referrals
			, Others.eRxId
			, Others.ReferralType

		FROM (

			SELECT	CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
					, PSI.UserId
					, PSI.CareOrganizationDatabaseId
					, PSI.[PatientId]
					, 1 as Referrals
					, NULL as eRxId
					, ServiceId
					, 'Nudge' AS 'ReferralType'
					, ROW_NUMBER() OVER (PARTITION BY CONCAT(PSI.PatientId, ';', CareOrganizationDatabaseId), ServiceId, CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) ORDER BY PSI.InteractionDate) AS [r]
			FROM fact.PatientSiteInteraction as PSI
					--AND PSI.OrganizationId in (@orgparam)
			WHERE PatientId IS NOT NULL AND InteractionTypeId in (5, 6)
				AND PSI.InteractionDate >= DATEADD(DAY, -7, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate)

			UNION ALL 		
		
			SELECT	CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
					, PSI.UserId
					, PSI.CareOrganizationDatabaseId
					, PSI.[PatientId]
					, 1 as Referrals
					, NULL as eRxId
					, ServiceId
					, 'Tracked' AS 'ReferralType'
					, ROW_NUMBER() OVER (PARTITION BY CONCAT(PSI.PatientId, ';', PSI.CareOrganizationDatabaseId), ServiceId, CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) ORDER BY PSI.InteractionDate) AS [r]
			FROM fact.PatientSiteInteraction as PSI
					--AND PSI.OrganizationId in (@orgparam)
			WHERE PatientId IS NOT NULL AND InteractionTypeId = 15
				AND PSI.InteractionDate >= DATEADD(DAY, -7, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate)
		) AS Others
		WHERE Others.[r] = 1


	) AS RefBase

	LEFT JOIN [dim].[User] AS U
	ON RefBase.UserId = U.UserId
	LEFT JOIN [dim].[Patient] AS P
	ON RefBase.PatientId = P.PatientId 
	AND RefBase.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId

	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
		AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)

) 
,
Base AS (
	SELECT 
		PatCnt.DateHeader
		, SUM(PatCnt.InteractionPatientCount) AS InteractionPatientCount
		, SUM(PatCnt.PatientsReceivingReferralsQty) AS PatientsReceivingReferrals
		, CASE WHEN SUM(PatCnt.PatientsReceivingReferralsQty) > 0 THEN
			ROUND(CONVERT(FLOAT,SUM(PatCnt.ReferralsReceivedByPatientsQty))/CONVERT(FLOAT,SUM(PatCnt.PatientsReceivingReferralsQty)) ,0)
			ELSE NULL END AS AvgReferralsReceivedByPatients

	FROM (
		SELECT 
			CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2)) AS 'DateHeader'
			, 0 AS InteractionPatientCount
			, COUNT(DISTINCT CONCAT(PatientId, ';', CareOrganizationDatabaseId)) AS PatientsReceivingReferralsQty
			, SUM(Referrals) AS ReferralsReceivedByPatientsQty
		FROM Refs
		WHERE CONVERT(VARCHAR, DateKey) >= @StartDate AND CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @Enddate)
		GROUP BY CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2))
	
		UNION ALL

		SELECT 
			CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2)) AS 'DateHeader'
			, COUNT(DISTINCT CONCAT(PatientId, ';', CareOrganizationDatabaseId)) AS InteractionPatientCount
			, 0 AS PatientsReceivingReferralsQty
			, 0 AS ReferralsReceivedByPatientsQty 
		FROM Ints
		WHERE CONVERT(VARCHAR, DateKey) >= @StartDate AND CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @Enddate)
		GROUP BY CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2))
		) AS PatCnt

	GROUP BY PatCnt.DateHeader
)
,
NoPercWeek AS (
	SELECT 
		'Dummy' AS Dummy
		, DateHeader
		, SUM(PatCnt.InteractionPatientCount) AS InteractionPatientCount
		, SUM(PatCnt.PatientsReceivingReferralsQty) AS PatientsReceivingReferrals
		, CASE WHEN SUM(PatCnt.PatientsReceivingReferralsQty) > 0 THEN
			ROUND(CONVERT(FLOAT,SUM(PatCnt.ReferralsReceivedByPatientsQty))/CONVERT(FLOAT,SUM(PatCnt.PatientsReceivingReferralsQty)) ,0)
			ELSE NULL END AS AvgReferralsReceivedByPatients

	FROM (
		SELECT 
			CASE WHEN CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @EndDate) AND CONVERT(VARCHAR, DateKey) >= @StartDate THEN 'Current Week'
				WHEN CONVERT(VARCHAR, DateKey) >= DATEADD(DAY, -7, @StartDate) AND CONVERT(VARCHAR, DateKey) < @StartDate THEN 'Last Week'
				ELSE 'Error' END AS 'DateHeader'
			, 0 AS InteractionPatientCount
			, COUNT(DISTINCT CONCAT(PatientId, ';', CareOrganizationDatabaseId)) AS PatientsReceivingReferralsQty
			, SUM(Referrals) AS ReferralsReceivedByPatientsQty
		FROM Refs
		GROUP BY (	CASE WHEN CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @EndDate) AND CONVERT(VARCHAR, DateKey) >= @StartDate THEN 'Current Week'
				WHEN CONVERT(VARCHAR, DateKey) >= DATEADD(DAY, -7, @StartDate) AND CONVERT(VARCHAR, DateKey) < @StartDate THEN 'Last Week'
				ELSE 'Error' END )
	
		UNION ALL

		SELECT 
			CASE WHEN CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @EndDate) AND CONVERT(VARCHAR, DateKey) >= @StartDate THEN 'Current Week'
				WHEN CONVERT(VARCHAR, DateKey) >= DATEADD(DAY, -7, @StartDate) AND CONVERT(VARCHAR, DateKey) < @StartDate THEN 'Last Week'
				ELSE 'Error' END AS 'DateHeader'
			, COUNT(DISTINCT CONCAT(PatientId, ';', CareOrganizationDatabaseId)) AS InteractionPatientCount
			, 0 AS PatientsReceivingReferralsQty
			, 0 AS ReferralsReceivedByPatientsQty 
		FROM Ints
		GROUP BY (	CASE WHEN CONVERT(VARCHAR, DateKey) < DATEADD(DAY, 1, @EndDate) AND CONVERT(VARCHAR, DateKey) >= @StartDate THEN 'Current Week'
				WHEN CONVERT(VARCHAR, DateKey) >= DATEADD(DAY, -7, @StartDate) AND CONVERT(VARCHAR, DateKey) < @StartDate THEN 'Last Week'
				ELSE 'Error' END )
		) AS PatCnt

	GROUP BY DateHeader
)
, 
DeltaWeek AS (
	SELECT 
		'Percent Change' AS DateHeader
		, CASE 
			WHEN (ThisWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InteractionPatientCount) - CONVERT(FLOAT,PriorWeek.InteractionPatientCount))/CONVERT(FLOAT,PriorWeek.InteractionPatientCount),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.InteractionPatientCount IS NOT NULL AND PriorWeek.InteractionPatientCount <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.InteractionPatientCount) - CONVERT(FLOAT,PriorWeek.InteractionPatientCount))/CONVERT(FLOAT,PriorWeek.InteractionPatientCount),0)
			ELSE NULL END AS InteractionPatientCount
		
		, CASE 
			WHEN (ThisWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.PatientsReceivingReferrals) - CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals))/CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.PatientsReceivingReferrals IS NOT NULL AND PriorWeek.PatientsReceivingReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.PatientsReceivingReferrals) - CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals))/CONVERT(FLOAT,PriorWeek.PatientsReceivingReferrals),0)
			ELSE NULL END AS PatientsReceivingReferrals
		
		, CASE 
			WHEN (ThisWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.AvgReferralsReceivedByPatients) - CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients))/CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.AvgReferralsReceivedByPatients IS NOT NULL AND PriorWeek.AvgReferralsReceivedByPatients <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.AvgReferralsReceivedByPatients) - CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients))/CONVERT(FLOAT,PriorWeek.AvgReferralsReceivedByPatients),0)
			ELSE NULL END AS AvgReferralsReceivedByPatients

	FROM (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Current Week'
			) AS ThisWeek
	FULL OUTER JOIN (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Last Week'
			) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN DeltaWeek.InteractionPatientCount IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, DeltaWeek.InteractionPatientCount) + '%' END AS 'InteractionPatientCount'
		, CASE WHEN DeltaWeek.PatientsReceivingReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, DeltaWeek.PatientsReceivingReferrals) + '%' END AS 'PatientsReceivingReferrals'
		, CASE WHEN DeltaWeek.AvgReferralsReceivedByPatients IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, DeltaWeek.AvgReferralsReceivedByPatients) + '%' END AS 'AvgReferralsReceivedByPatients'
	FROM DeltaWeek
)

SELECT 
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.InteractionPatientCount, DummyDates.InteractionPatientCount) AS InteractionPatientCount
	, COALESCE(Biggie.PatientsReceivingReferrals, DummyDates.PatientsReceivingReferrals) AS PatientsReceivingReferrals
	, COALESCE(Biggie.AvgReferralsReceivedByPatients, DummyDates.AvgReferralsReceivedByPatients) AS AvgReferralsReceivedByPatients
FROM (
	SELECT
		COALESCE(Base.DateHeader, NoPercWeek.DateHeader, AddFormat.DateHeader) AS DateHeader
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(Base.InteractionPatientCount AS VARCHAR), CAST(NoPercWeek.InteractionPatientCount AS VARCHAR), AddFormat.InteractionPatientCount) AS InteractionPatientCount
		, COALESCE(CAST(Base.PatientsReceivingReferrals AS VARCHAR), CAST(NoPercWeek.PatientsReceivingReferrals AS VARCHAR), AddFormat.PatientsReceivingReferrals) AS PatientsReceivingReferrals
		, COALESCE(CAST(Base.AvgReferralsReceivedByPatients AS VARCHAR), CAST(NoPercWeek.AvgReferralsReceivedByPatients AS VARCHAR), AddFormat.AvgReferralsReceivedByPatients) AS AvgReferralsReceivedByPatients
	FROM Base
	FULL OUTER JOIN NoPercWeek
	ON NoPercWeek.DateHeader = Base.DateHeader
	FULL OUTER JOIN AddFormat
	ON NoPercWeek.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY DateHeader