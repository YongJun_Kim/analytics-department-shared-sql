/*
At-A-Glance, Day Aggregation Query - MATCH
author: joanna.tung
date: 2018.12.05

	This query returns an export that can produce the desired table format presented for the At-A-Glance, week-by-day report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Prior Week Total" field), and 
			2) it requires a calculation of the percent change between the selected week's total and the prior week's total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for each day of the selected week (7 days), and are formatted as "mm/dd" 
			- There are row records for pre-calculated Week Total, Prior Week Total, and Percent Change 
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) day of week aggregation, 
			2) week aggregation, and 
			3) percent change between week aggregation
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for all 7 days of the selected week
					- one row record for the selected week's totals, prior week totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each day in the selected week and the prior week
			LOGIC: 1) contains necessary day aggregations for the selected week and 2) serves as a base table from which to perform aggregations for
			the selected week and the prior week (see NoPercWeek table)

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the selected week and the prior week
			LOGIC: uses Base (temp) table to produce necessary week aggregations to display in the "Current Week" and "Prior Week" columns of the final report

		4) AddDeltaWeek (temp):
			OUTPUT: percent change values for selected week and the prior week
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the selected week and the prior week, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between this week and last. Uses "dummy" field to
			perform 'arbitrary' join to append prior week data to the selected week's data along the horizontal axis.

		5) AddFormat (temp):
			OUTPUT: percent change values for selected week and the prior week, with '%' sign added for values that are not NULL, and 'N/A' inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		6) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each day of the selected week, by the selected week, and by the prior week, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from this week to the prior week.
			LOGIC: Perform full outer join to merge the individual day aggregation (Base), week aggregation (AddPercWeek), and formatted percent change (AddFormat) values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = DATEADD(DAY, -6, '2018-11-11') 
DECLARE @EndDate AS DATETIME = '2018-11-11' 
DECLARE @OrgParam AS INT  = 1077836

;
WITH 

/* The At-a-Glance report requires that all 7 days of the week appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all 7 days of the week. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 7 days of the week appear in the export. */

Dummydates AS (
	SELECT  
		
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table.
		   
		   NOTE: Must cast all integers as VARCHAR data type so that it will union properly with any 'N/A' values. */  
		  
		  CONCAT(LEFT(RIGHT(dimDate.DateKey,4),2), '/', RIGHT(dimDate.DateKey,2)) AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalReferrals
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)

	UNION ALL

	/* The AAG-day aggregation report requires us to provide values for "Current Week" totals, Prior Week" totals, and a separate column for "Percent Change" (betwen weeks). 
	   Create dummy rows to ensure that these column header fields are displayed, even when there are no available data. */

	SELECT
		'Last Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalReferrals	
		
	UNION ALL

	SELECT
		'Current Week' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TotalReferrals	
		
	UNION ALL

	SELECT
		'Percent Change' AS 'DateHeader'
		, 'N/A' AS TotalReferrals
)
,

/* This temp table returns aggregated sum of Searches conducted, by day of week. Query parameters are applied here to ensure that only the relevant
   row records are included in the aggregates. */

Base AS (
	SELECT 
		
		CAST(vfactStandardMetricsAAGCombinedMetrics.DateKey AS VARCHAR(8)) AS DateKey,
		SUM(vfactStandardMetricsAAGCombinedMetrics.TotalReferrals) AS TotalReferrals	
	FROM vfact.StandardMetricsAAGCombinedMetrics AS vfactStandardMetricsAAGCombinedMetrics
	WHERE CAST(vfactStandardMetricsAAGCombinedMetrics.DateKey AS VARCHAR(8)) >= DATEADD(DAY, -7, @StartDate) 
		AND CAST(vfactStandardMetricsAAGCombinedMetrics.DateKey AS VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
		AND OrganizationId IN (@orgparam) 
	GROUP BY DateKey
)
,

/* The AAG Report requires us to display aggregated values of searches for the selected week and the prior week.
   This temp table returns the aggregated sum of Searches conducted, grouped by the selected week and the prior week. */

NoPercWeek AS (
	SELECT
		'Dummy' AS Dummy,
		CASE WHEN (Base.DateKey >= @StartDate and Base.DateKey < DATEADD(DAY, 1, @EndDate)) THEN 'Current Week'
			WHEN (Base.DateKey >= dateadd(day, -7, @StartDate) and Base.DateKey < @StartDate) THEN 'Last Week'
			ELSE 'Error' END AS DateHeader,
		SUM(Base.TotalReferrals) AS TotalReferrals
	FROM Base
	GROUP BY (CASE WHEN (Base.DateKey >= @StartDate and Base.DateKey < DATEADD(DAY, 1, @EndDate)) THEN 'Current Week'
			WHEN (Base.DateKey >= dateadd(day, -7, @StartDate) and Base.DateKey < @StartDate) THEN 'Last Week'
			ELSE 'Error' END)
)
,

/* The AAG report requires us to display the percent change in metrics for this week and the prior week. 
   This temp table returns the percent change between the aggregated sum of Searches conducted between this week and last week. */

AddDeltaWeek AS (
	SELECT 
		'Percent Change' AS 'DateHeader',

		/* Create conditions to account for situations in which:
			1) records may not exist for this week and/or last week 
				- Because we use a full outer join (see below), the resulting table returns "NULL" values for any week during which searches were not conducted.
			2) where divisor may be zero (records of searches do not exist for "last week"). */

		CASE 

			/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisWeek.TotalReferrals IS NOT NULL and PriorWeek.TotalReferrals IS NOT NULL and PriorWeek.TotalReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalReferrals) - CONVERT(FLOAT,PriorWeek.TotalReferrals))/CONVERT(FLOAT,PriorWeek.TotalReferrals),0)

			/* This second condition addresses the situation in which there are no records of searches for this week (ThisWeek.DateHeader is null), and
			   last week's values are neither null nor zero. */ 

			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalReferrals IS NOT NULL AND PriorWeek.TotalReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorWeek.TotalReferrals))/CONVERT(FLOAT, PriorWeek.TotalReferrals),0)

			/* Use condition "else NULL" for situations in which divisor is zero */
			ELSE NULL END AS TotalReferrals
		FROM (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Current Week'
			) AS ThisWeek

	/* In order to calculate the percent difference between this week and last week, we must move this week and last week's aggregated sum of searches into a single
	   row record. Using a full outer join allows us to do this. By joining the selected week data with the prior week data on a "dummy" value, 
	   we can collapse last week and this week's records into a single row. */

	FULL OUTER JOIN (
		SELECT * FROM NoPercWeek
		WHERE DateHeader = 'Last Week'
			) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDeltaWeek.TotalReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalReferrals) + '%' END AS 'TotalReferrals'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TotalReferrals, DummyDates.TotalReferrals) AS TotalReferrals
FROM (
	SELECT 
		 COALESCE(WeekDays.DateHeader, NoPercWeek.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
			/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(WeekDays.TotalReferrals AS VARCHAR), CAST(NoPercWeek.TotalReferrals AS VARCHAR), AddFormat.TotalReferrals) AS TotalReferrals
	FROM (
		SELECT Base.* 
			, CONCAT(LEFT(RIGHT(DateKey,4),2), '/', RIGHT(DateKey,2)) AS 'DateHeader'
		FROM Base
		WHERE Base.DateKey >= @StartDate AND Base.DateKey < DATEADD(DAY, 1, @EndDate)
		) AS WeekDays
	FULL OUTER JOIN NoPercWeek
	ON NoPercWeek.DateHeader = WeekDays.DateHeader
	FULL OUTER JOIN AddFormat
	ON NoPercWeek.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader 
ORDER BY DateHeader