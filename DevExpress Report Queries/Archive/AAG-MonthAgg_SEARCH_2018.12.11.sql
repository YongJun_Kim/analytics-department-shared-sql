/*
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-01-01' --DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-08-31' 
DECLARE @OrgParam AS INT  = 1100668

;
WITH 

Dummydates AS (
	SELECT  
		  
		REPLACE(RIGHT(CONVERT(VARCHAR(9), MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS Search
		, ROW_NUMBER() OVER (ORDER BY MonthLastDay) AS [sortkey]
			
	FROM vdim.[Date] AS dimDate
	WHERE MonthLastDay > @StartDate AND MonthLastDay <= @EndDate
		AND MonthLastDay = DateValue
			
	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, 'N/A' AS Search
		, '13' AS sortkey
)
,
Base AS (
	SELECT
		
		REPLACE(RIGHT(CONVERT(VARCHAR(9), dimDate.MonthLastDay, 6), 6), ' ', '-') AS 'DateHeader'
		, 'Dummy' AS Dummy
		, dimDate.MonthLastDay
		, SUM(vfactStandardMetricsSearch.Search) AS Search		
		, ROW_NUMBER() OVER (ORDER BY dimDate.MonthLastDay) AS sortkey
		, ROW_NUMBER() OVER (ORDER BY dimDate.MonthLastDay DESC) AS [rPeriod]

	FROM vfact.StandardMetricsSearch AS vfactStandardMetricsSearch
		JOIN vdim.[Date] as dimDate
		ON dimDate.DateKey = vfactStandardMetricsSearch.DateKey
	WHERE dimDate.MonthLastDay > @StartDate AND dimDate.MonthLastDay <= @EndDate
		AND OrganizationId in (@orgparam)
	
	GROUP BY dimDate.MonthLastDay	

) 
,
AddDeltaWeek AS (
	SELECT
		'% Change' AS DateHeader
		, '13' AS sortkey
		, ThisMonth.DateHeader AS TDateHeader
		, PriorMonth.DateHeader AS PDateHeader
		, ThisMonth.Search AS TSearch
		, PriorMonth.Search AS PSearch

		, CASE 

			/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisMonth.Search IS NOT NULL and PriorMonth.Search IS NOT NULL and PriorMonth.Search <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisMonth.Search) - CONVERT(FLOAT,PriorMonth.Search))/CONVERT(FLOAT,PriorMonth.Search),0)

			/* This second condition addresses the situation in which there are no records of searches for this week (ThisMonth.DateHeader is null), and
				last week's values are neither null nor zero. */ 
			WHEN (ThisMonth.DateHeader IS NULL AND PriorMonth.Search IS NOT NULL AND PriorMonth.Search <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorMonth.Search))/CONVERT(FLOAT, PriorMonth.Search),0)
				
			/* Use condition "else NULL" for situations in which divisor is zero */
			ELSE NULL END AS Search

	FROM (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Current 4 Week Period */
		WHERE [rPeriod] = 1
		) AS ThisMonth
	FULL OUTER JOIN (
		SELECT *
		FROM Base
		/* Return only the row record for the last week of the Perior 4 Week Period */
		WHERE [rPeriod] = 2
		) AS PriorMonth
	ON ThisMonth.Dummy = PriorMonth.Dummy
) 
,
AddFormat AS (

	SELECT 
		DateHeader
		, sortkey
		, CASE WHEN AddDeltaWeek.Search IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.Search) + '%' END AS 'Search'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.Search, DummyDates.Search) AS 'Search'
	, COALESCE(Biggie.sortkey, DummyDates.sortkey) AS sortkey
FROM (
SELECT 
	 COALESCE(Base.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
	, COALESCE(CAST(Base.Search AS VARCHAR), AddFormat.Search) AS Search
	, COALESCE(Base.sortkey, AddFormat.sortkey) AS sortkey
FROM Base
FULL OUTER JOIN AddFormat
ON Base.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader 