/*
At-A-Glance, Week Aggregation Query - TRACK
author: joanna.tung
date: 2018.12.11

	This query returns an export that can produce the desired table format presented for the At-A-Glance, 4-week by week report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Last 4Wk Period" field), and 
			2) it requires a calculation of the percent change between the current 4Week period total and the prior 4Week period total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for week period for the current 4Week period 
			- There are row records for pre-calculated Prior 4Week Total, Current 4Week Total, and Percent Change between these 4Wk Date Periods
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) wek aggregation, 
			2) 4Wk Period aggregation, and 
			3) percent change between 4Week Period aggregations
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for each of the 4 weeks in the current 4Week period, where format lists
					  the start and end day for the week as follows: 'mm/dd - mm/dd'
					- one row record for the current 4Week period totals, prior 4Week period totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each week in the current 4Week period
			LOGIC: 1) contains necessary week aggregations for the current 4Week period and 2) serves as a base table from which to perform aggregations for
			the current 4Week period and the prior 4week period (see NoPercWeek table)

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the current 4Week period and the prior 4Week period
			LOGIC: uses Base (temp) table to produce necessary week aggregations to display in the "Current.." and "Prior.." columns of the final report

		4) AddDeltaWeek (temp):
			OUTPUT: percent change values for current 4Week period and the prior 4Week period
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the current and prior date period, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between the current and prior 4Week periods. Uses "dummy" field to
			perform 'arbitrary' join to append prior data to current data along the horizontal axis.

		5) AddFormat (temp):
			OUTPUT: percent change values for current 4week period and the prior 4week period, with '%' sign added for values that are not NULL, and 'N/A' 
			inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		6) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each week of the current 4week period, by the current 4week period, and by the prior 4week period, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from current to the prior 4week period.
			LOGIC: Perform full outer join to merge the individual week aggregations, 4week aggregations, and formatted percent change (AddFormat) values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = DATEADD(DAY, -27, '2018-08-26') -- DATEADD(DAY, -6, '2018-08-26') -- 
DECLARE @EndDate AS DATETIME = '2018-08-26' 
DECLARE @OrgParam AS INT  = 1077836

;
WITH 

/* The At-a-Glance report requires that all weeks in the current 4 week period appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all necessary weeks. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 4 weeks of the current 4 week period appear in the export. */

Dummydates AS (
	SELECT  
		
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table. 
		   
		   NOTE: Must cast all integers as VARCHAR data type so that it will union properly with any 'N/A' values. */ 
		  
		CASE 
			/* One case for each week in the Current 4Wk Period */
			WHEN DateValue >= @StartDate AND DateValue < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 7, @StartDate) AND DateValue < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 14, @StartDate) AND DateValue < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 21, @StartDate) AND DateValue < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
				END AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedUnsuccessfully
			
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= @StartDate AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)
	GROUP BY (CASE 
			/* One case for each week in the Current 4Wk Period */
			WHEN DateValue >= @StartDate AND DateValue < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 7, @StartDate) AND DateValue < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 14, @StartDate) AND DateValue < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateValue >= DATEADD(DAY, 21, @StartDate) AND DateValue < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
			END)

	UNION ALL

	/* The AAG-day aggregation report requires us to provide values for "Current Week" totals, Prior Week" totals, and a separate column for "Percent Change" (betwen weeks). 
	   Create dummy rows to ensure that these column header fields are displayed, even when there are no available data. */

	SELECT
		'Last 4Wk Period' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedUnsuccessfully
		
	UNION ALL

	SELECT
		'Current 4Wk Period' AS 'DateHeader'
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedSuccessfully
		, CAST(0 AS VARCHAR) AS TrackedServiceReferralsClosedUnsuccessfully
		
	UNION ALL

	SELECT
		'Percent Change' AS 'DateHeader'
		, 'N/A' AS TrackedReferralsShared
		, 'N/A' AS TrackedServiceReferralsClosedSuccessfully
		, 'N/A' AS TrackedServiceReferralsClosedUnsuccessfully
)
,
Base AS (

	SELECT 
		CAST(COALESCE(vfactStandardMetricsTrackedReferral.DateHeader, vfactStandardMetricsPSI.DateHeader) AS VARCHAR) AS DateHeader
		, CAST(COALESCE(vfactStandardMetricsTrackedReferral.DatePeriod, vfactStandardMetricsPSI.DatePeriod) AS VARCHAR) AS DatePeriod
		, (CASE WHEN vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully IS NULL THEN 0 
			ELSE vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully END) AS TrackedServiceReferralsClosedSuccessfully
		, (CASE WHEN vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully IS NULL THEN 0 
			ELSE vfactStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully END) as TrackedServiceReferralsClosedUnsuccessfully
		, (CASE WHEN vfactStandardMetricsPSI.TrackedReferralsShared IS NULL THEN 0 
			ELSE vfactStandardMetricsPSI.TrackedReferralsShared END) AS TrackedReferralsShared
	FROM (
		SELECT 
		/* Tag by Individual Weeks and Last 4 Wk Period */ 
		 CASE 
			/* For the Last 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(DateKey as VARCHAR(8)) < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= @StartDate AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 7, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 14, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 21, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
				END AS 'DateHeader'
		/* Tag by Last 4Wk and Current 4Wk Period */
		, CASE 
			/* For the Last 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(DateKey as VARCHAR(8)) < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= @StartDate AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
				END AS 'DatePeriod'	 
			, SUM(factStandardMetricsTrackedReferral.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
			, SUM(factStandardMetricsTrackedReferral.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully
		FROM vfact.StandardMetricsTrackedReferral AS factStandardMetricsTrackedReferral
	WHERE CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
		AND OrganizationId in (@orgparam)
	
	GROUP BY
		/* First Group By Last vs. Current 4Wk Period */ 
		(CASE 
			/* For the Last 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(DateKey as VARCHAR(8)) < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= @StartDate AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
			END),
		/* Then Group By Last 4Wk Period and Individual Weeks of the Current 4Wk Period */
		(CASE 
			/* For the Last 4-Week Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(DateKey as VARCHAR(8)) < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN CAST(DateKey as VARCHAR(8)) >= @StartDate AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 7, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 14, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN CAST(DateKey as VARCHAR(8)) >= DATEADD(DAY, 21, @StartDate) AND CAST(DateKey as VARCHAR(8)) < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
			END)
		) AS vfactStandardMetricsTrackedReferral

	FULL OUTER JOIN (
		SELECT 
		/* Tag by Individual Weeks and Last 4 Wk Period */ 
		 CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 7, @StartDate) AND DateKey < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 14, @StartDate) AND DateKey < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 21, @StartDate) AND DateKey < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
				END AS 'DateHeader'
		/* Tag by Last 4Wk and Current 4Wk Period */
		, CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
				END AS 'DatePeriod'	 
			, SUM(factStandardMetricsPSI.TrackedReferralsShared) AS TrackedReferralsShared
		FROM vfact.StandardMetricsPSI AS factStandardMetricsPSI
	WHERE DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < DATEADD(DAY, 1, @EndDate)
		AND OrganizationId in (@orgparam)
	
	GROUP BY
		/* First Group By Last vs. Current 4Wk Period */ 
		(CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk Period'
			/* For the Current 4-Week Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 1, @EndDate)
				THEN 'Current 4Wk Period'
			END),
		/* Then Group By Last 4Wk Period and Individual Weeks of the Current 4Wk Period */
		(CASE 
			/* For the Last 4-Week Period */
			WHEN DateKey >= DATEADD(DAY, -28, @StartDate) AND DateKey < @StartDate	
				THEN 'Last 4Wk'
			/* One case for each week in the Current 4Wk Period */
			WHEN DateKey >= @StartDate AND DateKey < DATEADD(DAY, 7, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), @StartDate, 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 7, @StartDate) AND DateKey < DATEADD(DAY, 14, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 14, @StartDate) AND DateKey < DATEADD(DAY, 21, @StartDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, @StartDate), 101), 5)
			WHEN DateKey >= DATEADD(DAY, 21, @StartDate) AND DateKey < DATEADD(DAY, 28, @EndDate)
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, @StartDate), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), @EndDate, 101), 5)
			END)

		) AS vfactStandardMetricsPSI
	ON vfactStandardMetricsTrackedReferral.DateHeader = vfactStandardMetricsPSI.DateHeader 
)
,
NoPercWeek AS (
	SELECT
		DatePeriod AS DateHeader
		, 'Dummy' AS Dummy
		, SUM(Base.TrackedReferralsShared) AS TrackedReferralsShared
		, SUM(Base.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
		, SUM(Base.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully
	FROM Base
	GROUP BY DatePeriod
	
)
,
AddDeltaWeek AS (
	SELECT 
		'Percent Change' AS 'DateHeader',

		/* Create conditions to account for situations in which:
			1) records may not exist for this week and/or last week 
				- Because we use a full outer join (see below), the resulting table returns "NULL" values for any week during which searches were not conducted.
			2) where divisor may be zero (records of searches do not exist for "last week." */
		CASE 

			/* This first condition addresses situations in which values exist for this week, and last week's values are neither null nor zero. */
			WHEN (ThisWeek.TrackedReferralsShared IS NOT NULL and PriorWeek.TrackedReferralsShared IS NOT NULL and PriorWeek.TrackedReferralsShared <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TrackedReferralsShared) - CONVERT(FLOAT,PriorWeek.TrackedReferralsShared))/CONVERT(FLOAT,PriorWeek.TrackedReferralsShared),0)

			/* This second condition addresses the situation in which there are no records of searches for this week (ThisWeek.DateHeader is null), and
			   last week's values are neither null nor zero. */ 
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TrackedReferralsShared IS NOT NULL AND PriorWeek.TrackedReferralsShared <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT, PriorWeek.TrackedReferralsShared))/CONVERT(FLOAT, PriorWeek.TrackedReferralsShared),0)

			/* Use condition "else NULL" for situations in which divisor is zero */
			ELSE NULL END AS TrackedReferralsShared,

		CASE 
			WHEN (ThisWeek.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedSuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TrackedServiceReferralsClosedSuccessfully) - CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedSuccessfully))/CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedSuccessfully),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TrackedServiceReferralsClosedSuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedSuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedSuccessfully))/CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedSuccessfully),0)
			ELSE NULL END AS TrackedServiceReferralsClosedSuccessfully,

		CASE 
			WHEN (ThisWeek.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TrackedServiceReferralsClosedUnsuccessfully) - CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedUnsuccessfully))/CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedUnsuccessfully),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TrackedServiceReferralsClosedUnsuccessfully IS NOT NULL AND PriorWeek.TrackedServiceReferralsClosedUnsuccessfully <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedUnsuccessfully))/CONVERT(FLOAT,PriorWeek.TrackedServiceReferralsClosedUnsuccessfully),0)
			ELSE NULL END AS TrackedServiceReferralsClosedUnsuccessfully

	FROM (
		SELECT *
		FROM NoPercWeek
		WHERE DateHeader = 'Current 4Wk Period'
		) AS ThisWeek
	FULL OUTER JOIN (
		SELECT *
		FROM NoPercWeek
		WHERE DateHeader = 'Last 4Wk Period'
		) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDeltaWeek.TrackedReferralsShared IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedReferralsShared) + '%' END AS 'TrackedReferralsShared'
		, CASE WHEN AddDeltaWeek.TrackedServiceReferralsClosedSuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedServiceReferralsClosedSuccessfully) + '%' END AS 'TrackedServiceReferralsClosedSuccessfully'
		, CASE WHEN AddDeltaWeek.TrackedServiceReferralsClosedUnsuccessfully IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TrackedServiceReferralsClosedUnsuccessfully) + '%' END AS 'TrackedServiceReferralsClosedUnsuccessfully'
	FROM AddDeltaWeek
)

SELECT
	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
	, COALESCE(Biggie.TrackedReferralsShared, DummyDates.TrackedReferralsShared) AS TrackedReferralsShared
	, COALESCE(Biggie.TrackedServiceReferralsClosedSuccessfully, DummyDates.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
	, COALESCE(Biggie.TrackedServiceReferralsClosedUnsuccessfully, DummyDates.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully

FROM (
	SELECT 
		 COALESCE(WeekDays.DateHeader, NoPercWeek.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		, COALESCE(CAST(WeekDays.TrackedReferralsShared AS VARCHAR), CAST(NoPercWeek.TrackedReferralsShared AS VARCHAR), AddFormat.TrackedReferralsShared) AS TrackedReferralsShared
		, COALESCE(CAST(WeekDays.TrackedServiceReferralsClosedSuccessfully AS VARCHAR), CAST(NoPercWeek.TrackedServiceReferralsClosedSuccessfully AS VARCHAR), AddFormat.TrackedServiceReferralsClosedSuccessfully) AS TrackedServiceReferralsClosedSuccessfully
		, COALESCE(CAST(WeekDays.TrackedServiceReferralsClosedUnsuccessfully AS VARCHAR), CAST(NoPercWeek.TrackedServiceReferralsClosedUnsuccessfully AS VARCHAR), AddFormat.TrackedServiceReferralsClosedUnsuccessfully) AS TrackedServiceReferralsClosedUnsuccessfully
	FROM (
		SELECT *
		FROM Base
		WHERE DatePeriod = 'Current 4Wk Period'
		) AS WeekDays
	FULL OUTER JOIN NoPercWeek
	ON NoPercWeek.DateHeader = WeekDays.DateHeader
	FULL OUTER JOIN AddFormat
	ON NoPercWeek.DateHeader = AddFormat.DateHeader
) AS Biggie
RIGHT OUTER JOIN DummyDates
ON DummyDates.DateHeader = Biggie.DateHeader
ORDER BY DateHeader