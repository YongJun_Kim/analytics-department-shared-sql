/* This query is used to tag reports with the start date and end date parameters used to generate the report. 
   Only those reports utilizing "custom" date periods require this field to be added to the template. */

SELECT 
	MIN(DateValue) as 'Start Date'
	, Max(DateValue) as 'End Date'
FROM vdim.[Date] 
WHERE DateValue IN (@startdate, @enddate)
