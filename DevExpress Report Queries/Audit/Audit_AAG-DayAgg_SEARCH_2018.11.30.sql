DECLARE @StartDate AS DATETIME = DATEADD(DAY, -6, '2018-09-16') 
DECLARE @EndDate AS DATETIME = '2018-09-16' 
DECLARE @OrgParam AS INT  = 1106525

SELECT *
FROM vfact.StandardMetricsSearch AS vfactStandardMetricsSearch
WHERE vfactStandardMetricsSearch.DateKey >= DATEADD(DAY, -7, @StartDate) 
AND vfactStandardMetricsSearch.DateKey < DATEADD(DAY, 1, @EndDate)
AND OrganizationId IN (@orgparam) 