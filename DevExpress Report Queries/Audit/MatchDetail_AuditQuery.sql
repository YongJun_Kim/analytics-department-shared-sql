/* 
Match Detail QA Audit Query
author: joanna.tung

- Designed to run in ODS:
    - Allows cross-DB query runs for validation
    - Available data has same end date time stamp as that in DW
    - Requires use of IsCurrent filters
- Designed for comparison to table records in 2nd page of Match Detail report
    - Note: This query is utilizes the "old" monthly report method for calculating distance (assumes EArth is flat), whereas the new Analytics 2.0 query assumes some curvature.
      This does result in some discrepancy between the values...will need to account for this in the QA comparison script.

- Purpose: Use this query to QA Match Detail reports generated through the reporting application.

- To use: Run this query in ODS, and set "report parameter" inputs in lines 15-17.
    - set @startdate
    - set @enddate
    - set @orgparam as comma-delimited string 

Original: 2019.03.29
Updated: 2019.04.11
*/ 

DECLARE @StartDate AS DATETIME
DECLARE @EndDate AS DATETIME
DECLARE @orgparam AS vARCHAR(MAX)
DECLARE @CareOrgDBID AS BIGINT
DECLARE @XML XML

/* Set these inputs based on the organizations and date ranges used to generate the report in the application */
SET @StartDate = '10/01/2018'
SET @EndDate = '12/31/2018'
SET @orgparam = '1100732,1100733,1104188,1104189,1104190,1104191,1104192,1104193,1104195,1104197,1104198,1104199,1104200,1104201,1104202,1104203,1104204,1104205,1104206,1104207,1104208,1104209,1104210,1104211,1104212,1104213,1104214,1104215,1104216,1104217,1107747,1117478'

/* Add this filter for Mt Sinai -- Daryl added extra data to CareOrgDbId = 212 for DB migration testing. Also remove comments at line 356. */
--SET @CareOrgDBID = 21 --17

SET @XML = CAST('<i>' + REPLACE(@orgparam, ',', '</i><i>') + '</i>' AS XML)

/* These temp tables mimic "IsCurrent" filtering completed for the data that moves into the DW */
;
WITH
tempUser AS (
	SELECT 
		UserId
		, ContactId
		, ExcludeFromReports
	FROM nowpow.[User]
	WHERE IsCurrent = 1
)
, 
tempCC AS (
	SELECT 
		CareCoordinatorId
		, ContactId
		, CareOrganizationDatabaseId
	FROM nowpow.CareCoordinator
	WHERE IsCurrent = 1
)
, 
tempSite AS (
	SELECT S.SiteId, S.OrganizationId
	FROM nowpow.[Site] AS S
		INNER JOIN @XML.nodes('i') x(i)
		ON S.OrganizationId = x.i.value('.', 'VARCHAR(MAX)')
	WHERE IsCurrent = 1
)
, 
tempPSISOR AS (
	SELECT *
	FROM nowpow.PatientSiteInteractionServiceOrganizationRelation
	WHERE IsCurrent = 1
)
, 
tempPPL AS (
	SELECT 
		PrescriptionId
		, CareOrganizationDatabaseId
		, Latitude
		, Longitude
	FROM [log].PrescriptionPatientLocation
	WHERE IsCurrent = 1
)
, 
tempPSI AS (
	SELECT 
		PatientSiteInteractionId
		, InteractionTypeId
		, InteractionDate
		, CareCoordinatorId
		, SiteId
		, PrescriptionId
		, ServiceId
		, PatientId
		, CareOrganizationDatabaseId
	FROM nowpow.patientsiteinteraction
	WHERE IsCurrent = 1
		AND InteractionTypeId in (3,4,5,6,11)
)
,
tempPS AS (
	SELECT 
		PrescriptionId
		, ServiceId
		, CareOrganizationDatabaseId
	FROM [log].PrescriptionService
	WHERE IsCurrent = 1
)
,
tempPatient AS (
	SELECT 
		PatientId
		, CareOrganizationDatabaseId
		, LastName
	FROM nowpow.Patient
	WHERE IsCurrent = 1
)
, 
tempOrganization AS (
	SELECT O.*, LOC.PostalCode, LOC.Latitude, LOC.Longitude
	FROM nowpow.Organization AS O
	LEFT JOIN nowpow.[Location] AS LOC
	ON O.LocationId = LOC.LocationId AND LOC.IsCurrent = 1
	WHERE O.IsCurrent = 1
)
, 
tempService AS (
	SELECT 
		Serv.*, LOC.PostalCode
	FROm nowpow.[Service] AS Serv
	LEFT JOIN nowpow.[Location] AS LOC
	ON Serv.LocationId = LOC.LocationId AND LOC.IsCurrent=1
	WHERE Serv.IsCurrent = 1
)
, 
tempLocation AS (
	SELECT *
	FROM nowpow.[Location]
	WHERE IsCurrent = 1
)
, 
tempCODSR AS (
	SELECT *
	FROM nowpow.CareOrganizationDatabaseSiteRelation 
	WHERE IsCurrent = 1
) 


SELECT

	OrganizationName AS 'SelectedOrganizationName'
	, ServiceProviderOrganizationName
	, ServiceTypeName AS 'ServiceType'
	, ServiceName
	, ZipCode
	, SUM(TotalReferrals) AS 'TotalReferrals'
	, CONVERT(DECIMAL(19,2),ROUND(SUM(Distance)/SUM(CASE WHEN Distance IS NULL THEN 0 ELSE TotalReferrals END),2)) AS 'AvgDistance'

FROM (
	SELECT 
		AR.DateKey
		, AR.ServiceId
		, ServName.[Term] AS 'ServiceName'
		, ST.[NameDefaultText] AS 'ServiceTypeName'
		, AR.OrganizationId
		, O.[Name] AS OrganizationName
		, CASE
			WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND AR.NudgedServiceOrganizationId IS NOT NULL THEN NudgedSPO.[Name]
			ELSE SPO.[Name]
			END AS ServiceProviderOrganizationName
		, CASE 
			WHEN (ReferralName = 'ServPrint' OR ReferralName = 'ServNudged') AND AR.NudgedServiceOrganizationId IS NOT NULL THEN NudgedSPO.PostalCode
			WHEN Serv.PostalCode IS NULL THEN SPO.PostalCode
			ELSE Serv.PostalCode
			END AS ZipCode
		, AR.CareOrganizationDatabaseId
		, AR.PrescriptionId
		, AR.UserId
		, AR.PatientId
		, AR.Latitude AS PatientLatitude
		, AR.Longitude AS PatientLongitude
		, O.Latitude AS ServiceLatitude
		, O.Longitude AS ServiceLongitude

		, CASE
			WHEN (AR.Latitude IS NULL OR AR.Longitude IS NULL) THEN NULL
			ELSE ((geography::Point(AR.Latitude, AR.Longitude, 4326).STDistance(geography::Point(AR.Latitude, O.Longitude, 4326)) 					
				   + (geography::Point(AR.Latitude, AR.Longitude, 4326).STDistance(geography::Point(O.Latitude, AR.Longitude, 4326)))) / 1609.344)
			END AS Distance

		, AR.ReferralName
		, AR.NudgedServiceOrganizationId
		, AR.Referral AS TotalReferrals

	FROM (

		/* Service Nudged(PSI 5,6) */
		SELECT CAST(CONVERT(VARCHAR(8),PSI.InteractionDate,112) AS INT) AS DateKey
			, PSI.ServiceId
			, S.OrganizationId
			, U.UserId
			, PSI.CareOrganizationDatabaseId
			, NULL AS PrescriptionId
			, PSI.PatientId
			, NULL AS Latitude
			, NULL AS Longitude
			, 'ServNudged' AS ReferralName
			, PSISOR.ServiceOrganizationId AS NudgedServiceOrganizationId
			, 1 AS Referral
		FROM tempPSI AS PSI
			LEFT JOIN tempCC AS CC
			ON CC.CareCoordinatorId = PSI.CareCoordinatorId AND CC.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
			LEFT JOIN tempUser AS U
			ON U.ContactId = CC.ContactId
			LEFT JOIN tempPSISOR PSISOR
			ON PSISOR.PatientSiteInteractionId = PSI.PatientSiteInteractionId AND PSISOR.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
			INNER JOIN tempSite AS S
			ON S.SiteId = PSI.SiteId
		WHERE PSI.InteractionTypeId IN (5,6)
			AND (PSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate))

		UNION ALL

		/* TrackedReferrals(fact.Referral) */
		SELECT CAST(CONVERT(VARCHAR(8), TR.CreatedDate,112) AS INT) AS DateKey
			, TR.ServiceId 
			, TR.MakerOrganizationId AS OrganizationId
			, U.UserId
			, CODSR.CareOrganizationDatabaseId
			, NULL AS PrescriptionId
			, TR.MakerPatientId AS PatientId
			, TR.PatientLatitude AS Latitude
			, TR.PatientLongitude AS Longitude
			, 'TrackedRef' AS ReferralName
			, NULL AS NudgedServiceOrganizationId
			, 1 AS Referral 
		FROM nowpow.Referral TR
			LEFT JOIN tempUser U
			ON U.ContactId = TR.MakerContactId
			INNER JOIN tempSite AS S
			ON S.OrganizationId = TR.MakerOrganizationId
			LEFT JOIN tempCODSR AS CODSR
			ON CODSR.SiteId = S.SiteId
		WHERE TR.IsCurrent = 1 
			AND (TR.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND TR.CreatedDate < DATEADD(DAY, 1, @EndDate))

		UNION ALL

		/* Referrals on Shared Erx(fact.ErxService) */
		SELECT CAST(CONVERT(VARCHAR(8), PSI.InteractionDate,112) AS INT) DateKey
			, ES.ServiceId
			, S.OrganizationId
			, U.UserId
			, PSI.CareOrganizationDatabaseId
			, PSI.PrescriptionId
			, PSI.PatientId
			, EL.Latitude
			, EL.Longitude
			, 'SharedErx' AS ReferralName
			, NULL AS NudgedServiceOrganizationId
			, 1 AS Referral
		FROM tempPS ES
			INNER JOIN tempPPL AS EL
			ON ES.PrescriptionId = EL.PrescriptionId AND ES.CareOrganizationDatabaseId = EL.CareOrganizationDatabaseId
			INNER JOIN (
				SELECT PSI.PrescriptionId, PSI.SiteId, PSI.CareCoordinatorId, PSI.PatientId, PSI.InteractionDate, PSI.CareOrganizationDatabaseId--, PSI.PatientSiteInteractionId
				FROM tempPSI AS PSI
				WHERE PSI.InteractionTypeId in (3,4,11) 
					AND PSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND PSI.InteractionDate < DATEADD(DAY, 1, @EndDate)
				) PSI
				ON ES.PrescriptionId = PSI.PrescriptionId
				AND ES.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
			LEFT JOIN tempCC CC
			ON CC.CareCoordinatorId = PSI.CareCoordinatorId AND CC.CareOrganizationDatabaseId = PSI.CareOrganizationDatabaseId
			LEFT JOIN tempUser U
			ON U.ContactId = CC.ContactId
			INNER JOIN tempSite as S
			ON S.SiteId = PSI.SiteId
	
		UNION ALL 

		/* Printed Service */
		SELECT CAST(CONVERT(VARCHAR(8), Uact.ActionDate, 112) AS INT) DateKey
			, CASE
				WHEN UAct.[Action] = 'Printed Service' THEN Uact.ReferenceId
				ELSE NULL 
			END AS ServiceId
			, Uact.ContactOrganizationId AS OrganizationId
			, Uact.UserId
			, CODSR.CareOrganizationDatabaseId
			, NULL AS PrescriptionId
			, NULL AS PatientId
			, NULL AS Latitude
			, NULL AS Longitude
			, 'ServPrint' AS ReferralName
			, Uact.ReferenceId AS NudgedServiceOrganizationId
			, 1 AS Referral
		FROM [log].UserActivity Uact
			INNER JOIN tempSite AS S
			ON S.OrganizationId = Uact.ContactOrganizationId
			LEFT JOIN tempCODSR AS CODSR
			ON CODSR.SiteId = S.SiteId
		WHERE UAct.[Action] = 'Printed Organization'
			AND (Uact.ActionDate >= DATEADD(DAY, 0, @StartDate) AND Uact.ActionDate < DATEADD(DAY, 1, @EndDate))

		UNION ALL

		SELECT CAST(CONVERT(VARCHAR(8), Uact.ActionDate, 112) AS INT) DateKey
			, CASE
				WHEN UAct.[Action] = 'Printed Service' THEN Uact.ReferenceId
				ELSE NULL 
			END AS ServiceId
			, Uact.ContactOrganizationId AS OrganizationId
			, Uact.UserId
			, CODSR.CareOrganizationDatabaseId
			, NULL AS PrescriptionId
			, NULL AS PatientId
			, NULL AS Latitude
			, NULL AS Longitude
			, 'ServPrint' AS ReferralName
			, Serv.OrganizationId AS NudgedServiceOrganizationId
			, 1 AS Referral
		FROM [log].UserActivity Uact
			LEFT JOIN tempService Serv
			ON Uact.ReferenceId = Serv.ServiceId
			INNER JOIN tempSite AS S
			ON S.OrganizationId = Uact.ContactOrganizationId
			LEFT JOIN tempCODSR AS CODSR
			ON CODSR.SiteId = S.SiteId
		WHERE UAct.[Action] = 'Printed Service'
			AND (Uact.ActionDate >= DATEADD(DAY, 0, @StartDate) AND Uact.ActionDate < DATEADD(DAY, 1, @EndDate))

	) AR

	LEFT JOIN tempUser AS U
	ON AR.UserId = U.UserId
	LEFT JOIN tempPatient AS P
	ON AR.PatientId = P.PatientId AND AR.CareOrganizationDatabaseId = P.CareOrganizationDatabaseId
	LEFT JOIN tempOrganization AS O
	ON AR.OrganizationId = O.OrganizationId
	LEFT JOIN tempService AS Serv
	ON AR.ServiceId = Serv.ServiceId

	LEFT JOIN tempOrganization AS NudgedSPO
	ON NudgedSPO.OrganizationId = AR.NudgedServiceOrganizationId

	LEFT JOIN tempOrganization AS SPO
	ON SPO.OrganizationId = Serv.OrganizationId

	LEFT JOIN nowpow.ServiceType AS ST
	ON ST.ServiceTypeId = Serv.TypeId AND ST.IsCurrent = 1

	LEFT JOIN (
		SELECT LT.Term, LT.TextId
		FROM nowpow.localizedtext AS LT
		LEFT JOIN nowpow.[text] AS TXT ON LT.TextId = TXT.TextId AND TXT.IsCurrent = 1
		WHERE TXT.Context = 'Service Name' AND (LT.Supportedlanguageid = 1 OR Supportedlanguageid is null) AND LT.IsCurrent = 1
		) AS ServName
	ON ServName.TextId = Serv.NameTextId


	WHERE (U.[ExcludeFromReports] <> 1 OR U.[ExcludeFromReports] IS NULL)
	AND ([P].[LASTNAME] NOT LIKE '%Zzztest%' OR [P].[LastName] IS NULL)
	--AND AR.CareOrganizationDatabaseId = @CareOrgDBID

) AS Biggie
GROUP BY OrganizationName 
	, ServiceProviderOrganizationName
	, ServiceTypeName
	, ServiceName
	, ZipCode

ORDER BY SelectedOrganizationName, ServiceProviderOrganizationName, ServiceType, ZipCode, TotalReferrals