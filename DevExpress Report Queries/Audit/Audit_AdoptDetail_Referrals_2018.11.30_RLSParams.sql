DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-09-16'
DECLARE @orgparam AS INT = 1100679

-- Test eRx Creation Part
SELECT 
	UserId
	,OrganizationId
	,COUNT(DISTINCT CONCAT(ErxId, ';', CareOrganizationDatabaseId)) AS 'ErxsCreated'
FROM vfact.Erx 
WHERE UserId IS NOT NULL
	AND CreatedDate >= @StartDate AND CreatedDate < DATEADD(DAY, 1, @EndDate)
	AND OrganizationId IN (@orgparam)
GROUP BY UserId, OrganizationId;

-- Test Erx shares and eRx Referrals
SELECT 
	PSI.UserId
	,PSI.OrganizationId
	,SUM(PSI.CountShares) AS 'ErxShares'
	,SUM(ErxRefs.Referrals) AS 'ErxReferrals'
FROM (
	/* By UserId, OrganizationId: Count of unique shares per eRxId */
		SELECT
			CONCAT(ErxId, ';',CareOrganizationDatabaseId) AS 'ErxId'
			,COUNT(InteractionDate) AS 'CountShares'
			,UserId
			,OrganizationId
		FROM vfact.PatientSiteInteraction
		WHERE InteractionTypeId IN (3, 4, 11) AND ErxId IS NOT NULL AND UserId IS NOT NULL
			AND InteractionDate >= @StartDate AND InteractionDate < DATEADD(DAY, 1, @Enddate)
			AND OrganizationId IN (@orgparam)
		GROUP BY CONCAT(ErxId, ';', CareOrganizationDatabaseId), UserId, OrganizationId

	) AS PSI
LEFT JOIN (
		/* By UserId, OrganizationId: Count of referrals per eRxId */
		SELECT 
			UserId
			,OrganizationId
			,concat(ErxId, ';', CareOrganizationDatabaseId) AS 'ErxId'
			,SUM(ErxServiceQuantity) AS Referrals
		FROM vfact.ErxService
		WHERE UserId IS NOT NULL
			AND CreatedDate >= @StartDate AND CreatedDate < DATEADD(DAY, 1, @Enddate)
			AND OrganizationId IN (@orgparam)
		GROUP BY CONCAT(ErxId, ';', CareOrganizationDatabaseId), UserId, OrganizationId
	) AS ErxRefs
ON PSI.ErxId = ErxRefs.ErxId
GROUP BY PSI.UserId, PSI.OrganizationId

-- Test Count of Tracked and Nudged Service Shares
SELECT 
	UserId
	,OrganizationId
	,SUM(CASE WHEN interactiontypeid IN (15, 23) THEN 1 ELSE 0 END) AS 'TrackedShares'
	,SUM(CASE WHEN interactiontypeid IN (5, 6) THEN 1 ELSE 0 END) AS 'NudgeShares'
FROM vfact.PatientSiteInteraction
WHERE interactiontypeid IN (5, 6, 15, 23) AND UserId IS NOT NULL
	AND InteractionDate >= @StartDate 
	AND InteractionDate < DATEADD(DAY, 1, @Enddate)
	AND OrganizationId IN (@orgparam)
GROUP BY UserId, OrganizationId

-- Test Count of Service Prints
SELECT
	UserId
	,ContactOrganizationId AS OrganizationId
	,SUM(UserActivityQuantity) AS 'PrintShares'
FROM vfact.UserActivity
WHERE UserActivityAction = 'Printed Service'
	AND UserActivityActionDate >= @StartDate 
	AND UserActivityActionDate < DATEADD(DAY, 1, @Enddate)
	AND ContactOrganizationId IN (@orgparam)
GROUP BY UserId, ContactOrganizationId