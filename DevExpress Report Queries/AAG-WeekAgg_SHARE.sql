/*
At-A-Glance, Week Aggregation Query - SHARE
author: joanna.tung
date: 2018.12.11

	This query returns an export that can produce the desired table format presented for the At-A-Glance, 4-week by week report in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrPivotGrid, where values are summarized as TEXT values (SummaryType = Max).
		
		The final table structure of the At-a-Glance report breaks the logic of a standard pivot grid in that
			1) it requires aggregation of data that is not present in the pivot table (see "Last 4Wk Period" field), and 
			2) it requires a calculation of the percent change between the current 4Week period total and the prior 4Week period total.
		
		To display the desired table format, this query must produce an export where each row record corresponds to each of the final pivot grid, wherein:
			- There is a row record provided for week period for the current 4Week period 
			- There are row records for pre-calculated Prior 4Week Total, Current 4Week Total, and Percent Change between these 4Wk Date Periods
		and wherein all metrics to be included in the final pivot grid are set as columns in the export.

	In addition, the report requires the calculation of percentages in the final report. 
		- For the "% Change" column, the percentages should be rounded to the nearest integer for non-zero values and should also include a '%' sign.
		- For all situations in which the denominator = 0, the report should display 'N/A.'

	This export format is accomplished using the following strategy:
		- Divide the final report's column fields into three categories: 
			1) wek aggregation, 
			2) 4Wk Period aggregation, and 
			3) percent change between 4Week Period aggregations
		- Create a temp table for each of the three categories above, returning each required metric
		- Label each record in the temp tables with a "DateHeader" field containing the desired PivotGrid column header names (in the final report)
		- In the main select statement, use full outer join on this "DateHeader" field to join the tables into a single table. Use "coalesce" function
		  to ignore null values.

	Summary of tables used:
		1) Dummydates (temp): 
			OUTPUT:
				DummyDates:
					- one row record with the correct DateHeader formatting for each of the 4 weeks in the current 4Week period, where format lists
					  the start and end day for the week as follows: 'mm/dd - mm/dd'
					- one row record for the current 4Week period totals, prior 4Week period totals, and percent change field
			LOGIC: account for situtaions where data does not exist for the selected time period, for the selected orgs. I perform an outer join
			to this table in the main select statement in order to ensure that all required row records are present in the final export.

		2) Base (temp):
			OUTPUT: aggregated sums for each week in the current 4Week period
			LOGIC: 1) contains necessary week aggregations for the current 4Week period and 2) serves as a base table from which to perform aggregations for
			the current 4Week period and the prior 4week period (see NoPercWeek table)

		3) NoPercWeek (temp):
			OUTPUT: aggregated sums for the current 4Week period and the prior 4Week period
			LOGIC: uses Base (temp) table to produce necessary week aggregations to display in the "Current.." and "Prior.." columns of the final 
		
		4) AddPercWeek (temp):
			OUTPUT: percentages for 4week totals for current and last 4week date periods
			LOGIC: aggregate across the "DatePeriod" column from the NoPercWeek table

		5) AddDeltaWeek (temp):
			OUTPUT: percent change values for current 4Week period and the prior 4Week period
			LOGIC: breaks out data in "NoPercWeek" table into separate tables for the current and prior date period, then performs a full outer
			join to collapse the data into a single row record, so that we can calculate the percent change between the current and prior 4Week periods. Uses "dummy" field to
			perform 'arbitrary' join to append prior data to current data along the horizontal axis.

		6) AddFormat (temp):
			OUTPUT: percent change values for current 4week period and the prior 4week period, with '%' sign added for values that are not NULL, and 'N/A' 
			inputted for NULL values (where divisor was 0)
			LOGIC: use conditional statement to choose what value should be propagated in the field

		7) MAIN select statement:
			OUTPUT: aggregated data for the selected organizations by each week of the current 4week period, by the current 4week period, and by the prior 4week period, displayed
			as row records. Also contains a row record containing the percent change in aggregate values from current to the prior 4week period.
			LOGIC: Perform full outer join to merge the individual week aggregations, 4week aggregations, and formatted percent change (AddFormat) values into a single table.  Use 
			"coalesce" to display the value DateHeader (column) values. Perform outer join to Dummydates table to ensure that all required row records for "DateHeader" 
			exist in the output.
	
	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/
DECLARE @StartDate AS DATETIME = '2018-07-30' --DATEADD(DAY, -27, '2018-09-23') 
DECLARE @EndDate AS DATETIME = '2018-08-26'
DECLARE @OrgParam AS INT = 1085389

;
WITH 

/* The At-a-Glance report requires that all weeks in the current 4 week period appear in the pivot grid, even if the fact table contains no data for the given date period.
   This temp table uses the dim.Date to create a table containing all necessary weeks. We use an outer join to the DummyDates temp table in the final select
   statement below in order to ensure that all 4 weeks of the current 4 week period appear in the export. */

Dummydates AS (
	SELECT  
		/* The AAG-day aggregation report requires date headers to be formatted as "month/day." The DevExpress xrPivotGrid processes this kind of DateHeader as a string. 
		   To ensure that day 9/10 appears after 9/9 when column headers are set to sort in "ascending order", use date format 09/10 and 09/09. 
		   We parse/concatenate the DateKey field to produce this result, such that when column headers are set to sort in ascending order, 
		   the oldest date occupies the leftmost position in the resulting table. 
		   
		   NOTE: Must cast all integers as VARCHAR data type so that it will union properly with any 'N/A' values. */ 
		
		CASE 
			WHEN WeekLastDay = DATEADD(DAY, 6, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), CAST(@StartDate AS DATE), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 13, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 20, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 27, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), CAST(@EndDate AS DATE), 101), 5)
			END AS 'DateHeader'
		, CASE
			WHEN WeekLastDay = DATEADD(DAY, 6, @StartDate) THEN '1'
			WHEN WeekLastDay = DATEADD(DAY, 13, @StartDate) THEN '2'
			WHEN WeekLastDay = DATEADD(DAY, 20, @StartDate) THEN '3'
			WHEN WeekLastDay = DATEADD(DAY, 27, @StartDate) THEN '4'
			END AS sortkey
		, CAST(0 AS VARCHAR) AS NudgeReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS ServicePrints 
		, CAST(0 AS VARCHAR) AS eRxReferralsShared
		, CAST(0 AS VARCHAR) AS TotalSharedSingleServiceReferrals
		, CAST(0 AS VARCHAR) AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
	FROM vdim.[Date] AS dimDate
	WHERE dimDate.DateValue >= DATEADD(DAY, 0, @StartDate) AND dimDate.DateValue < DATEADD(DAY, 1, @EndDate)
	GROUP BY (CASE 
			WHEN WeekLastDay = DATEADD(DAY, 6, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), CAST(@StartDate AS DATE), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 6, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 13, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 7, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 13, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 20, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 14, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 20, CAST(@StartDate AS DATE)), 101), 5)
			WHEN WeekLastDay = DATEADD(DAY, 27, @StartDate) 
				THEN LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, 21, CAST(@StartDate AS DATE)), 101), 5) + ' - ' + LEFT(CONVERT(VARCHAR(10), CAST(@EndDate AS DATE), 101), 5)
			END)
			, WeekLastDay

	UNION ALL
		
	/* The AAG-day aggregation report requires us to provide aggregates for "ThisWeek" and "LastWeek." 
	   Create dummy rows to ensure that these column header fields are displayed, even when there are no available data. */

	SELECT
		'Prior 4-Week Total' AS 'DateHeader'
		, '6' AS sortkey
		, CAST(0 AS VARCHAR) AS NudgeReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS ServicePrints 
		, CAST(0 AS VARCHAR) AS eRxReferralsShared
		, CAST(0 AS VARCHAR) AS TotalSharedSingleServiceReferrals
		, CAST(0 AS VARCHAR) AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
		
	UNION ALL

	SELECT
		'4-Week Total' AS 'DateHeader'
		, '5' AS sortkey
		, CAST(0 AS VARCHAR) AS NudgeReferralsShared
		, CAST(0 AS VARCHAR) AS TrackedReferralsShared
		, CAST(0 AS VARCHAR) AS ServicePrints 
		, CAST(0 AS VARCHAR) AS eRxReferralsShared
		, CAST(0 AS VARCHAR) AS TotalSharedSingleServiceReferrals
		, CAST(0 AS VARCHAR) AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
		
	UNION ALL

	SELECT
		'% Change' AS 'DateHeader'
		, '7' AS sortkey
		, 'N/A' AS NudgeReferralsShared
		, 'N/A' AS TrackedReferralsShared
		, 'N/A' AS ServicePrints 
		, 'N/A' AS eRxReferralsShared
		, 'N/A' AS TotalSharedSingleServiceReferrals
		, 'N/A' AS TotalSharedReferrals
		, 'N/A' AS TotalSingleServiceReferralsShared_Perc
		, 'N/A' AS eRxReferralsShared_Perc
)
,

/* This temp table returns aggregated metrics, by day of week. Query parameters are applied here to ensure that only the relevant
   row records are included in the aggregates. */

Base AS (
	SELECT 
		 CAST(COALESCE(vfactStandardMetricsPSI.DateHeader,vfactStandardMetricsUserActivity.DateHeader, vfactStandardMetricsErxService.DateHeader) AS VARCHAR) AS DateHeader
		--, CAST(COALESCE(vfactStandardMetricsPSI.DatePeriod,vfactStandardMetricsUserActivity.DatePeriod, vfactStandardMetricsErxService.DatePeriod) AS VARCHAR) AS DatePeriod
		, CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END AS eRxReferralsShared
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)) AS TotalSharedSingleServiceReferrals
		, ((CASE WHEN vfactStandardMetricsPSI.SingleServiceReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsPSI.SingleServiceReferralsShared END)
		+ (CASE WHEN vfactStandardMetricsUserActivity.SingleServicePrints IS NULL THEN 0 ELSE vfactStandardMetricsUserActivity.SingleServicePrints END)
		+ (CASE WHEN vfactStandardMetricsErxService.eRxReferralsShared IS NULL THEN 0 ELSE vfactStandardMetricsErxService.eRxReferralsShared END)) AS TotalSharedReferrals
		, DENSE_RANK() OVER (ORDER BY COALESCE(vfactStandardMetricsPSI.WeekLastDay, vfactStandardMetricsUserActivity.WeekLastDay, vfactStandardMetricsErxService.WeekLastDay)) as [rperiod]
			
	FROM (
	/* Metrics for electronic Single Service Nudges (Text/Email) and Tracked Referrals, by Day of Week */
		SELECT 
			 CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)) AS DateHeader
			, SUM(factStandardMetricsPSI.SingleServiceReferralsShared) AS SingleServiceReferralsShared
			, dimDate.WeekLastDay
		FROM vfact.StandardMetricsPSI AS factStandardMetricsPSI
		JOIN vdim.[Date] AS dimDate
		ON dimDate.DateKey = factStandardMetricsPSI.DateKey
		WHERE CAST(factStandardMetricsPSI.DateKey AS VARCHAR) >= DATEADD(DAY, -28, @StartDate) AND CAST(factStandardMetricsPSI.DateKey AS VARCHAR) < DATEADD(DAY, 1, @EndDate)
			AND factStandardMetricsPSI.OrganizationId IN (@orgparam)
		GROUP BY CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)), dimDate.WeekLastDay
		) AS vfactStandardMetricsPSI

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Single Service Prints, by Day of Week */
		SELECT
			/* Tag by Individual Weeks and Last 4 Wk Period */ 
			CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)) AS DateHeader
			, SUM(factStandardMetricsUserActivity.SingleServiceReferralPrinted) AS SingleServicePrints
			, dimDate.WeekLastDay
		FROM vfact.StandardMetricsUserActivity AS factStandardMetricsUserActivity
		JOIN vdim.[Date] AS dimDate
		ON dimDate.DateKey = factStandardMetricsUserActivity.DateKey
		WHERE CAST(factStandardMetricsUserActivity.DateKey as VARCHAR(8)) >= DATEADD(DAY, -28, @StartDate) AND CAST(factStandardMetricsUserActivity.DateKey as VARCHAR(8)) < DATEADD(DAY, 1, @EndDate)
			AND OrganizationId in (@orgparam)
		GROUP BY CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)), dimDate.WeekLastDay
	
		) AS vfactStandardMetricsUserActivity
	ON (vfactStandardMetricsUserActivity.DateHeader = vfactStandardMetricsPSI.DateHeader)

	/* Use Full outer join to ensure all records from both tables are preserved in the final output */
	FULL OUTER JOIN (
	/* Metrics for Referrals on shared eRxs, by Day of Week */
		SELECT 
			CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)) AS DateHeader
			, SUM(factStandardMetricsErxService.eRxReferralsShared) AS eRxReferralsShared
			, dimDate.WeekLastDay
		FROM vfact.StandardMetricsErxService AS factStandardMetricsErxService
		JOIN vdim.[Date] AS dimDate
		ON dimDate.DateKey = factStandardMetricsErxService.DateKey
		WHERE factStandardMetricsErxService.DateKey >= DATEADD(DAY, -28, @StartDate) AND factStandardMetricsErxService.DateKey < DATEADD(DAY, 1, @EndDate)
			AND OrganizationId in (@orgparam)
		GROUP BY CONCAT(CONVERT(VARCHAR(5), dimDate.WeekFirstDay, 101), ' - ', CONVERT(VARCHAR(5), dimDate.WeekLastDay, 101)), dimDate.WeekLastDay
		) AS vfactStandardMetricsErxService
	ON (vfactStandardMetricsErxService.DateHeader = vfactStandardMetricsPSI.DateHeader)

)
,
NoPercWeek as (
	SELECT
		CASE WHEN [rPeriod] in (1,2,3,4) THEN 'Prior 4-Week Total' WHEN [rPeriod] in (5,6,7,8) THEN '4-Week Total' END AS DateHeader
		, SUM(eRxReferralsShared) AS eRxReferralsShared
		, SUM(TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
		, SUM(TotalSharedReferrals) AS TotalSharedReferrals
	FROM Base
	GROUP BY (CASE WHEN [rPeriod] in (1,2,3,4) THEN 'Prior 4-Week Total' WHEN [rPeriod] in (5,6,7,8) THEN '4-Week Total' END)
)
,
AddPercWeek AS (
	SELECT *
		, 'Dummy' as Dummy
		, CASE WHEN TotalSharedReferrals > 0 THEN
		/* Must convert to DECIMAL(19,3), else whole-value numbers like "100.0%" will show up as "100%." */
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,TotalSharedSingleServiceReferrals)/CONVERT(FLOAT,TotalSharedReferrals),1))
			ELSE NULL END AS 'TotalSingleServiceReferralsShared_Perc'
		, CASE WHEN TotalSharedReferrals > 0 THEN
			CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,eRxReferralsShared)/CONVERT(FLOAT,TotalSharedReferrals),1))
			ELSE NULL END AS 'eRxReferralsShared_Perc'
		--, NULL AS eRxReferralsShared_Perc
	FROM NoPercWeek
)
,
AddDeltaWeek as (
	SELECT 
		'% Change' AS DateHeader,


		CASE 
			WHEN (ThisWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.eRxReferralsShared) - CONVERT(FLOAT,PriorWeek.eRxReferralsShared))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared),0)

		    WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.eRxReferralsShared IS NOT NULL AND PriorWeek.eRxReferralsShared <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.eRxReferralsShared))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared),0)

		/* Use condition "else NULL" for situations in which divisor is zero */	
			ELSE NULL END AS eRxReferralsShared,

		CASE 
			WHEN (ThisWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalSharedSingleServiceReferrals) - CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSharedSingleServiceReferrals IS NOT NULL AND PriorWeek.TotalSharedSingleServiceReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedSingleServiceReferrals),0)
			ELSE NULL END AS TotalSharedSingleServiceReferrals,

		CASE 
			WHEN (ThisWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalSharedReferrals) - CONVERT(FLOAT,PriorWeek.TotalSharedReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedReferrals),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSharedReferrals IS NOT NULL AND PriorWeek.TotalSharedReferrals <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSharedReferrals))/CONVERT(FLOAT,PriorWeek.TotalSharedReferrals),0)
			ELSE NULL end as TotalSharedReferrals,

		CASE 
			WHEN (ThisWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.TotalSingleServiceReferralsShared_Perc) - CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc IS NOT NULL AND PriorWeek.TotalSingleServiceReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.TotalSingleServiceReferralsShared_Perc),0)
			ELSE NULL END AS TotalSingleServiceReferralsShared_Perc,

		CASE 
			WHEN (ThisWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (CONVERT(FLOAT,ThisWeek.eRxReferralsShared_Perc) - CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc),0)
			WHEN (ThisWeek.DateHeader IS NULL AND PriorWeek.eRxReferralsShared_Perc IS NOT NULL AND PriorWeek.eRxReferralsShared_Perc <> 0) THEN 
				ROUND(100 * (0 - CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc))/CONVERT(FLOAT,PriorWeek.eRxReferralsShared_Perc),0)
			ELSE NULL END AS eRxReferralsShared_Perc

	FROM (
		SELECT *
		FROM AddPercWeek
		WHERE DateHeader = '4-Week Total'
		) AS ThisWeek
	FULL OUTER JOIN (
		SELECT *
		FROM AddPercWeek
		WHERE DateHeader = 'Prior 4-Week Total'
		) AS PriorWeek
	ON ThisWeek.Dummy = PriorWeek.Dummy
)
,
AddFormat AS (

	SELECT 
		DateHeader
		, CASE WHEN AddDeltaWeek.eRxReferralsShared IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.eRxReferralsShared) + '%' END AS 'eRxReferralsShared'
		, CASE WHEN AddDeltaWeek.TotalSharedSingleServiceReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSharedSingleServiceReferrals) + '%' END AS 'TotalSharedSingleServiceReferrals'
		, CASE WHEN AddDeltaWeek.TotalSharedReferrals IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSharedReferrals) + '%' END AS 'TotalSharedReferrals'
		, CASE WHEN AddDeltaWeek.TotalSingleServiceReferralsShared_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.TotalSingleServiceReferralsShared_Perc) + '%' END AS 'TotalSingleServiceReferralsShared_Perc'
		, CASE WHEN AddDeltaWeek.eRxReferralsShared_Perc IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR, AddDeltaWeek.eRxReferralsShared_Perc) + '%' END AS 'eRxReferralsShared_Perc'
	FROM AddDeltaWeek
)

--SELECT
--	COALESCE(Biggie.DateHeader, DummyDates.DateHeader) AS 'DateHeader'
--	, DummyDates.sortkey AS sortkey
--	, COALESCE(Biggie.eRxReferralsShared, DummyDates.eRxReferralsShared) AS eRxReferralsShared
--	, COALESCE(Biggie.TotalSharedSingleServiceReferrals, DummyDates.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
--	, COALESCE(Biggie.TotalSharedReferrals, DummyDates.TotalSharedReferrals) AS TotalSharedReferrals
--	, COALESCE(Biggie.TotalSingleServiceReferralsShared_Perc, DummyDates.TotalSingleServiceReferralsShared_Perc) AS TotalSingleServiceReferralsShared_Perc
--	, COALESCE(Biggie.eRxReferralsShared_Perc, DummyDates.eRxReferralsShared_Perc) AS eRxReferralsShared_Perc

--FROM (
	SELECT 
		COALESCE(WeekDays.DateHeader, AddPercWeek.DateHeader, AddFormat.DateHeader) AS 'DateHeader'
		/* Cast all integer fields as varchar so that the field is forced varchar to align with varchar fields from AddFormat table */
		,COALESCE(CAST(WeekDays.eRxReferralsShared AS VARCHAR), CAST(AddPercWeek.eRxReferralsShared AS VARCHAR), AddFormat.eRxReferralsShared) AS eRxReferralsShared
		,COALESCE(CAST(WeekDays.TotalSharedSingleServiceReferrals AS VARCHAR), CAST(AddPercWeek.TotalSharedSingleServiceReferrals AS VARCHAR), AddFormat.TotalSharedSingleServiceReferrals) AS TotalSharedSingleServiceReferrals
		,COALESCE(CAST(WeekDays.TotalSharedReferrals AS VARCHAR), CAST(AddPercWeek.TotalSharedReferrals AS VARCHAR), AddFormat.TotalSharedReferrals) AS TotalSharedReferrals
		/* This COALESCE order works becuase NULL values remain NULL even when non-NULL values are converted to [VARCHAR() + '%'] */
		,COALESCE(CONVERT(VARCHAR, WeekDays.TotalSingleServiceReferralsShared_Perc) + '%', CONVERT(VARCHAR, AddPercWeek.TotalSingleServiceReferralsShared_Perc) + '%', AddFormat.TotalSingleServiceReferralsShared_Perc) AS TotalSingleServiceReferralsShared_Perc
		,COALESCE(CONVERT(VARCHAR, WeekDays.eRxReferralsShared_Perc) + '%', CONVERT(VARCHAR, AddPercWeek.eRxReferralsShared_Perc) + '%', AddFormat.eRxReferralsShared_Perc) AS eRxReferralsShared_Perc

	FROM (

		/* As stated above, the Share section of the AAG report requires the calculation of "% Single Service Referrals Shared" and "% eRx Referrals Shared," by day, and by week. 
		   I calculate the 4-Week value for this metric in the "AddPercWeek" temp table above, and calculate the week value for this metric in the table below. */
		SELECT *
			, CASE WHEN CAST(TotalSharedReferrals AS INT) > 0 THEN
			/* Must convert to DECIMAL(19,1), else whole-value numbers like "100.0%" will show up as "100%." */
				CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,TotalSharedSingleServiceReferrals)/CONVERT(FLOAT,TotalSharedReferrals),1))
				ELSE NULL END AS 'TotalSingleServiceReferralsShared_Perc'
			, CASE WHEN CAST(TotalSharedReferrals AS INT) > 0 THEN
				CONVERT(DECIMAL(19,1),ROUND(100 * CONVERT(FLOAT,eRxReferralsShared)/CONVERT(FLOAT,TotalSharedReferrals),1))
				ELSE NULL END AS 'eRxReferralsShared_Perc'
		FROM Base
		WHERE [rPeriod] in (5,6,7,8)
		) as WeekDays
	FULL OUTER JOIN AddPercWeek
	ON AddPercWeek.DateHeader = WeekDays.DateHeader
	FULL OUTER JOIN AddFormat
	ON AddPercWeek.DateHeader = AddFormat.DateHeader
--) AS Biggie
--RIGHT OUTER JOIN DummyDates
--ON DummyDates.DateHeader = Biggie.DateHeader
--ORDER BY sortkey