/*
Adopt-Detail: non-Referral Activities Query
author: joanna.tung
date: 2018.12.12

	This query returns an export that can produce the desired table format presented for the Adopt-Detail: non-Referral Activities in the Standard Report sample template, 
	and is intended to be used in conjunction with the DevExpress xrTable functionality.

	Note: Query Parameters are filter conditions set by the report app user. Organization and Date Range pickers allow report app users to denote:
		- @enddate (where a week's enddate is always set to the Sunday of the selected week)
		- @startdate (where a week's startdate is always set to the Monday of the selected week)
		- @orgparam (a comma-delimited string of OrgIds)

Updates:

2019.03.28: per Analytics-497, removing "Concat" from joins to preserve indexing capabilities and increase performance. New report variant uploaded to PROD. 

*/

/* Use the declare statements below to filter the query, mimicking query parameter passing procedures. 
   When copy-pasting this query into the DevExpress Report Designer Query Builder:
		1) remove the "declare "statements below, and 
		2) set @orgparam, @Enddate and @startdate as query parameters. 

*/

DECLARE @StartDate AS DATETIME = '2018-01-01'
DECLARE @EndDate AS DATETIME = '2018-09-16'
DECLARE @orgparam AS INT = 1085389

SELECT 
	
	Base3.OrganizationId
	,dOrg.[Name] as OrganizationName
	,Base3.UserId
	,CONCAT(dUSer.LastName, ', ',dUser.FirstName) AS 'UserName'
	,dUser.Title
	,dUser.Department
	,CASE WHEN Base3.SuccessfulLogins IS NULL THEN 0 ELSE Base3.SuccessfulLogins END AS SuccessfulLogins
	,CASE WHEN Base3.Nudges IS NULL THEN 0 ELSE Base3.Nudges END AS Nudges
	,CASE WHEN Base3.Searches IS NULL THEN 0 ELSE Base3.Searches END AS Searches
	,CASE WHEN Base3.ScreeningsCompleted IS NULL THEN 0 ELSE Base3.ScreeningsCompleted END AS ScreeningsCompleted

FROM (
	SELECT
		COALESCE(Base2.UserId, Search.Searches) AS UserId
		, COALESCE(Base2.OrganizationId, Search.OrganizationId) AS OrganizationId
		, Base2.SuccessfulLogins
		, Base2.ScreeningsCompleted
		, Base2.Nudges
		, Search.Searches

	FROM (
		SELECT
			COALESCE(Base1.UserId, PSINudges.UserId) AS UserId
			,COALESCE(Base1.OrganizationId, PSINudges.OrganizationId) AS OrganizationId
			, Base1.SuccessfulLogins
			, Base1.ScreeningsCompleted
			, PSINudges.Nudges
		FROM (
			SELECT
				COALESCE(Logins.UserId, PSIScreenings.UserId) AS UserId
				,COALESCE(Logins.OrganizationId, PSIScreenings.OrganizationId) AS OrganizationId
				,Logins.SuccessfulLogins
				,PSIScreenings.ScreeningsCompleted

			FROM (
					/* By UserId, OrganizationId: Count of Logins */
					SELECT 
						UserId
						, CreatedByOrganizationId AS OrganizationId
						, COUNT(ExternalUserQuantity) AS SuccessfulLogins
					FROM [vFact].[ExternalUserActivity]
					WHERE ExternalUserActivityTypeId = 0
						AND CreatedByOrganizationId in (@orgparam)
						AND CreatedDate >= DATEADD(DAY, 0, @StartDate) AND CreatedDate < DATEADD(DAY, 1, @Enddate)
					GROUP BY UserId, CreatedByOrganizationId
					) AS Logins
				FULL OUTER JOIN (
					/* By UserId, OrganizationId: Count of Screenings Completed */
						SELECT
							factPSI.UserId
							, factPSI.OrganizationId
							, SUM(factPSI.PatientSiteInteractionQuantity) AS ScreeningsCompleted
						FROM vfact.PatientSiteInteraction AS factPSI
						LEFT JOIN vdim.ScreeningResponse as dimSR
						ON factPSI.ScreeningResponseId = dimSR.ScreeningResponseId AND factPSI.CareOrganizationDatabaseId = dimSR.CareOrganizationDatabaseId
						LEFT JOIN vdim.Patient as dPat
						ON dPat.PatientId = factPSI.PatientId AND dPat.CareOrganizationDatabaseId = factPSI.CareOrganizationDatabaseId
						WHERE UserId IS NOT NULL
							AND factPSI.InteractionTypeId = 21
							AND factPSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND factPSI.InteractionDate < DATEADD(DAY, 1, @Enddate)
							AND OrganizationId IN (@orgparam)
							AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
							AND dimSR.CompletedDate IS NOT NULL
						GROUP BY factPSI.UserId, factPSI.OrganizationId
						) AS PSIScreenings
				ON (Logins.UserId = PSIScreenings.UserId AND Logins.OrganizationId = PSIScreenings.OrganizationId)
				) AS Base1
				FULL OUTER JOIN (
					/* By UserId, OrganizationId: Count of Nudges (non-Service Referral) */
					SELECT 
						factPSI.UserId
						, factPSI.OrganizationId
						, COUNT(factPSI.PatientSiteInteractionQuantity) AS Nudges
					FROM vfact.PatientSiteInteraction as factPSI
					LEFT JOIN vdim.Patient as dPat
					ON dPat.PatientId = factPSI.PatientId AND dPat.CareOrganizationDatabaseId = factPSI.CareOrganizationDatabaseId
					WHERE UserId IS NOT NULL
						AND factPSI.InteractionTypeId in (1,2)
						AND factPSI.InteractionDate >= DATEADD(DAY, 0, @StartDate) AND factPSI.InteractionDate < DATEADD(DAY, 1, @Enddate)
						AND OrganizationId IN (@orgparam)
						AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL) 
					GROUP BY factPSI.UserId, factPSI.OrganizationId
						) AS PSINudges
				ON (Base1.UserId = PSINudges.UserId AND Base1.OrganizationId = PSINudges.OrganizationId)
			) AS Base2
			FULL OUTER JOIN (
				/* By UserId, OrganizationId: Count of Searches */
				SELECT 
					factSC.UserId
					,factSC.OrganizationId
					,COUNT(factSC.SearchCriteriaQuantity) AS Searches
				FROM vfact.SearchCriteria AS factSC
				LEFT JOIN vdim.Patient AS dPat
				ON dPat.PatientId = factSC.PatientId AND dPat.CareOrganizationDatabaseId = factSC.CareOrganizationDatabaseId
				WHERE UserId IS NOT NULL
					AND factSC.CreatedDate >= DATEADD(DAY, 0, @StartDate) AND factSC.CreatedDate < DATEADD(DAY, 1, @Enddate)
					AND OrganizationId IN (@orgparam)
					AND (dPat.[LASTNAME] NOT LIKE '%Zzztest%' OR dPat.[LastName] IS NULL)
					AND factSC.SearchType = 1
				GROUP BY factSC.UserId, factSC.OrganizationId
				) AS Search
			ON (Base2.UserId = Search.UserId AND Base2.OrganizationId = Search.OrganizationId)

		) AS Base3

JOIN vdim.[User] as dUser
ON Base3.UserId = dUser.UserId 
JOIN vdim.Organization as dOrg
ON Base3.OrganizationId = dOrg.OrganizationId

WHERE (dUser.ExcludeFromReports <> 1 OR dUser.ExcludeFromReports IS NULL) 

ORDER BY  UserName --OrganizationId,